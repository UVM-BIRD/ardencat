# ArdenCAT #

ArdenCAT – the Arden-based Cohort Abstraction Tool – developed at [The University of Vermont and State Agricultural College](https://www.uvm.edu/) in collaboration with [Vermont Oxford Network](https://public.vtoxford.org/) and [The University of Vermont Medical Center](https://www.uvmhealth.org/medcenter/Pages/default.aspx), is a free, generic, lightweight, cross-platform, and flexible record identification and extraction utility, geared for use by those in the biomedical informatics community.

ArdenCAT is capable of constructing arbitrarily complex records from one or more data sources, which may then be exported into either XML or CSV formats. Configuration is relatively simple, yet provides for a high degree of customization.

[Arden Syntax](http://www.hl7.org/implement/standards/product_brief.cfm?product_id=268), developed and maintained by [HL7](http://www.hl7.org/) since 1999, is a simple scripting language designed to encode medical knowledge. ArdenCAT uses Arden Syntax logic primarily in its decision-making process for determining cohort eligibility.

See the _ArdenCAT Reference Manual_ for more information.

### Building ArdenCAT ###

Building ArdenCAT is accomplished by running the following from the project's base directory:

    $ mvn package

This will build all ArdenCAT modules, and generate binary package distributions in the project's `target` folder:

    $ ls target
    archive-tmp			ardencat-1.0-bin.tar.bz2	ardencat-1.0-bin.tar.gz		ardencat-1.0-bin.zip

_Note that the "archive-tmp" folder listed above is a byproduct of the Maven build process and can be safely ignored._

Each of these generated archives contain the same files.  Below are listed the contents of the GZipped archive:

    $ tar -tzf target/ardencat-1.0-bin.tar.gz
    ardencat-1.0/COPYING
    ardencat-1.0/createdb.sql
    ardencat-1.0/README
    ardencat-1.0/mlm-engine-1.0.jar
    ardencat-1.0/mlm-engine-ui-1.0.jar
    ardencat-1.0/record-builder-1.0.jar
    ardencat-1.0/weekifier-1.0.jar
    ardencat-1.0/ArdenCAT Reference Manual.pdf
    ardencat-1.0/demo/ardencat.db
    ardencat-1.0/demo/demo.mlm
    ardencat-1.0/demo/ehr.db
    ardencat-1.0/demo/mlm-engine-demo.xml
    ardencat-1.0/demo/record-builder-demo.xml


### Running the ArdenCAT Demo ###

After building ArdenCAT, you can use the following instructions to run the ArdenCAT demo to get a feel for how the system operates.

#### 1. Start the MLM Engine ####

To run the ArdenCAT demo, you must first start the _MLM Engine_:

    $ java -jar modules/mlm-engine/target/mlm-engine-1.0.jar
    ArdenCAT MLM Engine
    -------------------
    Copyright 2015 The University of Vermont and State Agricultural College, Vermont Oxford Network, and The University of Vermont Medical Center.  All rights reserved.
    
    [main] INFO org.quartz.impl.StdSchedulerFactory - Using default implementation for ThreadExecutor
    [main] INFO org.quartz.core.SchedulerSignalerImpl - Initialized Scheduler Signaller of type: class org.quartz.core.SchedulerSignalerImpl
    [main] INFO org.quartz.core.QuartzScheduler - Quartz Scheduler v.2.2.1 created.
    [main] INFO org.quartz.simpl.RAMJobStore - RAMJobStore initialized.
    [main] INFO org.quartz.core.QuartzScheduler - Scheduler meta-data: Quartz Scheduler (v2.2.1) 'MLMScheduler' with instanceId 'NON_CLUSTERED'
      Scheduler class: 'org.quartz.core.QuartzScheduler' - running locally.
      NOT STARTED.
      Currently in standby mode.
      Number of jobs executed: 0
      Using thread pool 'org.quartz.simpl.SimpleThreadPool' - with 3 threads.
      Using job-store 'org.quartz.simpl.RAMJobStore' - which does not support persistence. and is not clustered.
    
    [main] INFO org.quartz.impl.StdSchedulerFactory - Quartz scheduler 'MLMScheduler' initialized from default resource file in Quartz package: 'quartz.properties'
    [main] INFO org.quartz.impl.StdSchedulerFactory - Quartz scheduler version: 2.2.1
    [main] INFO org.quartz.core.QuartzScheduler - Scheduler MLMScheduler_$_NON_CLUSTERED started.
    INFO  MLMScheduler - Quartz scheduler started.
    INFO  MLMEngine - RMI server is now listening on port 36251.
    INFO  MLMEngine - MLM Engine started successfully.

#### 2. Load the Demo Configuration ####

Once the _MLM Engine_ is started, you need to run the _MLM Engine User Interface_ and load the demo configuration.  Note that you will need to do this in a different terminal session from the one used to start the _MLM Engine_, since _MLM Engine_ is designed to run as a daemon or service.

    $ java -jar modules/mlm-engine-ui/target/mlm-engine-ui-1.0.jar

After accepting the license agreement, do the following:

1. Click the _Select File..._ button
2. Navigate to the `demo` folder
3. Select the file `mlm-engine-demo.xml` and click _Open_

You should now see the selected configuration file displayed in the _Configuration File_ field.  Next, click the _Apply_ button.  This will submit the selected configuration to the _MLM Engine_.  You can monitor the demo MLM's execution by monitoring the output from _MLM Engine_:

    INFO  MLMScheduler - all jobs successfully unscheduled.
    INFO  ConfigUtil - *** loading config '/projects/ardencat/demo/mlm-engine-demo.xml' ***
    INFO  MLMRegistry - register : registered MLM : demo:University of Vermont::1.00
    INFO  ConfigUtil - *** done loading config ***
    INFO  DataSource - getConnection : establishing connection to 'ardencat'
    INFO  MLM - executing MLM[id=demo:University of Vermont::1.00, executionTime=2015-06-12T16:35:06.189-04:00]
    DEBUG DataEvalVisitor - buildRecords : will execute SQL: select distinct p.PID,
                                       p3.IDENTITY_ID,
                                       p.FIRST_NAME,
                                       p.LAST_NAME,
                                       p.SEX,
                                       p.BIRTH_DATE,
                                       e.ADMSN_TIME,
                                       p2.WC,
                                       p2.TG,
                                       p2.HDL,
                                       p2.BP,
                                       p2.GLC,
                                       null, p.reg_date as p_reg_date, e.contact_date as e_contact_date
                                   from PERSON p
                                       join PERSON_2 p2 on p.PID=p2.PID
                                       join PERSON_3 p3 on p.PID=p3.PID
                                       left join ENCOUNTER e on p.PID=e.PID
                                   order by p.PID, e.ADMSN_TIME
    INFO  DataSource - getConnection : establishing connection to 'ehr'
    INFO  MLM - FINISHED executing MLM[id=demo:University of Vermont::1.00, executionTime=2015-06-12T16:35:06.189-04:00]
    INFO  DataSource - close : closing connection 'ardencat'
    INFO  DataSource - close : closing connection 'ehr'

At this point, the demo ArdenCAT SQLite3 database (`demo/ardencat.db`) will be populated with demo eligibility data.

#### 3. Split Eligibility Records into Logical Weekly Batches (OPTIONAL) ####

In a normal production environment, MLMs are intended to be executed at regular, periodic intervals, such as on a weekly basis.  Over the course of time (a year, for example), this will result in many small batches of records to process.  In the demo, however, the executed MLM analyzes records for a full year all at once, which results in a single batch with a full year's worth of records to process.

In order to illustrate certain _Record Builder_ functionality, it is useful to split these records into their logical weekly batches, based in this case on each record's _Admission Time_ field.

To split the demo records into their logical weekly batches, load the _Weekifier_ utility:

    $ java -jar modules/weekifier/target/weekifier-1.0.jar

After accepting the license agreement, do the following:

1. Click the _Select File..._ button
2. Navigate to the `demo` folder
3. Select the file `record-builder-demo.xml` and click _Open_

At this point, the _Weekifier_ utility will populate the _Source_, _Batch_, and _Field_ lists.  Leave the _Source_ and _Batch_ lists alone (there should only be one item in each list anyway), but select `atime` in the _Field_ list.  This tells the program to use the _Admission Time_ field to determine the logical weekly batch for each eligibility record.

After you've selected the `atime` field, click the _Weekify!_ button.  The system will work for a moment and then display the message "Weekification completed!"

At this point, the single batch of records that _MLM Engine_ generated will be split into logical weekly batches based on each record's _Admission Time_.

#### 4. Curate Eligibility and Export Records with Record Builder ####

Once the ArdenCAT database is updated, you can use _Record Builder_ to curate eligibility and export eligible records.  To do this, start the _Record Builder_ application:

    $ java -jar modules/record-builder/target/record-builder-1.0.jar

After accepting the license agreement, do the following:

1. Click the _New Configuration_ button
2. Navigate to the `demo` folder
3. Select the file `record-builder-demo.xml` and click _Open_

You should now see _Demo_ listed in the _Select Configuration_ screen, and it should be already selected.  Click the _Next_ button to curate eligibility, review, and export records to XML and CSV formats.

_This concludes the instructions for running the ArdenCAT demo._

### Production Database Setup ###

In a production environment, you will want to use a more robust DBMS than SQLite3, which is used for the demo.  Any SQL-compliant DBMS may be used, but it is recommended to use the [MySQL Community Server](https://dev.mysql.com/downloads/mysql/).

To create the ArdenCAT database, execute the following:

    $ mysql -uroot < createdb.sql

You can now use the following `ardencatDataSource` definition to reference the production database in _MLM Engine_ and _Record Builder_ configuration files:

    <ardencatDataSource driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost/ardencat">
        <properties>
            <property name="user" value="ardencat" />
            <property name="password" value="ardencat" />
        </properties>
    </ardencatDataSource>

### License and Copyright ###

ArdenCAT is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/), [Vermont Oxford Network](https://public.vtoxford.org/), and [The University of Vermont Medical Center](https://www.uvmhealth.org/medcenter/Pages/default.aspx).  All rights reserved.

ArdenCAT is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).