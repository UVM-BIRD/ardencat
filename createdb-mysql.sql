-- Copyright 2015 The University of Vermont and State
-- Agricultural College, Vermont Oxford Network, and The University
-- of Vermont Medical Center.  All rights reserved.
--
-- Written by Matthew B. Storer <matthewbstorer@gmail.com>
--
-- This file is part of ArdenCAT.
--
-- ArdenCAT is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- ArdenCAT is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.

create database if not exists ardencat;

grant all on ardencat.* to 'ardencat'@'localhost' identified by 'ardencat';

create table if not exists ardencat.tblACEligibility (
  id int not null auto_increment primary key,
  source varchar(255) not null,
  status char(1) not null,
  overrideStatus char(1),
  batch varchar(15) not null,
  created timestamp not null default current_timestamp,
  exportId int,
  data text not null,
  index batch_index (batch)
);

create table if not exists ardencat.tblACBatchData (
  source varchar(255) not null primary key,
  mostRecentBatch varchar(15) not null
);

create table if not exists ardencat.tblACEligAudit (
  id int not null auto_increment primary key,
  eligId int not null,
  origStatus char(1) not null,
  newStatus char(1) not null,
  authSig varchar(255) not null,
  reason varchar(255),
  created timestamp not null default current_timestamp,
  index eligId_index (eligId)
);

create table if not exists ardencat.tblACExport (
  id int not null auto_increment primary key,
  source varchar(255) not null,
  firstBatch varchar(15) not null,
  lastBatch varchar(15) not null,
  method varchar(20) not null,
  created timestamp not null default current_timestamp
);

create table if not exists ardencat.tblACExportRecord (
  id int not null auto_increment primary key,
  exportId int not null,
  recordType varchar(20) not null,
  data text not null,
  index exportId_index (exportId)
);

create table if not exists ardencat.tblACExportAudit (
  id int not null auto_increment primary key,
  exportId int not null,
  authSig varchar(255) not null,
  reason varchar(255),
  created timestamp not null default current_timestamp,
  index exportId_index (exportId)
);

create table if not exists ardencat.tblACMLMExecLog (
  id int not null auto_increment primary key,
  mlmId varchar(255) not null,
  executionTime datetime not null,
  status varchar(7) not null,
  message text,
  index executionTime_index (executionTime)
);
