-- Copyright 2015 The University of Vermont and State
-- Agricultural College, Vermont Oxford Network, and The University
-- of Vermont Medical Center.  All rights reserved.
--
-- Written by Matthew B. Storer <matthewbstorer@gmail.com>
--
-- This file is part of ArdenCAT.
--
-- ArdenCAT is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- ArdenCAT is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.

create table tblACEligibility(
  id integer not null primary key autoincrement,
  source text not null,
  status text not null,
  overrideStatus text,
  batch text,
  created text default CURRENT_TIMESTAMP,
  exportId integer,
  data text not null
);

create table tblACBatchData(
  source text not null primary key,
  mostRecentBatch text not null
);

create table tblACEligAudit(
  id integer not null primary key autoincrement,
  eligId integer not null,
  origStatus text not null,
  newStatus text not null,
  authSig text not null,
  reason text,
  created text default CURRENT_TIMESTAMP
);

create table tblACExport(
  id integer not null primary key autoincrement,
  source text not null,
  firstBatch text not null,
  lastBatch text not null,
  method text not null,
  created text default CURRENT_TIMESTAMP
);

create table tblACExportRecord(
  id integer not null primary key autoincrement,
  recordType text not null,
  exportId integer not null,
  data text not null
);

create table tblACExportAudit(
  id integer not null primary key autoincrement,
  exportId integer not null,
  authSig text not null,
  reason text,
  created text default CURRENT_TIMESTAMP
);

create table tblACMLMExecLog(
  id integer not null primary key autoincrement,
  mlmId text not null,
  executionTime text not null,
  status text not null,
  message text
);
