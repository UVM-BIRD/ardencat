-- Copyright 2015 The University of Vermont and State
-- Agricultural College, Vermont Oxford Network, and The University
-- of Vermont Medical Center.  All rights reserved.
--
-- Written by Matthew B. Storer <matthewbstorer@gmail.com>
--
-- This file is part of ArdenCAT.
--
-- ArdenCAT is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- ArdenCAT is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.

create table PERSON(
  PID text,
  FIRST_NAME text,
  LAST_NAME text,
  SEX text,
  BIRTH_DATE text,
  DEATH_DATE text,
  REG_DATE text
);

create table PERSON_2(
  PID text,
  WC real,
  TG real,
  HDL real,
  BP text,
  GLC real
);

create table PERSON_3(
  PID text,
  TYPE_ID integer,
  IDENTITY_ID text
);

create table ENCOUNTER(
  PID text,
  ADMSN_TIME text,
  CONTACT_DATE text
);
