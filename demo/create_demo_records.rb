# Copyright 2015 The University of Vermont and State
# Agricultural College, Vermont Oxford Network, and The University
# of Vermont Medical Center.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of ArdenCAT.
#
# ArdenCAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ArdenCAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.

require 'csv'
require 'date'

OUT_FILE = 'populate_ehr.sql'

PID_LIST = []

def gen_pid
  id = "#{rand(99999)}".rjust(5, '0')

  while PID_LIST.include? id
    id = "#{rand(99999)}".rjust(5, '0')
  end

  PID_LIST << id

  "DMO#{id}"
end

TMP_MRNS = []
def gen_mrn
  id = "#{rand(9999999)}".rjust(9, '0')

  while TMP_MRNS.include? id
    id = "#{rand(9999999)}".rjust(9, '0')
  end

  TMP_MRNS << id

  id
end

# http://baby-names.familyeducation.com/popular-names/boys/
BOYS_NAMES = %w[Noah Liam Jacob Mason William Ethan Michael Alexander Jayden Daniel Elijah Aiden James Benjamin Matthew Jackson Logan David Anthony Joseph Joshua Andrew Lucas Gabriel Samuel Christopher John Dylan Isaac Ryan Nathan Carter Caleb Luke Christian Hunter Henry Owen Landon Jack Wyatt Jonathan Eli Isaiah Sebastian Jaxon Julian Brayden Gavin Levi Aaron Oliver Jordan Nicholas Evan Connor Charles Jeremiah Cameron Adrian Thomas Robert Tyler Colton Austin Jace Angel Dominic Josiah Brandon Ayden Kevin Zachary Parker Blake Jose Chase Grayson Jason Ian Bentley Adam Xavier Cooper Justin Nolan Hudson Easton Jase Carson Nathaniel Jaxson Kayden Brody Lincoln Luis Tristan Damian Camden Juan]

# http://baby-names.familyeducation.com/popular-names/girls/
GIRLS_NAMES = %w[Sophia Emma Olivia Isabella Ava Mia Emily Abigail Madison Elizabeth Charlotte Avery Sofia Chloe Ella Harper Amelia Aubrey Addison Evelyn Natalie Grace Hannah Zoey Victoria Lillian Lily Brooklyn Samantha Layla Zoe Audrey Leah Allison Anna Aaliyah Savannah Gabriella Camila Aria Kaylee Scarlett Hailey Arianna Riley Alexis Nevaeh Sarah Claire Sadie Peyton Aubree Serenity Ariana Genesis Penelope Alyssa Bella Taylor Alexa Kylie Mackenzie Caroline Kennedy Autumn Lucy Ashley Madelyn Violet Stella Brianna Maya Skylar Ellie Julia Sophie Katherine Mila Khloe Paisley Annabelle Alexandra Nora Melanie London Gianna Naomi Eva Faith Madeline Lauren Nicole Ruby Makayla Kayla Lydia Piper Sydney Jocelyn Morgan]

def gen_given_name(sex)
  if sex == 'F'
    GIRLS_NAMES[rand(GIRLS_NAMES.size)]

  else
    BOYS_NAMES[rand(BOYS_NAMES.size)]
  end
end

# http://genealogy.about.com/library/weekly/aa_common_surnames.htm
SURNAMES = %w(Smith Johnson Williams Jones Brown Davis Miller Wilson Moore Taylor Anderson Thomas Jackson White Harris Martin Thompson Garcia Martinez Robinson Clark Rodriguez Lewis Lee Walker Hall Allen Young Hernandez King Wright Lopez Hill Scott Green Adams Baker Gonzalez Nelson Carter Mitchell Perez Roberts Turner Phillips Campbell Parker Evans Edwards Collins Stewart Sanchez Morris Rogers Reed Cook Morgan Bell Murphy Bailey Rivera Cooper Richardson Cox Howard Ward Torres Peterson Gray Ramirez James Watson Brooks Kelly Sanders Price Bennett Wood Barnes Ross Henderson Coleman Jenkins Perry Powell Long Patterson Hughes Flores Washington Butler Simmons Foster Gonzales Bryant Alexander Russell Griffin Diaz Hayes)

def gen_surname
  SURNAMES[rand(SURNAMES.size)]
end

SEX_CODES = %w[M F]

def gen_sex
  SEX_CODES[rand(SEX_CODES.size)]
end

#             J   F   M   A   M   J   J   A   S   O   N   D
MONTH_DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
DOB_START_YEAR = 1930
DOB_END_YEAR = 2014

def gen_dob
  if rand(10) > 0
    y = DOB_START_YEAR + rand(DOB_END_YEAR - DOB_START_YEAR)
    m = 1 + rand(12)
    d = 1 + rand(MONTH_DAYS[m - 1])

    DateTime.new(y, m, d, rand(24), rand(60), rand(60), '-4')

  else
    ''
  end
end

def gen_dod(dob)
  dod = ''
  if dob != ''
    max_age = DOB_END_YEAR - DOB_START_YEAR
    age_years = DOB_END_YEAR - dob.year

    if rand(55 - (50 * (age_years.to_f / max_age)).to_i) == 0
      end_date = Date.new(DOB_END_YEAR, 12, 31)
      days = (end_date - dob.to_date).to_i
      d = end_date - rand(days / 2)
      dod = DateTime.new(d.year, d.month, d.day, rand(24), rand(60), rand(60), '-4')
    end
  end
  dod
end

def gen_reg_date(date)
  d = date - rand(3)
  DateTime.new(d.year, d.month, d.day, rand(24), rand(60), rand(60), '-4')
end

DAY_SECONDS = 60 * 60 * 24

def gen_atime
  m = 1 + rand(12)
  d = 1 + rand(MONTH_DAYS[m - 1])

  DateTime.new(2014, m, d, rand(24), rand(60), rand(60), '-4')
end

def gen_contact_date(date)
  date.to_date
end

def gen_wc_cm(sex)
  if rand(20) > 0
    sex = 'M' ?
        75 + (rand(500) / 10.0) :
        65 + (rand(500) / 10.0)

  else
    ''
  end
end

def gen_tg
  rand(15) > 0 ?
      120 + (rand(600) / 10.0) :
      ''
end

def gen_hdl(sex)
  if rand(15) > 0
    sex = 'M' ?
        30 + (rand(200) / 10.0) :
        40 + (rand(200) / 10.0)

  else
    ''
  end
end

def gen_bp
  if rand(20) > 0
    sbp = 105 + rand(50)
    dbp = 65 + rand(40)
    "#{sbp}/#{dbp}"

  else
    ''
  end
end

def gen_glc
  rand(10) > 0 ?
      80 + (rand(400) / 10.0) :
      ''
end


#################################################################################

def conv(val)
  if val == nil || val == ''
    'null'

  else
    "\"#{val}\""
  end
end

def gen_record
  pid =           gen_pid
  mrn =           gen_mrn
  sex =           gen_sex
  given_name =    gen_given_name sex
  surname =       gen_surname
  dob =           gen_dob
  dod =           gen_dod dob

  atime =         gen_atime
  reg_date =      gen_reg_date atime
  contact_date =  gen_contact_date atime

  wc =            gen_wc_cm sex
  tg =            gen_tg
  hdl =           gen_hdl sex
  bp =            gen_bp
  glc =           gen_glc


  File.open(OUT_FILE, 'a') do |f|
    f << "insert into PERSON(PID, FIRST_NAME, LAST_NAME, SEX, BIRTH_DATE, DEATH_DATE, REG_DATE) values(#{conv(pid)}, #{conv(given_name)}, #{conv(surname)}, #{conv(sex)}, #{conv(dob)}, #{conv(dod)}, #{conv(reg_date)});\n"

    f << "insert into PERSON_2(PID, WC, TG, HDL, BP, GLC) values(#{conv(pid)}, #{conv(wc)}, #{conv(tg)}, #{conv(hdl)}, #{conv(bp)}, #{conv(glc)});\n"

    f << "insert into PERSON_3(PID, TYPE_ID, IDENTITY_ID) values(#{conv(pid)}, \"1\", #{conv(mrn)});\n"

    f << "insert into ENCOUNTER(PID, ADMSN_TIME, CONTACT_DATE) values(#{conv(pid)}, #{conv(atime)}, #{conv(contact_date)});\n\n"
  end
end


##################################################################################

File.delete OUT_FILE if File.exists? OUT_FILE

(1..1000).each do
  gen_record
end
