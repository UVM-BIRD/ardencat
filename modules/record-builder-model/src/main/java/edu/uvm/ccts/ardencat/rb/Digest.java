/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb;

import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.JarUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by mstorer on 9/24/13.
 */
public class Digest {
    // for complete list of MessageDigest algorithms,
    // see http://docs.oracle.com/javase/6/docs/technotes/guides/security/StandardNames.html#MessageDigest
    private static final String hashAlgorithm = "SHA-1";

    private static String SALT_FILE;
    static {
        try {
            SALT_FILE = FileUtil.buildPath(JarUtil.getJarDir(Digest.class), ".ardencat-salt");
        } catch (Exception e) {
            SALT_FILE = ".ardencat-salt";
        }
    }

    private static Digest digest = null;

    public static Digest getInstance() throws IOException, NoSuchAlgorithmException {
        if (digest == null) digest = new Digest();
        return digest;
    }

    private final MessageDigest md;
    private final byte[] salt;

    private Digest() throws IOException, NoSuchAlgorithmException {
        Path path = Paths.get(SALT_FILE);

        if ( ! path.toFile().exists() ) {
            SecureRandom random = new SecureRandom();
            salt = random.generateSeed(40);
            Files.write(Paths.get(SALT_FILE), salt);

        } else {
            salt = Files.readAllBytes(path);
        }

        md = MessageDigest.getInstance(hashAlgorithm);
    }

    public String getHash(String s) {
        if (s == null) return null;

        md.reset();
        md.update(s.getBytes());
        md.update(salt);
        byte[] bytes = md.digest();

        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(Integer.toHexString(b & 0xff));
        }
        return sb.toString();
    }
}
