/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.locators;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by mstorer on 7/11/13.
 */
public class BaseRecordLocator extends RecordLocator implements Serializable {
    private static final Log log = LogFactory.getLog(BaseRecordLocator.class);

    private static final String ID_TOKEN_REGEX = "#\\{ID_LIST\\}";
    private static final String ID_TOKEN = "#{ID_LIST}";
    private static final int MAX_VARS_PER_QUERY = 999;

    public BaseRecordLocator(DataSource dataSource, String query) throws ConfigurationException {
        super("base", dataSource, query);
    }

    @Override
    protected String getIdToken() {
        return ID_TOKEN;
    }

    @Override
    protected String getIdTokenRegex() {
        return ID_TOKEN_REGEX;
    }

    @Override
    protected String getTestId() {
        return "('0')";
    }

    public List<Record> execute(Collection<? extends BasicEligibilityRecord> eligibilityRecords) throws LocatorException {
        try {
            List<Record> records = new ArrayList<Record>();

            Map<Object, BasicEligibilityRecord> idEligRecordMap = new LinkedHashMap<Object, BasicEligibilityRecord>();
            for (BasicEligibilityRecord record : eligibilityRecords) {
                idEligRecordMap.put(record.getData().getId(), record);
            }

            if (idEligRecordMap.size() > 0) {
                PreparedStatement stmt = null;
                ResultSet rs = null;

                for (List<Object> ids : chunkRecordIds(idEligRecordMap.keySet())) {
                    try {
                        stmt = buildSelectStatement(getDataSource().getConnection(), ids);
                        rs = stmt.executeQuery();

                        while (rs.next()) {
                            Record rec = buildRecord(null, rs);

                            BasicEligibilityRecord ber = idEligRecordMap.get(rec.getId());
                            if (ber != null) {
                                rec.setEligibilityRecord(new BasicEligibilityRecord(ber));  // ensure this object is the basic variety -
                                                                                            // don't need or want audit info

                            } else {
                                log.warn("couldn't find eligibility data for record with id=" + rec.getId() + "!");
                            }

                            records.add(rec);
                        }

                    } finally {
                        try { if (rs   != null) rs.close();   } catch (Exception e) {}
                        try { if (stmt != null) stmt.close(); } catch (Exception e) {}
                    }
                }
            }

            return records;

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " locating base record - " + e.getMessage(), e);
            throw new LocatorException(e.getMessage(), e);
        }
    }


///////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private List<List<Object>> chunkRecordIds(Collection<Object> recordIds) {
        int chunks = recordIds.size() / MAX_VARS_PER_QUERY;
        if (recordIds.size() % MAX_VARS_PER_QUERY > 0) chunks += 1;

        List<List<Object>> chunkedRecordIds = new ArrayList<List<Object>>();
        for (int i = 0; i < chunks; i ++) {
            chunkedRecordIds.add(new ArrayList<Object>());
        }

        int i = 0;
        Iterator<Object> iter = recordIds.iterator();
        while (iter.hasNext()) {
            chunkedRecordIds.get(i++ % chunks).add(iter.next());
        }

        return chunkedRecordIds;
    }

    private PreparedStatement buildSelectStatement(Connection conn, Collection<Object> recordIds) throws SQLException {
        String query = getQuery().replaceFirst(getIdTokenRegex(), generateIdPlaceholders(recordIds.size()));

        PreparedStatement stmt = conn.prepareStatement(query);

        int i = 1;
        for (Object recordId : recordIds) stmt.setObject(i++, recordId);

        return stmt;
    }

    private String generateIdPlaceholders(int count) {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (int i = 0; i < count - 1; i ++) sb.append("?,");
        sb.append("?)");
        return sb.toString();
    }
}
