package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.eligibility.model.EligibilityColor;
import edu.uvm.ccts.ardencat.eligibility.model.StatusEligibilityColor;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 7/6/15.
 */
public class EligibilityUIConfig extends EligibilityConfig {
    private List<String> sortByFields = new ArrayList<String>();
    private Map<String, String> labels = new HashMap<String, String>();
    private EligibilityColor eligibleColor = ELIGIBLE_DFLT;
    private EligibilityColor unknownColor = UNKNOWN_DFLT;
    private EligibilityColor ineligibleColor = INELIGIBLE_DFLT;
    private List<CustomEligibilityColor> customColors = new ArrayList<CustomEligibilityColor>();

    public EligibilityUIConfig(String source) {
        super(source);
    }

    public List<String> getSortByFields() {
        return sortByFields;
    }

    public void setSortByFields(List<String> sortByFields) {
        this.sortByFields = sortByFields;
    }

    public void setFieldLabel(String field, String label) {
        labels.put(field.toLowerCase(), label);
    }

    public String getFieldLabel(String field) {
        return labels.get(field.toLowerCase());
    }

    @Override
    public EligibilityColor getEligibleColor() {
        return eligibleColor;
    }

    public void setEligibleColor(Color color) {
        this.eligibleColor = color != null ?
                new StatusEligibilityColor(EligibilityStatus.ELIGIBLE, color) :
                ELIGIBLE_DFLT;
    }

    @Override
    public EligibilityColor getUnknownColor() {
        return unknownColor;
    }

    public void setUnknownColor(Color color) {
        this.unknownColor = color != null ?
                new StatusEligibilityColor(EligibilityStatus.UNKNOWN, color) :
                UNKNOWN_DFLT;
    }

    @Override
    public EligibilityColor getIneligibleColor() {
        return ineligibleColor;
    }

    public void setIneligibleColor(Color color) {
        this.ineligibleColor = color != null ?
                new StatusEligibilityColor(EligibilityStatus.INELIGIBLE, color) :
                INELIGIBLE_DFLT;
    }

    public void addCustomColor(CustomEligibilityColor color) {
        customColors.add(color);
    }

    public List<CustomEligibilityColor> getCustomColors() {
        return customColors;
    }
}
