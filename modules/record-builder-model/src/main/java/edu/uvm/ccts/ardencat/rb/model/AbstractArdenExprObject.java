/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.arden.model.Variables;
import edu.uvm.ccts.arden.util.ObjectUtil;
import edu.uvm.ccts.ardencat.model.FieldObject;
import edu.uvm.ccts.ardencat.rb.arden.logic.LogicInterpreter;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by mstorer on 10/3/14.
 */
public abstract class AbstractArdenExprObject<F extends FieldObject> implements Serializable {
    private String ardenExpr;

    public AbstractArdenExprObject(String ardenExpr) throws IOException {
        testArdenExpr(ardenExpr);
        this.ardenExpr = ardenExpr;
    }

    public String getArdenExpr() {
        return ardenExpr;
    }

    protected Object execute(F f) throws IOException {
        return new LogicInterpreter().evaluate(buildVariables(f), ardenExpr);
    }

    protected void testArdenExpr(String ardenExpr) throws IOException {
        new LogicInterpreter().validateParse(ardenExpr);
    }

    protected Variables buildVariables(F f) {
        Variables vars = new Variables();

        for (String fieldName : f.getFieldNames()) {
            Object obj = f.getField(fieldName);
            vars.putVariable(fieldName, ObjectUtil.toArdenDataType(obj));
        }

        return vars;
    }
}
