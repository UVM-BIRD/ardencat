/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.arden.logic;

import edu.uvm.ccts.arden.logic.BailLogicLexer;
import edu.uvm.ccts.arden.logic.BailLogicParser;
import edu.uvm.ccts.arden.logic.antlr.LogicBaseVisitor;
import edu.uvm.ccts.arden.logic.antlr.LogicLexer;
import edu.uvm.ccts.arden.logic.antlr.LogicParser;
import edu.uvm.ccts.arden.model.Variables;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

/**
 * Interpret arbitrary Arden-syntax logic as defined by Arden Syntax for Medical Logic Systems Version 2.8
 * @author Matt Storer
 */
public class LogicInterpreter {
    private PrintStream printStream = System.out;

    public void setPrintStream(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void validateParse(String logic) throws IOException {
        new LogicBaseVisitor().visit(buildParseTree(logic));
    }

    public Object evaluate(String logic) throws IOException {
        return evaluate(null, logic);
    }

    public Object evaluate(Variables variables, String logic) throws IOException {
        LogicEvalVisitor visitor = new LogicEvalVisitor(variables);
        visitor.setPrintStream(printStream);
        visitor.visit(buildParseTree(logic));
        return visitor.getConcludeValue().toJavaObject();
    }

    private ParseTree buildParseTree(String logic) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(logic.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        LogicLexer lexer = new BailLogicLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LogicParser parser = new BailLogicParser(tokens);
        return parser.init();
    }
}
