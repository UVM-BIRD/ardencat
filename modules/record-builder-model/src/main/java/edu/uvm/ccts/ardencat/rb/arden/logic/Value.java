/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.arden.logic;

import edu.uvm.ccts.ardencat.arden.AbstractValue;
import edu.uvm.ccts.arden.model.ABoolean;
import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.ANull;
import edu.uvm.ccts.arden.model.AVoid;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 4/23/14.
 */
public class Value extends AbstractValue<Value> {
    private static final Value VOID = new Value(AVoid.VOID());

    public Value(ADataType obj) {
        super(obj);
    }

    @Override
    public Value VOID() {
        return VOID;
    }

    @Override
    public Value NULL() {
        return new Value(ANull.NULL());
    }

    @Override
    public Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    @Override
    public Value FALSE() {
        return new Value(ABoolean.FALSE());
    }

    @Override
    public boolean equals(Object o) {
        if      (o == null)                  return false;
        else if (o == this)                  return true;
        else if (o.getClass() != getClass()) return false;
        else {
            AbstractValue v = (AbstractValue) o;
            return new EqualsBuilder()
                    .append(obj, v)
                    .isEquals();
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(919, 677)
                .append(obj)
                .toHashCode();
    }
}
