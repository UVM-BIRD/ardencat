/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.locators;

import edu.uvm.ccts.arden.model.Variables;
import edu.uvm.ccts.arden.util.ObjectUtil;
import edu.uvm.ccts.ardencat.rb.model.AbstractArdenExprObject;
import edu.uvm.ccts.ardencat.rb.model.Record;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by mstorer on 10/3/13.
 */
public class DerivedField extends AbstractArdenExprObject<Record> implements Serializable {
    private String name;

    public DerivedField(String name, String ardenExpr) throws IOException {
        super(ardenExpr);
        this.name = name;
    }

    @Override
    public Object execute(Record record) throws IOException {
        return super.execute(record);
    }

    public String getName() {
        return name;
    }

    @Override
    protected Variables buildVariables(Record r) {
        Variables vars = super.buildVariables(r);

        // now append variables from parent record(s) -

        Record parent = r.getParent();
        while (parent != null) {
            for (String fieldName : parent.getFieldNames()) {
                if ( ! vars.hasVariable(fieldName) ) {                // do not want to clobber fields previously set above
                    Object obj = parent.getField(fieldName);
                    vars.putVariable(fieldName, ObjectUtil.toArdenDataType(obj));
                }
            }
            parent = parent.getParent();
        }

        return vars;
    }
}
