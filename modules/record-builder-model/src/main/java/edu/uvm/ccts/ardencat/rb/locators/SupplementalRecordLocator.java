/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.locators;

import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.util.ObjectUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by mstorer on 7/22/13.
 */
public class SupplementalRecordLocator extends RecordLocator implements Serializable {
    private static final Log log = LogFactory.getLog(SupplementalRecordLocator.class);

    public static final String ID_TOKEN_REGEX = "#\\{ID\\}";
    public static final String ID_TOKEN = "#{ID}";

    public enum Type {
        ONE,
        MANY,
        CONSOLIDATED
    }

    private Type type;
    private String consolidateDelim = "\n";
    private List<String> consolidateBy = null;
    private String versionField = null;
    private List<String> versionControlFields = null;
    private VersionIndex versionIndex = VersionIndex.DEFAULT;
    private boolean collapseToParent = false;

    private Map<Object, List<Record>> cache = new HashMap<Object, List<Record>>();

    public SupplementalRecordLocator(String name, Type type, DataSource dataSource, String query) throws ConfigurationException {
        super(name, dataSource, query);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String getConsolidateDelim() {
        return consolidateDelim;
    }

    public void setConsolidateDelim(String consolidateDelim) {
        this.consolidateDelim = consolidateDelim;
    }

    public List<String> getConsolidateBy() {
        return consolidateBy;
    }

    public void setConsolidateBy(List<String> consolidateBy) {
        this.consolidateBy = consolidateBy;
    }

    public boolean hasConsolidateBy() {
        return consolidateBy != null && consolidateBy.size() > 0;
    }

    public void setVersionField(String versionField) {
        this.versionField = versionField;
    }

    public void setVersionControlFields(List<String> versionControlFields) {
        this.versionControlFields = versionControlFields;
    }

    public void setVersionIndex(VersionIndex versionIndex) {
        this.versionIndex = (versionIndex == null) ? VersionIndex.DEFAULT : versionIndex;
    }

    public boolean isCollapseToParent() {
        return collapseToParent;
    }

    public void setCollapseToParent(boolean collapseToParent) {
        this.collapseToParent = collapseToParent;
    }

    @Override
    protected String getIdToken() {
        return ID_TOKEN;
    }

    @Override
    protected String getIdTokenRegex() {
        return ID_TOKEN_REGEX;
    }

    @Override
    protected String getTestId() {
        return "'0'";
    }

    public List<Record> execute(Record baseRecord) throws LocatorException {
        if ( ! cache.containsKey(baseRecord.getId()) ) {
            try {
                List<Record> records = new ArrayList<Record>();

                PreparedStatement stmt = null;
                ResultSet rs = null;

                try {
                    stmt = buildSelectStatement(getDataSource().getConnection(), baseRecord.getId());
                    rs = stmt.executeQuery();

                    while (rs.next()) {
                        records.add(buildRecord(baseRecord, rs));
                    }

                } finally {
                    try { if (rs   != null) rs.close();   } catch (Exception e) {}
                    try { if (stmt != null) stmt.close(); } catch (Exception e) {}
                }

                if (versionField != null) {
                    cache.put(baseRecord.getId(), filterRecordsByVersion(records));

                } else {
                    cache.put(baseRecord.getId(), records);
                }

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " locating supplemental record - " + e.getMessage(), e);
                throw new LocatorException(e.getMessage(), e);
            }
        }

        try {
            return deepCopy(cache.get(baseRecord.getId()));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " deep-copying supplemental record - " + e.getMessage(), e);
            throw new LocatorException(e.getMessage(), e);
        }
    }

    public void clearCache() {
        cache.clear();
    }

    private List<Record> deepCopy(List<Record> list) throws IOException, ClassNotFoundException {
        if (list == null) return null;

        List<Record> newList = new ArrayList<Record>();
        for (Record record : list) {
            newList.add((Record) ObjectUtil.deepCopy(record));
        }

        return newList;
    }

    private PreparedStatement buildSelectStatement(Connection conn, Object recordId) throws SQLException {
        String query = getQuery().replaceAll(getIdTokenRegex(), "?");

        PreparedStatement stmt = conn.prepareStatement(query);

        for (int i = 1; i <= StringUtils.countMatches(query, "?"); i ++) {
            stmt.setObject(i, recordId);
        }

        return stmt;
    }

    private List<Record> filterRecordsByVersion(List<Record> records) throws LocatorException {
        if (versionControlFields != null && versionControlFields.size() > 0) {

            // split into groups based on control fields

            Map<String, List<Record>> map = new LinkedHashMap<String, List<Record>>();

            for (Record r : records) {
                List<String> keyParts = new ArrayList<String>();
                for (String field : versionControlFields) {
                    if ( ! r.hasField(field) ) throw new LocatorException("invalid version control field '" +
                            field + "' for locator '" + getName() + "'");

                    keyParts.add(r.getField(field).toString());
                }
                String key = StringUtils.join(keyParts, "|");
                if ( ! map.containsKey(key) ) map.put(key, new ArrayList<Record>());
                map.get(key).add(r);
            }

            List<Record> filteredRecords = new ArrayList<Record>();

            for (List<Record> list : map.values()) {
                if (list.size() > 1) {
                    filteredRecords.addAll(filterRecordsByVersion(list, getDesiredVersion(list)));

                } else {
                    filteredRecords.addAll(list);
                }
            }

            return filteredRecords;

        } else {
            return records.size() > 1 ?
                    filterRecordsByVersion(records, getDesiredVersion(records)) :
                    records;
        }
    }

    private List<Record> filterRecordsByVersion(List<Record> records, Object version) {
        List<Record> filteredRecords = new ArrayList<Record>();

        for (Record r : records) {
            if (r.getField(versionField).equals(version)) filteredRecords.add(r);
        }

        return filteredRecords;
    }

    private Object getDesiredVersion(List<Record> records) {
        Set<Object> set = new TreeSet<Object>();
        for (Record r : records) set.add(r.getField(versionField));
        List<Object> versions = new ArrayList<Object>(set);

        switch (versionIndex.getType()) {
            case FIRST:
                return versions.get(0);

            case BY_INDEX:
                int i = versionIndex.getIndex();
                return (i >= 0) ?
                        versions.get(Math.min(i, versions.size() - 1)) :
                        versions.get(Math.max(0, versions.size() + i));

            case LAST:
            default:
                return versions.get(versions.size() - 1);
        }
    }
}

