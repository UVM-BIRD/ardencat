/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.eligibility.model.EligibilityColor;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.awt.*;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by mstorer on 10/2/14.
 */
public class CustomEligibilityColor extends AbstractArdenExprObject<EligibilityRecord> implements EligibilityColor, Serializable {
    private String label;
    private Color color;
    private EligibilityStatus status = null;

    public CustomEligibilityColor(String label, Color color, String ardenExpr) throws IOException {
        super(ardenExpr);
        this.label = label;
        this.color = color;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public boolean matches(EligibilityRecord record) {
        if (status == null || status == record.getStatus()) {
            Object rval;

            try {
                rval = super.execute(record);

            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            if (rval != null && rval instanceof Boolean) {
                return (Boolean) rval;
            }
        }
        return false;
    }

    @Override
    public EligibilityStatus getStatus() {
        return status;
    }

    public void setStatus(EligibilityStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        CustomEligibilityColor sec = (CustomEligibilityColor) o;
        return new EqualsBuilder()
                .append(label, sec.label)
                .append(color, sec.color)
                .append(status, sec.status)
                .append(getArdenExpr(), sec.getArdenExpr())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(1013, 1109)
                .append(label)
                .append(color)
                .append(status)
                .append(getArdenExpr())
                .toHashCode();
    }
}
