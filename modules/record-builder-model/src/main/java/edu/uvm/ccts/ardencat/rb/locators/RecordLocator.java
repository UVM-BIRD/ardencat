/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.locators;

import com.foundationdb.sql.StandardException;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.sql.SqlStatement;
import edu.uvm.ccts.common.sql.model.Field;

import java.io.IOException;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mstorer on 7/11/13.
 */
public abstract class RecordLocator implements Serializable {
    private String name;
    private DataSource dataSource;
    private String query;
    private Collection<DerivedField> derivedFields = null;
    private Set<String> transientFields = null;

    public RecordLocator(String name, DataSource dataSource, String query) throws ConfigurationException {
        this.name = name;
        this.dataSource = dataSource;

        validateQuery(query);
        this.query = query;
    }

    public String getName() {
        return name;
    }

    protected DataSource getDataSource() {
        return dataSource;
    }

    protected String getQuery() {
        return query;
    }

    protected abstract String getIdToken();
    protected abstract String getIdTokenRegex();
    protected abstract String getTestId();

    public void setDerivedFields(Collection<DerivedField> derivedFields) {
        this.derivedFields = derivedFields;
    }

    public void flagAsTransient(String fieldName) {
        if (transientFields == null) transientFields = new HashSet<String>();
        transientFields.add(fieldName);
    }

    public boolean selectsField(String fieldName) throws ConfigurationException {
        try {
            String testQuery = query.replaceAll(getIdTokenRegex(), getTestId());
            SqlStatement stmt = new SqlStatement(testQuery);

            for (Field f : stmt.getSelectedFields()) {
                if (fieldName.equalsIgnoreCase(f.getName()) || fieldName.equalsIgnoreCase(f.getAlias())) {
                    return true;
                }
            }

            return false;

        } catch (StandardException e) {
            throw new ConfigurationException("couldn't parse base-record locator query : " + e.getMessage(), e);
        }
    }

    protected Record buildRecord(Record parent, ResultSet rs) throws SQLException, IOException {
        Record r = new Record(parent);

        r.setTransientFields(transientFields);

        ResultSetMetaData meta = rs.getMetaData();

        for (int i = 1; i <= meta.getColumnCount(); i ++) {
            String fieldName = meta.getColumnLabel(i);

            if (fieldName.equalsIgnoreCase(Record.ID_FIELD_NAME))   r.setId(rs.getObject(i));
            else                                                    r.setField(fieldName, rs.getObject(i));
        }

        if (derivedFields != null) {
            for (DerivedField f : derivedFields) {
                r.setField(f.getName(), f.execute(r));
            }
        }

        return r;
    }

    private void validateQuery(String query) throws ConfigurationException {
        if (query == null)
            throw new ConfigurationException("query for locator '" + name + "' does not exist");

        if ( ! query.toLowerCase().startsWith("select"))
            throw new ConfigurationException("query for locator '" + name + "' is not a select statement");

        // some basic protection to ensure the query has an ID token in it somewhere.
        if ( ! query.toLowerCase().contains(getIdToken().toLowerCase()) )
            throw new ConfigurationException("query for locator '" + name + "' doesn't contain ID token '" + getIdToken() + "'");


        String testQuery = query.replaceAll(getIdTokenRegex(), getTestId());
        try {
            new SqlStatement(testQuery);

        } catch (StandardException e) {
            throw new ConfigurationException("couldn't parse query for locator '" + name + "'", e);
        }
    }
}
