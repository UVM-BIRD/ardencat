/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.arden.logic;

import edu.uvm.ccts.ardencat.arden.logic.AbstractLogicEvalVisitor;
import edu.uvm.ccts.arden.model.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 4/23/14.
 */
public class LogicEvalVisitor extends AbstractLogicEvalVisitor<Value, Arden> {
    private static final Log log = LogFactory.getLog(LogicEvalVisitor.class);

    public LogicEvalVisitor(Variables variables) {
        super(variables, new Arden(log));
    }

    @Override
    protected Value createValue(ADataType obj) {
        return new Value(obj);
    }

    @Override
    protected Value NULL() {
        return new Value(ANull.NULL());
    }

    @Override
    protected Value VOID() {
        return new Value(AVoid.VOID());
    }

    @Override
    protected Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    @Override
    protected Value FALSE() {
        return new Value(ABoolean.FALSE());
    }
}
