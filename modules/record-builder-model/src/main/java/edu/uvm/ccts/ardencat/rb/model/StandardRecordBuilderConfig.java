package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by mstorer on 7/6/15.
 */
public class StandardRecordBuilderConfig extends AbstractRecordBuilderConfig<EligibilityUIConfig> implements Serializable {
    private String name;

    public StandardRecordBuilderConfig(File file, Frame frame) throws ConfigurationException {
        super(file, frame);
    }

    public StandardRecordBuilderConfig(String filename, Frame frame, DataSource ardencatDataSource, Element root) throws ConfigurationException {
        super(filename, frame, ardencatDataSource, root);
    }


///////////////////////////////////////////////////////////////////////////////////
// public methods
//

    public String getName() {
        return name;
    }


///////////////////////////////////////////////////////////////////////////////////
// private methods
//

    @Override
    protected void buildOtherConfigs(Element root) throws ConfigurationException {
        buildDisplay(root);
    }

    private void buildDisplay(Element root) throws ConfigurationException {
        Element xmlDisplay = root.getElement(new XPath("display"));
        name = xmlDisplay.getAttribute("name");
    }

    @Override
    protected EligibilityUIConfig createEligibilityConfig(String source) {
        return new EligibilityUIConfig(source);
    }

    @Override
    protected void buildOtherEligibilityConfigs(EligibilityUIConfig eligibilityConfig, Element xmlEligibility) throws IOException {
        if (xmlEligibility.hasAttribute("sortBy")) {
            java.util.List<String> sortByFields = new ArrayList<String>();
            String sortBy = xmlEligibility.getAttribute("sortBy").trim();
            for (String field : sortBy.split("\\s*,\\s*")) {
                sortByFields.add(field);
            }
            eligibilityConfig.setSortByFields(sortByFields);
        }

        Elements xmlLabels = xmlEligibility.getElements(new XPath("labels/label"));
        while (xmlLabels.hasMoreElements()) {
            Element xmlLabel = xmlLabels.next();
            eligibilityConfig.setFieldLabel(xmlLabel.getAttribute("field"), xmlLabel.getAttribute("value"));
        }

        if (xmlEligibility.hasElement("eligibleColorOverride")) {
            Element xmlColor = xmlEligibility.getElement("eligibleColorOverride");
            eligibilityConfig.setEligibleColor(buildColor(xmlColor));
        }

        if (xmlEligibility.hasElement("unknownColorOverride")) {
            Element xmlColor = xmlEligibility.getElement("unknownColorOverride");
            eligibilityConfig.setUnknownColor(buildColor(xmlColor));
        }

        if (xmlEligibility.hasElement("ineligibleColorOverride")) {
            Element xmlColor = xmlEligibility.getElement("ineligibleColorOverride");
            eligibilityConfig.setIneligibleColor(buildColor(xmlColor));
        }

        Elements xmlCustomColors = xmlEligibility.getElements(new XPath("customColors/color"));
        while (xmlCustomColors.hasMoreElements()) {
            Element xmlColor = xmlCustomColors.next();
            CustomEligibilityColor c = new CustomEligibilityColor(
                    xmlColor.getAttribute("label"),
                    buildColor(xmlColor),
                    xmlColor.getTrimTextString());

            if (xmlColor.hasAttribute("status")) {
                c.setStatus(EligibilityStatus.valueOf(xmlColor.getAttribute("status")));
            }

            eligibilityConfig.addCustomColor(c);
        }
    }

    private Color buildColor(Element xmlColor) {
        return new Color(Integer.parseInt(xmlColor.getAttribute("r")),
                Integer.parseInt(xmlColor.getAttribute("g")),
                Integer.parseInt(xmlColor.getAttribute("b")));
    }
}
