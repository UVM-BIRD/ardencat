/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mstorer on 10/13/14.
 */
public class EligibilityAudit implements Serializable {
    private int id;
    private int eligId;
    private EligibilityStatus origStatus;
    private EligibilityStatus newStatus;
    private String authSig;
    private String reason;
    private Date created;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEligId() {
        return eligId;
    }

    public void setEligId(int eligId) {
        this.eligId = eligId;
    }

    public EligibilityStatus getOrigStatus() {
        return origStatus;
    }

    public void setOrigStatus(EligibilityStatus origStatus) {
        this.origStatus = origStatus;
    }

    public EligibilityStatus getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(EligibilityStatus newStatus) {
        this.newStatus = newStatus;
    }

    public String getAuthSig() {
        return authSig;
    }

    public void setAuthSig(String authSig) {
        this.authSig = authSig;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}