/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.ardencat.model.FieldConstants;
import edu.uvm.ccts.ardencat.model.FieldObject;
import edu.uvm.ccts.ardencat.rb.Digest;
import edu.uvm.ccts.ardencat.rb.exceptions.NameCollisionException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XStreamAlias("record")
public class Record implements FieldObject, FieldConstants, Serializable {
    private static final Log log = LogFactory.getLog(Record.class);

    public static final String ID_FIELD_NAME = FieldConstants.ID_FIELD_NAME;
    public static final String STATUS_FIELD_NAME = FieldConstants.STATUS_FIELD_NAME;
    public static final String ELIGIBILITY_RECORD_FIELD_NAME = FieldConstants.ELIGIBILITY_RECORD_FIELD_NAME;

    private static final String DELIM = "/";
    private static final String DELIM_REGEX = "\\/";

    private Object id = null;
    private String exportId = null;
    private BasicEligibilityRecord eligibilityRecord = null;
    private Map<String, Object> fields = new LinkedHashMap<String, Object>();
    private Set<String> transientFields = null;

    private Record parent = null;

    public Record(Record parent) {
        this.parent = parent;
    }

    public Record getParent() {
        return parent;
    }

    public void consume(Record r) throws NameCollisionException {
        if (r == null) return;

        for (String fieldName : r.getFieldNames()) {
            if ( ! fields.containsKey(fieldName) ) {
                fields.put(fieldName, r.getField(fieldName));
                if (r.isTransient(fieldName)) {
                    if (transientFields == null) transientFields = new HashSet<String>();
                    transientFields.add(fieldName);
                }

            } else {
                throw new NameCollisionException("collision on record attribute name '" + fieldName + "'");
            }
        }
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;

        try {
            exportId = Digest.getInstance().getHash(id.toString());

        } catch (Exception e) {
            exportId = null;
        }
    }

    public String getExportId() {
        return exportId;
    }

    public BasicEligibilityRecord getEligibilityRecord() {
        return eligibilityRecord;
    }

    public void setEligibilityRecord(BasicEligibilityRecord eligibilityRecord) {
        this.eligibilityRecord = eligibilityRecord;
    }

    public EligibilityStatus getStatus() {
        return eligibilityRecord != null ? eligibilityRecord.getStatus() : null;
    }

    public Set<String> getTransientFields() {
        return transientFields;
    }

    public void setTransientFields(Set<String> transientFields) {
        this.transientFields = transientFields;
    }

    public Map<String, Object> flatten(boolean identify, boolean includeEligibilityData) {
        return flatten(identify, includeEligibilityData, "");
    }

    @Override
    public Collection<String> getFieldNames() {
        return fields.keySet();
    }

    public List<String> getExportableFieldNames() {
        if (transientFields == null || transientFields.isEmpty()) {
            return new ArrayList<String>(fields.keySet());

        } else {
            List<String> col = new ArrayList<String>();
            for (String field : fields.keySet()) {
                if ( ! transientFields.contains(field) ) col.add(field);
            }
            return col;
        }
    }

    public void setField(String field, Object value) {
        if (value instanceof Timestamp) {
            Date d = new Date();
            d.setTime(((Timestamp) value).getTime());
            value = d;
        }

        fields.put(field, value);
    }

    @Override
    public Object getField(String field) {
        return fields.get(field);
    }

    @Override
    public boolean hasField(String field) {
        return fields.containsKey(field);
    }

    public boolean isTransient(String field) {
        return transientFields != null && transientFields.contains(field);
    }


////////////////////////////////////////////////////////////////////////
// UI functions

    public Object getFieldByColumnName(String name) {
        int pos = name.indexOf(DELIM);
        if (pos == -1) {
            switch (name) {
                case ID_FIELD_NAME:     return getId();
                default:                return getObjectByName(name);
            }

        } else {
            Object o = getObjectByName(name.substring(0, pos));
            if (o != null && o instanceof Record) {
                return ((Record) o).getFieldByColumnName(name.substring(pos + 1));
            }
        }

        return null;
    }

    public void setFieldByColumnName(String name, Object value) {
        int pos = name.indexOf(DELIM);
        if (pos == -1) {
            if (name.equals(ID_FIELD_NAME)) {
                setId(value);

            // todo : what if the user tries to set a field that belongs to the eligibility record, e.g. status?

            } else {
                setObjectByName(name, value);
            }

        } else {
            String objName = name.substring(0, pos);

            Object o = getObjectByName(objName);
            if (o == null) {
                o = new Record(this);
                setObjectByName(objName, o);
            }

            if (o instanceof Record) {
                ((Record) o).setFieldByColumnName(name.substring(pos + 1), value);
            }
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private static final Pattern INDEX_PATTERN = Pattern.compile("^([^" + DELIM_REGEX + "]+)\\[(\\d+)\\].*");

    private Object getObjectByName(String name) {
        Matcher m = INDEX_PATTERN.matcher(name);

        if (m.matches()) {
            String fieldName = m.group(1);
            int index = Integer.parseInt(m.group(2));
            return getListIndexField(fieldName, index);

        } else {
            return getField(name);
        }
    }

    private void setObjectByName(String name, Object obj) {
        Matcher m = INDEX_PATTERN.matcher(name);

        if (m.matches()) {
            String fieldName = m.group(1);
            int index = Integer.parseInt(m.group(2));
            setListIndexField(fieldName, index, obj);

        } else {
            setField(name, obj);
        }
    }

    private Object getListIndexField(String fieldName, int index) {
        if (hasField(fieldName)) {
            Object obj = getField(fieldName);
            if (obj instanceof List) {
                List list = (List) obj;
                if (index < list.size()) {
                    return list.get(index);

                } else {
                    log.error("cannot get index " + index + " of field '" + fieldName + "'");
                }

            } else {
                throw new RuntimeException("cannot index into " + obj.getClass().getName());
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    private void setListIndexField(String fieldName, int index, Object obj) {
        Object o = getField(fieldName);
        if (o == null) {
            o = new ArrayList<Object>();
            setField(fieldName, o);
        }

        if (o instanceof List) {
            List<Object> list = (List<Object>) o;
            if (index < list.size()) {
                list.set(index, obj);

            } else {
                for (int i = list.size(); i < index; i ++) {
                    list.add(null);
                }

                list.add(obj);
            }

        } else {
            throw new RuntimeException("cannot index into " + o.getClass().getName());
        }
    }

    private Map<String, Object> flatten(boolean identify, boolean includeEligibilityData, String prefix) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();

        if ( ! prefix.equals("") ) prefix = prefix + DELIM;

        if (id != null && ! isTransient(ID_FIELD_NAME)) {
            map.put(prefix + ID_FIELD_NAME, identify ? id : exportId);
        }

        if (includeEligibilityData && eligibilityRecord != null) {

            // todo : include all eligibility data, not just status - see XmlRecordConverter

            map.put(STATUS_FIELD_NAME, eligibilityRecord.getStatus());      // do not include prefix
        }

        for (String key : getExportableFieldNames()) {
            Object val = fields.get(key);
            if (val == null) continue;

            if (val instanceof List) {
                if (((List) val).isEmpty()) continue;

                int i = 0;

                for (Object o : (List) val) {
                    if (o instanceof Record) {
                        Record r = (Record) o;
                        map.putAll(r.flatten(identify, false, prefix + key + "[" + i + "]"));

                    } else {
                        map.put(prefix + key + "[" + i + "]", o);
                    }

                    i ++;
                }

            } else if (val instanceof Record) {
                Record r = (Record) val;
                map.putAll(r.flatten(identify, false, prefix + key));

            } else {
                map.put(prefix + key, val);
            }
        }

        return map;
    }
}
