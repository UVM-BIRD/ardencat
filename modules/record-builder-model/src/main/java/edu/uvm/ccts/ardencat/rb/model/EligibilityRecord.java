/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.model.FieldObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by mstorer on 3/18/14.
 */
public class EligibilityRecord extends BasicEligibilityRecord implements FieldObject, Serializable {
    private List<EligibilityAudit> auditHistory = null;

    public EligibilityRecord(BasicEligibilityRecord basicRec) {
        super(basicRec);
    }

    public boolean hasAudits() {
        return auditHistory != null && auditHistory.size() > 0;
    }

    public List<EligibilityAudit> getAuditHistory() {
        return auditHistory;
    }

    public void setAuditHistory(List<EligibilityAudit> auditHistory) {
        this.auditHistory = auditHistory;
    }

    @Override
    public Collection<String> getFieldNames() {
        return data.getKeys();
    }

    @Override
    public boolean hasField(String fieldName) {
        return data.getKeys().contains(fieldName);
    }

    @Override
    public Object getField(String fieldName) {
        return data.getByKey(fieldName);
    }
}
