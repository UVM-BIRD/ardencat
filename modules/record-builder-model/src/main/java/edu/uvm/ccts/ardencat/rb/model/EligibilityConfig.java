/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 9/4/14.
 */
public class EligibilityConfig extends BasicEligibilityConfig implements Serializable {
    private String source;
    private List<String> keyFields = new ArrayList<String>();
    private List<String> hiddenFields = new ArrayList<String>();

    public EligibilityConfig(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public List<String> getKeyFields() {
        return keyFields;
    }

    public void setKeyFields(List<String> keyFields) {
        this.keyFields = keyFields;
    }

    public void markFieldHidden(String field) {
        if (field != null) {
            hiddenFields.add(field.trim().toLowerCase());
        }
    }

    public boolean isFieldHidden(String field) {
        return field != null && hiddenFields.contains(field.trim().toLowerCase());
    }
}
