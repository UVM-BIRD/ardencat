/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.locators;

import java.io.Serializable;

/**
 * Created by mstorer on 10/10/13.
 */
public class VersionIndex implements Serializable {
    public static final VersionIndex DEFAULT = new VersionIndex("last");

    public enum Type {
        FIRST,
        LAST,
        BY_INDEX
    }

    private Type type;
    private int index;

    public VersionIndex(String index) throws NumberFormatException {
        if (index == null || index.equalsIgnoreCase("last")) {
            type = Type.LAST;

        } else if (index.equalsIgnoreCase("first")) {
            type = Type.FIRST;

        } else {
            type = Type.BY_INDEX;
            this.index = Integer.parseInt(index);
        }
    }

    public Type getType() {
        return type;
    }

    public int getIndex() {
        return index;
    }
}
