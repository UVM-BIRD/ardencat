/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.appenders;

import edu.uvm.ccts.ardencat.rb.exceptions.AppenderException;
import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.locators.SupplementalRecordLocator;
import edu.uvm.ccts.ardencat.rb.model.Record;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.*;

/**
 * Created by mstorer on 7/11/13.
 */
public class RecordAppender implements Serializable {
    private static final Log log = LogFactory.getLog(RecordAppender.class);

    private SupplementalRecordLocator locator;
    private Collection<RecordAppender> subAppenders;

    public RecordAppender(SupplementalRecordLocator locator) {
        this.locator = locator;
        subAppenders = new ArrayList<RecordAppender>();
    }

    public void addSubAppender(RecordAppender subAppender) {
        subAppenders.add(subAppender);
    }

    public void execute(Record baseRecord) throws AppenderException, LocatorException {
        if (baseRecord.getId() == null) {
            log.warn("execute : cannot execute for locator '" + locator.getName() + "' - parent record is missing ID");
            return;
        }

        List<Record> records = locator.execute(baseRecord);

        append(records, baseRecord);

        // do sub-appender processing

        try {
            Object obj = baseRecord.getField(getLocatorName());

            if (obj instanceof Collection) {
                for (Object o : (Collection) obj) {
                    if (o instanceof Record) {
                        executeSubAppenders((Record) o);
                    }
                }

            } else {
                if (obj instanceof Record) {
                    executeSubAppenders((Record) obj);
                }
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " processing sub-appenders for " + baseRecord + " - " + e.getMessage(), e);
            throw new AppenderException(e);
        }
    }

    private void executeSubAppenders(Record r) throws AppenderException, LocatorException {
        for (RecordAppender subAppender : subAppenders) {
            subAppender.execute(r);
        }
    }

    private String getLocatorName() {
        return locator.getName();
    }

    private void append(List<Record> records, Record baseRecord) throws LocatorException, AppenderException {
        if (records.size() > 0) {
            Object o;

            switch (locator.getType()) {
                case ONE :
                    o = records.get(0);
                    break;

                case CONSOLIDATED :
                    if (locator.hasConsolidateBy()) o = consolidate(records, locator.getConsolidateBy());
                    else                            o = consolidate(records);
                    break;

                case MANY:
                default:
                    o = records;
                    break;
            }

            if (locator.isCollapseToParent()) {
                // combine this record with its parent
                // NOTE : ONLY SINGLE RECORDS CAN BE COLLAPSED INTO THEIR PARENT!

                if (o instanceof Record) {
                    try {
                        baseRecord.consume((Record) o);

                    } catch (Exception e) {
                        throw new AppenderException(e.getMessage(), e);
                    }
                }

            } else {
                baseRecord.setField(getLocatorName(), o);
            }
        }
    }

    private List<Record> consolidate(List<Record> records, List<String> consolidateBy) throws LocatorException {
        Map<String, List<Record>> map = new LinkedHashMap<String, List<Record>>();

        for (Record r : records) {
            List<String> keyParts = new ArrayList<String>();
            for (String field : consolidateBy) {
                if ( ! r.hasField(field) ) {

                    // should never get here - there is validation in Config where the associated locator is defined
                    // that ensures fields specified in the consolidateBy clause actually exist

                    throw new LocatorException("locator '" + locator.getName() +
                            "' cannot consolidate by missing field '" + field + "'");
                }

                keyParts.add(r.getField(field).toString());
            }
            String key = StringUtils.join(keyParts, "|");
            if ( ! map.containsKey(key) ) map.put(key, new ArrayList<Record>());
            map.get(key).add(r);
        }

        List<Record> list = new ArrayList<Record>();
        for (Map.Entry<String, List<Record>> entry : map.entrySet()) {
            Record cr;

            if (entry.getValue().size() == 1) {
                cr = entry.getValue().get(0);

            } else {
                Record r = entry.getValue().get(0);
                cr = consolidate(entry.getValue());

                for (String field : consolidateBy) {        // we don't want to consolidate the common fields
                    cr.setField(field, r.getField(field));
                }
            }

            list.add(cr);
        }

        return list;
    }

    private Record consolidate(List<Record> records) {
        if (records.isEmpty()) return null;

        // presume all records are of like-type

        if (records.size() == 1) {
            return records.get(0);

        } else {
            Record template = records.get(0);

            Record cr = new Record(template.getParent());
            cr.setTransientFields(template.getTransientFields());

            for (String fieldName : template.getFieldNames()) {
                List<String> list = new ArrayList<String>();
                for (Record r : records) {
                    Object o = r.getField(fieldName);
                    if (o != null) list.add(o.toString());
                }
                cr.setField(fieldName, StringUtils.join(list, locator.getConsolidateDelim()));
            }

            return cr;
        }
    }
}
