/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import com.foundationdb.sql.StandardException;
import edu.uvm.ccts.ardencat.rb.appenders.RecordAppender;
import edu.uvm.ccts.ardencat.rb.locators.BaseRecordLocator;
import edu.uvm.ccts.ardencat.rb.locators.DerivedField;
import edu.uvm.ccts.ardencat.rb.locators.SupplementalRecordLocator;
import edu.uvm.ccts.ardencat.rb.locators.VersionIndex;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.sql.SqlStatement;
import edu.uvm.ccts.common.sql.model.Field;
import electric.xml.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.List;

/**
 * Created by mstorer on 7/24/13.
 */
public abstract class AbstractRecordBuilderConfig<EC extends EligibilityConfig> implements Serializable {
    private String filename;
    private DataSource ardencatDataSource;
    private Map<String, DataSource> locatorDataSources;
    private EC eligibilityConfig;
    private BaseRecordLocator baseLocator;
    private Map<String, SupplementalRecordLocator> supplementalLocators;
    private Collection<RecordAppender> appenders;

    protected transient Frame frame;

    public AbstractRecordBuilderConfig(File file, Frame frame) throws ConfigurationException {
        try {
            this.filename = file.getCanonicalPath();
            this.frame = frame;
            populateFromXml(file);

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    public AbstractRecordBuilderConfig(String filename, Frame frame, DataSource ardencatDataSource, Element root) throws ConfigurationException {
        this.filename = filename;
        this.frame = frame;
        this.ardencatDataSource = ardencatDataSource;
        populateFromXml(root);
    }


///////////////////////////////////////////////////////////////////////////////////
// public methods
//

    public String getFilename() {
        return filename;
    }

    public DataSource getArdencatDataSource() {
        return ardencatDataSource;
    }

    public EC getEligibilityConfig() {
        return eligibilityConfig;
    }

    public BaseRecordLocator getBaseLocator() {
        return baseLocator;
    }

    public Collection<RecordAppender> getAppenders() {
        return appenders;
    }

    public void clearSupplementalLocatorCaches() {
        for (Map.Entry<String, SupplementalRecordLocator> entry : supplementalLocators.entrySet()) {
            entry.getValue().clearCache();
        }
    }

    public void closeAllDataSources() {
        ardencatDataSource.close();
        for (DataSource ds : locatorDataSources.values()) {
            ds.close();
        }
    }


///////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void populateFromXml(File file) throws ConfigurationException {
        try {
            Document doc = new Document(file);
            Element root = doc.getElement(new XPath("recordBuilder"));

            buildArdencatDataSource(root);
            populateFromXml(root);

        } catch (ParseException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    protected abstract void buildOtherConfigs(Element root) throws ConfigurationException;

    private void populateFromXml(Element root) throws ConfigurationException {
        try {
            buildDataSources(root);
            buildEligibilityConfig(root);
            buildLocators(root);
            buildAppenders(root);
            buildOtherConfigs(root);        // may be implemented by subclass to add additional config options

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    private void buildArdencatDataSource(Element root) throws ConfigurationException {
        Element xmlSystemDataSource = root.getElement(new XPath("ardencatDataSource"));
        ardencatDataSource = DataSource.buildFromXml("ardencat", xmlSystemDataSource, frame);
    }

    private void buildDataSources(Element root) throws ConfigurationException, IOException {
        locatorDataSources = new HashMap<String, DataSource>();
        Elements xmlDataSources = root.getElements(new XPath("locatorDataSources/dataSource"));
        while (xmlDataSources.hasMoreElements()) {
            Element xmlDataSource = xmlDataSources.next();
            DataSource ds = DataSource.buildFromXml(xmlDataSource, frame);

            String key = ds.getName().toLowerCase();
            if (locatorDataSources.containsKey(key)) {
                throw new ConfigurationException("Locator data source IDs must be unique - found multiple definitions for '" +
                        ds.getName() + "'");
            }
            locatorDataSources.put(key, ds);
        }
    }

    protected abstract EC createEligibilityConfig(String source);
    protected abstract void buildOtherEligibilityConfigs(EC eligibilityConfig, Element xmlEligibility) throws IOException;

    private void buildEligibilityConfig(Element root) throws IOException {
        Element xmlEligibility = root.getElement(new XPath("eligibility"));
        eligibilityConfig = createEligibilityConfig(xmlEligibility.getAttribute("source"));

        List<String> keyFields = new ArrayList<String>();
        String key = xmlEligibility.getAttribute("key").trim();
        for (String field : key.split("\\s*,\\s*")) {
            keyFields.add(field);
        }
        eligibilityConfig.setKeyFields(keyFields);

        if (xmlEligibility.hasAttribute("hiddenFields")) {
            String hiddenFields = xmlEligibility.getAttribute("hiddenFields").trim();
            for (String field : hiddenFields.split("\\s*,\\s*")) {
                eligibilityConfig.markFieldHidden(field);
            }
        }

        buildOtherEligibilityConfigs(eligibilityConfig, xmlEligibility);
    }

    private void buildLocators(Element root) throws IOException, ConfigurationException {
        Element xmlBaseLocator = root.getElement(new XPath("locators/baseLocator"));
        baseLocator = buildBaseLocator(xmlBaseLocator);

        supplementalLocators = new HashMap<String, SupplementalRecordLocator>();
        Elements xmlLocators = root.getElements(new XPath("locators/supplementalLocators/locator"));
        while (xmlLocators.hasMoreElements()) {
            SupplementalRecordLocator locator = buildSupplementalLocator(xmlLocators.next());
            supplementalLocators.put(locator.getName(), locator);
        }
    }

    private void buildAppenders(Element root) throws ConfigurationException {
        appenders = new ArrayList<RecordAppender>();
        Elements xmlAppenders = root.getElements(new XPath("appenders/appender"));

        while (xmlAppenders.hasMoreElements()) {
            appenders.add(buildAppender(xmlAppenders.next()));
        }
    }

    private BaseRecordLocator buildBaseLocator(Element xmlBaseLocator) throws ConfigurationException, IOException {
        String query = xmlBaseLocator.getElement("query").getTrimTextString();

        String dsName = xmlBaseLocator.getAttribute("dataSource");
        DataSource ds = locatorDataSources.get(dsName.toLowerCase());
        if (ds == null) throw new ConfigurationException("baseLocator references invalid dataSource '" + dsName + "'");

        BaseRecordLocator locator = new BaseRecordLocator(ds, query);

        if (xmlBaseLocator.hasAttribute("transientFields")) {
            String s = xmlBaseLocator.getAttribute("transientFields").trim();
            for (String fieldName : s.split("\\s*,\\s*")) {
                locator.flagAsTransient(fieldName);
            }
        }

        if (xmlBaseLocator.hasElement("derivedFields")) {
            Collection<DerivedField> derivedFields = new ArrayList<DerivedField>();

            Elements xmlDerivedFields = xmlBaseLocator.getElements(new XPath("derivedFields/field"));
            while (xmlDerivedFields.hasMoreElements()) {
                derivedFields.add(buildDerivedField(xmlDerivedFields.next()));
            }

            locator.setDerivedFields(derivedFields);
        }

        return locator;
    }

    private SupplementalRecordLocator buildSupplementalLocator(Element xmlLocator) throws ConfigurationException, IOException {
        String name = xmlLocator.getAttribute("name");
        SupplementalRecordLocator.Type type = SupplementalRecordLocator.Type.valueOf(xmlLocator.getAttribute("type").toUpperCase());

        String query = xmlLocator.getElement("query").getTrimTextString();

        String dsName = xmlLocator.getAttribute("dataSource");
        DataSource ds = locatorDataSources.get(dsName.toLowerCase());
        if (ds == null) throw new ConfigurationException("baseLocator references invalid dataSource '" + dsName + "'");

        SupplementalRecordLocator locator = new SupplementalRecordLocator(name, type, ds, query);

        if (xmlLocator.hasAttribute("transientFields")) {
            String s = xmlLocator.getAttribute("transientFields").trim();
            for (String fieldName : s.split("\\s*,\\s*")) {
                locator.flagAsTransient(fieldName);
            }
        }

        List<String> fieldNames = new ArrayList<String>();

        if (xmlLocator.hasElement("derivedFields")) {
            Collection<DerivedField> derivedFields = new ArrayList<DerivedField>();

            Elements xmlDerivedFields = xmlLocator.getElements(new XPath("derivedFields/field"));
            while (xmlDerivedFields.hasMoreElements()) {
                DerivedField derivedField = buildDerivedField(xmlDerivedFields.next());
                derivedFields.add(derivedField);

                fieldNames.add(derivedField.getName());
            }

            locator.setDerivedFields(derivedFields);
        }

        if (type == SupplementalRecordLocator.Type.CONSOLIDATED) {
            if (xmlLocator.hasAttribute("consolidateDelim")) {
                locator.setConsolidateDelim(xmlLocator.getAttribute("consolidateDelim"));
            }

            if (xmlLocator.hasAttribute("consolidateBy")) {
                String s = xmlLocator.getAttribute("consolidateBy").trim();
                List<String> consolidateByFields = Arrays.asList(s.split("\\s*,\\s*"));

                // validate that consolidateBy fields are actually selected
                try {
                    String testQuery = query.replaceAll(SupplementalRecordLocator.ID_TOKEN_REGEX, "1");
                    SqlStatement sqlStatement = new SqlStatement(testQuery);

                    for (Field field : sqlStatement.getSelectedFields()) {
                        fieldNames.add(field.getAlias());
                    }

                    for (String fieldName : consolidateByFields) {
                        if ( ! fieldNames.contains(fieldName) ) {
                            throw new ConfigurationException("invalid consolidateBy field '" + fieldName +
                                    "' for locator '" + name + "'");
                        }
                    }

                } catch (StandardException e) {
                    throw new ConfigurationException("cannot parse query for locator '" + name + "'", e);
                }

                if ( ! s.isEmpty()) locator.setConsolidateBy(consolidateByFields);
            }
        }

        if (xmlLocator.hasAttribute("versionField")) {
            locator.setVersionField(xmlLocator.getAttribute("versionField"));

            if (xmlLocator.hasAttribute("versionControlFields")) {
                String s = xmlLocator.getAttribute("versionControlFields").trim();
                locator.setVersionControlFields(Arrays.asList(s.split("\\s*,\\s*")));
            }

            if (xmlLocator.hasAttribute("versionIndex")) {
                String s = xmlLocator.getAttribute("versionIndex").trim();

                try {
                    locator.setVersionIndex(new VersionIndex(s));

                } catch (NumberFormatException e) {
                    throw new ConfigurationException("invalid version index for locator '" + name + "' : " + s);
                }
            }
        }

        if (xmlLocator.hasAttribute("collapseToParent")) {
            boolean collapseToParent = xmlLocator.getAttribute("collapseToParent").equalsIgnoreCase("true");
            switch (type) {
                case ONE:
                case CONSOLIDATED:
                    locator.setCollapseToParent(collapseToParent);
                    break;

                default:
                    if (collapseToParent) {
                        throw new ConfigurationException("cannot collapse locator '" + name + "' with type=" + type);
                    }
            }
        }

        return locator;
    }

    private DerivedField buildDerivedField(Element xmlField) throws IOException {
        String name = xmlField.getAttribute("name");
        String arden = xmlField.getTrimTextString();
        return new DerivedField(name, arden);
    }

    private RecordAppender buildAppender(Element xmlAppender) throws ConfigurationException {
        String locatorName = xmlAppender.getAttribute("locator");
        SupplementalRecordLocator locator = supplementalLocators.get(locatorName);
        if (locator == null) throw new ConfigurationException("invalid locator : " + locatorName);

        RecordAppender appender = new RecordAppender(locator);

        Elements xmlSubAppenders = xmlAppender.getElements(new XPath("appenders/appender"));
        if (xmlSubAppenders.size() > 0) {
            while (xmlSubAppenders.hasMoreElements()) {
                appender.addSubAppender(buildAppender(xmlSubAppenders.next()));
            }
        }

        return appender;
    }
}
