/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.common.util.ColorUtil;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by mstorer on 10/15/14.
 */
public class RecordTable extends JTable {
    private static final Color EVEN_ROW_COLOR = new Color(229, 243, 255);
    private static final Color ODD_ROW_COLOR = new Color(255, 255, 255);

    private static final Color NON_SELECTED_ROW_FG_COLOR = Color.BLACK;
    private static final Color SELECTED_ROW_BG_COLOR = new Color(0, 53, 115);
    private static final Color SELECTED_ROW_FG_COLOR = Color.WHITE;

    private static final Color SEARCH_MATCH_COLOR = new Color(205, 255, 66);

    public RecordTable() {
        setAutoCreateRowSorter(true);
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        Point p = event.getPoint();
        return String.valueOf(getModel().getValueAt(
                convertRowIndexToModel(rowAtPoint(p)),
                convertColumnIndexToModel(columnAtPoint(p))));
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);

        boolean matchesSearch = (((RecordTableModel) getModel()).matchesSearch(convertRowIndexToModel(row)));

        if (row == getSelectedRow()) {
            c.setForeground(matchesSearch ? SEARCH_MATCH_COLOR : SELECTED_ROW_FG_COLOR);
            c.setBackground(SELECTED_ROW_BG_COLOR);

        } else {
            c.setForeground(NON_SELECTED_ROW_FG_COLOR);

            if (matchesSearch) {
                c.setBackground(row % 2 == 0 ?
                        ColorUtil.darken(SEARCH_MATCH_COLOR, 20) :
                        SEARCH_MATCH_COLOR);

            } else if (row % 2 == 0) {
                c.setBackground(EVEN_ROW_COLOR);

            } else {
                c.setBackground(ODD_ROW_COLOR);
            }
        }

        return c;
    }
}
