/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.renderers.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.renderers.Renderer;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

public class XmlRenderer implements Renderer {
    @Override
    public void render(List<Record> records, boolean identify, OutputStream outputStream) throws Exception {
        XStream xstream = new XStream(new StaxDriver());
        xstream.registerConverter(new XmlRecordConverter(identify));
        xstream.autodetectAnnotations(true);

        BufferedOutputStream out = new BufferedOutputStream(outputStream);
        PrettyPrintWriter writer = new PrettyPrintWriter(new OutputStreamWriter(out));

        writer.startNode("recordList");
        for (Record r : records) {
            xstream.marshal(r, writer);
        }
        writer.endNode();
    }
}
