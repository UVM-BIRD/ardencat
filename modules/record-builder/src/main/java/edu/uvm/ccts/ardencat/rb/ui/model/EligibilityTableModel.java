/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.eligibility.model.EligibilityColor;
import edu.uvm.ccts.ardencat.eligibility.model.EligibilityData;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.ardencat.rb.model.*;
import edu.uvm.ccts.ardencat.rb.ui.AuditForm;
import edu.uvm.ccts.ardencat.rb.util.EligibilityAuditUtil;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.util.ResourceUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 1/30/14.
 */
public class EligibilityTableModel extends AbstractTableModel {
    private static final Log log = LogFactory.getLog(EligibilityTableModel.class);

    public static final int ICONS_COL = 0;
    private static final int STATUS_COL = 1;
    private static final int METADATA_COL_COUNT = 2;

    private static ImageIcon MODIFIED_ICON = null;
    private static ImageIcon AUDITS_ICON = null;
    static {
        try {
            MODIFIED_ICON = new ImageIcon(ImageIO.read(ResourceUtil.getResourceAsStream("/modified.png")));
            AUDITS_ICON = new ImageIcon(ImageIO.read(ResourceUtil.getResourceAsStream("/audits.png")));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " building icons - " + e.getMessage(), e);
        }
    }

    private List<Integer> displayRowIndexList = new ArrayList<Integer>();
    private Map<EligibilityColor, Boolean> visibilityMap = new HashMap<EligibilityColor, Boolean>();
    private List<ColumnInfo> colInfoList = new ArrayList<ColumnInfo>();
    private List<EligibilityRecord> list;
    private StandardRecordBuilderConfig config;
    private UIConfig uiConfig;
    private JButton revertButton;

    private Map<Integer, EligibilityColor> colorCache = new HashMap<Integer, EligibilityColor>();

    public EligibilityTableModel(List<EligibilityRecord> list, StandardRecordBuilderConfig config, UIConfig uiConfig,
                                 JButton revertButton) {
        this.list = list;
        this.config = config;
        this.uiConfig = uiConfig;
        this.revertButton = revertButton;

        setup();
    }

    public EligibilityRecord getRecordAtRow(int row) {
        return list.get(displayRowIndexList.get(row));
    }

    public EligibilityColor getColorForRecordAtRow(int row) {
        int listIndex = displayRowIndexList.get(row);
        return getColorForRecordAtListIndex(listIndex);
    }

    public List<EligibilityRecord> getList() {
        return list;
    }

    public void setList(List<EligibilityRecord> list) {
        this.list = list;
        setup();
        fireTableDataChanged();
    }

    public void setVisibility(EligibilityColor color, boolean visible) {
        visibilityMap.put(color, visible);
        updateDisplayRowIndexes();
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == STATUS_COL;
    }

    @Override
    public String getColumnName(int column) {
        if (list.isEmpty()) {
            return super.getColumnName(column);

        } else if (column == ICONS_COL) {
            return "";

        } else if (column == STATUS_COL) {
            return "Status";

        } else {
            return colInfoList.get(column - METADATA_COL_COUNT).getColumnName();
        }
    }

    @Override
    public int getRowCount() {
        return displayRowIndexList.size();
    }

    @Override
    public int getColumnCount() {
        return list.isEmpty() ?
                0 :
                colInfoList.size() + METADATA_COL_COUNT;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (list.isEmpty()) {
            return super.getColumnClass(columnIndex);

        } else if (columnIndex == ICONS_COL) {
            return Icon.class;

        } else if (columnIndex == STATUS_COL) {
            return list.get(0).getStatus().getClass();

        } else {
            return colInfoList.get(columnIndex - METADATA_COL_COUNT).getColumnClass();
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        int listIndex = displayRowIndexList.get(rowIndex);

        if (list.isEmpty()) {
            return null;

        } else if (columnIndex == ICONS_COL) {
            EligibilityRecord rec = list.get(listIndex);
            if (rec.hasOverriddenStatus())  return MODIFIED_ICON;
            else if (rec.hasAudits())       return AUDITS_ICON;
            else                            return null;

        } else if (columnIndex == STATUS_COL) {
            return list.get(listIndex).getStatus();

        } else {
            int dataIndex = colInfoList.get(columnIndex - METADATA_COL_COUNT).getIndex();
            return list.get(listIndex).getData().getByIndex(dataIndex);
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        int listIndex = displayRowIndexList.get(rowIndex);

        if (columnIndex == ICONS_COL) {
            // do nothing

        } else if (columnIndex == STATUS_COL) {
            if (aValue instanceof EligibilityStatus) {
                EligibilityStatus status = (EligibilityStatus) aValue;
                EligibilityRecord rec = list.get(listIndex);

                boolean save = false;
                if (status == rec.getOriginalStatus() && rec.getOverrideStatus() != null) {
                    status = null;
                    save = true;

                } else if (status != rec.getOriginalStatus() && status != rec.getOverrideStatus()) {
                    save = true;
                }

                try {
                    if (save && "true".equalsIgnoreCase(uiConfig.getProperty("curate.auditChanges"))) {
                        AuditForm af = new AuditForm(uiConfig.getFrame(), "Eligibility Status Override Audit");
                        af.setVisible(true);

                        if (af.isCancelled()) {
                            save = false;

                        } else {
                            EligibilityStatus newStatus = status == null ? rec.getOriginalStatus() : status;
                            DataSource ds = config.getArdencatDataSource();

                            int id = EligibilityAuditUtil.saveAuditRecord(ds, rec.getId(), rec.getStatus(), newStatus,
                                    af.getSignature(), af.getReason());

                            if (rec.getAuditHistory() == null) {
                                rec.setAuditHistory(new ArrayList<EligibilityAudit>());
                            }

                            rec.getAuditHistory().add(EligibilityAuditUtil.getAuditRecordById(ds, id));
                        }
                    }

                    if (save) {
                        rec.setOverrideStatus(status);
                        saveOverride(rec.getId(), status);
                        enableDisableRevertButton();
                        enableDisableNextButton();

                        colorCache.remove(listIndex);

                        fireTableDataChanged();
                    }

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " updating status - " + e.getMessage(), e);
                }

            } else {
                log.warn("invalid filter status for row=" + listIndex + ", col=" + columnIndex + " : " +
                        aValue.getClass().getName() + "{" + aValue.toString() + "}");
            }

        } else {
            int dataIndex = colInfoList.get(columnIndex - METADATA_COL_COUNT).getIndex();
            list.get(listIndex).getData().putByIndex(dataIndex, aValue);
        }
    }


///////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void setup() {
        EligibilityUIConfig eCfg = config.getEligibilityConfig();
        EligibilityData data = list.get(0).getData();

        for (int i = 0; i < data.size(); i ++) {
            String field = data.getKeyAtIndex(i);

            if ( ! eCfg.isFieldHidden(field) ) {
                Object obj = data.getByIndex(i);

                if (obj == null) {
                    for (EligibilityRecord rec : list) {
                        obj = rec.getData().getByIndex(i);
                        if (obj != null) break;
                    }
                }

                Class c = obj != null ? obj.getClass() : String.class;

                String label = eCfg.getFieldLabel(field);
                if (label == null) label = field.toUpperCase();

                colInfoList.add(new ColumnInfo(i, label, c));
            }
        }

        colorCache.clear();

        updateDisplayRowIndexes();

        enableDisableRevertButton();
        enableDisableNextButton();
    }

    private void updateDisplayRowIndexes() {
        displayRowIndexList.clear();

        EligibilityConfig eCfg = config.getEligibilityConfig();

        boolean showEligible = isColorVisible(eCfg.getEligibleColor());
        boolean showUnknown = isColorVisible(eCfg.getUnknownColor());
        boolean showIneligible = isColorVisible(eCfg.getIneligibleColor());

        for (int i = 0; i < list.size(); i ++) {
            EligibilityStatus status = list.get(i).getStatus();
            boolean showByStatus = (status == EligibilityStatus.ELIGIBLE && showEligible) ||
                    (status == EligibilityStatus.UNKNOWN && showUnknown) ||
                    (status == EligibilityStatus.INELIGIBLE && showIneligible);

            EligibilityColor color = getColorForRecordAtListIndex(i);

            if (showByStatus && isColorVisible(color)) {
                displayRowIndexList.add(i);
            }
        }
    }

    private boolean isColorVisible(EligibilityColor color) {
        Boolean show = visibilityMap.get(color);
        return show == null || show;
    }

    private EligibilityColor getColorForRecordAtListIndex(int listIndex) {
        EligibilityColor color = colorCache.get(listIndex);
        if (color == null) {
            EligibilityUIConfig eCfg = config.getEligibilityConfig();

            EligibilityRecord rec = list.get(listIndex);

            List<CustomEligibilityColor> colors = eCfg.getCustomColors();
            if (colors.size() > 0) {
                for (CustomEligibilityColor c : colors) {
                    if (c.matches(rec)) {
                        color = c;
                        break;
                    }
                }
            }

            if (color == null) {
                switch (rec.getStatus()) {
                    case ELIGIBLE:
                        color = eCfg.getEligibleColor();
                        break;
                    case UNKNOWN:
                        color = eCfg.getUnknownColor();
                        break;
                    case INELIGIBLE:
                        color = eCfg.getIneligibleColor();
                        break;
                }
            }

            colorCache.put(listIndex, color);
        }

        return color;
    }

    private void enableDisableRevertButton() {
        boolean enable = false;
        for (EligibilityRecord rec : list) {
            if (rec.getOverrideStatus() != null) {
                enable = true;
                break;
            }
        }
        revertButton.setEnabled(enable);
    }

    private void enableDisableNextButton() {
        boolean enable = false;
        for (EligibilityRecord rec : list) {
            if (rec.getStatus() == EligibilityStatus.ELIGIBLE) {
                enable = true;
                break;
            }
        }
        uiConfig.getNextButton().setEnabled(enable);
    }

    private void saveOverride(int id, EligibilityStatus status) {
        try {
            Connection conn = config.getArdencatDataSource().getConnection();

            String query = "update tblACEligibility set overrideStatus = ? where id = ?";
            PreparedStatement stmt = conn.prepareStatement(query);

            stmt.setString(1, convertToChar(status));
            stmt.setInt(2, id);

            stmt.executeUpdate();

            log.info("saveOverride : saved override status=" + status + " for eligibility record with id=" + id);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " setting status override {id=" + id + ", status=" +
                    status + "} - " + e.getMessage(), e);
        }
    }

    private String convertToChar(EligibilityStatus status) {
        return status != null ?
                status.getStringVal() :
                null;
    }

    private static final class ColumnInfo {
        private int index;
        private String name;
        private Class klass;

        private ColumnInfo(int index, String name, Class klass) {
            this.index = index;
            this.name = name;
            this.klass = klass;
        }

        private int getIndex() {
            return index;
        }

        private String getColumnName() {
            return name;
        }

        private Class getColumnClass() {
            return klass;
        }
    }
}
