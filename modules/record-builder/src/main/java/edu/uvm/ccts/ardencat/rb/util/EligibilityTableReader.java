/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.eligibility.util.BasicEligibilityTableReader;
import edu.uvm.ccts.ardencat.rb.model.EligibilityRecord;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 7/28/14.
 */
public class EligibilityTableReader {
    @SuppressWarnings("unchecked")
    public static List<EligibilityRecord> buildList(DataSource dataSource, String source,
                                                    List<String> keyFields, List<String> sortByFields,
                                                    Object firstBatch, Object lastBatch)
            throws SQLException, ValidationException, DataException {

        List<BasicEligibilityRecord> records = BasicEligibilityTableReader.buildList(dataSource, source, keyFields,
                sortByFields, firstBatch, lastBatch);

        return appendAuditInfo(dataSource, records);
    }

    public static List<EligibilityRecord> getByIdList(DataSource dataSource, List<Integer> idList) throws SQLException, DataException {
        List<BasicEligibilityRecord> records = BasicEligibilityTableReader.buildList(dataSource, idList);

        return appendAuditInfo(dataSource, records);
    }

    private static List<EligibilityRecord> appendAuditInfo(DataSource dataSource, List<BasicEligibilityRecord> records) throws SQLException {
        List<EligibilityRecord> list = new ArrayList<EligibilityRecord>();
        for (BasicEligibilityRecord basicRec : records) {
            EligibilityRecord rec = new EligibilityRecord(basicRec);
            rec.setAuditHistory(EligibilityAuditUtil.getAuditRecordListByEligId(dataSource, rec.getId()));
            list.add(rec);
        }
        return list;
    }
}
