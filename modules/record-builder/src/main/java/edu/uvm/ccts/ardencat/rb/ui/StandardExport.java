/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Jan 28 11:58:39 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.model.ExportInfo;
import edu.uvm.ccts.ardencat.rb.model.ExportMethod;
import edu.uvm.ccts.ardencat.rb.model.StandardRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.renderers.Renderer;
import edu.uvm.ccts.ardencat.rb.renderers.csv.CsvRenderer;
import edu.uvm.ccts.ardencat.rb.renderers.xml.XmlRenderer;
import edu.uvm.ccts.ardencat.rb.ui.model.UIConfig;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Matt Storer
 */
public class StandardExport extends AbstractExportPanel {
    private static final Log log = LogFactory.getLog(StandardExport.class);

    private ExportInfo exportInfo = null;

    public StandardExport(UIConfig uiConfig, StandardRecordBuilderConfig config) {
        super(uiConfig, config);
        initComponents();

        setNavLabels("Back", "Finish");
    }

    @Override
    public void load(JPanel contentPane, ExportInfo exportInfo) throws ValidationException, SQLException, DataException {
        this.exportInfo = exportInfo;
        txtFilename.setText(null);
        btnExport.setEnabled(false);

        if (ExportMethod.CSV.equals(exportInfo.getMethod())) {
            rbCsv.setEnabled(true);
            rbCsv.setSelected(true);
            rbXml.setEnabled(false);

        } else if (ExportMethod.XML.equals(exportInfo.getMethod())) {
            rbXml.setEnabled(true);
            rbXml.setSelected(true);
            rbCsv.setEnabled(false);
        }

        List<Record> list = buildExportedRecordList(exportInfo.getId());

        load(contentPane, list, exportInfo.getFirstBatch(), exportInfo.getLastBatch());
    }

    @Override
    public UIState getUIState() {
        UIState state = super.getUIState();

        String filename = txtFilename.getText();
        if (filename != null && ! filename.trim().equals("")) {
            state.setProperty("filename", txtFilename.getText());
        }

        return state;
    }

    @Override
    public void restoreUIState(UIState state) throws Exception {
        super.restoreUIState(state);

        String filename = state.getString("filename");
        if (filename != null && ! filename.trim().equals("")) {
            txtFilename.setText(filename);
            btnExport.setEnabled(true);

        } else {
            btnExport.setEnabled(false);
        }
    }

    @Override
    protected boolean mayProceed() {
        boolean outputFormatSelected = rbXml.isSelected() || rbCsv.isSelected();
        return outputFormatSelected && ! txtFilename.getText().isEmpty();
    }


////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private Renderer getOutputRenderer() {
        if      (rbXml.isSelected())    return new XmlRenderer();
        else if (rbCsv.isSelected())    return new CsvRenderer();
        else                            return null;
    }

    private String getOutputFilename() {
        return txtFilename.getText();
    }

    private void btnSelectFileActionPerformed() {
        FileFilter filter = null;
        String fileType = null;

        if (rbXml.isSelected()) {
            filter = new FileNameExtensionFilter("XML Files (*.xml)", "xml");
            fileType = "xml";

        } else if (rbCsv.isSelected()) {
            filter = new FileNameExtensionFilter("CSV Files (*.csv)", "csv");
            fileType = "csv";
        }

        if (filter == null) {
            DialogUtil.showErrorDialog(this, "You must select an output format.");

        } else {
            String defaultFilename = getOutputFilename().isEmpty() ?
                    buildDefaultFilename(firstBatch, lastBatch, fileType) :
                    getOutputFilename();

            String savePath = uiConfig.getUserProperty("savePath");
            File file = FileUtil.selectFileToSave(this, filter, savePath, defaultFilename);
            if (file != null) {
                uiConfig.setUserProperty("savePath", FileUtil.getPathPart(file.getPath()));

                try {
                    txtFilename.setText(file.getCanonicalPath());
                    uiConfig.getNextButton().setEnabled(true);
                    btnExport.setEnabled(true);

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " selecting output file - " + e.getMessage(), e);
                }
            }
        }
    }

    private String getExportMethod() {
        if      (rbXml.isSelected())    return ExportMethod.XML;
        else if (rbCsv.isSelected())    return ExportMethod.CSV;
        else                            return null;
    }

    private void btnExportActionPerformed() {
        if ( ! shouldExport() ) return;

        Renderer renderer = getOutputRenderer();
        if (renderer == null) {
            // should never get here, but just in case -
            DialogUtil.showInformationDialog(this, "Select Export Method", "Please select an export method and try again.");
            return;
        }

        boolean audit = "true".equalsIgnoreCase(uiConfig.getProperty("export.auditChanges"));
        String authSig = null;
        String reason = null;

        if (audit) {
            AuditForm af = new AuditForm(uiConfig.getFrame(), "Export Audit");
            af.setVisible(true);

            if (af.isCancelled()) {
                return;

            } else {
                authSig = af.getSignature();
                reason = af.getReason();
            }
        }

        if (exportInfo == null) btnExport.setEnabled(false);
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        try {
            renderer.render(records, cbIdentify.isSelected(), new FileOutputStream(getOutputFilename()));

            int exportId = exportInfo != null ?
                    exportInfo.getId() :
                    writeExportRecords(firstBatch, lastBatch, getExportMethod(), records);

            if (exportInfo == null) markBatchRangeExported(firstBatch, lastBatch, exportId);

            if (audit) auditExport(exportId, authSig, reason);

            if (exportInfo == null) {
                rbXml.setEnabled(false);
                rbCsv.setEnabled(false);
                cbIdentify.setEnabled(false);
                btnSelectFile.setEnabled(false);
                uiConfig.getBackButton().setEnabled(false);
            }

            this.setCursor(Cursor.getDefaultCursor());
            DialogUtil.showInformationDialog(this, "Export Successful",
                    "Records written successfully to " + getOutputFilename() + ".");

        } catch (Exception e) {
            this.setCursor(Cursor.getDefaultCursor());
            log.error("caught " + e.getClass().getName() + " exporting records - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());

            if (exportInfo == null) btnExport.setEnabled(true);

        } finally {
            generateStatisticsReport();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        label1 = new JLabel();
        label2 = new JLabel();
        rbXml = new JRadioButton();
        rbCsv = new JRadioButton();
        label3 = new JLabel();
        txtFilename = new JTextField();
        btnSelectFile = new JButton();
        cbIdentify = new JCheckBox();
        label4 = new JLabel();
        btnExport = new JButton();
        label5 = new JLabel();

        //======== this ========

        //---- label1 ----
        label1.setText("Use this screen to configure export options and export records.");

        //---- label2 ----
        label2.setText("Export format:");

        //---- rbXml ----
        rbXml.setText("XML");
        rbXml.setSelected(true);

        //---- rbCsv ----
        rbCsv.setText("CSV");

        //---- label3 ----
        label3.setText("Output filename:");

        //---- txtFilename ----
        txtFilename.setEditable(false);

        //---- btnSelectFile ----
        btnSelectFile.setText("Select File...");
        btnSelectFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSelectFileActionPerformed();
            }
        });

        //---- label4 ----
        label4.setText("Identify records?");

        //---- btnExport ----
        btnExport.setText("Export");
        btnExport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnExportActionPerformed();
            }
        });

        //---- label5 ----
        label5.setText("Record Export");
        label5.setFont(new Font("Lucida Grande", Font.BOLD, 20));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(label3)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFilename)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSelectFile, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(label5)
                                        .addComponent(label1)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(label4)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cbIdentify))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(label2)
                                                .addGap(24, 24, 24)
                                                .addComponent(rbXml)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(rbCsv)))
                                .addGap(0, 62, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 358, Short.MAX_VALUE)
                                .addComponent(btnExport)))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label5)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(label1)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label2)
                        .addComponent(rbXml)
                        .addComponent(rbCsv))
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(cbIdentify)
                        .addComponent(label4))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(btnSelectFile)
                        .addComponent(txtFilename, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label3))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                    .addComponent(btnExport)
                    .addContainerGap())
        );

        //---- bgFormat ----
        ButtonGroup bgFormat = new ButtonGroup();
        bgFormat.add(rbXml);
        bgFormat.add(rbCsv);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JLabel label1;
    private JLabel label2;
    private JRadioButton rbXml;
    private JRadioButton rbCsv;
    private JLabel label3;
    private JTextField txtFilename;
    private JButton btnSelectFile;
    private JCheckBox cbIdentify;
    private JLabel label4;
    private JButton btnExport;
    private JLabel label5;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
