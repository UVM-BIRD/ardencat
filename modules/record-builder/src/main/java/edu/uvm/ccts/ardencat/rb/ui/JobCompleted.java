/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Jan 28 12:05:11 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.ui.model.ContentPanel;
import edu.uvm.ccts.ardencat.rb.ui.model.UIConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Matt Storer
 */
public class JobCompleted extends ContentPanel {
    public JobCompleted(UIConfig uiConfig) {
        super(uiConfig);
        initComponents();

        setNavLabels("Back", "Process Another");
    }

    @Override
    protected boolean mayProceed() {
        return true;
    }

    private void btnQuitActionPerformed() {
        System.exit(0);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        label2 = new JLabel();
        label3 = new JLabel();
        panel2 = new JPanel();
        btnQuit = new JButton();
        label1 = new JLabel();

        //======== this ========

        //---- label2 ----
        label2.setText("What would you like to do?");
        label2.setFont(new Font("Lucida Grande", Font.BOLD, 14));

        //---- label3 ----
        label3.setText("<html>Click 'Process Another' to restart the workflow, or click 'Quit' to exit.</html>");

        //======== panel2 ========
        {
            panel2.setLayout(new FlowLayout());

            //---- btnQuit ----
            btnQuit.setText("Quit");
            btnQuit.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
            btnQuit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnQuitActionPerformed();
                }
            });
            panel2.add(btnQuit);
        }

        //---- label1 ----
        label1.setText("Job Completed");
        label1.setFont(new Font("Lucida Grande", Font.BOLD, 20));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(label2))
                            .addGap(0, 247, Short.MAX_VALUE))
                        .addComponent(label3, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                        .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1)
                    .addGap(18, 18, 18)
                    .addComponent(label2)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(51, Short.MAX_VALUE))
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JLabel label2;
    private JLabel label3;
    private JPanel panel2;
    private JButton btnQuit;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
