/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mstorer on 2/6/14.
 */
public class DateEditor extends AbstractCellEditor implements TableCellEditor {
    private static final Log log = LogFactory.getLog(DateEditor.class);

    private static final Border BLACK_BORDER = new LineBorder(Color.BLACK);
    private static final Border RED_BORDER = new LineBorder(Color.RED);

    private static final SimpleDateFormat sdf;
    static {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
    }

    private JFormattedTextField component;

    public DateEditor() throws ParseException {
        MaskFormatter mf = new MaskFormatter("####-##-##");
        mf.setPlaceholderCharacter('_');
        mf.setAllowsInvalid(false);
        mf.setCommitsOnValidEdit(true);
        component = new JFormattedTextField(mf);
    }

    @Override
    public Object getCellEditorValue() {
        try {
            return sdf.parse(String.valueOf(component.getValue()));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
            return null;
        }
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        String text = value != null ? sdf.format((Date) value) : "";
        component.setText(text);
        component.setBorder(BLACK_BORDER);
        return component;
    }

    @Override
    public boolean stopCellEditing() {
        try {
            sdf.parse(component.getText());

        } catch (ParseException e) {
            component.setBorder(RED_BORDER);
            return false;
        }

        return super.stopCellEditing();
    }
}
