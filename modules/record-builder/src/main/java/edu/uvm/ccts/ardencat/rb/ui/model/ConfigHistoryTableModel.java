/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 9/8/14.
 */
public class ConfigHistoryTableModel extends AbstractTableModel {
    private List<ConfigHistoryItem> list;

    public ConfigHistoryTableModel(List<ConfigHistoryItem> list) {
        this.list = list;
        fireTableDataChanged();
    }

    public int addItem(ConfigHistoryItem item) {
        String filename = item.getFilename();

        for (int i = 0; i < list.size(); i ++) {
            ConfigHistoryItem chi = list.get(i);
            if (filename.equalsIgnoreCase(chi.getFilename())) {
                return i;                                           // do not add : already added.
            }
        }

        list.add(0, item);
        fireTableDataChanged();
        return 0;
    }

    public ConfigHistoryItem getItem(int index) {
        return list.get(index);
    }

    public List<ConfigHistoryItem> getList() {
        return list;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:     return "Configuration";
            case 1:     return "Last Used";
            default:    return super.getColumnName(column);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:     return String.class;
            case 1:     return Date.class;
            default:    return super.getColumnClass(columnIndex);
        }
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ConfigHistoryItem item = list.get(rowIndex);
        switch (columnIndex) {
            case 0:     return item.getName();
            case 1:     return item.getLastUsed();
            default:    return null;
        }
    }
}
