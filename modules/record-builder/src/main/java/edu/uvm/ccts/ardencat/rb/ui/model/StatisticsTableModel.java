/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.rb.model.BatchStatistics;
import edu.uvm.ccts.common.model.DateTime;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by mstorer on 9/25/14.
 */
public class StatisticsTableModel extends AbstractTableModel {
    private List<BatchStatistics> list;

    public StatisticsTableModel(List<BatchStatistics> list) {
        this.list = list;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:  return "Batch";
            case 1:  return "# Eligible";
            case 2:  return "# Eligible Overridden";
            case 3:  return "# Unknown";
            case 4:  return "# Unknown Overridden";
            case 5:  return "# Ineligible";
            case 6:  return "# Ineligible Overridden";
            case 7:  return "Generated";
            case 8:  return "Exported";
            default: return super.getColumnName(column);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:  return String.class;
            case 1:  return Integer.class;
            case 2:  return Integer.class;
            case 3:  return Integer.class;
            case 4:  return Integer.class;
            case 5:  return Integer.class;
            case 6:  return Integer.class;
            case 7:  return DateTime.class;
            case 8:  return DateTime.class;
            default: return super.getColumnClass(columnIndex);
        }
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BatchStatistics stats = list.get(rowIndex);

        switch (columnIndex) {
            case 0:  return stats.getBatch();
            case 1:  return stats.getNumEligible();
            case 2:  return stats.getNumEligibleOverridden();
            case 3:  return stats.getNumUnknown();
            case 4:  return stats.getNumUnknownOverridden();
            case 5:  return stats.getNumIneligible();
            case 6:  return stats.getNumIneligibleOverridden();
            case 7:  return stats.getGeneratedDate();
            case 8:  return stats.getExportedDate();
            default: return null;
        }
    }
}
