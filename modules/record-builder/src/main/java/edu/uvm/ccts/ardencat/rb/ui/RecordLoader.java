/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Mon Feb 24 14:24:47 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.rb.appenders.RecordAppender;
import edu.uvm.ccts.ardencat.rb.model.EligibilityRecord;
import edu.uvm.ccts.ardencat.rb.model.StandardRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.exceptions.GUIThreadExceptionHandler;
import edu.uvm.ccts.common.util.DialogUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * @author Matthew Storer
 */
public class RecordLoader extends JDialog {
    private static final Log log = LogFactory.getLog(RecordLoader.class);

    private Thread workerThread = null;
    private List<Record> records = null;

    public RecordLoader(Frame owner, StandardRecordBuilderConfig config, List<EligibilityRecord> eligibilityRecords) {
        super(owner);
        initComponents();
        loadRecords(config, eligibilityRecords);
    }

    public RecordLoader(Dialog owner, StandardRecordBuilderConfig config, List<EligibilityRecord> eligibilityRecords) {
        super(owner);
        initComponents();
        loadRecords(config, eligibilityRecords);
    }

    public List<Record> getRecords() {
        return records;
    }

    private void loadRecords(final StandardRecordBuilderConfig config, List<EligibilityRecord> eligibilityRecords) {
        // only build records that haven't previously been generated.  everything previously generated
        // should be pulled from cache instead.

        final RecordCache cache = RecordCache.getInstance();
        final List<EligibilityRecord> newEligibilityRecords = new ArrayList<EligibilityRecord>();

        records = new ArrayList<Record>();

        try {
            for (EligibilityRecord eligibilityRecord : eligibilityRecords) {
                Object recordId = eligibilityRecord.getData().getId();

                if (cache.hasRecordWithId(recordId)) {
                    records.add(cache.getRecordById(recordId));

                } else {
                    newEligibilityRecords.add(eligibilityRecord);
                }
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " loading records - " + e.getMessage(), e);
        }

        if (newEligibilityRecords.size() > 0) {
            workerThread = buildWorkerThread(config, newEligibilityRecords);
            workerThread.setUncaughtExceptionHandler(new GUIThreadExceptionHandler(this, "Record Loading Error"));
            workerThread.start();

            setVisible(true);
        }
    }

    private Thread buildWorkerThread(final StandardRecordBuilderConfig config, final Collection<? extends BasicEligibilityRecord> eligibilityRecords) {
        final Dialog parent = this;

        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RecordCache cache = RecordCache.getInstance();

                    lblStatus.setText("Building base records...");
                    List<Record> newRecords = config.getBaseLocator().execute(eligibilityRecords);

                    int x = 0;
                    int max = newRecords.size();

                    progressBar.setMinimum(0);
                    progressBar.setMaximum(max);

                    if (config.getAppenders().size() > 0) {
                        for (Record record : newRecords) {
                            if (Thread.currentThread().isInterrupted()) {
                                lblStatus.setText("Canceled.");
                                break;
                            }

                            updateStatus(++x, max);

                            for (RecordAppender appender : config.getAppenders()) {
                                appender.execute(record);
                            }

                            cache.storeRecord(record);
                        }
                    }

                    for (BasicEligibilityRecord eligibilityRecord : eligibilityRecords) {
                        Object recordId = eligibilityRecord.getData().getId();

                        if (cache.hasRecordWithId(recordId)) {
                            records.add(cache.getRecordById(recordId));

                        } else {
                            log.error("couldn't find record with id='" + recordId + "'!  skipping -");
                        }
                    }

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " loading records - " + e.getMessage(), e);
                    DialogUtil.showException(parent, "Record Loading Error", e);

                } finally {
                    config.clearSupplementalLocatorCaches();
                }

                btnCancel.setText("Close");

                startCloseTimer();
            }

            private void updateStatus(int x, int max) {
                int pct = (int) ((((float) x) / max) * 100);
                progressBar.setValue(x);
                lblStatus.setText("Building record " + x + " of " + max + " (" + pct + "%)");
            }
        });
    }

    private void startCloseTimer() {
        final Component gui = this;
        Timer t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.setVisible(false);
            }
        });
        t.setRepeats(false);
        t.start();
    }

    private void btnCancelActionPerformed() {
        if (workerThread.isAlive()) {
            lblStatus.setText("Canceling ...");
            workerThread.interrupt();

        } else {
            this.setVisible(false);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        progressBar = new JProgressBar();
        lblStatus = new JLabel();
        btnCancel = new JButton();

        //======== this ========
        setModal(true);
        setResizable(false);
        setTitle("Loading Records");
        Container contentPane = getContentPane();

        //---- lblStatus ----
        lblStatus.setText("Building record X of Y (Z% completed)");

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCancelActionPerformed();
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addGap(0, 327, Short.MAX_VALUE)
                            .addComponent(btnCancel))
                        .addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(39, 39, 39)
                    .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(lblStatus)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                    .addComponent(btnCancel)
                    .addContainerGap())
        );
        setSize(455, 175);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JProgressBar progressBar;
    private JLabel lblStatus;
    private JButton btnCancel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
