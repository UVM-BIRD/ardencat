/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mstorer on 1/29/14.
 */
public abstract class ContentPanel extends JPanel {
    protected UIConfig uiConfig;
    private String prevLabel = null;
    private String nextLabel = null;
    private boolean loaded = false;

    public ContentPanel(LayoutManager layout, UIConfig uiConfig) {
        super(layout, true);
        this.uiConfig = uiConfig;
    }

    public ContentPanel(UIConfig uiConfig) {
        super(true);
        this.uiConfig = uiConfig;
    }

    public void load(JPanel contentPane) {
        setBounds(contentPane.getBounds());
        contentPane.removeAll();

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup()
                        .addComponent(this, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup()
                        .addComponent(this, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        contentPane.revalidate();
        contentPane.repaint();

        JButton backButton = uiConfig.getBackButton();
        JButton nextButton = uiConfig.getNextButton();

        if (hasPrevPanel()) {
            backButton.setText(prevLabel);
            backButton.setEnabled(mayGoBack());
            backButton.setVisible(true);

        } else {
            backButton.setEnabled(false);
            backButton.setVisible(false);
        }

        if (hasNextPanel()) {
            nextButton.setText(nextLabel);
            nextButton.setEnabled(mayProceed());
            nextButton.setVisible(true);

        } else {
            nextButton.setEnabled(false);
            nextButton.setVisible(false);
        }

        loaded = true;
    }

    public boolean hasBeenLoaded() {
        return loaded;
    }

    protected void flagAsLoaded() {
        loaded = true;
    }

    protected abstract boolean mayProceed();

    protected boolean mayGoBack() {
        return true;
    }

    public void setNavLabels(String prevLabel, String nextLabel) {
        this.prevLabel = prevLabel;
        this.nextLabel = nextLabel;
    }

    public boolean hasPrevPanel() {
        return prevLabel != null;
    }

    public String getPrevLabel() {
        return prevLabel;
    }

    public boolean hasNextPanel() {
        return nextLabel != null;
    }

    public String getNextLabel() {
        return nextLabel;
    }
}
