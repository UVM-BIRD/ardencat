/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import javax.swing.table.DefaultTableCellRenderer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mstorer on 9/9/14.
 */
public class VarHistDateTimeRenderer extends DefaultTableCellRenderer {
    private static final long MS_IN_DAY = 1000 * 60 * 60 * 24;

    private static final SimpleDateFormat TODAY_FORMAT = new SimpleDateFormat("'Today at' h:mm a");
    private static final SimpleDateFormat RECENT_FORMAT = new SimpleDateFormat("EEEE 'at' h:mm a");
    private static final SimpleDateFormat NOT_RECENT_FORMAT = new SimpleDateFormat("MMM d, yyyy 'at' h:mm a");


    @Override
    protected void setValue(Object value) {
        if (value != null && value instanceof Date) {
            Date d = (Date) value;
            if      (isToday(d))    super.setValue(TODAY_FORMAT.format(d));
            else if (isRecent(d))   super.setValue(RECENT_FORMAT.format(d));
            else                    super.setValue(NOT_RECENT_FORMAT.format(d));

        } else {
            super.setValue(null);
        }
    }

    boolean isToday(Date d) {
        long min = startOfToday();
        long max = min + MS_IN_DAY;
        long t = d.getTime();

        return t >= min && t < max;
    }

    boolean isRecent(Date d) {      // previous week
        long max = startOfToday();
        long min = max - (MS_IN_DAY * 7);
        long t = d.getTime();

        return t >= min && t < max;
    }

    private long startOfToday() {
        Calendar c = Calendar.getInstance();

        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime().getTime();
    }
}
