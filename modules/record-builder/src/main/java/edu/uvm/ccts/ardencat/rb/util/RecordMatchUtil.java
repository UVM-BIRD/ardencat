/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.rb.model.Record;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 10/15/14.
 */
public class RecordMatchUtil {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static boolean matches(Record r, String search) {
        if (search == null || search.trim().isEmpty()) return false;

        List<String> matchedFieldNames = new ArrayList<String>();

        for (String part : search.trim().toLowerCase().split("\\s+")) {
            boolean matchesPart = false;

            for (String field : r.getFieldNames()) {
                if (matchedFieldNames.contains(field)) continue;       // only allow one match per field

                Object obj = r.getField(field);

                if (obj != null) {
                    String s;
                    if (obj instanceof Date)    s = sdf.format((Date) obj);
                    else                        s = String.valueOf(obj);

                    if (s.toLowerCase().contains(part)) {
                        matchesPart = true;
                        matchedFieldNames.add(field);
                        break;
                    }
                }
            }

            if ( ! matchesPart ) {
                return false;
            }
        }

        return true;
    }
}
