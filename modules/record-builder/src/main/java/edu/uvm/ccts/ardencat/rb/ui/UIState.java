/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 10/8/14.
 */
public class UIState implements Serializable {
    private Map<String, Object> map = new HashMap<String, Object>();

    public boolean hasProperty(String key) {
        return map.containsKey(key);
    }

    public void setProperty(String key, Object value) {
        map.put(key, value);
    }

    public Object getProperty(String key) {
        return map.get(key);
    }

    public boolean getBoolean(String key) {
        Object obj = map.get(key);
        return obj != null && obj instanceof Boolean ?
                (Boolean) obj :
                false;
    }

    public String getString(String key) {
        Object obj = map.get(key);
        return obj != null && obj instanceof String ?
                (String) obj :
                null;
    }

    public Integer getInteger(String key) {
        Object obj = map.get(key);
        return obj != null && obj instanceof Integer ?
                (Integer) obj :
                null;
    }


}
