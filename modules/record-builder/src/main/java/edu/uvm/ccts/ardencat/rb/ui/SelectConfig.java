/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Jan 28 11:11:42 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.RecordBuilder;
import edu.uvm.ccts.ardencat.rb.ui.model.*;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.JarUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Matt Storer
 */
public class SelectConfig extends ContentPanel {
    private static final Log log = LogFactory.getLog(SelectConfig.class);

    private static String CONFIG_HISTORY_FILE;
    static {
        try {
            CONFIG_HISTORY_FILE = FileUtil.buildPath(JarUtil.getJarDir(RecordBuilder.class), ".config-history");
        } catch (Exception e) {
            CONFIG_HISTORY_FILE = ".config-history";
        }
    }

    private ConfigHistoryItem selectedItem = null;

    public SelectConfig(UIConfig uiConfig) {
        super(uiConfig);
        initComponents();

        setNavLabels(null, "Next");
    }

    @Override
    public void load(JPanel contentPane) {
        super.load(contentPane);

        if ( ! configHistoryLoaded() ) {
            loadConfigHistory(false);

            List<ConfigHistoryItem> list = ((ConfigHistoryTableModel) tblConfigHistory.getModel()).getList();
            if (list.size() > 0) {
                uiConfig.getNextButton().setEnabled(true);
            }
        }
    }

    public UIState getUIState() {
        UIState state = new UIState();

        String configFile = selectedItem != null ?
                selectedItem.getFilename() :
                null;

        state.setProperty("selectedConfig", configFile);

        return state;
    }

    @SuppressWarnings("unchecked")
    public void restoreUIState(UIState state) {
        String configFile = (String) state.getProperty("selectedConfig");

        if (configFile == null) return;

        if ( ! configHistoryLoaded() ) {
            loadConfigHistory(true);
        }

        // now select the loaded configuration.  if it's in the list, move it to the top and select it.  if it's
        // not in the list, add it to the top and select it.

        List<ConfigHistoryItem> list = ((ConfigHistoryTableModel) tblConfigHistory.getModel()).getList();
        int selectedIndex = -1;
        for (int i = 0; i < list.size(); i ++) {
            if (configFile.equalsIgnoreCase(list.get(i).getFilename())) {
                selectedIndex = i;
                break;
            }
        }

        if (selectedIndex == -1) {
            try {
                list.add(0, new ConfigHistoryItem(configFile, null));

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " loading config '" + configFile + "' - " + e.getMessage(), e);
            }

        } else if (selectedIndex > 0) {
            ConfigHistoryItem item = list.get(selectedIndex);
            list.remove(selectedIndex);
            list.add(0, item);
        }

        if (list.size() > 0) {
            selectedItem = list.get(0);
            tblConfigHistory.setRowSelectionInterval(0, 0);
            uiConfig.getNextButton().setEnabled(true);
        }

        flagAsLoaded();
    }

    @Override
    protected boolean mayProceed() {
        return tblConfigHistory.getSelectedRow() >= 0;
    }

    public ConfigHistoryItem getActiveConfigItem() {
        return selectedItem;
    }

    public void saveConfigHistory() throws IOException {
        List<String> list = new ArrayList<String>();

        list.add(selectedItem.toSerializedRecord());

        for (ConfigHistoryItem item : ((ConfigHistoryTableModel) tblConfigHistory.getModel()).getList()) {
            if (selectedItem != item) {
                list.add(item.toSerializedRecord());
            }
        }

        FileUtil.write(CONFIG_HISTORY_FILE, StringUtils.join(list, "\n"), false);
    }

    private boolean configHistoryLoaded() {
        return tblConfigHistory.getModel() instanceof ConfigHistoryTableModel;
    }

    private void loadConfigHistory(boolean silent) {
        List<ConfigHistoryItem> list = new ArrayList<ConfigHistoryItem>();

        try {
            if (FileUtil.exists(CONFIG_HISTORY_FILE)) {
                for (String line : FileUtil.readLines(CONFIG_HISTORY_FILE)) {
                    try {
                        list.add(new ConfigHistoryItem(line));

                    } catch (Exception e) {
                        // handle silently
                    }
                }
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " loading config history file - " + e.getMessage(), e);
            if ( ! silent ) DialogUtil.showErrorDialog(this, "Couldn't load config history file - " + e.getMessage());
        }

        tblConfigHistory.setModel(new ConfigHistoryTableModel(list));
        tblConfigHistory.setDefaultRenderer(Date.class, new VarHistDateTimeRenderer());
        tblConfigHistory.getColumnModel().setColumnMargin(0);

        if (list.size() > 0) {
            selectedItem = list.get(0);
            tblConfigHistory.setRowSelectionInterval(0, 0);
        }
    }

    private void btnSelectFileActionPerformed() {
        File file = FileUtil.selectFileToOpen(this,
                new FileNameExtensionFilter("XML Files", "xml"),
                System.getProperty("user.dir"));

        if (file != null) {
            try {
                ConfigHistoryItem item = new ConfigHistoryItem(file.getCanonicalPath(), null);

                int rowIndex = ((ConfigHistoryTableModel) tblConfigHistory.getModel()).addItem(item);

                tblConfigHistory.changeSelection(rowIndex, 0, false, false);

                doSelectedConfig(item);

                saveConfigHistory();

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " selecting config file - " + e.getMessage(), e);
                DialogUtil.showErrorDialog(this, "Couldn't load '" + file.getName() + "' - " + e.getMessage());

                doSelectedConfig(null);
            }
        }
    }

    private void tblConfigHistoryMouseClicked(MouseEvent event) {
        Point p = event.getPoint();
        ConfigHistoryItem item = ((ConfigHistoryTable) tblConfigHistory).getItemAtPoint(p);
        doSelectedConfig(item);

        if (event.getClickCount() > 1 && uiConfig.getNextButton().isEnabled()) {
            uiConfig.getNextButton().doClick();
        }
    }

    private void tblConfigHistoryKeyReleased(KeyEvent event) {
        if (event.isActionKey()) {
            ConfigHistoryItem item = ((ConfigHistoryTable) tblConfigHistory).getItemAtRow(tblConfigHistory.getSelectedRow());
            doSelectedConfig(item);
        }
    }

    private void doSelectedConfig(ConfigHistoryItem item) {
        selectedItem = item;
        uiConfig.getNextButton().setEnabled(selectedItem != null);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        label1 = new JLabel();
        btnSelectFile = new JButton();
        scrollPane1 = new JScrollPane();
        tblConfigHistory = new ConfigHistoryTable();
        label2 = new JLabel();

        //======== this ========

        //---- label1 ----
        label1.setText("Select a recent configuration, or choose a new one using the button below.");

        //---- btnSelectFile ----
        btnSelectFile.setText("New Configuration");
        btnSelectFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSelectFileActionPerformed();
            }
        });

        //======== scrollPane1 ========
        {

            //---- tblConfigHistory ----
            tblConfigHistory.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            tblConfigHistory.setSelectionForeground(Color.black);
            tblConfigHistory.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblConfigHistory.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    tblConfigHistoryMouseClicked(e);
                }
            });
            tblConfigHistory.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    tblConfigHistoryKeyReleased(e);
                }
            });
            scrollPane1.setViewportView(tblConfigHistory);
        }

        //---- label2 ----
        label2.setText("Select Configuration");
        label2.setFont(new Font("Lucida Grande", Font.BOLD, 20));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addComponent(label1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                .addComponent(label2)
                                .addComponent(btnSelectFile, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
                            .addGap(0, 279, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label2)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(label1)
                    .addGap(18, 18, 18)
                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnSelectFile)
                    .addContainerGap())
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JLabel label1;
    private JButton btnSelectFile;
    private JScrollPane scrollPane1;
    private JTable tblConfigHistory;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
