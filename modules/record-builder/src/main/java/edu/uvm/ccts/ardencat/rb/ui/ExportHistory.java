/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Fri Sep 19 10:37:56 EDT 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.ui.model.DateRenderer;
import edu.uvm.ccts.ardencat.rb.model.AbstractRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.ExportAudit;
import edu.uvm.ccts.ardencat.rb.model.ExportInfo;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.ui.model.*;
import edu.uvm.ccts.ardencat.rb.util.RecordMatchUtil;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.model.DateTime;
import edu.uvm.ccts.common.util.Base64Util;
import edu.uvm.ccts.common.util.DateUtil;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.TableUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.List;

/**
 * @author Matthew Storer
 */
public class ExportHistory extends JDialog {
    private static final Log log = LogFactory.getLog(ExportHistory.class);

    private static final String RECORD_TYPE = "rec";    // note : duplicated in AbstractExportPanel

    private AbstractRecordBuilderConfig config;
    private UIConfig uiConfig;

    public ExportHistory(Frame owner, AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        super(owner);
        initComponents();
        setup(config, uiConfig);
    }

    public ExportHistory(Dialog owner, AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        super(owner);
        initComponents();
        setup(config, uiConfig);
    }

    private void setup(AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        this.config = config;
        this.uiConfig = uiConfig;

        loadExportHistory(null);

        if ( ! "true".equalsIgnoreCase(uiConfig.getProperty("export.auditChanges")) ) {
            btnAudits.setVisible(false);
        }

        TableUtil.makeAutoResizable(tblExportHistory);
        TableUtil.addContextMenu(tblExportHistory);

        TableUtil.makeAutoResizable(tblRecords);
        TableUtil.addContextMenu(tblRecords);
    }

    private void loadExportHistory(String search) {
        clearRecordsTable();

        List<ExportInfo> list = buildExportInfoList();

        if (search != null && ! search.trim().isEmpty()) {
            Iterator<ExportInfo> iter = list.iterator();

            while (iter.hasNext()) {
                ExportInfo ei = iter.next();
                if ( ! matchesSearch(ei, search) ) {
                    iter.remove();
                }
            }
        }

        tblExportHistory.setModel(new ExportHistoryTableModel(list));

        tblExportHistory.setDefaultRenderer(DateTime.class, new DateTimeRenderer());

        TableUtil.adjustColumnWidths(tblExportHistory);
    }

    private boolean matchesSearch(ExportInfo ei, String search) {
        try {
            for (Record r : buildExportedRecordList(ei.getId())) {
                if (RecordMatchUtil.matches(r, search)) {
                    return true;
                }
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " searching records for export " + ei.getId() +
                    " for '" + search + "' - " + e.getMessage(), e);
        }

        return false;
    }

    private List<ExportInfo> buildExportInfoList() {
        List<ExportInfo> list = new ArrayList<ExportInfo>();

        try {
            String query = "select id, firstBatch, lastBatch, method, created from tblACExport where source = ? order by id desc";

            PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

            stmt.setString(1, config.getEligibilityConfig().getSource());

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ExportInfo ei = new ExportInfo();
                ei.setId(rs.getInt(1));
                ei.setFirstBatch(rs.getString(2));
                ei.setLastBatch(rs.getString(3));
                ei.setMethod(rs.getString(4));
                ei.setCreated(buildResultSetDate(rs, 5));

                ei.setAuditHistory(buildAuditHistory(ei.getId()));

                list.add(ei);
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " building export info list - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, "Error loading export info : " + e.getMessage());
        }

        return list;
    }

    private List<ExportAudit> buildAuditHistory(int exportId) throws SQLException {
        String query = "select id, exportId, authSig, reason, created from tblACExportAudit where exportId = ?";

        PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

        stmt.setInt(1, exportId);

        List<ExportAudit> list = new ArrayList<ExportAudit>();

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            ExportAudit ea = new ExportAudit();
            ea.setId(rs.getInt(1));
            ea.setExportId(rs.getInt(2));
            ea.setAuthSig(rs.getString(3));
            ea.setReason(rs.getString(4));
            ea.setCreated(buildResultSetDate(rs, 5));

            list.add(ea);
        }

        return list;
    }

    private Date buildResultSetDate(ResultSet rs, int index) throws SQLException {
        Object obj = rs.getObject(index);
        try {
            return DateUtil.castToDate(obj);

        } catch (ParseException e) {
            log.error("couldn't parse " + obj.getClass().getName() + " to Date - " + e.getMessage(), e);
            return null;
        }
    }

    private void btnCloseActionPerformed() {
        this.setVisible(false);
    }

    private void tblExportHistoryMousePressed(MouseEvent event) {
        ExportInfo ei = ((ExportHistoryTable) tblExportHistory).getExportInfoAtPoint(event.getPoint());

        try {
            loadRecords(ei);
            btnExport.setEnabled(true);
            if (btnAudits.isVisible()) btnAudits.setEnabled(true);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " loading records for batch with exportId=" +
                    ei.getId() + " - " + e.getMessage(), e);

            btnExport.setEnabled(false);
            if (btnAudits.isVisible()) btnAudits.setEnabled(false);
        }
    }

    private void loadRecords(ExportInfo ei) throws SQLException, DataException, IOException, ParseException {
        List<Record> records = buildExportedRecordList(ei.getId());

        RecordTableModel model = new RecordTableModel(records, false);
        model.highlightRecordsMatchingSearch(txtSearch.getText().trim());
        tblRecords.setModel(model);

        if ("true".equalsIgnoreCase(uiConfig.getProperty("review.dateDisplay.showTime"))) {
            tblRecords.setDefaultRenderer(Date.class, new DateTimeRenderer());

        } else {
            tblRecords.setDefaultRenderer(Date.class, new DateRenderer());
        }
        tblRecords.setDefaultEditor(Date.class, new DateTimeEditor());

        Enumeration<TableColumn> e = tblRecords.getColumnModel().getColumns();
        while (e.hasMoreElements()) {
            TableColumn col = e.nextElement();

            Class klass = model.getColumnClass(col.getModelIndex());

            col.setCellRenderer(tblRecords.getDefaultRenderer(klass));
            col.setCellEditor(tblRecords.getDefaultEditor(klass));
        }

        tblRecords.getColumnModel().setColumnMargin(0);

        TableUtil.adjustColumnWidths(tblRecords);
    }

    private void clearRecordsTable() {
        try {
            tblRecords.setModel(new RecordTableModel(new ArrayList<Record>(), false));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " clearing records table - " + e.getMessage(), e);
        }
    }

    private List<Record> buildExportedRecordList(int exportId) throws SQLException, DataException {
        List<Record> list = new ArrayList<Record>();

        String query = "select data from tblACExportRecord where exportId = ? and recordType = ?";

        PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

        stmt.setInt(1, exportId);
        stmt.setString(2, RECORD_TYPE);

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String dataStr = rs.getString(1);
            try {
                Record r = (Record) Base64Util.deserialize(dataStr);
                list.add(r);

            } catch (Exception e) {
                throw new DataException(e);
            }
        }

        return list;
    }

    private void btnExportActionPerformed() {
        ExportInfo ei = ((ExportHistoryTable) tblExportHistory).getExportInfoAtRow(tblExportHistory.getSelectedRow());

        try {
            ExportDialog ed = new ExportDialog(this, ei);
            ed.setVisible(true);

            ei.setAuditHistory(buildAuditHistory(ei.getId()));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " loading export dialog - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());
        }
    }

    private void btnSearchActionPerformed() {
        loadExportHistory(txtSearch.getText().trim());
    }

    private void txtSearchKeyReleased(KeyEvent e) {
        if (e.getKeyCode() == 10) {
            btnSearchActionPerformed();
        }
    }

    private void btnAuditsActionPerformed() {
        int row = tblExportHistory.convertRowIndexToModel(tblExportHistory.getSelectedRow());
        ExportInfo ei = ((ExportHistoryTableModel) tblExportHistory.getModel()).getExportInfoAtRow(row);
        new ExportAuditHistory(this, ei).setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        scrollPane1 = new JScrollPane();
        tblExportHistory = new ExportHistoryTable();
        txtSearch = new JTextField();
        btnSearch = new JButton();
        scrollPane2 = new JScrollPane();
        tblRecords = new RecordTable();
        buttonBar = new JPanel();
        btnClose = new JButton();
        btnExport = new JButton();
        btnAudits = new JButton();

        //======== this ========
        setTitle("Export History");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("Export History");
                label1.setFont(new Font("Lucida Grande", Font.BOLD, 20));

                //---- label2 ----
                label2.setText("This screen lets you search, view, and re-export previously exported batches.");

                //======== scrollPane1 ========
                {

                    //---- tblExportHistory ----
                    tblExportHistory.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent e) {
                            tblExportHistoryMousePressed(e);
                        }
                    });
                    scrollPane1.setViewportView(tblExportHistory);
                }

                //---- txtSearch ----
                txtSearch.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyReleased(KeyEvent e) {
                        txtSearchKeyReleased(e);
                    }
                });

                //---- btnSearch ----
                btnSearch.setText("Search");
                btnSearch.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnSearchActionPerformed();
                    }
                });

                //======== scrollPane2 ========
                {

                    //---- tblRecords ----
                    tblRecords.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    tblRecords.setAutoCreateRowSorter(true);
                    scrollPane2.setViewportView(tblRecords);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(label2))
                            .addContainerGap(240, Short.MAX_VALUE))
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addComponent(txtSearch, GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 729, Short.MAX_VALUE)
                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 729, Short.MAX_VALUE)
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label2)
                            .addGap(18, 18, 18)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(btnSearch)
                                .addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));

                //---- btnClose ----
                btnClose.setText("Close");
                btnClose.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnCloseActionPerformed();
                    }
                });

                //---- btnExport ----
                btnExport.setText("Re-export");
                btnExport.setEnabled(false);
                btnExport.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnExportActionPerformed();
                    }
                });

                //---- btnAudits ----
                btnAudits.setText("View Audit History");
                btnAudits.setEnabled(false);
                btnAudits.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnAuditsActionPerformed();
                    }
                });

                GroupLayout buttonBarLayout = new GroupLayout(buttonBar);
                buttonBar.setLayout(buttonBarLayout);
                buttonBarLayout.setHorizontalGroup(
                    buttonBarLayout.createParallelGroup()
                        .addGroup(buttonBarLayout.createSequentialGroup()
                            .addComponent(btnExport)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnAudits)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 380, Short.MAX_VALUE)
                            .addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                );
                buttonBarLayout.setVerticalGroup(
                    buttonBarLayout.createParallelGroup()
                        .addGroup(buttonBarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(btnExport)
                            .addComponent(btnClose)
                            .addComponent(btnAudits))
                );
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel label2;
    private JScrollPane scrollPane1;
    private JTable tblExportHistory;
    private JTextField txtSearch;
    private JButton btnSearch;
    private JScrollPane scrollPane2;
    private JTable tblRecords;
    private JPanel buttonBar;
    private JButton btnClose;
    private JButton btnExport;
    private JButton btnAudits;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
