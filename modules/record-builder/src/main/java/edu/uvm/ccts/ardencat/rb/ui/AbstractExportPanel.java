/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.model.BatchStatistics;
import edu.uvm.ccts.ardencat.rb.model.StandardRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.ui.model.ContentPanel;
import edu.uvm.ccts.ardencat.rb.ui.model.UIConfig;
import edu.uvm.ccts.ardencat.rb.util.ExportUtil;
import edu.uvm.ccts.ardencat.rb.util.StatisticsReader;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 9/15/14.
 */
public abstract class AbstractExportPanel extends ContentPanel implements Export {
    private static final Log log = LogFactory.getLog(AbstractExportPanel.class);

    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    protected StandardRecordBuilderConfig config;

    private boolean exported = false;

    protected List<Record> records;
    protected String firstBatch;
    protected String lastBatch;


    @Override
    public void load(JPanel contentPane, List<Record> records, String firstBatch, String lastBatch)
            throws ValidationException {
        this.records = records;
        this.firstBatch = firstBatch;
        this.lastBatch = lastBatch;

        load(contentPane);
    }

    @Override
    public UIState getUIState() {
        UIState state = new UIState();

        state.setProperty("firstBatch", firstBatch);
        state.setProperty("lastBatch", lastBatch);

        // note : do NOT save records, even though it is restored in restoreUIState (below).
        //        records are generated at runtime to ensure the latest data are displayed,
        //        and to avoid storing PHI in the state file

        return state;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void restoreUIState(UIState state) throws Exception {
        firstBatch = (String) state.getProperty("firstBatch");
        lastBatch = (String) state.getProperty("lastBatch");
        records = (List<Record>) state.getProperty("records");
    }

    protected AbstractExportPanel(UIConfig uiConfig, StandardRecordBuilderConfig config) {
        super(uiConfig);

        this.config = config;
    }

    protected boolean shouldExport() {
        return DialogUtil.showConfirmDialog(this, "You are about to export records.  This action will mark selected batches as exported, and associated records will no longer be available for curation.  Are you sure you want to proceed?");
    }

    protected void markBatchRangeExported(Object firstBatch, Object lastBatch, int exportId) throws SQLException {
        ExportUtil.markBatchRangeExported(
                config.getArdencatDataSource(),
                config.getEligibilityConfig().getSource(),
                firstBatch, lastBatch,
                exportId);

        exported = true;
    }

    protected void auditExport(int exportId, String authSig, String reason) throws SQLException {
        ExportUtil.auditExport(config.getArdencatDataSource(), exportId, authSig, reason);
    }

    @Override
    public boolean hasExported() {
        return exported;
    }

    @Override
    protected boolean mayGoBack() {
        return ! hasExported();
    }

    public int writeExportRecords(Object firstBatch, Object lastBatch, String method, List<Record> records) throws SQLException, IOException {
        return ExportUtil.writeExportRecords(
                config.getArdencatDataSource(),
                config.getEligibilityConfig().getSource(),
                firstBatch, lastBatch,
                method,
                records);
    }

    protected String buildDefaultFilename(Object firstBatch, Object lastBatch, String fileType) {
        String dt;
        if      (firstBatch == null)            dt = "null";
        else if (firstBatch instanceof Date)    dt = sdf.format((Date) firstBatch);
        else                                    dt = firstBatch.toString();

        return "ardencat_export_" + dt + "." + fileType;
    }

    protected List<Record> buildExportedRecordList(int exportId) throws SQLException, DataException {
        return ExportUtil.buildExportedRecordList(config.getArdencatDataSource(), exportId);
    }

    protected void generateStatisticsReport() {
        String report = buildStatisticsReport();

        try {
            FileUtil.write("reports/" + replaceInvalidChars(config.getEligibilityConfig().getSource()) + "_stats.html", report, false);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " writing statistics report - " + e.getMessage(), e);
        }
    }

    private String replaceInvalidChars(String s) {
        return s.replaceAll("[^a-zA-Z0-9-]", "_");
    }

    private String buildStatisticsReport() {
        StringBuilder sb = new StringBuilder();

// wrapper ---

        String title = uiConfig.getProperty("gui.title") + " - " +
                config.getEligibilityConfig().getSource() + " Batch Statistics";

        sb.append("<html><head>\n<title>")
                .append(title)
                .append("</title>\n");

        sb.append("<style type=\"text/css\">\n");
        sb.append("body { font-family: sans-serif; }\n");
        sb.append("h2 { border-bottom: 1px solid black; }\n");
        sb.append("#details td { text-align: center; }\n");
        sb.append("#details tr:first-child { background: #ddd; }\n");
        sb.append("</style>\n</head><body>\n");

// header ---

        sb.append("<h1>")
                .append(title)
                .append("</h1>\n");

        sb.append("<p>This report contains basic statistics for all batches of records in the ArdenCAT database.</p>\n");

        sb.append("<p>Source: <strong>")
                .append(config.getEligibilityConfig().getSource())
                .append("</strong></p>\n");

        sb.append("<p>Generated: <strong>")
                .append(new Date().toString())
                .append("</strong></p>\n");

// summary ---

        sb.append("<h2>Summary</h2>\n");
        sb.append("<p><table id=\"summary\" border=\"0\">\n");

        List<BatchStatistics> list = StatisticsReader.buildStatistics(config);

        sb.append("<tr><td>Total # of Batches:</td><td>")
                .append(list.size())
                .append("</td></tr>\n");

        int exportedCount = 0;
        for (BatchStatistics stats : list) {
            if (stats.getExportedDate() != null) exportedCount ++;
        }

        sb.append("<tr><td># of Batches Exported:</td><td>")
                .append(exportedCount)
                .append("</td></tr>\n");

        sb.append("</table></p>\n");

// details ---

        sb.append("<h2>Details</h2>\n");
        sb.append("<p><table id=\"details\" border=\"1\">\n");
        sb.append("<tr><th>Batch</th><th># Eligible</th><th># Eligible Overridden</th>")
                .append("<th># Unknown</th><th># Unknown Overridden</th>")
                .append("<th># Ineligible</th><th># Ineligible Overridden</th>")
                .append("<th>Date Generated</th>")
                .append("<th>Date Exported</th></tr>\n");

        for (BatchStatistics stats : list) {
            sb.append("<tr><td>")
                    .append(stats.getBatch())
                    .append("</td><td>")
                    .append(stats.getNumEligible())
                    .append("</td><td>")
                    .append(stats.getNumEligibleOverridden())
                    .append("</td><td>")
                    .append(stats.getNumUnknown())
                    .append("</td><td>")
                    .append(stats.getNumUnknownOverridden())
                    .append("</td><td>")
                    .append(stats.getNumIneligible())
                    .append("</td><td>")
                    .append(stats.getNumIneligibleOverridden())
                    .append("</td><td>")
                    .append(stats.getGeneratedDate())
                    .append("</td><td>")
                    .append(stats.getExportedDate())
                    .append("</td><tr>\n");
        }

        sb.append("</table></p>\n");

// wrapper ---

        sb.append("</body>\n</html>\n");

        return sb.toString();
    }
}
