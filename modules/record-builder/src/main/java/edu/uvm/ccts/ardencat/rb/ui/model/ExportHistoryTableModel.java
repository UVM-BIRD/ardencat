/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.rb.model.ExportInfo;
import edu.uvm.ccts.common.model.DateTime;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by mstorer on 9/19/14.
 */
public class ExportHistoryTableModel extends AbstractTableModel {
    private List<ExportInfo> list;

    public ExportHistoryTableModel(List<ExportInfo> list) {
        this.list = list;
    }

    public ExportInfo getExportInfoAtRow(int row) {
        return list.get(row);
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:  return Integer.class;
            case 1:  return String.class;
            case 2:  return String.class;
            case 3:  return String.class;
            case 4:  return DateTime.class;
            default: return super.getColumnClass(columnIndex);
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:  return "Export #";
            case 1:  return "First Batch";
            case 2:  return "Last Batch";
            case 3:  return "Method";
            case 4:  return "Created";
            default: return super.getColumnName(column);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ExportInfo ei = list.get(rowIndex);

        switch (columnIndex) {
            case 0:  return ei.getId();
            case 1:  return ei.getFirstBatch();
            case 2:  return ei.getLastBatch();
            case 3:  return ei.getMethod();
            case 4:  return ei.getCreated();
            default: return null;
        }
    }
}
