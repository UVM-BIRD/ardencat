/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 9/19/14.
 */
public class ExportInfo {
    private int id;
    private String firstBatch;
    private String lastBatch;
    private String method;
    private Date created;
    List<ExportAudit> auditHistory = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstBatch() {
        return firstBatch;
    }

    public void setFirstBatch(String firstBatch) {
        this.firstBatch = firstBatch;
    }

    public String getLastBatch() {
        return lastBatch;
    }

    public void setLastBatch(String lastBatch) {
        this.lastBatch = lastBatch;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<ExportAudit> getAuditHistory() {
        return auditHistory;
    }

    public void setAuditHistory(List<ExportAudit> auditHistory) {
        this.auditHistory = auditHistory;
    }
}
