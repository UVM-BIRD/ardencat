/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.common.exceptions.ConfigurationException;
import electric.xml.Document;
import electric.xml.ParseException;
import electric.xml.XPath;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

/**
 * Created by mstorer on 9/8/14.
 */
public class ConfigHistoryItem {
    private static final String NULL = "null";

    private String name;
    private String filename;
    private Date lastUsed = null;

    public ConfigHistoryItem(String filename, Date lastUsed) throws FileNotFoundException, ParseException {
        File f = new File(filename);
        if ( ! f.exists() ) throw new FileNotFoundException("couldn't find file '" + filename + "'");

        this.name = getConfigDisplayName(f);
        this.filename = filename;
        this.lastUsed = lastUsed;
    }

    public ConfigHistoryItem(String serializedRecord) throws FileNotFoundException, ParseException, ConfigurationException {
        String[] parts = serializedRecord.split("\\s*\\|\\s*");
        if (parts.length == 2) {
            filename = parts[0];

            File f = new File(filename);
            if ( ! f.exists() ) throw new FileNotFoundException("couldn't find file '" + filename + "'");

            name = getConfigDisplayName(f);
            if ( ! NULL.equalsIgnoreCase(parts[1]) ) {
                lastUsed = new Date();
                lastUsed.setTime(Long.parseLong(parts[1]));
            }

        } else {
            throw new ConfigurationException("serialized config history item has invalid format");
        }
    }

    public String toSerializedRecord() {
        return filename + "|" + (lastUsed != null ? lastUsed.getTime() : NULL);
    }

    public String getName() {
        return name;
    }

    public String getFilename() {
        return filename;
    }

    public void updateLastUsedTimestamp() {
        lastUsed = new Date();
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    private String getConfigDisplayName(File file) throws ParseException {
        Document doc = new Document(file);
        return doc.getElement(new XPath("recordBuilder/display")).getAttribute("name");
    }
}
