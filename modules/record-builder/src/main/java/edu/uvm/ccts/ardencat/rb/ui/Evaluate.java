/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Fri Feb 21 10:33:52 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.AttributeEvaluator;
import edu.uvm.ccts.ardencat.evaluation.model.EvaluationResults;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.evaluation.ui.model.EvaluationReportTableModel;
import edu.uvm.ccts.common.exceptions.ValidationException;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * @author Matthew Storer
 */
public class Evaluate extends JDialog {
    private static final Log log = LogFactory.getLog(Evaluate.class);

    private Collection<Record> records;

    public Evaluate(Frame owner, Collection<Record> records) {
        super(owner);
        initComponents();
        setup(records);
    }

    public Evaluate(Dialog owner, Collection<Record> records) {
        super(owner);
        initComponents();
        setup(records);
    }

    private void setup(Collection<Record> records) {
        this.records = records;
        tblDetails.setModel(new EvaluationReportTableModel());
    }

    private void btnSelectFileActionPerformed() {
        File file = FileUtil.selectFileToOpen(this,
                new FileNameExtensionFilter("CSV Files", "csv"),
                System.getProperty("user.dir"));

        if (file != null) {
            try {
                txtFilename.setText(file.getCanonicalPath());
                updateResults();

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " selecting config file - " + e.getMessage(), e);
                DialogUtil.showErrorDialog(this, e.getMessage());
            }
        }
    }

    private void updateResults() throws IOException, ValidationException {
        AttributeEvaluator evaluator = new AttributeEvaluator(txtFilename.getText());
        EvaluationResults results = evaluator.execute(records);

        updateSummaryTab(results);
        updateDetailsTab(results);
    }

    private void updateSummaryTab(EvaluationResults results) {
        lblExpectedCount.setText(String.valueOf(results.getExpectedRecordCount()));
        lblFoundCount.setText(String.valueOf(results.getFoundRecordCount()));
        lblTruePositiveCount.setText(String.valueOf(results.getTruePositiveCount()));
        lblFalsePositiveCount.setText(String.valueOf(results.getFalsePositiveCount()));
        lblFalseNegativeCount.setText(String.valueOf(results.getFalseNegativeCount()));
        lblPrecision.setText(String.valueOf(results.getPrecision()));
        lblRecall.setText(String.valueOf(results.getRecall()));
    }

    private void updateDetailsTab(EvaluationResults results) {
        tblDetails.setModel(new EvaluationReportTableModel(results));
    }

    private void btnCloseActionPerformed() {
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        panel1 = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        txtFilename = new JTextField();
        btnSelectFile = new JButton();
        tabbedPane1 = new JTabbedPane();
        pnlSummary = new JPanel();
        panel3 = new JPanel();
        label3 = new JLabel();
        lblExpectedCount = new JLabel();
        label4 = new JLabel();
        lblFoundCount = new JLabel();
        label5 = new JLabel();
        lblTruePositiveCount = new JLabel();
        label6 = new JLabel();
        lblFalsePositiveCount = new JLabel();
        label7 = new JLabel();
        lblFalseNegativeCount = new JLabel();
        label8 = new JLabel();
        lblPrecision = new JLabel();
        label9 = new JLabel();
        lblRecall = new JLabel();
        pnlDetails = new JPanel();
        scrollPane1 = new JScrollPane();
        tblDetails = new JTable();
        btnClose = new JButton();

        //======== this ========
        setTitle("Evaluation");
        setModal(true);
        setAlwaysOnTop(true);
        Container contentPane = getContentPane();

        //======== panel1 ========
        {

            //---- label1 ----
            label1.setText("<html>This form facilitates the validation of generated records, by comparing one field from each record against an authoritative \"gold standard\" list of values.</html>");

            //---- label2 ----
            label2.setText("\"Gold standard\" file:");

            //---- btnSelectFile ----
            btnSelectFile.setText("Select file...");
            btnSelectFile.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnSelectFileActionPerformed();
                }
            });

            //======== tabbedPane1 ========
            {

                //======== pnlSummary ========
                {

                    //======== panel3 ========
                    {

                        //---- label3 ----
                        label3.setText("# records expected:");

                        //---- lblExpectedCount ----
                        lblExpectedCount.setText("0");

                        //---- label4 ----
                        label4.setText("# records found:");

                        //---- lblFoundCount ----
                        lblFoundCount.setText("0");

                        //---- label5 ----
                        label5.setText("# true positives:");

                        //---- lblTruePositiveCount ----
                        lblTruePositiveCount.setText("0");

                        //---- label6 ----
                        label6.setText("# false positives:");

                        //---- lblFalsePositiveCount ----
                        lblFalsePositiveCount.setText("0");

                        //---- label7 ----
                        label7.setText("# false negatives:");

                        //---- lblFalseNegativeCount ----
                        lblFalseNegativeCount.setText("0");

                        //---- label8 ----
                        label8.setText("Precision:");

                        //---- lblPrecision ----
                        lblPrecision.setText("0.0");

                        //---- label9 ----
                        label9.setText("Recall:");

                        //---- lblRecall ----
                        lblRecall.setText("0.0");

                        GroupLayout panel3Layout = new GroupLayout(panel3);
                        panel3.setLayout(panel3Layout);
                        panel3Layout.setHorizontalGroup(
                            panel3Layout.createParallelGroup()
                                .addGroup(panel3Layout.createParallelGroup()
                                    .addGroup(panel3Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addComponent(label3)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblExpectedCount))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(20, 20, 20)
                                                .addComponent(label4)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblFoundCount))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(22, 22, 22)
                                                .addComponent(label5)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblTruePositiveCount))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(17, 17, 17)
                                                .addComponent(label6)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblFalsePositiveCount))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(label7)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblFalseNegativeCount))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(63, 63, 63)
                                                .addComponent(label8)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblPrecision))
                                            .addGroup(panel3Layout.createSequentialGroup()
                                                .addGap(83, 83, 83)
                                                .addComponent(label9)
                                                .addGap(8, 8, 8)
                                                .addComponent(lblRecall)))
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addGap(0, 285, Short.MAX_VALUE)
                        );
                        panel3Layout.setVerticalGroup(
                            panel3Layout.createParallelGroup()
                                .addGroup(panel3Layout.createParallelGroup()
                                    .addGroup(panel3Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label3)
                                            .addComponent(lblExpectedCount))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label4)
                                            .addComponent(lblFoundCount))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label5)
                                            .addComponent(lblTruePositiveCount))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label6)
                                            .addComponent(lblFalsePositiveCount))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label7)
                                            .addComponent(lblFalseNegativeCount))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label8)
                                            .addComponent(lblPrecision))
                                        .addGap(8, 8, 8)
                                        .addGroup(panel3Layout.createParallelGroup()
                                            .addComponent(label9)
                                            .addComponent(lblRecall))
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addGap(0, 179, Short.MAX_VALUE)
                        );
                    }

                    GroupLayout pnlSummaryLayout = new GroupLayout(pnlSummary);
                    pnlSummary.setLayout(pnlSummaryLayout);
                    pnlSummaryLayout.setHorizontalGroup(
                        pnlSummaryLayout.createParallelGroup()
                            .addGroup(pnlSummaryLayout.createSequentialGroup()
                                .addGap(117, 117, 117)
                                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(130, 130, 130))
                    );
                    pnlSummaryLayout.setVerticalGroup(
                        pnlSummaryLayout.createParallelGroup()
                            .addGroup(pnlSummaryLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(20, Short.MAX_VALUE))
                    );
                }
                tabbedPane1.addTab("Summary", pnlSummary);

                //======== pnlDetails ========
                {

                    //======== scrollPane1 ========
                    {

                        //---- tblDetails ----
                        tblDetails.setAutoCreateRowSorter(true);
                        scrollPane1.setViewportView(tblDetails);
                    }

                    GroupLayout pnlDetailsLayout = new GroupLayout(pnlDetails);
                    pnlDetails.setLayout(pnlDetailsLayout);
                    pnlDetailsLayout.setHorizontalGroup(
                        pnlDetailsLayout.createParallelGroup()
                            .addComponent(scrollPane1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                    );
                    pnlDetailsLayout.setVerticalGroup(
                        pnlDetailsLayout.createParallelGroup()
                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                    );
                }
                tabbedPane1.addTab("Details", pnlDetails);
            }

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addComponent(label1, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(label2)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFilename)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSelectFile))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(tabbedPane1)
                        .addContainerGap())
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(label2)
                            .addComponent(btnSelectFile)
                            .addComponent(txtFilename, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tabbedPane1)
                        .addContainerGap())
            );
        }

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCloseActionPerformed();
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(btnClose))
                        .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnClose)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel panel1;
    private JLabel label1;
    private JLabel label2;
    private JTextField txtFilename;
    private JButton btnSelectFile;
    private JTabbedPane tabbedPane1;
    private JPanel pnlSummary;
    private JPanel panel3;
    private JLabel label3;
    private JLabel lblExpectedCount;
    private JLabel label4;
    private JLabel lblFoundCount;
    private JLabel label5;
    private JLabel lblTruePositiveCount;
    private JLabel label6;
    private JLabel lblFalsePositiveCount;
    private JLabel label7;
    private JLabel lblFalseNegativeCount;
    private JLabel label8;
    private JLabel lblPrecision;
    private JLabel label9;
    private JLabel lblRecall;
    private JPanel pnlDetails;
    private JScrollPane scrollPane1;
    private JTable tblDetails;
    private JButton btnClose;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
