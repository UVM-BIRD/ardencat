package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.rb.model.AbstractRecordBuilderConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 7/6/15.
 */
public class EligibilityUtil {
    private static final Log log = LogFactory.getLog(EligibilityUtil.class);

    @SuppressWarnings("unchecked")
    public static List<String> buildBatchList(AbstractRecordBuilderConfig config) {
        return buildBatchList(config, null);
    }

    public static List<String> buildBatchList(AbstractRecordBuilderConfig config, String startBatch) {
        List<String> list = new ArrayList<String>();

        try {
            Connection conn = config.getArdencatDataSource().getConnection();

            StringBuilder sb = new StringBuilder();
            sb.append("select distinct batch from tblACEligibility where source = ? and exportId is null");
            if (startBatch != null) {
                sb.append(" and batch >= ?");
            }
            sb.append(" order by batch");

            String query = sb.toString();
            PreparedStatement stmt = conn.prepareStatement(query);

            int index = 1;
            stmt.setString(index++, config.getEligibilityConfig().getSource());

            if (startBatch != null) {
                stmt.setString(index++, startBatch);
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(rs.getString(1));
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " populating batch list - " + e.getMessage(), e);
        }

        return list;
    }
}