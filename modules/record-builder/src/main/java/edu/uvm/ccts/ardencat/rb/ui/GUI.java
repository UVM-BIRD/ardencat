/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Jan 28 14:06:04 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.ExportPanelRegistry;
import edu.uvm.ccts.ardencat.rb.model.EligibilityRecord;
import edu.uvm.ccts.ardencat.rb.model.ExportMethod;
import edu.uvm.ccts.ardencat.rb.model.StandardRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.ui.model.ConfigHistoryItem;
import edu.uvm.ccts.ardencat.rb.ui.model.UIConfig;
import edu.uvm.ccts.common.db.DomainCredentialRegistry;
import edu.uvm.ccts.common.exceptions.CanceledException;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.ui.DisplayProgress;
import edu.uvm.ccts.common.util.Base64Util;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Matt Storer
 */
public class GUI extends JFrame {
    private static final Log log = LogFactory.getLog(GUI.class);

    private static final String STATE_FILENAME = FileUtil.buildPath(System.getProperty("user.home"), ".ardencat-state");
    private static final int SEL_CONFIG = 1;
    private static final int CUR_ELIG = 2;
    private static final int PREVIEW_RECS = 3;
    private static final int EXPORT = 4;
    private static final int JOB_COMP = 5;

    private StandardRecordBuilderConfig config = null;
    private SelectConfig selectConfig;
    private CurateEligibility curateEligibility;
    private PreviewRecords previewRecords;
    private Export export;
    private JobCompleted jobCompleted;
    private UIConfig uiConfig = null;

    public GUI() {
        initComponents();

        uiConfig = new UIConfig(this, btnBack, btnNext, lblStatus);

        this.setTitle(uiConfig.getProperty("gui.title"));

        lblStatus.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLoweredBevelBorder(),
                BorderFactory.createEmptyBorder(4, 4, 4, 4)));

        try {
            resumeWorkflow();

        } catch (Exception e) {
            if (FileUtil.exists(STATE_FILENAME)) {
                log.error("caught " + e.getClass().getName() + " attempting to resume workflow - " + e.getMessage(), e);
                DialogUtil.showException(this, "Error Restoring Previous State", e);
                FileUtil.delete(STATE_FILENAME);
            }

            startWorkflow();
        }

        addSaveStateShutdownHook();
    }

    protected StandardRecordBuilderConfig getConfig() {
        return config;
    }

    protected UIConfig getUIConfig() {
        return uiConfig;
    }

    private void startWorkflow() {
        reset();
        selectConfig.load(contentPane);
    }

    private void addSaveStateShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    UIState state = new UIState();
                    state.setProperty("basic", buildBasicUIState());
                    if (shouldRetainWorkflowState()) {
                        state.setProperty("workflow", buildWorkflowUIState());
                    }

                    log.info("shutdownHook : saving state to " + STATE_FILENAME);
                    FileUtil.write(STATE_FILENAME, Base64Util.serialize(state), false);

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " saving GUI state - " + e.getMessage(), e);
                }
            }
        });
    }

    private boolean shouldRetainWorkflowState() {
        if (export != null && export.hasBeenLoaded()) {
            return ! export.hasExported();

        } else if (curateEligibility.hasBeenLoaded()) {
            return curateEligibility.hasBatches();
        }

        return false;
    }

    private UIState buildBasicUIState() {
        UIState state = new UIState();

        state.setProperty("location", getLocationOnScreen());
        state.setProperty("size", getSize());
        state.setProperty("uiConfig", uiConfig.getUIState());

        return state;
    }

    private UIState buildWorkflowUIState() {
        UIState state = buildBasicUIState();

        state.setProperty("status", lblStatus.getText());
        state.setProperty("selectConfig", selectConfig.getUIState());
        state.setProperty("curateEligibility", curateEligibility.getUIState());
        state.setProperty("previewRecords", previewRecords.getUIState());

        if (export != null) {
            state.setProperty("export", export.getUIState());
        }

        Integer currentPanel = null;
        Component cp = contentPane.getComponent(0);
        if      (cp == selectConfig)        currentPanel = SEL_CONFIG;
        else if (cp == curateEligibility)   currentPanel = CUR_ELIG;
        else if (cp == previewRecords)      currentPanel = PREVIEW_RECS;
        else if (cp == export)              currentPanel = EXPORT;
        else if (cp == jobCompleted)        currentPanel = JOB_COMP;

        state.setProperty("currentPanel", currentPanel);

        Integer greatestVisitedPanel = null;
        if      (jobCompleted.hasBeenLoaded())      greatestVisitedPanel = JOB_COMP;
        else if (export.hasBeenLoaded())            greatestVisitedPanel = EXPORT;
        else if (previewRecords.hasBeenLoaded())    greatestVisitedPanel = PREVIEW_RECS;
        else if (curateEligibility.hasBeenLoaded()) greatestVisitedPanel = CUR_ELIG;
        else if (selectConfig.hasBeenLoaded())      greatestVisitedPanel = SEL_CONFIG;

        state.setProperty("greatestVisitedPanel", greatestVisitedPanel);

        return state;
    }

    private void resumeWorkflow() throws Exception {
        String s = FileUtil.read(STATE_FILENAME);
        UIState state = (UIState) Base64Util.deserialize(s);

        log.info("resumeWorkflow : found state file.  restoring previous system state -");

        reset();

        UIState bState = (UIState) state.getProperty("basic");
        UIState wState = (UIState) state.getProperty("workflow");

        restoreBasicState(bState);

        if (wState != null) restoreWorkflowState(wState);
        else                selectConfig.load(contentPane);
    }


    private void restoreBasicState(UIState state) {
        setLocation((Point) state.getProperty("location"));
        setSize((Dimension) state.getProperty("size"));
        uiConfig.restoreUIState((UIState) state.getProperty("uiConfig"));
    }

    private void restoreWorkflowState(UIState state) throws Exception {
        lblStatus.setText(state.getString("status"));

        selectConfig.restoreUIState((UIState) state.getProperty("selectConfig"));

        ConfigHistoryItem item = selectConfig.getActiveConfigItem();
        if (item == null) throw new ConfigurationException("couldn't find configuration file");

        Integer greatestVisitedPanel = state.getInteger("greatestVisitedPanel");

        int progressBarSize = 2;                                        // loading config + done status
        if (greatestVisitedPanel >= CUR_ELIG)       progressBarSize ++; // load eligibility data
        if (greatestVisitedPanel >= PREVIEW_RECS)   progressBarSize ++; // load records
        if (greatestVisitedPanel >= EXPORT)         progressBarSize ++; // load export panel

        DisplayProgress dp = new DisplayProgress(
                this.getTitle(),
                "Please wait while the system's previous state is restored.  This may take a few minutes.",
                progressBarSize);

        dp.start();

        try {
            dp.advance("Loading configuration from " + FileUtil.getFilenamePart(item.getFilename()));
            doConfigSetup(item.getFilename());

            if (greatestVisitedPanel >= CUR_ELIG) {
                dp.advance("Loading eligibility data");
                curateEligibility.restoreUIState((UIState) state.getProperty("curateEligibility"), config);
                mnuTools.setEnabled(true);

                if (greatestVisitedPanel >= PREVIEW_RECS) {
                    dp.advance("Loading records");
                    previewRecords.restoreUIState((UIState) state.getProperty("previewRecords"), config);

                    if (greatestVisitedPanel >= EXPORT) {
                        dp.advance("Restoring export screen state");
                        UIState exportState = (UIState) state.getProperty("export");
                        exportState.setProperty("records", previewRecords.getRecords());
                        export.restoreUIState(exportState);
                    }
                }
            }

            Integer currentPanel = state.getInteger("currentPanel");

            if (currentPanel == null || currentPanel == SEL_CONFIG) selectConfig.load(contentPane);
            else if (currentPanel == CUR_ELIG) curateEligibility.load(contentPane);
            else if (currentPanel == PREVIEW_RECS) previewRecords.load(contentPane);
            else if (currentPanel == EXPORT) export.load(contentPane);
            else if (currentPanel == JOB_COMP) jobCompleted.load(contentPane);

        } catch (CanceledException ce) {
            selectConfig.load(contentPane);

        } finally {
            dp.stop("Done.");
        }
    }

    protected StandardRecordBuilderConfig buildConfig(File file) throws ConfigurationException {
        try {
            return new StandardRecordBuilderConfig(file, this);

        } finally {
            DomainCredentialRegistry.getInstance().clear();
        }
    }

    protected void populateExportPanelRegistry() {
        ExportPanelRegistry registry = ExportPanelRegistry.getInstance();

        registry.registerPanel(ExportMethod.CSV, new StandardExport(getUIConfig(), getConfig()));
        registry.registerPanel(ExportMethod.XML, new StandardExport(getUIConfig(), getConfig()));
    }

    protected Export buildExport() {
        return new StandardExport(getUIConfig(), config);
    }

    private void reset() {
        mnuTools.setEnabled(false);
        selectConfig = new SelectConfig(uiConfig);
        curateEligibility = new CurateEligibility(uiConfig);
        previewRecords = new PreviewRecords(uiConfig);

        // export pane is built when configuration is selected

        jobCompleted = new JobCompleted(uiConfig);

        RecordCache.getInstance().clear();

        lblStatus.setText("");

        if (config != null) {
            config.closeAllDataSources();
            config = null;
            ExportPanelRegistry.getInstance().clear();
        }
    }

    private void btnNextActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        Component cp = contentPane.getComponent(0);
        if (cp == selectConfig) {
            ConfigHistoryItem item = selectConfig.getActiveConfigItem();

            if (item != null) {
                String filename = item.getFilename();

                try {
                    if (config == null || ! config.getFilename().equalsIgnoreCase(filename)) {
                        doConfigSetup(filename);
                    }

                    try {
                        // todo : only update this if config was just selected.  this is updating when just switching
                        //        between screens, which shouldn't happen.  not a big deal, but still shouldn't update

                        item.updateLastUsedTimestamp();
                        selectConfig.saveConfigHistory();

                    } catch (IOException e) {
                        log.error("caught " + e.getClass().getName() + " updating config history - " + e.getMessage(), e);
                    }

                    lblStatus.setText("Loading eligibility data...");
                    curateEligibility.load(contentPane, config);
                    lblStatus.setText("Eligibility data loaded.");

                    mnuTools.setEnabled(true);

                } catch (CanceledException e) {
                    // thrown from [ccts-common] GetCredentials
                    lblStatus.setText("Operation canceled.");

                } catch (Exception e) {
                    log.error("invalid configuration file '" + filename + "' - " + e.getMessage(), e);
                    DialogUtil.showErrorDialog(this, "Invalid configuration file '" + filename + "' - " + e.getMessage());
                }
            }

        } else if (cp == curateEligibility) {
            List<EligibilityRecord> eligibleRecords = curateEligibility.getEligibleRecords();

            try {
                lblStatus.setText("Generating records...");
                previewRecords.load(contentPane, config, eligibleRecords);
                lblStatus.setText("Records generated.");

            } catch (Exception e) {
                lblStatus.setText("Error: Couldn't build base records - " + e.getMessage());
                log.error("caught " + e.getClass().getName() + " building base records - " + e.getMessage(), e);
            }

        } else if (cp == previewRecords) {
            try {
                export.load(contentPane, previewRecords.getRecords(), curateEligibility.getFirstBatch(), curateEligibility.getLastBatch());

            } catch (Exception e) {
                lblStatus.setText("Error: Couldn't load records to export - " + e.getMessage());
                log.error("caught " + e.getClass().getName() + " loading records for export - " + e.getMessage(), e);
            }

        } else if (cp == export) {
            try {
                jobCompleted.load(contentPane);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " exporting records - " + e.getMessage(), e);
                DialogUtil.showErrorDialog(this, e.getMessage());
            }

        } else if (cp == jobCompleted) {
            startWorkflow();
        }

        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    private void doConfigSetup(String filename) throws ConfigurationException {
        config = buildConfig(new File(filename));

        populateExportPanelRegistry();

        this.export = buildExport();
    }

    private void btnBackActionPerformed() {
        Component cp = contentPane.getComponent(0);
        if (cp == curateEligibility) {
            selectConfig.load(contentPane);

        } else if (cp == previewRecords) {
            curateEligibility.load(contentPane);

        } else if (cp == export) {
            previewRecords.load(contentPane);

        } else if (cp == jobCompleted) {
            export.load(contentPane);
        }
    }

    private void miQuitActionPerformed() {
        System.exit(0);
    }

    private void miLoadConfigActionPerformed() {
        startWorkflow();
    }

    private void miExportHistoryActionPerformed() {
        ExportHistory exportHistory = new ExportHistory(this, config, uiConfig);
        exportHistory.setVisible(true);
    }

    private void miStatisticsActionPerformed() {
        SystemStatistics systemStatistics = new SystemStatistics(this, config, uiConfig);
        systemStatistics.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        menuBar1 = new JMenuBar();
        mnuFile = new JMenu();
        miLoadConfig = new JMenuItem();
        miQuit = new JMenuItem();
        mnuTools = new JMenu();
        miExportHistory = new JMenuItem();
        miStatistics = new JMenuItem();
        panel1 = new JPanel();
        contentPane = new JPanel();
        btnBack = new JButton();
        btnNext = new JButton();
        lblStatus = new JLabel();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("ArdenCAT Record Builder");
        Container contentPane2 = getContentPane();

        //======== menuBar1 ========
        {

            //======== mnuFile ========
            {
                mnuFile.setText("File");

                //---- miLoadConfig ----
                miLoadConfig.setText("Load Configuration");
                miLoadConfig.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        miLoadConfigActionPerformed();
                    }
                });
                mnuFile.add(miLoadConfig);

                //---- miQuit ----
                miQuit.setText("Quit");
                miQuit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        miQuitActionPerformed();
                        miQuitActionPerformed();
                    }
                });
                mnuFile.add(miQuit);
            }
            menuBar1.add(mnuFile);

            //======== mnuTools ========
            {
                mnuTools.setText("Tools");

                //---- miExportHistory ----
                miExportHistory.setText("Export History");
                miExportHistory.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        miExportHistoryActionPerformed();
                    }
                });
                mnuTools.add(miExportHistory);

                //---- miStatistics ----
                miStatistics.setText("System Statistics");
                miStatistics.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        miStatisticsActionPerformed();
                    }
                });
                mnuTools.add(miStatistics);
            }
            menuBar1.add(mnuTools);
        }
        setJMenuBar(menuBar1);

        //======== panel1 ========
        {
            panel1.setBorder(new EmptyBorder(12, 12, 12, 12));

            //======== contentPane ========
            {
                contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
            }

            //---- btnBack ----
            btnBack.setText("Back");
            btnBack.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnBackActionPerformed();
                }
            });

            //---- btnNext ----
            btnNext.setText("Next");
            btnNext.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnNextActionPerformed();
                }
            });

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 529, Short.MAX_VALUE)
                        .addComponent(btnNext))
                    .addComponent(contentPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(contentPane, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBack)
                            .addComponent(btnNext)))
            );
        }

        //---- lblStatus ----
        lblStatus.setBorder(new BevelBorder(BevelBorder.LOWERED));
        lblStatus.setText("Ready");

        GroupLayout contentPane2Layout = new GroupLayout(contentPane2);
        contentPane2.setLayout(contentPane2Layout);
        contentPane2Layout.setHorizontalGroup(
            contentPane2Layout.createParallelGroup()
                .addComponent(lblStatus, GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
                .addGroup(contentPane2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
        );
        contentPane2Layout.setVerticalGroup(
            contentPane2Layout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPane2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(18, 18, 18)
                    .addComponent(lblStatus, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
        );
        setSize(745, 420);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JMenuBar menuBar1;
    private JMenu mnuFile;
    private JMenuItem miLoadConfig;
    private JMenuItem miQuit;
    private JMenu mnuTools;
    private JMenuItem miExportHistory;
    private JMenuItem miStatistics;
    private JPanel panel1;
    private JPanel contentPane;
    private JButton btnBack;
    private JButton btnNext;
    private JLabel lblStatus;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
