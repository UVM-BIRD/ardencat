package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.util.Base64Util;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbstorer on 8/5/15.
 */
public class ExportUtil {
    private static final String RECORD_TYPE = "rec";        // note : duplicated in ExportHistory

    public static void markBatchRangeExported(DataSource ardencatDataSource, String source, Object firstBatch, Object lastBatch, int exportId) throws SQLException {
        String query = "update tblACEligibility set exportId = ? where exportId is null and source = ? and batch >= ? and batch <= ?";

        PreparedStatement stmt = ardencatDataSource.getConnection().prepareStatement(query);

        int index = 1;
        stmt.setInt(index++, exportId);
        stmt.setString(index++, source);
        stmt.setObject(index++, firstBatch);
        stmt.setObject(index++, lastBatch);

        stmt.executeUpdate();
    }

    public static int writeExportRecords(DataSource ardencatDataSource, String source, Object firstBatch, Object lastBatch, String method, List<Record> records)
            throws SQLException, IOException {

        String query = "insert into tblACExport (source, firstBatch, lastBatch, method) values (?, ?, ?, ?)";

        PreparedStatement stmt = ardencatDataSource.getConnection().prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, source);
        stmt.setObject(2, firstBatch);
        stmt.setObject(3, lastBatch);
        stmt.setString(4, method);

        int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("log insert failed - no records affected");
        }

        int exportId;

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            exportId = rs.getInt(1);

        } else {
            throw new SQLException("no auto-generated keys found");
        }

        if (records != null) {
            for (Record r : records) {
                writeExportRecord(ardencatDataSource, exportId, r);
            }
        }

        return exportId;
    }

    public static List<Record> buildExportedRecordList(DataSource ardencatDataSource, int exportId) throws SQLException, DataException {
        List<Record> list = new ArrayList<Record>();

        String query = "select data from tblACExportRecord where exportId = ? and recordType = ?";

        PreparedStatement stmt = ardencatDataSource.getConnection().prepareStatement(query);

        stmt.setInt(1, exportId);
        stmt.setString(2, RECORD_TYPE);

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String dataStr = rs.getString(1);
            try {
                Record r = (Record) Base64Util.deserialize(dataStr);
                list.add(r);

            } catch (Exception e) {
                throw new DataException(e);
            }
        }

        return list;
    }

    public static void auditExport(DataSource ardencatDataSource, int exportId, String authSig, String reason) throws SQLException {
        String query = "insert into tblACExportAudit (exportId, authSig, reason) values(?, ?, ?)";

        PreparedStatement stmt = ardencatDataSource.getConnection().prepareStatement(query);

        stmt.setInt(1, exportId);
        stmt.setString(2, authSig);
        stmt.setString(3, reason);

        stmt.executeUpdate();
    }


//////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private static void writeExportRecord(DataSource ardencatDataSource, int exportId, Record record) throws SQLException, IOException {
        String query = "insert into tblACExportRecord (exportId, recordType, data) values(?, ?, ?)";

        PreparedStatement stmt = ardencatDataSource.getConnection().prepareStatement(query);

        stmt.setInt(1, exportId);
        stmt.setString(2, RECORD_TYPE);
        stmt.setString(3, Base64Util.serialize(record));

        stmt.executeUpdate();
    }
}
