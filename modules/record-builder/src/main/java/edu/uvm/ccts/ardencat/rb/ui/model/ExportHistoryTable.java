/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.rb.model.ExportInfo;
import edu.uvm.ccts.common.ui.model.BasicTable;

import java.awt.*;

/**
 * Created by mstorer on 9/19/14.
 */
public class ExportHistoryTable extends BasicTable {
    public ExportInfo getExportInfoAtPoint(Point p) {
        return getExportInfoAtRow(rowAtPoint(p));
    }

    public ExportInfo getExportInfoAtRow( int row) {
        return ((ExportHistoryTableModel) getModel()).getExportInfoAtRow(convertRowIndexToModel(row));
    }
}
