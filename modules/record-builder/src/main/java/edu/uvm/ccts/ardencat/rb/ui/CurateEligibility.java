/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Thu Sep 11 10:06:23 EDT 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.eligibility.model.EligibilityColor;
import edu.uvm.ccts.ardencat.eligibility.model.StatusEligibilityColor;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.ardencat.rb.model.*;
import edu.uvm.ccts.ardencat.rb.ui.model.*;
import edu.uvm.ccts.ardencat.rb.util.EligibilityAuditUtil;
import edu.uvm.ccts.ardencat.rb.util.EligibilityTableReader;
import edu.uvm.ccts.ardencat.rb.util.EligibilityUtil;
import edu.uvm.ccts.ardencat.ui.model.DateRenderer;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.util.ColorUtil;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.TableUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

/**
 * @author Matthew Storer
 */
public class CurateEligibility extends ContentPanel {
    private static final Log log = LogFactory.getLog(CurateEligibility.class);

    private StandardRecordBuilderConfig config = null;
    private String firstBatch = null;
    private String currentLastBatch = null;
    private boolean populatingBatchList = false;

    public CurateEligibility(UIConfig uiConfig) {
        super(uiConfig);
        initComponents();

        setNavLabels("Back", "Next");

        pnlRandomization.setVisible("true".equalsIgnoreCase(uiConfig.getProperty("curate.allowRandomize")));

        lblFirstBatch.setBorder(BorderFactory.createEmptyBorder(4, 16, 4, 16));

        TableUtil.makeAutoResizable(tblEligibilityData);
        TableUtil.addContextMenu(tblEligibilityData);
    }

    @Override
    protected boolean mayProceed() {
        return getEligibleRecords().size() > 0;
    }

    public boolean hasBatches() {
        return firstBatch != null;
    }

    public String getFirstBatch() {
        return firstBatch;
    }

    public String getLastBatch() {
        return String.valueOf(cboLastBatch.getSelectedItem());
    }

    public void load(JPanel contentPane, StandardRecordBuilderConfig config) {
        if (this.config != config) {
            this.config = config;
            setup();
            loadEligibilityData();
        }

        load(contentPane);
        enableDisableNextButton();
    }

    @SuppressWarnings("unchecked")
    private void setup() {
        firstBatch = null;
        lblFirstBatch.setText(null);
        cboLastBatch.removeAllItems();
        currentLastBatch = null;

        populatingBatchList = true;
        for (String batch : EligibilityUtil.buildBatchList(config)) {
            if (firstBatch == null) {
                firstBatch = batch;
                lblFirstBatch.setText(firstBatch);
            }
            cboLastBatch.addItem(batch);
        }
        populatingBatchList = false;

        populateLegend(pnlStatusFilter, config.getEligibilityConfig().getCustomColors());
    }

    public UIState getUIState() {
        UIState state = new UIState();

        state.setProperty("firstBatch", firstBatch);
        state.setProperty("lastBatch", cboLastBatch.getSelectedItem());

        state.setProperty("splitPaneDividerLocation", splitPane.getDividerLocation());

        if (legendItems != null) {
            Map<String, Boolean> legendItemCheckMap = new HashMap<String, Boolean>();
            for (JLegendItemPanel legendItem : legendItems) {
                legendItemCheckMap.put(legendItem.getColor().getLabel(), legendItem.getCheckbox().isSelected());
            }
            state.setProperty("legendItemCheckMap", legendItemCheckMap);
        }

        state.setProperty("sortKeys", serializeSortKeys(tblEligibilityData.getRowSorter().getSortKeys()));
        state.setProperty("firstVisibleRow", tblEligibilityData.rowAtPoint(spEligibilityData.getViewport().getViewPosition()));
        state.setProperty("selectedRow", tblEligibilityData.getSelectedRow());

        return state;
    }

    @SuppressWarnings("unchecked")
    public void restoreUIState(UIState state, StandardRecordBuilderConfig config) throws ConfigurationException {
        this.config = config;

        setup();

        String firstBatchStr = state.getString("firstBatch");
        if ( ! firstBatchStr.equals(firstBatch) ) {
            throw new ConfigurationException("first batch mismatch {expected=" + firstBatchStr + "}");
        }

        boolean matchedLastBatch = false;
        String lastBatchStr = state.getString("lastBatch");
        for (int i = 0; i < cboLastBatch.getItemCount(); i ++) {
            if (lastBatchStr.equals(String.valueOf(cboLastBatch.getItemAt(i)))) {
                cboLastBatch.setSelectedIndex(i);
                matchedLastBatch = true;
                break;
            }
        }

        if ( ! matchedLastBatch ) {
            throw new ConfigurationException("last batch mismatch {expected=" + lastBatchStr + "}");
        }

        Integer divLoc = state.getInteger("splitPaneDividerLocation");
        if (divLoc != null) splitPane.setDividerLocation(divLoc);

        Map<String, Boolean> legendItemCheckMap = (Map<String, Boolean>) state.getProperty("legendItemCheckMap");
        if (legendItemCheckMap != null && legendItems != null) {
            for (JLegendItemPanel legendItem : legendItems) {
                Boolean checked = legendItemCheckMap.get(legendItem.getColor().getLabel());
                if (checked != null) {
                    legendItem.getCheckbox().setSelected(checked);
                    doChkLegendItemActionPerformed(legendItem.getCheckbox());
                }
            }
        }

        loadEligibilityData();

        List<SortKeyData> sortKeyDataList = (List<SortKeyData>) state.getProperty("sortKeys");
        if (sortKeyDataList != null && sortKeyDataList.size() > 0) {
            List<? extends RowSorter.SortKey> sortKeys = deserializeSortKeys(sortKeyDataList);
            tblEligibilityData.getRowSorter().setSortKeys(sortKeys);
        }

        Integer firstVisibleRow = state.getInteger("firstVisibleRow");
        if (firstVisibleRow != null) {
            tblEligibilityData.scrollRectToVisible(new Rectangle(tblEligibilityData.getCellRect(firstVisibleRow, 0, true)));
        }

        Integer selectedRow = state.getInteger("selectedRow");
        if (selectedRow != null && selectedRow >= 0) {
            tblEligibilityData.setRowSelectionInterval(selectedRow, selectedRow);
        }

        flagAsLoaded();
    }

    public List<EligibilityRecord> getEligibleRecords() {
        List<EligibilityRecord> eligibleRecords = new ArrayList<EligibilityRecord>();

        TableModel model = tblEligibilityData.getModel();
        if (model instanceof EligibilityTableModel) {       // will be DefaultTableModel on first call
            List<EligibilityRecord> records = ((EligibilityTableModel) model).getList();

            for (EligibilityRecord rec : records) {
                if (rec.getStatus() == EligibilityStatus.ELIGIBLE) {
                    eligibleRecords.add(rec);
                }
            }
        }

        return shouldRandomize() ?
                getRandomSubset(eligibleRecords, randomRecordCount()) :
                eligibleRecords;
    }


///////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void enableDisableNextButton() {
        uiConfig.getNextButton().setEnabled(mayProceed());
    }

    private void loadEligibilityData() {
        if (populatingBatchList) return;
        if (firstBatch == null) return;

        String lastBatch = String.valueOf(cboLastBatch.getSelectedItem());
        if (lastBatch.equals(currentLastBatch)) return;

        log.info("loading eligibility data for batches " + lblFirstBatch.getText() + " - " + lastBatch);

        currentLastBatch = lastBatch;

        try {
            EligibilityUIConfig eCfg = config.getEligibilityConfig();

            List<EligibilityRecord> list = EligibilityTableReader.buildList(
                    config.getArdencatDataSource(), eCfg.getSource(),
                    eCfg.getKeyFields(), eCfg.getSortByFields(),
                    firstBatch, lastBatch);

            if (tblEligibilityData.getModel() instanceof EligibilityTableModel) {
                ((EligibilityTableModel) tblEligibilityData.getModel()).setList(list);

            } else {
                tblEligibilityData.setModel(new EligibilityTableModel(list, config, uiConfig, btnRevert));
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " loading eligibility data - " + e.getMessage(), e);
        }

        cboRandCount.setEnabled(false);

        TableCellEditor filterStatusEditor = new FilterStatusEditor();
        filterStatusEditor.addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                refreshRandCountList();
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
            }
        });
        tblEligibilityData.setDefaultEditor(EligibilityStatus.class, filterStatusEditor);
        refreshRandCountList();

        if ("true".equalsIgnoreCase(uiConfig.getProperty("curate.dateDisplay.showTime"))) {
            tblEligibilityData.setDefaultRenderer(Date.class, new DateTimeRenderer());

        } else {
            tblEligibilityData.setDefaultRenderer(Date.class, new DateRenderer());
        }

        tblEligibilityData.getColumnModel().setColumnMargin(0);

        TableUtil.adjustColumnWidths(tblEligibilityData);
    }

    @SuppressWarnings("unchecked")
    private void refreshRandCountList() {
        EligibilityTableModel model = (EligibilityTableModel) tblEligibilityData.getModel();

        int count = 0;
        for (EligibilityRecord rec : model.getList()) {
            if (rec.getStatus() == EligibilityStatus.ELIGIBLE) {
                count ++;
            }
        }

        if (count > 0) count --;    // if randomizing, we implicitly want a subset of records, not all of them

        if (count == 0) {
            if (chkRand.isEnabled()) chkRand.setEnabled(false);

        } else {
            if ( ! chkRand.isEnabled() ) chkRand.setEnabled(true);

            if (count < cboRandCount.getItemCount()) {
                for (int i = cboRandCount.getItemCount() - 1; i >= count; i --) {
                    cboRandCount.removeItemAt(i);
                }

            } else if (count > cboRandCount.getItemCount()) {
                for (int i = cboRandCount.getItemCount() + 1; i <= count; i ++) {
                    cboRandCount.addItem(i);
                }
            }
        }
    }

    private boolean shouldRandomize() {
        return chkRand.isEnabled() && chkRand.isSelected();
    }

    private int randomRecordCount() {
        return shouldRandomize() ? (Integer) cboRandCount.getSelectedItem() : 0;
    }

    private List<EligibilityRecord> getRandomSubset(List<EligibilityRecord> recs, int recsToGet) {
        if (recsToGet <= 0)             return new ArrayList<EligibilityRecord>();
        if (recsToGet >= recs.size())   return recs;

        List<Integer> indexList = new ArrayList<Integer>();
        for (int i = 0; i < recs.size(); i ++) indexList.add(i);    // so we can remove those we've already
                                                                    // found, and not add them again

        List<EligibilityRecord> randomRecords = new ArrayList<EligibilityRecord>();
        for (int i = 0; i < recsToGet; i ++) {
            int r = (int) (Math.random() * indexList.size());       // a random index to retrieve
            int index = indexList.get(r);                           // the random index
            randomRecords.add(recs.get(index));                     // collect the random record
            indexList.remove(r);                                    // remove the random index from the index list,
                                                                    // so as to not inadvertently get it again
        }

        return randomRecords;
    }


/////////////////////////////////////////////////////////////////////////////////////////
// sort key serialization / deserialization stuff
//

    private static final class SortKeyData implements Serializable {
        private static final int ASCENDING = 0;
        private static final int DESCENDING = 1;
        private static final int UNSORTED = 2;

        private int column;
        private int sortOrder;

        public SortKeyData(RowSorter.SortKey sortKey) {
            column = sortKey.getColumn();
            switch (sortKey.getSortOrder()) {
                case ASCENDING:     sortOrder = ASCENDING; break;
                case DESCENDING:    sortOrder = DESCENDING; break;
                default:            sortOrder = UNSORTED;
            }
        }

        public RowSorter.SortKey asSortKey() {
            SortOrder sortOrder;
            switch (this.sortOrder) {
                case ASCENDING:     sortOrder = SortOrder.ASCENDING; break;
                case DESCENDING:    sortOrder = SortOrder.DESCENDING; break;
                default:            sortOrder = SortOrder.UNSORTED;
            }

            return new RowSorter.SortKey(column, sortOrder);
        }
    }

    private List<? extends RowSorter.SortKey> deserializeSortKeys(List<SortKeyData> list) {
        List<RowSorter.SortKey> sortKeyList = new ArrayList<RowSorter.SortKey>();

        for (SortKeyData skd : list) {
            sortKeyList.add(skd.asSortKey());
        }

        return sortKeyList;
    }

    private List<SortKeyData> serializeSortKeys(List<? extends RowSorter.SortKey> list) {
        List<SortKeyData> dataList = new ArrayList<SortKeyData>();

        for (RowSorter.SortKey sk : list) {
            dataList.add(new SortKeyData(sk));
        }

        return dataList;
    }


/////////////////////////////////////////////////////////////////////////////////////////
// legend / status filter
//

    private List<JLegendItemPanel> legendItems = null;

    private void populateLegend(JPanel container, List<CustomEligibilityColor> colorList) {
        legendItems = buildLegendItems(colorList);
        GroupLayout contentPanelLayout = new GroupLayout(container);
        container.setLayout(contentPanelLayout);
        contentPanelLayout.setHorizontalGroup(buildHorizontalGroup(contentPanelLayout, legendItems));
        contentPanelLayout.setVerticalGroup(buildVerticalGroup(contentPanelLayout, legendItems));
    }

    private List<JLegendItemPanel> buildLegendItems(List<CustomEligibilityColor> colorList) {
        List<JLegendItemPanel> list = new ArrayList<JLegendItemPanel>();

        EligibilityConfig eCfg = config.getEligibilityConfig();

        int i = 0;
        list.add(buildLegendItem(i++, eCfg.getEligibleColor(), false));
        for (CustomEligibilityColor color : colorList) {
            if (color.getStatus() == EligibilityStatus.ELIGIBLE) {
                list.add(buildLegendItem(i++, color, true));
            }
        }

        list.add(buildLegendItem(i++, eCfg.getUnknownColor(), false));
        for (CustomEligibilityColor color : colorList) {
            if (color.getStatus() == EligibilityStatus.UNKNOWN) {
                list.add(buildLegendItem(i++, color, true));
            }
        }

        list.add(buildLegendItem(i++, eCfg.getIneligibleColor(), false));
        for (CustomEligibilityColor color : colorList) {
            if (color.getStatus() == EligibilityStatus.INELIGIBLE) {
                list.add(buildLegendItem(i++, color, true));
            }
        }

        for (CustomEligibilityColor color : colorList) {
            if (color.getStatus() == null) {
                list.add(buildLegendItem(i++, color, false));
            }
        }

        return list;
    }

    private GroupLayout.ParallelGroup buildHorizontalGroup(GroupLayout layout, List<? extends JPanel> legendItems) {
        GroupLayout.ParallelGroup parallelGroup = layout.createParallelGroup();

        for (JPanel pnlLegendItem : legendItems) {
            if (pnlLegendItem instanceof JIndentedLegendItemPanel) {
                parallelGroup.addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(pnlLegendItem, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

            } else {
                parallelGroup.addComponent(pnlLegendItem, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
            }
        }

        GroupLayout.SequentialGroup sequentialGroup = layout.createSequentialGroup();
        sequentialGroup.addContainerGap()
                .addGroup(parallelGroup)
                .addContainerGap();

        return layout.createParallelGroup().addGroup(sequentialGroup);
    }

    private GroupLayout.ParallelGroup buildVerticalGroup(GroupLayout layout, List<? extends JPanel> legendItems) {
        GroupLayout.SequentialGroup sequentialGroup = layout.createSequentialGroup();

        if (legendItems.size() > 0) {
            sequentialGroup.addContainerGap()
                    .addComponent(legendItems.get(0), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                            GroupLayout.PREFERRED_SIZE);

            for (int i = 1; i < legendItems.size(); i ++) {
                sequentialGroup.addGap(10, 10, 10)
                        .addComponent(legendItems.get(i), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE);
            }
            sequentialGroup.addContainerGap();
        }

        return layout.createParallelGroup().addGroup(sequentialGroup);
    }

    private JLegendItemPanel buildLegendItem(int i, EligibilityColor color, boolean indent) {
        JEligibilityColorCheckBox chkLegendItem = new JEligibilityColorCheckBox(color);

        chkLegendItem.setName("chkLegendItem_" + i);
        chkLegendItem.setSelected(true);
        chkLegendItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chkLegendItemActionPerformed(e);
            }
        });

        return indent ?
                new JIndentedLegendItemPanel(chkLegendItem) :
                new JLegendItemPanel(chkLegendItem);
    }

    private void chkLegendItemActionPerformed(ActionEvent e) {
        JEligibilityColorCheckBox checkBox = (JEligibilityColorCheckBox) e.getSource();
        doChkLegendItemActionPerformed(checkBox);
    }

    private void doChkLegendItemActionPerformed(JEligibilityColorCheckBox checkBox) {

        EligibilityColor color = checkBox.getColor();
        if (color instanceof StatusEligibilityColor) {
            for (Component c1 : pnlStatusFilter.getComponents()) {
                if (c1 instanceof JIndentedLegendItemPanel) {
                    JEligibilityColorCheckBox ecChk = ((JIndentedLegendItemPanel) c1).getCheckbox();
                    if (color.getStatus() == ecChk.getColor().getStatus()) {
                        ecChk.setEnabled(checkBox.isSelected());
                    }
                }
            }
        }

        ((EligibilityTableModel) tblEligibilityData.getModel()).setVisibility(checkBox.getColor(), checkBox.isSelected());
        TableUtil.adjustColumnWidth(tblEligibilityData, 0);
    }

    private static final class JIndentedLegendItemPanel extends JLegendItemPanel {
        public JIndentedLegendItemPanel(JEligibilityColorCheckBox checkBox) {
            super(checkBox);
        }
    }

    private static class JLegendItemPanel extends JPanel {
        private JEligibilityColorCheckBox checkBox;

        public JLegendItemPanel(JEligibilityColorCheckBox checkBox) {
            this.checkBox = checkBox;

            setBorder(LineBorder.createBlackLineBorder());

            GroupLayout layout = new GroupLayout(this);
            setLayout(layout);
            layout.setHorizontalGroup(
                    layout.createParallelGroup()
                    .addComponent(checkBox, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
            );
            layout.setVerticalGroup(
                    layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(checkBox))
            );
        }

        public JEligibilityColorCheckBox getCheckbox() {
            return checkBox;
        }

        public EligibilityColor getColor() {
            return checkBox.getColor();
        }
    }

    private static final class JEligibilityColorCheckBox extends JCheckBox {
        private EligibilityColor color;

        public JEligibilityColorCheckBox(EligibilityColor color) {
            this.color = color;
            setText(color.getLabel());
            setOpaque(true);
            setBackground(color.getColor());
            setForeground(ColorUtil.foregroundForColor(color.getColor()));
        }

        public EligibilityColor getColor() {
            return color;
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////


    private void chkRandActionPerformed(ActionEvent e) {
        JCheckBox cb = (JCheckBox) e.getSource();
        cboRandCount.setEnabled(cb.isEnabled() && cb.isSelected());
    }

    private void btnRevertActionPerformed() {
        boolean revert = DialogUtil.showConfirmDialog(this,
                "Are you sure you want to revert changes?  This action cannot be undone.");

        boolean audit = "true".equalsIgnoreCase(uiConfig.getProperty("curate.auditChanges"));
        String auditSig = null;
        String auditReason = null;

        if (revert && audit) {
            AuditForm af = new AuditForm(uiConfig.getFrame(), "Eligibility Status Override Audit");
            af.setVisible(true);

            if (af.isCancelled()) {
                revert = false;

            } else {
                auditSig = af.getSignature();
                auditReason = af.getReason();
            }
        }

        if (revert) {
            clearStatusOverrides(audit, auditSig, auditReason);

            log.info("btnRevertActionPerformed : loading eligibility data");
            loadEligibilityData();

            btnRevert.setEnabled(false);
            uiConfig.setStatus("Changes reverted.");
        }
    }

    private void clearStatusOverrides(boolean audit, String auditSig, String auditReason) {
        try {
            DataSource ds = config.getArdencatDataSource();

            String clearOverrideQuery = "update tblACEligibility set overrideStatus = null where id = ?";
            PreparedStatement updateStmt = ds.getConnection().prepareStatement(clearOverrideQuery);

            for (EligibilityStatusInfo esi : getOverriddenRecordStatusInfo()) {
                if (audit) {
                    EligibilityAuditUtil.saveAuditRecord(ds, esi.getId(), esi.getOverrideStatus(),
                            esi.getOriginalStatus(), auditSig, auditReason);
                }

                updateStmt.setInt(1, esi.getId());

                log.info("clearStatusOverrides : clearing status overrides for id=" + esi.getId());

                updateStmt.executeUpdate();
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " clearing status overrides - " + e.getMessage(), e);
        }
    }

    private List<EligibilityStatusInfo> getOverriddenRecordStatusInfo() throws SQLException {
        List<EligibilityStatusInfo> list = new ArrayList<EligibilityStatusInfo>();

        if (firstBatch == null) return list;

        Object lastBatch = cboLastBatch.getSelectedItem();

        StringBuilder sb = new StringBuilder();
        sb.append("select id, status, overrideStatus from tblACEligibility where overrideStatus is not null and source = ?");

        if (lastBatch != null) {
            sb.append(" and batch >= ? and batch <= ?");

        } else {
            sb.append(" and batch = ?");
        }

        Connection conn = config.getArdencatDataSource().getConnection();
        PreparedStatement stmt = conn.prepareStatement(sb.toString());

        int index = 1;

        stmt.setString(index++, config.getEligibilityConfig().getSource());
        stmt.setObject(index++, firstBatch);

        if (lastBatch != null) stmt.setObject(index++, lastBatch);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            int id = rs.getInt(1);
            EligibilityStatus status = EligibilityStatus.fromString(rs.getString(2));
            EligibilityStatus overrideStatus = EligibilityStatus.fromString(rs.getString(3));

            list.add(new EligibilityStatusInfo(id, status, overrideStatus));
        }

        return list;
    }

    private void cboLastBatchActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        try {
            loadEligibilityData();

        } finally {
            this.setCursor(Cursor.getDefaultCursor());
        }
    }

    private void tblEligibilityDataMousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            int col = tblEligibilityData.convertColumnIndexToModel(tblEligibilityData.columnAtPoint(e.getPoint()));

            if (col == EligibilityTableModel.ICONS_COL) {
                EligibilityRecord rec = ((EligibilityTable) tblEligibilityData).getRecordAtPoint(e.getPoint());
                if (rec.hasAudits()) {
                    new EligibilityAuditHistory(DialogUtil.getParentFrame(this), rec).setVisible(true);
                }
            }
        }
    }

    private static final class EligibilityStatusInfo {
        private int id;
        private EligibilityStatus originalStatus;
        private EligibilityStatus overrideStatus;

        private EligibilityStatusInfo(int id, EligibilityStatus originalStatus, EligibilityStatus overrideStatus) {
            this.id = id;
            this.originalStatus = originalStatus;
            this.overrideStatus = overrideStatus;
        }

        public int getId() {
            return id;
        }

        public EligibilityStatus getOriginalStatus() {
            return originalStatus;
        }

        public EligibilityStatus getOverrideStatus() {
            return overrideStatus;
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        label1 = new JLabel();
        label2 = new JLabel();
        splitPane = new JSplitPane();
        scrollPane1 = new JScrollPane();
        pnlControlPanel = new JPanel();
        pnlBatchSelection = new JPanel();
        label3 = new JLabel();
        label4 = new JLabel();
        cboLastBatch = new JComboBox();
        lblFirstBatch = new JLabel();
        pnlStatusFilter = new JPanel();
        pnlRandomization = new JPanel();
        chkRand = new JCheckBox();
        label5 = new JLabel();
        cboRandCount = new JComboBox();
        btnRevert = new JButton();
        spEligibilityData = new JScrollPane();
        tblEligibilityData = new EligibilityTable();

        //======== this ========

        //---- label1 ----
        label1.setText("Eligibility Status Curation");
        label1.setFont(new Font("Lucida Grande", Font.BOLD, 20));

        //---- label2 ----
        label2.setText("Use this screen to curate the eligibility status of identified records.");

        //======== splitPane ========
        {
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation(230);
            splitPane.setDividerSize(12);

            //======== scrollPane1 ========
            {
                scrollPane1.setPreferredSize(new Dimension(220, 364));

                //======== pnlControlPanel ========
                {
                    pnlControlPanel.setPreferredSize(new Dimension(200, 360));
                    pnlControlPanel.setMinimumSize(new Dimension(160, 360));

                    //======== pnlBatchSelection ========
                    {
                        pnlBatchSelection.setBorder(new TitledBorder("Batch Selection"));

                        //---- label3 ----
                        label3.setText("From:");

                        //---- label4 ----
                        label4.setText("To:");

                        //---- cboLastBatch ----
                        cboLastBatch.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                cboLastBatchActionPerformed();
                            }
                        });

                        //---- lblFirstBatch ----
                        lblFirstBatch.setText("text");

                        GroupLayout pnlBatchSelectionLayout = new GroupLayout(pnlBatchSelection);
                        pnlBatchSelection.setLayout(pnlBatchSelectionLayout);
                        pnlBatchSelectionLayout.setHorizontalGroup(
                            pnlBatchSelectionLayout.createParallelGroup()
                                .addGroup(pnlBatchSelectionLayout.createSequentialGroup()
                                    .addGroup(pnlBatchSelectionLayout.createParallelGroup()
                                        .addComponent(label3)
                                        .addComponent(label4))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(pnlBatchSelectionLayout.createParallelGroup()
                                        .addComponent(cboLastBatch, GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                        .addComponent(lblFirstBatch, GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)))
                        );
                        pnlBatchSelectionLayout.setVerticalGroup(
                            pnlBatchSelectionLayout.createParallelGroup()
                                .addGroup(pnlBatchSelectionLayout.createSequentialGroup()
                                    .addGroup(pnlBatchSelectionLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label3)
                                        .addComponent(lblFirstBatch))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(pnlBatchSelectionLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label4)
                                        .addComponent(cboLastBatch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        );
                    }

                    //======== pnlStatusFilter ========
                    {
                        pnlStatusFilter.setBorder(new TitledBorder("Status Filter"));

                        GroupLayout pnlStatusFilterLayout = new GroupLayout(pnlStatusFilter);
                        pnlStatusFilter.setLayout(pnlStatusFilterLayout);
                        pnlStatusFilterLayout.setHorizontalGroup(
                            pnlStatusFilterLayout.createParallelGroup()
                                .addGap(0, 157, Short.MAX_VALUE)
                        );
                        pnlStatusFilterLayout.setVerticalGroup(
                            pnlStatusFilterLayout.createParallelGroup()
                                .addGap(0, 53, Short.MAX_VALUE)
                        );
                    }

                    //======== pnlRandomization ========
                    {
                        pnlRandomization.setBorder(new TitledBorder("Randomization"));

                        //---- chkRand ----
                        chkRand.setText("Randomize?");
                        chkRand.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                chkRandActionPerformed(e);
                            }
                        });

                        //---- label5 ----
                        label5.setText("Count:");

                        //---- cboRandCount ----
                        cboRandCount.setEnabled(false);

                        GroupLayout pnlRandomizationLayout = new GroupLayout(pnlRandomization);
                        pnlRandomization.setLayout(pnlRandomizationLayout);
                        pnlRandomizationLayout.setHorizontalGroup(
                            pnlRandomizationLayout.createParallelGroup()
                                .addGroup(pnlRandomizationLayout.createSequentialGroup()
                                    .addComponent(chkRand)
                                    .addGap(0, 51, Short.MAX_VALUE))
                                .addGroup(pnlRandomizationLayout.createSequentialGroup()
                                    .addComponent(label5)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cboRandCount, GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                        );
                        pnlRandomizationLayout.setVerticalGroup(
                            pnlRandomizationLayout.createParallelGroup()
                                .addGroup(pnlRandomizationLayout.createSequentialGroup()
                                    .addComponent(chkRand)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(pnlRandomizationLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label5)
                                        .addComponent(cboRandCount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        );
                    }

                    //---- btnRevert ----
                    btnRevert.setText("Revert Changes");
                    btnRevert.setEnabled(false);
                    btnRevert.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnRevertActionPerformed();
                        }
                    });

                    GroupLayout pnlControlPanelLayout = new GroupLayout(pnlControlPanel);
                    pnlControlPanel.setLayout(pnlControlPanelLayout);
                    pnlControlPanelLayout.setHorizontalGroup(
                        pnlControlPanelLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, pnlControlPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlControlPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addGroup(pnlControlPanelLayout.createSequentialGroup()
                                        .addGap(0, 28, Short.MAX_VALUE)
                                        .addComponent(btnRevert))
                                    .addComponent(pnlBatchSelection, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlStatusFilter, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlRandomization, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                    );
                    pnlControlPanelLayout.setVerticalGroup(
                        pnlControlPanelLayout.createParallelGroup()
                            .addGroup(pnlControlPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(pnlBatchSelection, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pnlStatusFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pnlRandomization, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRevert)
                                .addContainerGap(45, Short.MAX_VALUE))
                    );
                }
                scrollPane1.setViewportView(pnlControlPanel);
            }
            splitPane.setLeftComponent(scrollPane1);

            //======== spEligibilityData ========
            {

                //---- tblEligibilityData ----
                tblEligibilityData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                tblEligibilityData.setAutoCreateRowSorter(true);
                tblEligibilityData.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        tblEligibilityDataMousePressed(e);
                    }
                });
                spEligibilityData.setViewportView(tblEligibilityData);
            }
            splitPane.setRightComponent(spEligibilityData);
        }

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(label2))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(label2)
                    .addGap(18, 18, 18)
                    .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                    .addGap(20, 20, 20))
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JLabel label1;
    private JLabel label2;
    private JSplitPane splitPane;
    private JScrollPane scrollPane1;
    private JPanel pnlControlPanel;
    private JPanel pnlBatchSelection;
    private JLabel label3;
    private JLabel label4;
    private JComboBox cboLastBatch;
    private JLabel lblFirstBatch;
    private JPanel pnlStatusFilter;
    private JPanel pnlRandomization;
    private JCheckBox chkRand;
    private JLabel label5;
    private JComboBox cboRandCount;
    private JButton btnRevert;
    private JScrollPane spEligibilityData;
    private JTable tblEligibilityData;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
