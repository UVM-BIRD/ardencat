/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb;

import edu.uvm.ccts.ardencat.evaluation.model.EvaluationResults;
import edu.uvm.ccts.ardencat.model.FieldObject;
import edu.uvm.ccts.common.exceptions.ValidationException;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mstorer on 10/14/13.
 */
public class AttributeEvaluator {
    private String filename;
    private String fieldName = null;
    private Set<Object> expectedValues = new HashSet<Object>();

    public AttributeEvaluator(String filename) throws IOException {
        this.filename = filename;

        Path path = Paths.get(filename);
        InputStream in = new FileInputStream(path.toFile());
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        Set<String> col = new HashSet<String>(); // ensure unique values
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.trim().isEmpty()) continue;

                if (fieldName == null)  fieldName = line.trim();
                else                    col.add(line.trim());
            }

        } finally {
            reader.close();
        }

        try {
            for (String s : col) {
                expectedValues.add(Integer.parseInt(s));
            }

        } catch (NumberFormatException e) {
            expectedValues.clear();
            for (String s : col) {
                expectedValues.add(s);
            }
        }
    }

    public String getFilename() {
        return filename;
    }

    public String getFieldName() {
        return fieldName;
    }

    public EvaluationResults execute(Collection<? extends FieldObject> fieldObjects) throws ValidationException, FileNotFoundException {
        Set<Object> actualValues = pluck(fieldName, fieldObjects);
        return new EvaluationResults(fieldName, expectedValues, actualValues);
    }

    private Set<Object> pluck(String field, Collection<? extends FieldObject> fieldObjects) throws ValidationException {
        Set<Object> values = new HashSet<Object>();

        if (fieldObjects.size() > 0) {
            if ( ! fieldObjects.iterator().next().hasField(field) )
                throw new ValidationException("object does not contain field '" + field + "'");

            // try to build as integer first
            try {
                for (FieldObject fo : fieldObjects) {
                    Object o = fo.getField(field);
                    if (o != null) {
                        String s = o.toString().trim();
                        if ( ! s.isEmpty() ) values.add(Integer.parseInt(s));
                    }
                }

            } catch (NumberFormatException e) {
                values.clear();
                for (FieldObject r : fieldObjects) {
                    Object o = r.getField(field);
                    if (o != null) {
                        String s = o.toString().trim();
                        if ( ! s.isEmpty() ) values.add(s);
                    }
                }
            }
        }

        return values;
    }
}
