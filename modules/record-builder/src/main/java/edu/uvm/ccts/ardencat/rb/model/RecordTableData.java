/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import java.util.*;

/**
 * Created by mstorer on 1/31/14.
 */
public class RecordTableData {
    private List<Record> records;
    private boolean includeEligibilityData;
    private Map<String, Class> columnNameClassMap;
    private List<String> columnNames;

    public RecordTableData(List<Record> records, boolean includeEligibilityData) {
        this.records = records;
        this.includeEligibilityData = includeEligibilityData;
        columnNameClassMap = buildColumnNameClassMap(records);
        columnNames = new ArrayList<String>(columnNameClassMap.keySet());
    }

    public List<Record> getRecords() {
        return records;
    }

    public Record getRecord(int rowIndex) {
        return records.get(rowIndex);
    }

    public int getColumnCount() {
        return columnNames.size();
    }

    public String getColumnName(int columnIndex) {
        return columnNames.get(columnIndex);
    }

    public Class getColumnClass(int columnIndex) {
        return columnNameClassMap.get(getColumnName(columnIndex));
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public int getRowCount() {
        return records.size();
    }

    public List<List<Object>> buildRows(boolean identifyRecords) {
        List<List<Object>> rows = new ArrayList<List<Object>>();

        for (Record r : records) {
            Map<String, Object> flattened = r.flatten(identifyRecords, includeEligibilityData);
            List<Object> row = new ArrayList<Object>();

            for (String col : columnNames) {
                if (flattened.containsKey(col)) row.add(flattened.get(col));
                else                            row.add(null);
            }

            rows.add(row);
        }

        return rows;
    }

    private Map<String, Class> buildColumnNameClassMap(Collection<Record> records) {
        List<String> columns = new ArrayList<String>();
        Map<String, Class> colClassMap = new HashMap<String, Class>();

        for (Record r : records) {
            Map<String, Object> flattened = r.flatten(true, includeEligibilityData);

            for (Map.Entry<String, Object> entry : flattened.entrySet()) {
                if ( ! colClassMap.containsKey(entry.getKey()) ) {
                    colClassMap.put(entry.getKey(), entry.getValue().getClass());
                }
            }

            List<String> cols = new ArrayList<String>(flattened.keySet());

            for (int i = 0; i < cols.size(); i ++) {
                String col = cols.get(i);
                int index = columns.indexOf(col);

                if (index < 0) {                // not found - if found, we don't care - it's already in the list
                    int insertPos = -1;         // where we will insert into columns; if insertion is appropriate, will be >= 0
                    int colCount = 1;           // the number of cols to insert, starting with the current one


                    for (int j = i + 1; j < cols.size(); j ++) {    // determine whether or not we should insert, and
                        int idx = columns.indexOf(cols.get(j));     // if we should, HOW MANY and WHERE.
                        if (idx >= 0) {
                            insertPos = idx;                        // this is the WHERE
                            break;
                        }
                        colCount ++;                                // this is the HOW MANY
                    }


                    if (insertPos >= 0) {
                        List<String> subList = cols.subList(i, i + colCount);       // this is the WHAT
                        columns.addAll(insertPos, subList);
                        i = i + colCount - 1;       // this should advance the pointer to the next col after the block
                                                    // of cols we just added.  the "- 1" is because i will increment again
                                                    // after re-entering the loop on the next pass

                    } else {
                        columns.add(col);           // col is new, so append to the end
                    }
                }
            }
        }

        Map<String, Class> map = new LinkedHashMap<String, Class>();
        for (String col : columns) {
            map.put(col, colClassMap.get(col));
        }

        return map;
    }
}
