/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.rb.model.AbstractRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.BatchStatistics;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 9/26/14.
 */
public class StatisticsReader {
    private static final Log log = LogFactory.getLog(StatisticsReader.class);

    public static List<BatchStatistics> buildStatistics(AbstractRecordBuilderConfig config) {
        List<BatchStatistics> list = new ArrayList<BatchStatistics>();

        try {
            for (String batch : buildBatchList(config)) {
                list.add(buildBatchStatistics(config, batch));
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " building batch statistics - " + e.getMessage(), e);
        }

        return list;
    }

    private static List<String> buildBatchList(AbstractRecordBuilderConfig config) throws SQLException {
        List<String> list = new ArrayList<String>();

        String query = "select distinct batch from tblACEligibility order by batch";

        PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            list.add(rs.getString(1));
        }

        return list;
    }

    private static BatchStatistics buildBatchStatistics(AbstractRecordBuilderConfig config, String batch) throws SQLException, ParseException {
        BatchStatistics stats = new BatchStatistics();

        String query = "select status, overrideStatus, created, exportId from tblACEligibility where source = ? and batch = ?";

        PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

        int index = 1;
        stmt.setString(index++, config.getEligibilityConfig().getSource());
        stmt.setString(index++, batch);

        ResultSet rs = stmt.executeQuery();

        int numEligible = 0;
        int numEligibleOverridden = 0;
        int numUnknown = 0;
        int numUnknownOverridden = 0;
        int numIneligible = 0;
        int numIneligibleOverridden = 0;
        Date generated = null;
        Integer exportId = null;

        while (rs.next()) {
            EligibilityStatus status = EligibilityStatus.fromString(rs.getString(1));
            String overrideStatus = rs.getString(2);

            switch (status) {
                case ELIGIBLE:
                    numEligible ++;
                    if (overrideStatus != null) numEligibleOverridden ++;
                    break;
                case UNKNOWN:
                    numUnknown ++;
                    if (overrideStatus != null) numUnknownOverridden ++;
                    break;
                case INELIGIBLE:
                    numIneligible ++;
                    if (overrideStatus != null) numIneligibleOverridden ++;
                    break;
            }

            if (generated == null) generated = buildResultSetDate(rs, 3);
            if (exportId == null) exportId = rs.getInt(4);
        }

        stats.setBatch(batch);
        stats.setNumEligible(numEligible);
        stats.setNumEligibleOverridden(numEligibleOverridden);
        stats.setNumUnknown(numUnknown);
        stats.setNumUnknownOverridden(numUnknownOverridden);
        stats.setNumIneligible(numIneligible);
        stats.setNumIneligibleOverridden(numIneligibleOverridden);
        stats.setGeneratedDate(generated);
        if (exportId != null) stats.setExportedDate(getExportedDate(config, exportId));

        return stats;
    }

    private static Date getExportedDate(AbstractRecordBuilderConfig config, int exportId) {
        try {
            String query = "select created from tblACExport where id = ?";

            PreparedStatement stmt = config.getArdencatDataSource().getConnection().prepareStatement(query);

            stmt.setInt(1, exportId);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return buildResultSetDate(rs, 1);
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " getting export date for exportId=" + exportId +
                    " - " + e.getMessage(), e);
        }

        return null;
    }

    private static Date buildResultSetDate(ResultSet rs, int index) throws SQLException {
        Object obj = rs.getObject(index);
        try {
            return DateUtil.castToDate(obj);

        } catch (ParseException e) {
            log.error("couldn't parse " + obj.getClass().getName() + " to Date - " + e.getMessage(), e);
            return null;
        }
    }
}
