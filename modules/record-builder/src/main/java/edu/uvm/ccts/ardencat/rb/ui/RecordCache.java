/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.util.ObjectUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 4/14/14.
 */
public class RecordCache {
    private static RecordCache cache = null;

    public static RecordCache getInstance() {
        if (cache == null) {
            cache = new RecordCache();
        }
        return cache;
    }

    private Map<String, Record> map = new HashMap<String, Record>();

    private RecordCache() {
    }

    public boolean hasRecordWithId(Object id) {
        return map.containsKey(String.valueOf(id));
    }

    public void storeRecord(Record record) {
        map.put(String.valueOf(record.getId()), record);
    }

    public Record getRecordById(Object id) throws IOException, ClassNotFoundException {
        return (Record) ObjectUtil.deepCopy(map.get(String.valueOf(id)));
    }

    public void clear() {
        map.clear();
    }
}
