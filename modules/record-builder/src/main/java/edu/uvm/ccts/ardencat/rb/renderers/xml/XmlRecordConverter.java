/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.renderers.xml;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.eligibility.model.EligibilityData;
import edu.uvm.ccts.ardencat.model.FieldConstants;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.common.exceptions.UnhandledClassException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class XmlRecordConverter implements Converter, FieldConstants {
    private boolean identify;

    private static final Map<String, String> simplifiedClassNameMap = new HashMap<String, String>();

    static {
        simplifiedClassNameMap.put("java.lang.String", TYPE_STRING);
        simplifiedClassNameMap.put("java.util.Date", TYPE_DATE);
        simplifiedClassNameMap.put("java.lang.Double", TYPE_DOUBLE);
        simplifiedClassNameMap.put("java.lang.Float", TYPE_FLOAT);
        simplifiedClassNameMap.put("java.lang.Long", TYPE_LONG);
        simplifiedClassNameMap.put("java.lang.Integer", TYPE_INTEGER);
        simplifiedClassNameMap.put("java.lang.Boolean", TYPE_BOOLEAN);
        simplifiedClassNameMap.put("edu.uvm.ccts.ardencat.model.EligibilityStatus", TYPE_ELIGIBILITY_STATUS);
        simplifiedClassNameMap.put("edu.uvm.ccts.ardencat.rb.model.Record", TYPE_RECORD);
    }


    public XmlRecordConverter(boolean identify) {
        this.identify = identify;
    }

    @Override
    public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext ctx) {
        Record rec = (Record) obj;

        if (rec.getId() != null && ! rec.isTransient(Record.ID_FIELD_NAME)) {
            Object id = identify ? rec.getId() : rec.getExportId();
            writeNode(writer, ctx, Record.ID_FIELD_NAME, id);
        }

        if (rec.getEligibilityRecord() != null) {
            writeEligibilityRecord(writer, ctx, rec.getEligibilityRecord());
        }

        for (String fieldName : rec.getExportableFieldNames()) {
            Object val = rec.getField(fieldName);
            if (val == null) continue;

            if (val instanceof Collection) {         // assume no mixed-type collections
                if (((Collection) val).isEmpty()) continue;

                writer.startNode(fieldName + "List");

                for (Object o : (Collection) val) {
                    writeNode(writer, ctx, fieldName, o);
                }

                writer.endNode();

            } else {
                writeNode(writer, ctx, fieldName, val);
            }
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext ctx) {
        return null;    // not implemented - export only
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass.equals(Record.class);
    }


///////////////////////////////////////////////////////////////////////////
// private methods
//

    private void writeEligibilityRecord(HierarchicalStreamWriter writer, MarshallingContext ctx, BasicEligibilityRecord ber) {
        writer.startNode(Record.ELIGIBILITY_RECORD_FIELD_NAME);

        writeNode(writer, ctx, BasicEligibilityRecord.ID_FIELD, ber.getId());
        writeNode(writer, ctx, BasicEligibilityRecord.SOURCE_FIELD, ber.getSource());
        writeNode(writer, ctx, BasicEligibilityRecord.ORIG_STATUS_FIELD, ber.getOriginalStatus());

        if (ber.getOverrideStatus() != null) {
            writeNode(writer, ctx, BasicEligibilityRecord.OVER_STATUS_FIELD, ber.getOverrideStatus());
        }

        writeNode(writer, ctx, BasicEligibilityRecord.BATCH_FIELD, ber.getBatch());

        writeEligibilityData(writer, ctx, ber.getData());

        writer.endNode();
    }

    private void writeEligibilityData(HierarchicalStreamWriter writer, MarshallingContext ctx, EligibilityData data) {
        writer.startNode(BasicEligibilityRecord.DATA_FIELD);

        for (String key : data.getKeys()) {
            writeNode(writer, ctx, key, data.getByKey(key));
        }

        writer.endNode();
    }

    private String getSimplifiedClassName(String className) {
        String simplified = simplifiedClassNameMap.get(className);

        if (simplified != null) return simplified;
        else                    throw new UnhandledClassException("cannot handle class '" + className + "'");
    }

    private void writeNode(HierarchicalStreamWriter writer, MarshallingContext ctx, String fieldName, Object value) {
        writer.startNode(fieldName);

        if (value != null) {
            writer.addAttribute(ATTR_CLASS, getSimplifiedClassName(value.getClass().getName()));
            ctx.convertAnother(value);
        }

        writer.endNode();
    }
}
