/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb;

import edu.uvm.ccts.ardencat.rb.ui.GUI;
import edu.uvm.ccts.common.util.LicenseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;

/**
 * Created by mstorer on 2/25/15.
 */
public class RecordBuilder {
    private static final Log log = LogFactory.getLog(RecordBuilder.class);

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " attempting to set system look and feel - " + e.getMessage(), e);
        }

        LicenseUtil.requireLicenseAcceptance(RecordBuilder.class, "ArdenCAT Licensing", "/gpl-3.0-standalone.html");

        GUI gui = new GUI();
        gui.setVisible(true);
    }
}
