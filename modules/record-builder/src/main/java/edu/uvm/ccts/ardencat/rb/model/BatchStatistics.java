/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.model;

import java.util.Date;

/**
 * Created by mstorer on 9/25/14.
 */
public class BatchStatistics {
    private String batch;
    private int numEligible;
    private int numEligibleOverridden;
    private int numUnknown;
    private int numUnknownOverridden;
    private int numIneligible;
    private int numIneligibleOverridden;
    private Date generatedDate;
    private Date exportedDate;

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public int getNumEligible() {
        return numEligible;
    }

    public void setNumEligible(int numEligible) {
        this.numEligible = numEligible;
    }

    public int getNumEligibleOverridden() {
        return numEligibleOverridden;
    }

    public void setNumEligibleOverridden(int numEligibleOverridden) {
        this.numEligibleOverridden = numEligibleOverridden;
    }

    public int getNumUnknown() {
        return numUnknown;
    }

    public void setNumUnknown(int numUnknown) {
        this.numUnknown = numUnknown;
    }

    public int getNumUnknownOverridden() {
        return numUnknownOverridden;
    }

    public void setNumUnknownOverridden(int numUnknownOverridden) {
        this.numUnknownOverridden = numUnknownOverridden;
    }

    public int getNumIneligible() {
        return numIneligible;
    }

    public void setNumIneligible(int numIneligible) {
        this.numIneligible = numIneligible;
    }

    public int getNumIneligibleOverridden() {
        return numIneligibleOverridden;
    }

    public void setNumIneligibleOverridden(int numIneligibleOverridden) {
        this.numIneligibleOverridden = numIneligibleOverridden;
    }

    public Date getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    public Date getExportedDate() {
        return exportedDate;
    }

    public void setExportedDate(Date exportedDate) {
        this.exportedDate = exportedDate;
    }
}
