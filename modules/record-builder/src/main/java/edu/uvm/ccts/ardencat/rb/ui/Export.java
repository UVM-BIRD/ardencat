/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.model.ExportInfo;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.ui.model.Statefulness;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mstorer on 5/27/14.
 */
public interface Export extends Statefulness {
    void load(JPanel contentPane);
    void load(JPanel contentPane, ExportInfo exportInfo) throws ValidationException, SQLException, DataException;
    void load(JPanel contentPane, List<Record> records, String firstBatch, String lastBatch) throws ValidationException;
    boolean hasBeenLoaded();
    boolean hasExported();
}
