/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.rb.ui.UIState;
import edu.uvm.ccts.common.util.PropertiesUtil;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by mstorer on 4/15/14.
 */
public class UIConfig implements Statefulness {
    private Frame frame;
    private JButton backButton;
    private JButton nextButton;
    private JLabel statusLabel;

    private Properties properties;
    private Properties userProperties = new Properties();

    public UIConfig(Frame frame, JButton backButton, JButton nextButton, JLabel statusLabel) {
        try {
            properties = PropertiesUtil.loadFromResource("/config.properties");

        } catch (IOException e) {
            throw new RuntimeException("couldn't load config.properties", e);
        }

        this.frame = frame;
        this.backButton = backButton;
        this.nextButton = nextButton;
        this.statusLabel = statusLabel;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public void setStatus(String status) {
        statusLabel.setText(status);
    }

    public Frame getFrame() {
        return frame;
    }

    public JButton getBackButton() {
        return backButton;
    }

    public JButton getNextButton() {
        return nextButton;
    }

    public void setUserProperty(String key, String value) {
        userProperties.setProperty(key, value);
    }

    public String getUserProperty(String key) {
        return userProperties.getProperty(key);
    }

    @Override
    public UIState getUIState() {
        UIState state = new UIState();

        state.setProperty("userProperties", userProperties);

        return state;
    }

    @Override
    public void restoreUIState(UIState state) {
        if (state.hasProperty("userProperties")) {
            userProperties = (Properties) state.getProperty("userProperties");
        }
    }
}
