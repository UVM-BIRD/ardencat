/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.renderers.csv;

import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.model.RecordTableData;
import edu.uvm.ccts.ardencat.rb.renderers.Renderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Created by mstorer on 9/26/13.
 */
public class CsvRenderer implements Renderer {
    private static final Log log = LogFactory.getLog(CsvRenderer.class);

    @Override
    public void render(List<Record> records, boolean identify, OutputStream outputStream) throws Exception {
        BufferedOutputStream out = new BufferedOutputStream(outputStream);
        ICsvListWriter writer = new CsvListWriter(new OutputStreamWriter(out), CsvPreference.STANDARD_PREFERENCE);

        try {
            RecordTableData tableData = new RecordTableData(records, true);

            writer.write(tableData.getColumnNames());
            for (List<Object> row : tableData.buildRows(identify)) {
                writer.write(row);
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " rendering CSV - " + e.getMessage(), e);

        } finally {
            try {
                writer.close();

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " attempting to close CSV writer - " + e.getMessage(), e);
            }
        }
    }
}
