/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.model.RecordTableData;
import edu.uvm.ccts.ardencat.rb.util.RecordMatchUtil;

import javax.swing.table.AbstractTableModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 1/31/14.
 */
public class RecordTableModel extends AbstractTableModel {
    private RecordTableData tableData;
    private boolean allowEdits;
    private List<Boolean> rowSearchMatchList = null;

    public RecordTableModel(List<Record> records, boolean allowEdits) throws IOException {
        tableData = new RecordTableData(records, false);
        this.allowEdits = allowEdits;
    }

    public void highlightRecordsMatchingSearch(String searchText) {
        if (searchText == null || searchText.trim().isEmpty()) {
            rowSearchMatchList = null;

        } else {
            rowSearchMatchList = new ArrayList<Boolean>();
            for (Record r : tableData.getRecords()) {
                rowSearchMatchList.add(RecordMatchUtil.matches(r, searchText));
            }
        }
    }

    public boolean matchesSearch(int row) {
        return rowSearchMatchList != null && rowSearchMatchList.get(row);
    }

    @Override
    public String getColumnName(int column) {
        return tableData.getColumnName(column);
    }

    public Class getColumnClass(int column) {
        return tableData.getColumnClass(column);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return allowEdits;
    }

    @Override
    public int getRowCount() {
        return tableData.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return tableData.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String columnName = tableData.getColumnName(columnIndex);
        return tableData.getRecord(rowIndex).getFieldByColumnName(columnName);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String columnName = tableData.getColumnName(columnIndex);
        tableData.getRecord(rowIndex).setFieldByColumnName(columnName, aValue);
    }
}
