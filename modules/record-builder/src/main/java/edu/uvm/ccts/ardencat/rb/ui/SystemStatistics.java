/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Thu Sep 25 10:16:37 EDT 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.model.AbstractRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.BatchStatistics;
import edu.uvm.ccts.ardencat.ui.model.DateRenderer;
import edu.uvm.ccts.ardencat.rb.ui.model.StatisticsTableModel;
import edu.uvm.ccts.ardencat.rb.ui.model.UIConfig;
import edu.uvm.ccts.ardencat.rb.util.StatisticsReader;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.model.DateTime;
import edu.uvm.ccts.common.ui.model.BasicTable;
import edu.uvm.ccts.common.util.TableUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;

/**
 * @author Matthew Storer
 */
public class SystemStatistics extends JDialog {
    private static final Log log = LogFactory.getLog(SystemStatistics.class);

    private AbstractRecordBuilderConfig config;
    private UIConfig uiConfig;

    public SystemStatistics(Frame owner, AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        super(owner);
        initComponents();

        setup(config, uiConfig);
    }

    public SystemStatistics(Dialog owner, AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        super(owner);
        initComponents();

        setup(config, uiConfig);
    }

    private void setup(AbstractRecordBuilderConfig config, UIConfig uiConfig) {
        this.config = config;
        this.uiConfig = uiConfig;

        loadBatchStatistics();

        TableUtil.makeAutoResizable(tblBatchStats);
        TableUtil.addContextMenu(tblBatchStats);
    }

    private void loadBatchStatistics() {
        List<BatchStatistics> list = StatisticsReader.buildStatistics(config);

        tblBatchStats.setModel(new StatisticsTableModel(list));

        tblBatchStats.setDefaultRenderer(Date.class, new DateRenderer());
        tblBatchStats.setDefaultRenderer(DateTime.class, new DateTimeRenderer());

        DefaultTableCellRenderer intRenderer = new DefaultTableCellRenderer();
        intRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        tblBatchStats.setDefaultRenderer(Integer.class, intRenderer);

        TableUtil.adjustColumnWidths(tblBatchStats);
    }

    private void closeButtonActionPerformed() {
        this.setVisible(false);
    }

    private void closeButtonKeyReleased(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            closeButtonActionPerformed();
        }
    }

    private void tabbedPaneKeyReleased(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            closeButtonActionPerformed();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        tabbedPane = new JTabbedPane();
        pnlBatchStats = new JPanel();
        scrollPane1 = new JScrollPane();
        tblBatchStats = new BasicTable();
        buttonBar = new JPanel();
        closeButton = new JButton();

        //======== this ========
        setTitle("System Statistics");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("System Statistics");
                label1.setFont(new Font("Lucida Grande", Font.BOLD, 20));

                //---- label2 ----
                label2.setText("This screen provides an overview of system activity.");

                //======== tabbedPane ========
                {
                    tabbedPane.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyReleased(KeyEvent e) {
                            tabbedPaneKeyReleased(e);
                        }
                    });

                    //======== pnlBatchStats ========
                    {

                        //======== scrollPane1 ========
                        {

                            //---- tblBatchStats ----
                            tblBatchStats.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                            tblBatchStats.setFocusable(false);
                            scrollPane1.setViewportView(tblBatchStats);
                        }

                        GroupLayout pnlBatchStatsLayout = new GroupLayout(pnlBatchStats);
                        pnlBatchStats.setLayout(pnlBatchStatsLayout);
                        pnlBatchStatsLayout.setHorizontalGroup(
                            pnlBatchStatsLayout.createParallelGroup()
                                .addGroup(pnlBatchStatsLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
                                    .addContainerGap())
                        );
                        pnlBatchStatsLayout.setVerticalGroup(
                            pnlBatchStatsLayout.createParallelGroup()
                                .addGroup(pnlBatchStatsLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                                    .addContainerGap())
                        );
                    }
                    tabbedPane.addTab("Batch Statistics", pnlBatchStats);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(label2))
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(tabbedPane)
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label2)
                            .addGap(18, 18, 18)
                            .addComponent(tabbedPane))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        closeButtonActionPerformed();
                        closeButtonActionPerformed();
                    }
                });
                closeButton.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyReleased(KeyEvent e) {
                        closeButtonKeyReleased(e);
                    }
                });
                buttonBar.add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel label2;
    private JTabbedPane tabbedPane;
    private JPanel pnlBatchStats;
    private JScrollPane scrollPane1;
    private JTable tblBatchStats;
    private JPanel buttonBar;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
