/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Jan 28 11:25:44 EST 2014
 */

package edu.uvm.ccts.ardencat.rb.ui;

import edu.uvm.ccts.ardencat.rb.exceptions.AppenderException;
import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.model.EligibilityRecord;
import edu.uvm.ccts.ardencat.rb.model.StandardRecordBuilderConfig;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.ui.model.*;
import edu.uvm.ccts.ardencat.rb.util.EligibilityTableReader;
import edu.uvm.ccts.ardencat.ui.model.DateRenderer;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.TableUtil;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.List;

/**
 * @author Matt Storer
 */
public class PreviewRecords extends ContentPanel {
    private StandardRecordBuilderConfig config = null;
    private List<Integer> eligibleRecordIds = null;
    private List<EligibilityRecord> eligibilityRecords = null;
    private List<Record> records = null;

    public PreviewRecords(UIConfig uiConfig) {
        super(uiConfig);
        initComponents();

        setNavLabels("Back", "Next");

        btnReset.setVisible("true".equalsIgnoreCase(uiConfig.getProperty("review.allowEdits")));
        btnEvaluate.setVisible("true".equalsIgnoreCase(uiConfig.getProperty("review.evaluate.visible")));

        TableUtil.makeAutoResizable(tblRecords);
        TableUtil.addContextMenu(tblRecords);
    }

    public void load(JPanel contentPane, StandardRecordBuilderConfig config, List<EligibilityRecord> eligibilityRecords)
            throws LocatorException, AppenderException, IOException, ParseException {

        this.eligibilityRecords = eligibilityRecords;

        List<Integer> eligibleRecordIds = new ArrayList<Integer>();
        for (EligibilityRecord record : eligibilityRecords) {
            eligibleRecordIds.add(record.getId());
        }

        boolean recordsChanged = true;
        if (this.eligibleRecordIds != null &&
                this.eligibleRecordIds.containsAll(eligibleRecordIds) &&
                eligibleRecordIds.containsAll(this.eligibleRecordIds)) {

            recordsChanged = false;
        }

        if (this.config != config || recordsChanged) {
            this.config = config;
            this.eligibleRecordIds = eligibleRecordIds;

            loadRecords();
        }

        load(contentPane);
    }

    public UIState getUIState() {
        UIState state = new UIState();

        state.setProperty("eligibleRecordIds", eligibleRecordIds);
        state.setProperty("firstVisibleRow", tblRecords.rowAtPoint(spRecords.getViewport().getViewPosition()));
        state.setProperty("selectedRow", tblRecords.getSelectedRow());

        return state;
    }

    @SuppressWarnings("unchecked")
    public void restoreUIState(UIState state, StandardRecordBuilderConfig config) throws Exception {
        this.config = config;
        this.eligibleRecordIds = (List<Integer>) state.getProperty("eligibleRecordIds");

        repopulateEligibilityRecords();

        loadRecords();

        Integer firstVisibleRow = state.getInteger("firstVisibleRow");
        if (firstVisibleRow != null) {
            tblRecords.scrollRectToVisible(new Rectangle(tblRecords.getCellRect(firstVisibleRow, 0, true)));
        }

        Integer selectedRow = state.getInteger("selectedRow");
        if (selectedRow != null && selectedRow >= 0) {
            tblRecords.setRowSelectionInterval(selectedRow, selectedRow);
        }

        flagAsLoaded();
    }

    public List<Record> getRecords() {
        return records;
    }

    @Override
    protected boolean mayProceed() {
        return true;
    }

    private void btnResetActionPerformed() {
        try {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            loadRecords();

        } catch (Exception e) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            DialogUtil.showErrorDialog(this, e.getMessage());

        } finally {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void repopulateEligibilityRecords() throws SQLException, DataException {
        if (eligibleRecordIds != null && eligibleRecordIds.size() > 0) {
            eligibilityRecords = EligibilityTableReader.getByIdList(config.getArdencatDataSource(), eligibleRecordIds);
        }
    }

    private void loadRecords() throws LocatorException, AppenderException, IOException, ParseException {
        RecordLoader loader = new RecordLoader(uiConfig.getFrame(), config, eligibilityRecords);
        records = loader.getRecords();
        refreshTable();
    }

    private void refreshTable() throws IOException, ParseException {
        boolean allowEdits = "true".equalsIgnoreCase(uiConfig.getProperty("review.allowEdits"));

        RecordTableModel model = new RecordTableModel(records, allowEdits);
        tblRecords.setModel(model);

        if ("true".equalsIgnoreCase(uiConfig.getProperty("review.dateDisplay.showTime"))) {
            tblRecords.setDefaultRenderer(Date.class, new DateTimeRenderer());

        } else {
            tblRecords.setDefaultRenderer(Date.class, new DateRenderer());
        }
        tblRecords.setDefaultEditor(Date.class, new DateTimeEditor());

        Enumeration<TableColumn> e = tblRecords.getColumnModel().getColumns();
        while (e.hasMoreElements()) {
            TableColumn col = e.nextElement();

            Class klass = model.getColumnClass(col.getModelIndex());

            col.setCellRenderer(tblRecords.getDefaultRenderer(klass));
            col.setCellEditor(tblRecords.getDefaultEditor(klass));
        }

        tblRecords.getColumnModel().setColumnMargin(0);

        TableUtil.adjustColumnWidths(tblRecords);
    }

    private void btnEvaluateActionPerformed() {
        Evaluate evaluate = new Evaluate(uiConfig.getFrame(), records);
        evaluate.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        label1 = new JLabel();
        spRecords = new JScrollPane();
        tblRecords = new RecordTable();
        label2 = new JLabel();
        btnReset = new JButton();
        btnEvaluate = new JButton();
        label3 = new JLabel();

        //======== this ========

        //---- label1 ----
        label1.setText("Use this screen to review record data before export.");

        //======== spRecords ========
        {

            //---- tblRecords ----
            tblRecords.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            tblRecords.setAutoCreateRowSorter(true);
            tblRecords.setSelectionForeground(Color.black);
            tblRecords.setSelectionBackground(Color.white);
            spRecords.setViewportView(tblRecords);
        }

        //---- label2 ----
        label2.setText("<html>If everything looks good, click 'Next' to export these records.  You may also click 'Back' to return to the Eligibility Curation screen.</html>");

        //---- btnReset ----
        btnReset.setText("Reset");
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnResetActionPerformed();
            }
        });

        //---- btnEvaluate ----
        btnEvaluate.setText("Evaluate");
        btnEvaluate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnEvaluateActionPerformed();
            }
        });

        //---- label3 ----
        label3.setText("Record Review");
        label3.setFont(new Font("Lucida Grande", Font.BOLD, 20));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(label3)
                                    .addGap(0, 0, Short.MAX_VALUE))
                                .addComponent(label1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnReset)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnEvaluate))
                        .addComponent(label2, GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE)
                        .addComponent(spRecords, GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE))
                    .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label3)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(btnEvaluate)
                            .addComponent(btnReset))
                        .addComponent(label1))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(spRecords, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JLabel label1;
    private JScrollPane spRecords;
    private JTable tblRecords;
    private JLabel label2;
    private JButton btnReset;
    private JButton btnEvaluate;
    private JLabel label3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
