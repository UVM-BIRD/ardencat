/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.ui.model;

import edu.uvm.ccts.ardencat.eligibility.model.EligibilityColor;
import edu.uvm.ccts.ardencat.rb.model.EligibilityRecord;
import edu.uvm.ccts.common.util.ColorUtil;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by mstorer on 1/31/14.
 */
public class EligibilityTable extends JTable {
    private static final Color NON_SELECTED_ROW_FG_COLOR = Color.BLACK;
    private static final Color SELECTED_ROW_BG_COLOR = new Color(0, 53, 115);
    private static final Color SELECTED_ROW_FG_COLOR = Color.WHITE;

    @Override
    public String getToolTipText(MouseEvent event) {
        Point p = event.getPoint();
        int row = convertRowIndexToModel(rowAtPoint(p));
        int col = convertColumnIndexToModel(columnAtPoint(p));

        if (col == EligibilityTableModel.ICONS_COL) {
            EligibilityRecord rec = ((EligibilityTableModel) getModel()).getRecordAtRow(row);
            if (rec.hasOverriddenStatus())  return "Has modified status.";
            else if (rec.hasAudits())       return "Has audit history.";
            else                            return null;

        } else {
            return String.valueOf(getModel().getValueAt(row, col));
        }
    }

    public EligibilityRecord getRecordAtPoint(Point p) {
        int modelRow = convertRowIndexToModel(rowAtPoint(p));
        return ((EligibilityTableModel) getModel()).getRecordAtRow(modelRow);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);

        int modelRow = convertRowIndexToModel(row);

        EligibilityColor c = ((EligibilityTableModel) getModel()).getColorForRecordAtRow(modelRow);

        if (row == getSelectedRow()) {
            comp.setForeground(SELECTED_ROW_FG_COLOR);
            comp.setBackground(SELECTED_ROW_BG_COLOR);

        } else if (c != null) {
            comp.setForeground(ColorUtil.foregroundForColor(c.getColor()));
            comp.setBackground(row % 2 == 0 ? ColorUtil.darken(c.getColor(), 20) : c.getColor());

        } else {
            comp.setForeground(NON_SELECTED_ROW_FG_COLOR);
            comp.setBackground(null);
        }

        return comp;
    }
}
