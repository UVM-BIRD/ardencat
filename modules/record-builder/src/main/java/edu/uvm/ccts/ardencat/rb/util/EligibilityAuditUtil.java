/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb.util;

import edu.uvm.ccts.ardencat.rb.model.EligibilityAudit;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 9/15/14.
 */
public class EligibilityAuditUtil {
    private static final Log log = LogFactory.getLog(EligibilityAuditUtil.class);

    public static int saveAuditRecord(DataSource ds, int eligId, EligibilityStatus origStatus,
                                       EligibilityStatus newStatus, String signature, String reason) throws Exception {
        try {
            Connection conn = ds.getConnection();

            String query = "insert into tblACEligAudit (eligId, origStatus, newStatus, authSig, reason) values(?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, eligId);
            stmt.setString(2, convertToChar(origStatus));
            stmt.setString(3, convertToChar(newStatus));
            stmt.setString(4, signature);
            stmt.setString(5, reason);

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                log.info("saveAuditRecord : " + signature + " changed eligibility record with id=" + eligId + " from " +
                        origStatus + " to " + newStatus + " - reason='" + reason + "'");

                return rs.getInt(1);

            } else {
                throw new SQLException("no auto-generated keys found");
            }

        } catch (Exception e) {
            log.info("caught " + e.getClass().getName() + " saving audit record {eligId=" + eligId +
                    ", origStatus=" + origStatus + ", newStatus=" + newStatus + ", signature='" + signature +
                    "', reason='" + reason + "'} - " + e.getMessage(), e);

            throw e;
        }
    }

    public static List<EligibilityAudit> getAuditRecordListByEligId(DataSource dataSource, int eligId) throws SQLException {
        String query = "select id, eligId, origStatus, newStatus, authSig, reason, created from tblACEligAudit where eligId = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        stmt.setInt(1, eligId);

        ResultSet rs = stmt.executeQuery();

        List<EligibilityAudit> auditList = new ArrayList<EligibilityAudit>();
        while (rs.next()) {
            auditList.add(buildAuditRecord(rs));
        }

        return auditList;
    }

    public static EligibilityAudit getAuditRecordById(DataSource dataSource, int id) throws SQLException {
        String query = "select id, eligId, origStatus, newStatus, authSig, reason, created from tblACEligAudit where id = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            return buildAuditRecord(rs);

        } else {
            throw new SQLException("couldn't find eligibility audit record with id=" + id);
        }
    }

    private static EligibilityAudit buildAuditRecord(ResultSet rs) throws SQLException {
        EligibilityAudit audit = new EligibilityAudit();

        audit.setId(rs.getInt(1));
        audit.setEligId(rs.getInt(2));
        audit.setOrigStatus(EligibilityStatus.fromString(rs.getString(3)));
        audit.setNewStatus(EligibilityStatus.fromString(rs.getString(4)));
        audit.setAuthSig(rs.getString(5));
        audit.setReason(rs.getString(6));
        audit.setCreated(buildResultSetDate(rs, 7));

        return audit;
    }

    private static Date buildResultSetDate(ResultSet rs, int index) throws SQLException {
        Object obj = rs.getObject(index);
        try {
            return DateUtil.castToDate(obj);

        } catch (ParseException e) {
            log.error("couldn't parse " + obj.getClass().getName() + " to Date - " + e.getMessage(), e);
            return null;
        }
    }

    private static String convertToChar(EligibilityStatus status) {
        return status != null ?
                status.getStringVal() :
                null;
    }
}
