/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rb;

import edu.uvm.ccts.ardencat.rb.ui.AbstractExportPanel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 9/22/14.
 */
public class ExportPanelRegistry {
    private static ExportPanelRegistry registry = null;

    public static ExportPanelRegistry getInstance() {
        if (registry == null) registry = new ExportPanelRegistry();
        return registry;
    }

    private Map<String, AbstractExportPanel> map = new HashMap<String, AbstractExportPanel>();

    private ExportPanelRegistry() {
        // private to make singleton
    }

    public void registerPanel(String method, AbstractExportPanel panel) {
        map.put(method.toLowerCase(), panel);
    }

    public AbstractExportPanel getPanel(String method) {
        return map.get(method.toLowerCase());
    }

    public void clear() {
        map.clear();
    }
}
