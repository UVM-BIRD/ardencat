/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents Simple Trigger
 */
public class SimpleTrigger extends EvokeCondition<SimpleTrigger> {
    public static final String CALL_TOKEN = "call";

    private List<Event> triggerOnEvents = new ArrayList<Event>();
    private boolean mayCall = false;

    public SimpleTrigger(List<Event> triggerOnEvents, boolean mayCall) {
        this.triggerOnEvents = triggerOnEvents;
        this.mayCall = mayCall;
    }

    public SimpleTrigger(SimpleTrigger st) {
        for (Event event : st.triggerOnEvents) {
            triggerOnEvents.add(event);
        }
        this.mayCall = st.mayCall;
    }

    public boolean isTriggeredBy(Event event) {
        return event != null && triggerOnEvents.contains(event);
    }

    public boolean mayBeCalled() {
        return mayCall;
    }

    @Override
    public SimpleTrigger copy() {
        return new SimpleTrigger(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        SimpleTrigger st = (SimpleTrigger) o;
        return new EqualsBuilder()
                .append(mayCall, st.mayCall)
                .append(triggerOnEvents, st.triggerOnEvents)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(607, 349)
                .append(mayCall)
                .append(triggerOnEvents)
                .toHashCode();
    }
}
