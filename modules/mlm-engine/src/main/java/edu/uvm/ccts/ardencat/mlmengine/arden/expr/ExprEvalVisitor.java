/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.expr;

import edu.uvm.ccts.arden.expr.antlr.ExprBaseVisitor;
import edu.uvm.ccts.arden.expr.antlr.ExprParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.DurationUtil;
import edu.uvm.ccts.arden.util.NumberUtil;
import edu.uvm.ccts.ardencat.mlmengine.arden.Arden;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.common.exceptions.InvalidAttributeException;
import edu.uvm.ccts.common.exceptions.antlr.GrammarException;
import org.antlr.v4.runtime.misc.NotNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 1/13/14.
 */
public class ExprEvalVisitor extends ExprBaseVisitor<Value> {
    private static final Log log = LogFactory.getLog(ExprEvalVisitor.class);

    private Arden arden;
    private Value value = Value.VOID;

    public ExprEvalVisitor(MLM caller, Variables variables) {
        arden = new Arden(log, caller);

        if (variables != null) {
            for (String id : variables.getVariableNames()) {
                arden.assign(id, variables.getVariable(id));
            }
        }
    }

    public ADataType getValue() {
        return value.getBackingObject();
    }

    @Override
    public Value visitInit(@NotNull ExprParser.InitContext ctx) {
        value = visit(ctx.expr());
        return super.visitInit(ctx);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// from LogicEvalVisitor
//

    @Override
    public Value visitParens(@NotNull ExprParser.ParensContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Value visitId(@NotNull ExprParser.IdContext ctx) {
        String id = ctx.ID().getText();
        return arden.id(id);
    }

    @Override
    public Value visitNullVal(@NotNull ExprParser.NullValContext ctx) {
        return arden.nullVal();
    }

    @Override
    public Value visitStringVal(@NotNull ExprParser.StringValContext ctx) {
        String str = ctx.StringVal().getText();
        return arden.stringVal(str);
    }

    @Override
    public Value visitNumberVal(@NotNull ExprParser.NumberValContext ctx) {
        String numStr = ctx.NumberVal().getText();
        return arden.numberVal(numStr);
    }

    @Override
    public Value visitBooleanVal(@NotNull ExprParser.BooleanValContext ctx) {
        String boolStr = ctx.BooleanVal().getText();
        return arden.booleanVal(boolStr);
    }

    @Override
    public Value visitBinaryList(@NotNull ExprParser.BinaryListContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.binaryList(left, right);
    }

    @Override
    public Value visitUnaryList(@NotNull ExprParser.UnaryListContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryList(v);
    }

    @Override
    public Value visitEmptyList(@NotNull ExprParser.EmptyListContext ctx) {
        return arden.emptyList();
    }

    @Override
    public Value visitNow(@NotNull ExprParser.NowContext ctx) {
        return arden.now();
    }

    @Override
    public Value visitCurrentTime(@NotNull ExprParser.CurrentTimeContext ctx) {
        return arden.currentTime();
    }

    @Override
    public Value visitTimeVal(@NotNull ExprParser.TimeValContext ctx) {
        return arden.timeVal(ctx.getText());
    }

    @Override
    public Value visitTimeOfDayVal(@NotNull ExprParser.TimeOfDayValContext ctx) {
        return arden.timeOfDayVal(ctx.getText());
    }

    /**
     * The duration data type signifies an interval of time that is not anchored to any particular point in absolute
     * time. There are no duration constants. Instead one builds durations using the duration operators (see Section
     * 9.10.7). For example, <strong>1 day</strong>, <strong>45 seconds</strong>, and <strong>3.2 months</strong> are
     * durations.
     * @see {@link edu.uvm.ccts.arden.model.Duration}, {@link edu.uvm.ccts.arden.model.DurationUnit}, {@link edu.uvm.ccts.arden.model.SecondsDuration}, {@link edu.uvm.ccts.arden.model.MonthsDuration}, 8.5, 9.10.7, 9.11
     * @param ctx
     * @return
     */
    @Override
    public Value visitDuration(@NotNull ExprParser.DurationContext ctx) {
        return new Value(buildDuration(ctx.durationExpr()));
    }

    private Duration buildDuration(ExprParser.DurationExprContext durExprCtx) {
        // first, split duration ("3 days 2 hours") into component durations ("3 days", "2 hours").

        List<Duration> componentDurations = new ArrayList<Duration>();
        for (int i = 0; i < durExprCtx.NumberVal().size(); i ++) {
            ANumber n = NumberUtil.parseNumber(durExprCtx.NumberVal(i).getText());
            String s = durExprCtx.durationUnit(i).getText();

            try {
                componentDurations.add(DurationUtil.buildDuration(n, s));

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // next, build a Duration object that represents the sum of each component duration, generated above

        Duration duration = null;
        for (Duration d : componentDurations) {
            if (duration == null) {
                duration = d;

            } else {
                duration = duration.add(d);     // note: might change form from MonthsDuration to SecondsDuration
            }
        }

        return duration;
    }

    @Override
    public Value visitDayOfWeek(@NotNull ExprParser.DayOfWeekContext ctx) {
        String dayOfWeekStr = ctx.DayOfWeek().getText();
        return arden.dayOfWeek(dayOfWeekStr);
    }

    /**
     * The <strong>new</strong> statement causes a new object to be created, and assigns it to the named variable.
     * <br>
     * In the simple case (without the <strong>with</strong> clause) all attributes of the object are initialized to
     * {@code null}. In the full statement, a set of 1 or more comma-separated expressions should follow the
     * <strong>with</strong> reserved word. Each expression is evaluated and assigned as a value of an attribute of
     * the object. They are assigned in the order the attributes were declared in the <strong>object</strong> statement.
     * If the number of expressions is less than the number of attributes, remaining attributes are initialized to
     * {@code null}. If the number of expressions is greater than the number of attributes, the extra expressions are
     * evaluated but the results are silently discarded.
     * @see {@link AObject}, 10.2.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitNewObject(@NotNull ExprParser.NewObjectContext ctx) {
        String typeName = ctx.ID().getText();

        AObjectType type = arden.getObjectType(typeName);
        if (type == null) throw new RuntimeException(typeName + " does not refer to a valid Arden object type");

        AObject obj = new AObject(type);

        // ordered-with refers to e.g., "new <object> with <attrVal1>, <attrVal2>"
        if (ctx.objOrderedWith() != null) {
            ExprParser.ObjOrderedWithContext octx = ctx.objOrderedWith();
            List<ADataType> list = new ArrayList<ADataType>();

            for (ExprParser.ExprContext ectx : octx.expr()) {
                Value v = visit(ectx);
                list.add(v.getBackingObject());
            }

            obj.populate(list);
        }

        // named-with refers to e.g., "new <object> with <attrName1> := <attrVal1>, <attrName2> := <attrVal2>"
        if (ctx.objNamedWith() != null) {
            ExprParser.ObjNamedWithContext nctx = ctx.objNamedWith();
            int max = nctx.ID().size();

            for (int i = 0; i < max; i ++) {
                String attrName = nctx.ID(i).getText();
                Value v = visit(nctx.expr(i));

                try {
                    obj.setAttribute(attrName, v.getBackingObject());

                } catch (InvalidAttributeException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return new Value(obj);
    }

    /**
     * The <strong>dot</strong> operator ("{@code .}") selects an attribute from an object based on the name following
     * the dot. It takes an expression and an identifier. The expression typically evaluates to an object or a list of
     * objects.
     * <br>
     * If the expression does not evaluate to an object, or if the object does not contain the named attribute, then
     * {@code null} is returned. If the expression evaluates to a list, normal Arden list handling is used and a list is
     * returned. Therefore, if the expression is a list of objects, then a list (of the same length) of the attribute
     * values named by the identifier is returned (a common usage).
     * @see {@link AObject}, 9.18.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitDot(@NotNull ExprParser.DotContext ctx) {
        Value v = visit(ctx.expr());

        if (v.isObject()) {                                                         // if its an object, return the
            return new Value(visitDotHelper(v.asObject(), ctx));                    // requested attribute

        } else if (v.isList() && v.asList().getListType() == ListType.OBJECT) {     // if it's a list of objects,
            AList list = new AList();                                               // return a list containing the
            for (ADataType adt : v.asList()) {                                      // requested attribute, one for
                list.add(visitDotHelper((AObject) adt, ctx));                       // each object in the source list
            }
            return new Value(list);
        }

        return NULL();
    }

    private ADataType visitDotHelper(AObject ao, ExprParser.DotContext ctx) {
        String attrName = ctx.ID().getText();

        if (ao.hasAttribute(attrName)) {
            try {
                return ao.getAttribute(attrName);

            } catch (InvalidAttributeException e) {
                throw new RuntimeException(e);          // should never get here, for call to hasAttribute() above
            }
        }

        return new ANull();
    }

    @Override
    public Value visitClone(@NotNull ExprParser.CloneContext ctx) {
        Value v = visit(ctx.expr());
        return arden.clone(v);
    }

    @Override
    public Value visitExtractAttrNames(@NotNull ExprParser.ExtractAttrNamesContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractAttrNames(v);
    }

    @Override
    public Value visitAttributeFrom(@NotNull ExprParser.AttributeFromContext ctx) {
        Value vAttrName = visit(ctx.expr(0));
        Value vObj = visit(ctx.expr(1));
        return arden.attributeFrom(vAttrName, vObj);
    }

    @Override
    public Value visitSort(@NotNull ExprParser.SortContext ctx) {
        Value v = visit(ctx.expr());
        boolean sortByTime = ctx.Time() != null;
        return arden.sort(v, sortByTime);
    }

    @Override
    public Value visitMerge(@NotNull ExprParser.MergeContext ctx) {
        Value v1 = visit(ctx.expr(0));
        Value v2 = visit(ctx.expr(1));
        return arden.merge(v1, v2);
    }

    @Override
    public Value visitWhereTimeIsPresent(@NotNull ExprParser.WhereTimeIsPresentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.whereTimeIsPresent(v);
    }

    @Override
    public Value visitAddToList(@NotNull ExprParser.AddToListContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if (ctx.At() != null) {
            Value vAt = visit(ctx.expr(2));
            return arden.addToList(vObj, vList, vAt);

        } else {
            return arden.addToList(vObj, vList);
        }
    }

    @Override
    public Value visitRemoveFromList(@NotNull ExprParser.RemoveFromListContext ctx) {
        Value vPos = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.removeFromList(vPos, vList);
    }

    @Override
    public Value visitWhere(@NotNull ExprParser.WhereContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTest = visit(ctx.expr(1));
        return arden.where(vObj, vTest);
    }

    @Override
    public Value visitConcat(@NotNull ExprParser.ConcatContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.concat(left, right);
    }

    @Override
    public Value visitBuildString(@NotNull ExprParser.BuildStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.string(v);
    }

    @Override
    public Value visitMatches(@NotNull ExprParser.MatchesContext ctx) {
        Value vStr = visit(ctx.expr(0));
        Value vPattern = visit(ctx.expr(1));
        return arden.matches(vStr, vPattern);
    }

    @Override
    public Value visitLength(@NotNull ExprParser.LengthContext ctx) {
        Value v = visit(ctx.expr());
        return arden.length(v);
    }

    @Override
    public Value visitUppercase(@NotNull ExprParser.UppercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.uppercase(v);
    }

    @Override
    public Value visitLowercase(@NotNull ExprParser.LowercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.lowercase(v);
    }

    /**
     * The <strong>trim</strong> operator removes leading and trailing white space from a string (see Section 7.1.10).
     * The optional <strong>left</strong> or <strong>right</strong> modifier can be applied to remove leading or
     * trailing white space respectively. Printable characters and embedded white space characters are not affected.
     * The trim of a non-string data type or empty list is {@code null}. Primary times are preserved.
     * @see 9.8.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitTrim(@NotNull ExprParser.TrimContext ctx) {
        Value v = visit(ctx.expr());
        TrimType type;
        if      (ctx.Left() != null)    type = TrimType.LEFT;
        else if (ctx.Right() != null)   type = TrimType.RIGHT;
        else                            type = TrimType.LEFT_AND_RIGHT;
        return arden.trim(v, type);
    }

    /**
     * The <strong>find ... string</strong> operator locates a substring within a target string, and returns a number
     * that represents the starting position of the substring. <strong>Find ... string</strong> is similar to
     * <strong>matches pattern</strong>, but returns a number (rather than a boolean), and does not support wildcards.
     * <strong>Find ... string</strong> is case-sensitive, and returns a zero if the target string does not contain the
     * exact substring. If either the substring or target is not a string data type, {@code null} is returned. Primary
     * times are not preserved.
     * <br>
     * The optional modifier <strong>starting at...</strong> can be appended to the <strong>find ... string</strong>
     * operator to control where the search for the substring begins. Omitting the modifier causes the search to begin
     * at the first character of the string. The value following <strong>starting at...</strong> must be an integer,
     * otherwise {@code null} is returned. If the value following <strong>starting at...</strong> is an integer beyond
     * the length of the target string (i.e. less than 1 or greater than length target), zero is returned.
     * @see 9.8.9
     * @param ctx
     * @return
     */
    @Override
    public Value visitFindInString(@NotNull ExprParser.FindInStringContext ctx) {
        Value vNeedle = visit(ctx.expr(0));
        Value vHaystack = visit(ctx.expr(1));

        if (vNeedle.isString() && vHaystack.isString()) {
            int start = 1;
            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(2));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    start = vStartAt.asInteger();

                } else {
                    return NULL();
                }
            }

            return new Value(vHaystack.asString().findPositionOf(vNeedle.asString(), start));
        }

        return NULL();
    }

    /**
     * The <strong>substring ... characters [starting at ...] from ...</strong> operator returns a substring of
     * characters from a designated target string. This substring consists of the specified number of characters from
     * the source string beginning with the starting position (either the first character of the string or the specified
     * location within the string). For example <strong>substring 3 characters starting at 2 from "Example"</strong>
     * would return "xam" - a 3 character string beginning with the second character in the source string "Example".
     * <br>
     * The target string must be a string data type, the starting location within the string must be a positive
     * integer, and the number of characters to be returned must be an integer, or the operator returns {@code null}. If a
     * starting position is specified, its value must be an integer between 1 and the length of the string, otherwise
     * an empty string is returned. If the requested number of characters is greater than the length of the string,
     * the entire string is returned. If a starting point is specified, and the requested number of characters is
     * greater than the length of the string minus the starting point, the resulting string is the original string
     * to the right of and including the starting position. If the number of characters requested is positive the
     * characters are counted from left to right. If the number of characters requested is negative, the characters
     * are counted from right to left. The characters in a substring are always returned in the order that they
     * appear in the string. Default list handling is observed. Primary times are preserved.
     * @see 9.8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubstring(@NotNull ExprParser.SubstringContext ctx) {
        Value vNumChars = visit(ctx.expr(0));

        if (vNumChars.isNumber() && vNumChars.asNumber().isWholeNumber()) {
            Value vObj;
            int numChars = vNumChars.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vObj = visit(ctx.expr(2));

            } else {
                vObj = visit(ctx.expr(1));
            }

            if (vObj.isString()) {
                return new Value(vObj.asString().substring(startAt, numChars));

            } else if (vObj.isList()) {
                AList list = new AList();
                for (ADataType obj : vObj.asList()) {
                    if (obj instanceof AString) {
                        list.add(((AString) obj).substring(startAt, numChars));

                    } else {
                        ANull n = new ANull();
                        n.setPrimaryTime(obj.getPrimaryTime());                     // preserve primary time
                        list.add(n);
                    }
                }
                return new Value(list);

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitAsString(@NotNull ExprParser.AsStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asString(v);
    }

    @Override
    public Value visitCount(@NotNull ExprParser.CountContext ctx) {
        Value v = visit(ctx.expr());
        return arden.count(v);
    }

    @Override
    public Value visitExist(@NotNull ExprParser.ExistContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exist(v);
    }

    @Override
    public Value visitAverage(@NotNull ExprParser.AverageContext ctx) {
        Value v = visit(ctx.expr());
        return arden.average(v);
    }

    @Override
    public Value visitMedian(@NotNull ExprParser.MedianContext ctx) {
        Value v = visit(ctx.expr());
        return arden.median(v);
    }

    @Override
    public Value visitSum(@NotNull ExprParser.SumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sum(v);
    }

    @Override
    public Value visitStdDev(@NotNull ExprParser.StdDevContext ctx) {
        Value v = visit(ctx.expr());
        return arden.stdDev(v);
    }

    @Override
    public Value visitVariance(@NotNull ExprParser.VarianceContext ctx) {
        Value v = visit(ctx.expr());
        return arden.variance(v);
    }

    @Override
    public Value visitMinimum(@NotNull ExprParser.MinimumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.minimum(v);
    }

    @Override
    public Value visitMaximum(@NotNull ExprParser.MaximumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.maximum(v);
    }

    @Override
    public Value visitFirst(@NotNull ExprParser.FirstContext ctx) {
        Value v = visit(ctx.expr());
        return arden.first(v);
    }

    @Override
    public Value visitLast(@NotNull ExprParser.LastContext ctx) {
        Value v = visit(ctx.expr());
        return arden.last(v);
    }

    @Override
    public Value visitAny(@NotNull ExprParser.AnyContext ctx) {
        Value v = visit(ctx.expr());
        return arden.any(v);
    }

    @Override
    public Value visitAll(@NotNull ExprParser.AllContext ctx) {
        Value v = visit(ctx.expr());
        return arden.all(v);
    }

    @Override
    public Value visitNo(@NotNull ExprParser.NoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.no(v);
    }

    @Override
    public Value visitElement(@NotNull ExprParser.ElementContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vIndex = visit(ctx.expr(1));
        return arden.element(vObj, vIndex);
    }

    @Override
    public Value visitEarliest(@NotNull ExprParser.EarliestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.earliest(v);
    }

    @Override
    public Value visitLatest(@NotNull ExprParser.LatestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.latest(v);
    }

    @Override
    public Value visitExtractChars(@NotNull ExprParser.ExtractCharsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractChars(v);
    }

    @Override
    public Value visitSeqto(@NotNull ExprParser.SeqtoContext ctx) {
        Value vFrom = visit(ctx.expr(0));
        Value vTo = visit(ctx.expr(1));
        return arden.seqto(vFrom, vTo);
    }

    @Override
    public Value visitReverse(@NotNull ExprParser.ReverseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.reverse(v);
    }

    @Override
    public Value visitIndex(@NotNull ExprParser.IndexContext ctx) {
        Value vList = visit(ctx.expr());
        String indexTypeStr = ctx.indexType().getText();
        return arden.index(vList, indexTypeStr);
    }

    @Override
    public Value visitIndexFrom(@NotNull ExprParser.IndexFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        String indexTypeStr = ctx.indexType().getText();
        return arden.indexFrom(vNum, vList, indexTypeStr);
    }

    @Override
    public Value visitIndexOfFrom(@NotNull ExprParser.IndexOfFromContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.indexOfFrom(vObj, vList);
    }

    @Override
    public Value visitAfter(@NotNull ExprParser.AfterContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.after(vDur, vTime);
    }

    @Override
    public Value visitBefore(@NotNull ExprParser.BeforeContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.before(vDur, vTime);
    }

    @Override
    public Value visitAgo(@NotNull ExprParser.AgoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ago(v);
    }

    @Override
    public Value visitTimeOfDayFunc(@NotNull ExprParser.TimeOfDayFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeOfDayFunc(v);
    }

    @Override
    public Value visitTimeFunc(@NotNull ExprParser.TimeFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeFunc(v);
    }

    @Override
    public Value visitAtTime(@NotNull ExprParser.AtTimeContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vTimeOfDay = visit(ctx.expr(1));
        return arden.atTime(vTime, vTimeOfDay);
    }

    @Override
    public Value visitAsTime(@NotNull ExprParser.AsTimeContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asTime(v);
    }

    @Override
    public Value visitDayOfWeekFunc(@NotNull ExprParser.DayOfWeekFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.dayOfWeekFunc(v);
    }

    @Override
    public Value visitExtract(@NotNull ExprParser.ExtractContext ctx) {
        Value v = visit(ctx.expr());
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.extract(v, temporalUnitStr);
    }

    @Override
    public Value visitReplace(@NotNull ExprParser.ReplaceContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vNumber = visit(ctx.expr(1));
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.replace(vTime, vNumber, temporalUnitStr);
    }

    @Override
    public Value visitUnaryPlus(@NotNull ExprParser.UnaryPlusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryPlus(v);
    }

    @Override
    public Value visitUnaryMinus(@NotNull ExprParser.UnaryMinusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryMinus(v);
    }

    @Override
    public Value visitAdd(@NotNull ExprParser.AddContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.add(left, right);
    }

    @Override
    public Value visitSubtract(@NotNull ExprParser.SubtractContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.subtract(left, right);
    }

    @Override
    public Value visitMultiply(@NotNull ExprParser.MultiplyContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.multiply(left, right);
    }

    @Override
    public Value visitDivide(@NotNull ExprParser.DivideContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.divide(left, right);
    }

    @Override
    public Value visitRaiseToPower(@NotNull ExprParser.RaiseToPowerContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.raiseToPower(left, right);
    }

    @Override
    public Value visitArccos(@NotNull ExprParser.ArccosContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arccos(v);
    }

    @Override
    public Value visitArcsin(@NotNull ExprParser.ArcsinContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arcsin(v);
    }

    @Override
    public Value visitArctan(@NotNull ExprParser.ArctanContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arctan(v);
    }

    @Override
    public Value visitCosine(@NotNull ExprParser.CosineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.cosine(v);
    }

    @Override
    public Value visitSine(@NotNull ExprParser.SineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sine(v);
    }

    @Override
    public Value visitTangent(@NotNull ExprParser.TangentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.tangent(v);
    }

    @Override
    public Value visitExp(@NotNull ExprParser.ExpContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exp(v);
    }

    @Override
    public Value visitLog(@NotNull ExprParser.LogContext ctx) {
        Value v = visit(ctx.expr());
        return arden.log(v);
    }

    @Override
    public Value visitLog10(@NotNull ExprParser.Log10Context ctx) {
        Value v = visit(ctx.expr());
        return arden.log10(v);
    }

    @Override
    public Value visitFloor(@NotNull ExprParser.FloorContext ctx) {
        Value v = visit(ctx.expr());
        return arden.floor(v);
    }

    @Override
    public Value visitCeiling(@NotNull ExprParser.CeilingContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ceiling(v);
    }

    @Override
    public Value visitTruncate(@NotNull ExprParser.TruncateContext ctx) {
        Value v = visit(ctx.expr());
        return arden.truncate(v);
    }

    @Override
    public Value visitRound(@NotNull ExprParser.RoundContext ctx) {
        Value v = visit(ctx.expr());
        return arden.round(v);
    }

    @Override
    public Value visitAbs(@NotNull ExprParser.AbsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.abs(v);
    }

    @Override
    public Value visitSqrt(@NotNull ExprParser.SqrtContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sqrt(v);
    }

    @Override
    public Value visitAsNumber(@NotNull ExprParser.AsNumberContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asNumber(v);
    }

    @Override
    public Value visitAnd(@NotNull ExprParser.AndContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.and(vLeft, vRight);
    }

    @Override
    public Value visitOr(@NotNull ExprParser.OrContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.or(vLeft, vRight);
    }

    @Override
    public Value visitNot(@NotNull ExprParser.NotContext ctx) {
        Value v = visit(ctx.expr());
        return arden.not(v);
    }

    @Override
    public Value visitIsEqual(@NotNull ExprParser.IsEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isEqual(left, right);
    }

    @Override
    public Value visitIsNotEqual(@NotNull ExprParser.IsNotEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isNotEqual(left, right);
    }

    @Override
    public Value visitIsLessThan(@NotNull ExprParser.IsLessThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThan(left, right);
    }

    @Override
    public Value visitIsLessThanOrEqual(@NotNull ExprParser.IsLessThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThanOrEqual(left, right);
    }

    @Override
    public Value visitIsGreaterThan(@NotNull ExprParser.IsGreaterThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThan(left, right);
    }

    @Override
    public Value visitIsGreaterThanOrEqual(@NotNull ExprParser.IsGreaterThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThanOrEqual(left, right);
    }

    @Override
    public Value visitIsWithinTo(@NotNull ExprParser.IsWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitIsWithinPreceding(@NotNull ExprParser.IsWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinFollowing(@NotNull ExprParser.IsWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinSurrounding(@NotNull ExprParser.IsWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinPast(@NotNull ExprParser.IsWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitIsWithinSameDay(@NotNull ExprParser.IsWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitIsBefore(@NotNull ExprParser.IsBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitIsAfter(@NotNull ExprParser.IsAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitIsIn(@NotNull ExprParser.IsInContext ctx) {
        Value left = visit(ctx.expr(0));
        boolean flip = ctx.Not() != null;

        if (left.isNull()) {
            return flip ? FALSE() : TRUE();

        } else {
            Value right = visit(ctx.expr(1));
            return arden.isIn(left, right, flip);
        }
    }

    @Override
    public Value visitIsDataType(@NotNull ExprParser.IsDataTypeContext ctx) {
        Value v = visit(ctx.expr());
        String dataTypeStr = ctx.dataType().getText();
        boolean flip = ctx.Not() != null;
        return arden.isDataType(v, dataTypeStr, flip);
    }

    @Override
    public Value visitIsObjectType(@NotNull ExprParser.IsObjectTypeContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vType = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isObjectType(vObj, vType, flip);
    }

    @Override
    public Value visitOccurEqual(@NotNull ExprParser.OccurEqualContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurEqual(vObj, vTime, flip);
    }

    @Override
    public Value visitOccurWithinTo(@NotNull ExprParser.OccurWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitOccurWithinPreceding(@NotNull ExprParser.OccurWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinFollowing(@NotNull ExprParser.OccurWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinSurrounding(@NotNull ExprParser.OccurWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinPast(@NotNull ExprParser.OccurWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitOccurWithinSameDay(@NotNull ExprParser.OccurWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurBefore(@NotNull ExprParser.OccurBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurAfter(@NotNull ExprParser.OccurAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitMinimumFrom(@NotNull ExprParser.MinimumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.minimumFrom(vNum, vList);
    }

    @Override
    public Value visitMaximumFrom(@NotNull ExprParser.MaximumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.maximumFrom(vNum, vList);
    }

    @Override
    public Value visitFirstFrom(@NotNull ExprParser.FirstFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.firstFrom(vNum, vList);
    }

    @Override
    public Value visitLastFrom(@NotNull ExprParser.LastFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.lastFrom(vNum, vList);
    }

    @Override
    public Value visitNearest(@NotNull ExprParser.NearestContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return ctx.Index() != null ?
                arden.indexNearest(vTime, vList) :
                arden.nearest(vTime, vList);
    }

    @Override
    public Value visitAtMostOrLeast(@NotNull ExprParser.AtMostOrLeastContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if      (ctx.Least() != null)   return arden.atLeast(vNum, vList);
        else if (ctx.Most() != null)    return arden.atMost(vNum, vList);
        else                            throw new GrammarException("expected 'least' or 'most'");
    }

    /**
     * The <strong>sublist ... elements [starting at ...] from</strong> operator returns a sublist of elements from a
     * designated target list and is similar to the <strong>substring</strong> operator (see 9.8.10). This sublist
     * consists of the specified number of elements from the source list beginning with the starting position (either
     * the first elements of the list or the specified location within the list).
     * <br>
     * The target list must be a list data type, the starting location within the list must be a positive integer,
     * and the number of elements to be returned must be an integer, or the operator returns {@code null}. If target is not a
     * list data type, a list with one element is assumed. If a starting position is specified, its value must be an
     * integer between 1 and the length of the list, otherwise an empty list is returned. If the requested number of
     * elements is greater than the length of the list, the entire list is returned. If a starting point is specified,
     * and the requested number of elements is greater than the size of the list minus the starting point, the
     * resulting list is the original list to the right of and including the starting position. If the number of
     * elements requested is positive the elements are counted from left to right. If the number of elements requested
     * is negative, the elements are counted from right to left. The elements in a sublist are always returned in the
     * order that they appear in the original list. Default list handling is observed. Primary times are preserved.
     * @see 9.14.6
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubList(@NotNull ExprParser.SubListContext ctx) {
        Value vNumElements = visit(ctx.expr(0));

        if (vNumElements.isNumber() && vNumElements.asNumber().isWholeNumber()) {
            Value vList;
            int numElements = vNumElements.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vList = visit(ctx.expr(2));

            } else {
                vList = visit(ctx.expr(1));
            }

            if (vList.isList()) {
                return new Value(vList.asList().sublist(startAt, numElements));

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitIncrease(@NotNull ExprParser.IncreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.increase(v, pct);
    }

    @Override
    public Value visitDecrease(@NotNull ExprParser.DecreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.decrease(v, pct);
    }

    @Override
    public Value visitInterval(@NotNull ExprParser.IntervalContext ctx) {
        Value v = visit(ctx.expr());
        return arden.interval(v);
    }

    @Override
    public Value visitEarliestFrom(@NotNull ExprParser.EarliestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.earliestFrom(vNum, vList);
    }

    @Override
    public Value visitLatestFrom(@NotNull ExprParser.LatestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.latestFrom(vNum, vList);
    }

    private Value NULL() {
        return new Value(ANull.NULL());
    }

    private Value VOID() {
        return new Value(AVoid.VOID());
    }

    private Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    private Value FALSE() {
        return new Value(ABoolean.FALSE());
    }
}
