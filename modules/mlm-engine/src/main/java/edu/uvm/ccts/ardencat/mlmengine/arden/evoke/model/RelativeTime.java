/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.arden.model.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Calendar;

/**
 * Created by mstorer on 1/3/14.
 */
public class RelativeTime extends NonPrimaryTimeDataType<RelativeTime> implements ISimpleTimeComponent, ITimeComponent {
    private DayOfWeek dayOfWeek = null;
    private RelativeDayOfWeek relDayOfWeek = null;
    private TimeOfDay timeOfDay;

    public RelativeTime(DayOfWeek dayOfWeek, TimeOfDay timeOfDay) {
        this.dayOfWeek = dayOfWeek;
        this.timeOfDay = timeOfDay;
    }

    public RelativeTime(RelativeDayOfWeek relDayOfWeek, TimeOfDay timeOfDay) {
        this.relDayOfWeek = relDayOfWeek;
        this.timeOfDay = timeOfDay;
    }

    public RelativeTime(RelativeTime rt) {
        this.dayOfWeek = rt.dayOfWeek;
        this.relDayOfWeek = rt.relDayOfWeek;
        this.timeOfDay = rt.timeOfDay.copy();
    }

    @Override
    public Time asTime(Time relativeToTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(relativeToTime.asDate());

        if (dayOfWeek != null) {
            int execDayOfWeek = dayOfWeek.toJavaCalendarValue();
            int nowDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            if (nowDayOfWeek == execDayOfWeek) {                            // executes on same day of week as today
                TimeOfDay nowTimeOfDay = new TimeOfDay(relativeToTime);
                if (timeOfDay.compareTo(nowTimeOfDay) < 0) {                // already passed execution time for today
                    cal.add(Calendar.DATE, 7);                              // - so advance one week
                }

            } else {                                                        // does not execute today
                while (cal.get(Calendar.DAY_OF_WEEK) != execDayOfWeek) {    // - so advance date to next occurrence
                    cal.add(Calendar.DATE, 1);                              // of that day
                }
            }

        } else {
            // only need to adjust calendar if relDayOfWeek is TOMORROW - if it's TODAY, no calendar changes are needed
            if (relDayOfWeek.equals(RelativeDayOfWeek.TOMORROW)) {
                cal.add(Calendar.DATE, 1);
            }
        }

        cal.set(Calendar.HOUR_OF_DAY, timeOfDay.getHour().intValue());
        cal.set(Calendar.MINUTE, timeOfDay.getMinute().intValue());
        cal.set(Calendar.SECOND, timeOfDay.getSecond().intValue());

        return new Time(cal.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        RelativeTime rt = (RelativeTime) o;
        return new EqualsBuilder()
                .append(dayOfWeek, rt.dayOfWeek)
                .append(relDayOfWeek, rt.relDayOfWeek)
                .append(timeOfDay, rt.timeOfDay)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(743, 307)
                .append(dayOfWeek)
                .append(relDayOfWeek)
                .append(timeOfDay)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }

    @Override
    public RelativeTime copy() {
        return new RelativeTime(this);
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
