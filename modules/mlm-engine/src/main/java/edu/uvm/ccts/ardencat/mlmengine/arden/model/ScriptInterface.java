/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ABoolean;
import edu.uvm.ccts.arden.model.ADataType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public class ScriptInterface extends Interface {
    private String cmd;

    public ScriptInterface(String cmd) {
        this.cmd = cmd;
    }

    public ScriptInterface(ScriptInterface si) {
        cmd = si.cmd;
    }

    @Override
    public ADataType doExecute(List<ADataType> arguments) throws IOException, InterruptedException {
        Process p;
        if (arguments == null || arguments.isEmpty()) {
            p = Runtime.getRuntime().exec(cmd);

        } else {
            String[] args = new String[arguments.size()];
            for (int i = 0; i < arguments.size(); i ++) {
                args[i] = arguments.get(i).toString();
            }
            p = Runtime.getRuntime().exec(cmd, args);
        }

        p.waitFor();

        // note : interface return value for shell script can only be a number, with 0 meaning success

        int exitCode = p.exitValue();

        return new ABoolean(exitCode == 0);
    }

    public String getCmd() {
        return cmd;
    }

    @Override
    public String toString() {
        return "ScriptInterface{cmd=" + cmd + ", time=" + executionTime + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ScriptInterface si = (ScriptInterface) o;
        return new EqualsBuilder()
                .append(cmd, si.cmd)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(197, 241)
                .append(cmd)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (cmd == null)                return ((ScriptInterface) o).cmd == null;

        return cmd.equalsIgnoreCase(((ScriptInterface) o).cmd);
    }

    @Override
    public Interface copy() {
        return new ScriptInterface(this);
    }

    @Override
    public Object toJavaObject() {
        return cmd;
    }
}
