/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.common.exceptions.UnhandledClassException;
import edu.uvm.ccts.jmethodsig.model.JMethodSig;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 11/20/13.
 */
public class MethodInterface extends Interface {
    private JMethodSig sig;

    public MethodInterface(JMethodSig sig) {
        this.sig = sig;
    }

    public MethodInterface(MethodInterface mi) {
        this.sig = mi.sig;                          // todo : deep copy?
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ADataType doExecute(List<ADataType> arguments) throws Exception {
        Object[] args = convertToJavaObjectArray(arguments);
        Object obj = sig.execute(args);
        return convertToArdenObject(obj);
    }

    @Override
    public String toString() {
        return "MethodInterface{method=" + sig + ", time=" + executionTime + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        MethodInterface mi = (MethodInterface) o;
        return new EqualsBuilder()
                .append(sig, mi.sig)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(283, 127)
                .append(sig)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (sig == null)                return ((MethodInterface) o).sig == null;

        return sig.equals(((MethodInterface) o).sig);
    }

    @Override
    public Interface copy() {
        return new MethodInterface(this);
    }

    @Override
    public Object toJavaObject() {
        return sig;
    }

    private Object[] convertToJavaObjectArray(List<ADataType> arguments) {
        Object[] args = new Object[sig.getParameters().size()];

        // ensure that any extra arguments are ignored - would otherwise trigger an IndexOutOfBoundsException
        for (int i = 0; i < Math.min(arguments.size(), args.length); i ++) {
            args[i] = arguments.get(i).toJavaObject();
        }

        // ensure that any unspecified arguments are passed as null
        for (int i = arguments.size(); i < args.length; i ++) {
            args[i] = null;
        }

        return args;
    }

    private ADataType convertToArdenObject(Object obj) {
        if (obj == null) {
            return new ANull();

        } else if (obj instanceof Number) {
            return new ANumber((Number) obj);

        } else if (obj instanceof Boolean) {
            return new ABoolean((Boolean) obj);

        } else if (obj instanceof Character || obj instanceof String) {
            return new AString(obj.toString());

        } else if (obj instanceof Date) {
            return new Time((Date) obj);

        } else if (obj instanceof Collection) {       // includes List
            AList list = new AList();

            for (Object item : (Collection) obj) {
                if (item instanceof Collection) {
                    throw new RuntimeException("cannot convert object : Arden does not permit embedded lists");

                } else {
                    list.add(convertToArdenObject(item));
                }
            }

            return list;

        } else {
            throw new UnhandledClassException("unhandled class : " + obj.getClass().getName());
        }
    }
}
