/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model;

import edu.uvm.ccts.arden.action.BailActionLexer;
import edu.uvm.ccts.arden.action.BailActionParser;
import edu.uvm.ccts.arden.action.antlr.ActionBaseVisitor;
import edu.uvm.ccts.arden.action.antlr.ActionLexer;
import edu.uvm.ccts.arden.action.antlr.ActionParser;
import edu.uvm.ccts.arden.data.BailDataLexer;
import edu.uvm.ccts.arden.data.BailDataParser;
import edu.uvm.ccts.arden.data.antlr.DataBaseVisitor;
import edu.uvm.ccts.arden.data.antlr.DataLexer;
import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.arden.evoke.BailEvokeLexer;
import edu.uvm.ccts.arden.evoke.BailEvokeParser;
import edu.uvm.ccts.arden.evoke.antlr.EvokeBaseVisitor;
import edu.uvm.ccts.arden.evoke.antlr.EvokeLexer;
import edu.uvm.ccts.arden.evoke.antlr.EvokeParser;
import edu.uvm.ccts.arden.expr.BailExprLexer;
import edu.uvm.ccts.arden.expr.BailExprParser;
import edu.uvm.ccts.arden.expr.antlr.ExprLexer;
import edu.uvm.ccts.arden.expr.antlr.ExprParser;
import edu.uvm.ccts.arden.logic.BailLogicLexer;
import edu.uvm.ccts.arden.logic.BailLogicParser;
import edu.uvm.ccts.arden.logic.antlr.LogicBaseVisitor;
import edu.uvm.ccts.arden.logic.antlr.LogicLexer;
import edu.uvm.ccts.arden.logic.antlr.LogicParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.action.ActionEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.DataEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.IncludeDataEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.ValidationDataEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.EvokeEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model.EvokeCondition;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model.SimpleTrigger;
import edu.uvm.ccts.ardencat.mlmengine.arden.expr.ExprEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.logic.LogicEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMLogUtil;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.exceptions.CategoryNotFoundException;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.exceptions.MetUntilConditionException;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.ConcurrentExecutionException;
import edu.uvm.ccts.ardencat.mlmengine.model.log.Status;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.util.ExceptionUtil;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 8/9/13.
 */
public class MLM {
    private static final Log log = LogFactory.getLog(MLM.class);

    private String id = null;
    private List<Category> categories;
    private Time executionTime = null;
    private List<EvokeCondition> evokeConditions = null;

    public MLM(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<String> getCategoryNames() {
        List<String> list = new ArrayList<String>();
        for (Category category : categories) {
            list.add(category.getName());
        }
        return list;
    }

    public Category getCategory(String name) throws CategoryNotFoundException {
        for (Category category : categories) {
            if (category.getName().equals(name)) return category;
        }

        throw new CategoryNotFoundException("category '" + name + "' does not exist.");
    }

    public boolean isRunning() {
        return executionTime != null;
    }

    public Time getExecutionTime() {
        return executionTime;
    }

    public String getId() {
        if (id == null) {
            try {
                Category mCat = getCategory("maintenance");
                List<String> list = new ArrayList<String>();

                list.add(mCat.getSlot("mlmname").getData());
                list.add(mCat.getSlot("institution").getData());
                list.add(mCat.getSlot("validation").getData());
                list.add(mCat.getSlot("version").getData());

                id = StringUtils.join(list, ":");

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return id;
    }

    public void populateEvokeConditions() throws IOException, ConfigurationException {
        if (evokeConditions == null) {
            Variables variables = getVariablesForInclusion();

            String evoke = getCategory("knowledge").getSlot("evoke").getData();
            EvokeEvalVisitor visitor = new EvokeEvalVisitor(this, variables);
            visitor.setPrintStream(null);
            visitor.visit(buildEvokeParseTree(evoke));

            evokeConditions = visitor.getEvokeConditions();
        }
    }

    public List<EvokeCondition> getEvokeConditions() {
        return evokeConditions;
    }

    public boolean mayBeCalled() {
        for (EvokeCondition ec : evokeConditions) {
            if (ec instanceof SimpleTrigger) {
                if (((SimpleTrigger) ec).mayBeCalled()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isTriggeredBy(Event event) {
        for (EvokeCondition ec : evokeConditions) {
            if (ec instanceof SimpleTrigger) {
                if (((SimpleTrigger) ec).isTriggeredBy(event)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void validateParse() throws ConfigurationException, IOException {
        Category kCat = getCategory("knowledge");

        String data = kCat.getSlot("data").getData();
        ParseTree dataParseTree = buildDataParseTree(data);

        new DataBaseVisitor().visit(dataParseTree);

        ValidationDataEvalVisitor vdev = new ValidationDataEvalVisitor();
        vdev.visit(dataParseTree);
        if ( ! vdev.isValid() ) {
            throw new ConfigurationException(vdev.getMessage());
        }

        String logic = kCat.getSlot("logic").getData();
        new LogicBaseVisitor().visit(buildLogicParseTree(logic));

        String evoke = kCat.getSlot("evoke").getData();
        new EvokeBaseVisitor().visit(buildEvokeParseTree(evoke));

        String action = kCat.getSlot("action").getData();
        new ActionBaseVisitor().visit(buildActionParseTree(action));
    }

    public ADataType execute() throws IOException, ConfigurationException, MetUntilConditionException, ConcurrentExecutionException {
        return execute(System.out, null, null);
    }

    public ADataType execute(PrintStream printStream) throws IOException, ConfigurationException,
            MetUntilConditionException, ConcurrentExecutionException {

        return execute(printStream, null, null);
    }

    public ADataType execute(PrintStream printStream, List<ADataType> arguments) throws ConfigurationException,
            IOException, MetUntilConditionException, ConcurrentExecutionException {

        return execute(printStream, arguments, null);
    }

    public ADataType execute(PrintStream printStream, List<ADataType> arguments, String untilExpr)
            throws IOException, ConfigurationException, MetUntilConditionException, ConcurrentExecutionException {

        if (isRunning()) {
            throw new ConcurrentExecutionException("MLM {" + getId() + "} is currently executing (executionTime=" + executionTime + ")");
        }

        Status status = Status.SUCCESS;
        String message = null;
        Integer logId = null;

        try {
            executionTime = new Time(new Date());

            log.info("executing MLM[id=" + getId() + ", executionTime=" + executionTime + "]");
            logId = MLMLogUtil.createRunningExecutionLog(getId(), executionTime);

            Category kCat = getCategory("knowledge");

            DataEvalVisitor dv = new DataEvalVisitor(this, arguments);
            dv.setPrintStream(printStream);
            dv.visit(buildDataParseTree(kCat.getSlot("data").getData()));
            Variables variables = dv.getVariables();

            // 13.3.4.2
            // If there is an until clause, then it is evaluated as soon as the MLM is triggered; the clause may
            // contain references to the patient database unrelated to the event. If it is true then the MLM exits
            // immediately, and no further triggering occurs. Otherwise, the MLM is executed, and it is triggered
            // again after the every duration (assuming the for duration has not run out).

            if (untilExpr != null) {
                ExprEvalVisitor ev = new ExprEvalVisitor(this, variables);
                ev.visit(buildExprParseTree(untilExpr));
                ADataType obj = ev.getValue();

                if (obj instanceof ABoolean) {
                    if (((ABoolean) obj).isTrue()) {
                        throw new MetUntilConditionException();
                    }
                }
            }

            LogicEvalVisitor lv = new LogicEvalVisitor(this, variables);
            lv.setPrintStream(printStream);
            lv.visit(buildLogicParseTree(kCat.getSlot("logic").getData()));
            ADataType concludeVal = lv.getConcludeValue();

            if (concludeVal instanceof ABoolean) {
                if (((ABoolean) concludeVal).isTrue()) {
                    variables = lv.getVariables();
                    ActionEvalVisitor av = new ActionEvalVisitor(this, variables);
                    av.setPrintStream(printStream);
                    av.visit(buildActionParseTree(kCat.getSlot("action").getData()));

                    // If the called MLM concludes true and there is a return statement in the called MLM's action slot
                    // (see Section 12.2.2), then the value of its expression is assigned to <var>. If the return
                    // statement has more values than the calling MLM can accept, then the extra return values are
                    // silently dropped. If the return statement has fewer values than the calling MLM is expecting,
                    // then the extra return values are null. If there is no return statement, or if the called MLM
                    // concludes false, then null is assigned to <var>.

                    return av.getReturnValue();
                }
            }

            return new ANull();

        } catch (RuntimeException e) {
            status = Status.FAIL;
            message = ExceptionUtil.getStackTrace(ExceptionUtil.getOriginalCausedByException(e));
            throw e;

        } catch (IOException e) {
            status = Status.FAIL;
            message = ExceptionUtil.getStackTrace(ExceptionUtil.getOriginalCausedByException(e));
            throw e;

        } catch (ConfigurationException e) {
            status = Status.FAIL;
            message = ExceptionUtil.getStackTrace(ExceptionUtil.getOriginalCausedByException(e));
            throw e;

        } finally {
            log.info("FINISHED executing MLM[id=" + getId() + ", executionTime=" + executionTime + "]");
            MLMLogUtil.updateExecutionLog(logId, status, message);
            executionTime = null;
        }
    }

    public Variables getVariablesForInclusion() throws  IOException, ConfigurationException {
        String data = getCategory("knowledge").getSlot("data").getData();

        IncludeDataEvalVisitor visitor = new IncludeDataEvalVisitor(this);
        visitor.setPrintStream(null);
        visitor.visit(buildDataParseTree(data));

        return visitor.getVariables();
    }


//////////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private ParseTree buildDataParseTree(String data) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        DataLexer lexer = new BailDataLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DataParser parser = new BailDataParser(tokens);
        return parser.init();
    }

    private ParseTree buildLogicParseTree(String logic) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(logic.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        LogicLexer lexer = new BailLogicLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LogicParser parser = new BailLogicParser(tokens);
        return parser.init();
    }

    private ParseTree buildExprParseTree(String expr) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(expr.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        ExprLexer lexer = new BailExprLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ExprParser parser = new BailExprParser(tokens);
        return parser.init();
    }

    private ParseTree buildActionParseTree(String action) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(action.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        ActionLexer lexer = new BailActionLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ActionParser parser = new BailActionParser(tokens);
        return parser.init();
    }

    private ParseTree buildEvokeParseTree(String evoke) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(evoke.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        EvokeLexer lexer = new BailEvokeLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        EvokeParser parser = new BailEvokeParser(tokens);
        return parser.init();
    }
}
