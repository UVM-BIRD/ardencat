/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.SchedulerException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by mstorer on 11/15/13.
 */

// todo : implement primary time / clinically relevant time for this object (it is presently inoperable)

public class Event extends NonPrimaryTimeDataType<Event> {
    private static final Log log = LogFactory.getLog(Event.class);

    private String name;
    private Time executionTime = null;

    public Event(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * The variable that represents the event can be treated like a Boolean in the logic or action slots. The Boolean
     * value of the variable is false until the MLM is called by the referred event.
     * @spec 11.2.3.1
     * @return
     */
    public ABoolean asBoolean() {
        return new ABoolean(hasExecuted());
    }

    /**
     * If <name> is an event variable, then execution is similar. The main MLM is interrupted, and all the MLMs whose
     * evoke slots refer to the named event are executed (see Section 13). They each receive the parameters if there
     * are any via their argument statement(s). The results of all called MLM's return statements are concatenated
     * together into a list; called MLMs with no return statement and called MLMs that return a single null are not
     * included in the result. The order of the returned values is implementation dependent. The result is assigned to
     * <var>, and execution continues. <var> will always be a list, even if it has one item.
     * @see 10.2.5.6
     * @param printStream
     * @param arguments
     * @return
     */
    public AList execute(PrintStream printStream, List<ADataType> arguments) throws SchedulerException {
        executionTime = new Time(System.currentTimeMillis() / 1000);

        log.info("executing Event[name=" + getName() + ", executionTime=" + executionTime + "]");

        AList list = new AList();

        for (MLM mlm : MLMRegistry.getInstance().getMLMs()) {
            if (mlm.isTriggeredBy(this)) {
                try {
                    ADataType rval = mlm.execute(printStream, arguments);

                    // todo : trigger record builder action if appropriate.
                    // but this is easier said than done, as we can't check to see if mlm is an instance of ServiceMLM

                    if ( ! (rval instanceof ANull) ) {
                        list.add(rval);
                    }

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " executing event '" + name +
                            "' - " + e.getMessage());
                }
            }
        }

        MLMScheduler.getInstance().scheduleEventBasedTriggers(this);

        return list;
    }

    public boolean hasExecuted() {
        return executionTime != null;
    }

    public Time getExecutionTime() {
        return executionTime;
    }

    @Override
    public String toString() {
        return "Event{name=" + name + ", time=" + executionTime + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        Event e = (Event) o;
        return new EqualsBuilder()
                .append(name, e.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(317, 241)
                .append(name)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (name == null)               return ((Event) o).name == null;

        return name.equalsIgnoreCase(((Event) o).name);
    }

    @Override
    public Event copy() {       // event should not be copied - want to keep same object
        return this;
    }

    @Override
    public Object toJavaObject() {
        return name;
    }
}
