/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.config.StandardConfigurator;
import edu.uvm.ccts.ardencat.mlmengine.remote.MLMEngineRemoteEndpoint;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import edu.uvm.ccts.ardencat.rmi.RMIConfigUtil;
import edu.uvm.ccts.ardencat.rmi.model.RMIConfig;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.rmi.RMILoopbackClientSocketFactory;
import edu.uvm.ccts.common.rmi.RMILoopbackServerSocketFactory;
import edu.uvm.ccts.common.rmi.RMIServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by mstorer on 1/15/14.
 */
public class MLMEngine implements MLMEngineRemoteEndpoint {
    private static final Log log = LogFactory.getLog(MLMEngine.class);

    private static final Object sync = new Object();

    private static boolean running = true;
    private static MLMEngine svc;

    public static void main(String[] args) {
        System.out.println("ArdenCAT MLM Engine");
        System.out.println("-------------------");
        System.out.println("Copyright 2015 The University of Vermont and State Agricultural College, Vermont Oxford Network, and The University of Vermont Medical Center.  All rights reserved.\n");

        svc = new MLMEngine();
        svc.start();

        log.info("MLM Engine started successfully.");

        // fix to ensure service stays running - main thread cannot terminate before stop() is called
        // see http://www.advancedinstaller.com/forums/viewtopic.php?f=2&t=1996#p6803
        synchronized(sync) {
            while (running) {
                try {
                    sync.wait();

                } catch (InterruptedException e) {
                    log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
                }
            }
        }

        log.info("MLM Engine has stopped.");
    }

    public static void stop() {         // used by Windows Service -
                                        // see http://www.advancedinstaller.com/user-guide/tutorial-java-service.html#preparing
        svc.shutdown();

        running = false;
        synchronized(sync) {
            sync.notify();
        }
    }


/////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private RMIServer rmiServer;

    private MLMEngine() {
    }

    private void start() {
        try {
            MLMScheduler.getInstance().start();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " starting MLM Scheduler - " + e.getMessage(), e);
            System.exit(-1);
        }

        try {
            setupRMI();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " configuring RMI - " + e.getMessage(), e);
            System.exit(-1);
        }

        DataSource.startMaintenanceThread(20, 15);


        new StandardConfigurator().restoreConfigIfExists();
    }

    private void shutdown() {
        try {
            teardownRMI();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " shutting down RMI registry - " + e.getMessage(), e);
        }

        try {
            MLMScheduler.getInstance().stop();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " stopping MLM Scheduler - " + e.getMessage(), e);
        }

        try {
            DataSourceRegistry.getInstance().closeAll();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " closing Data Source connections - " + e.getMessage(), e);
        }
    }

    private void setupRMI() throws Exception {
        RMIConfig rmiConfig = RMIConfigUtil.getRMIConfig();

        rmiServer = new RMIServer(REGISTRY_NAME, rmiConfig.getRegistryPort(), rmiConfig.getCommPort(), new StandardManagementImpl(),
                new RMILoopbackClientSocketFactory(),
                new RMILoopbackServerSocketFactory());

        boolean started = rmiServer.start();
        if (started) log.info("RMI server is now listening on port " + rmiConfig.getRegistryPort() + ".");
    }

    private void teardownRMI() throws RemoteException, NotBoundException {
        boolean stopped = rmiServer.stop();
        if (stopped) log.info("RMI server has been shut down.");
    }
}
