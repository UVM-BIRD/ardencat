/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data.model;

import com.foundationdb.sql.StandardException;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.PrimaryTimeNotFoundException;
import edu.uvm.ccts.arden.model.Duration;
import edu.uvm.ccts.arden.model.Time;
import edu.uvm.ccts.ardencat.mlmengine.model.PrimaryTimeField;
import edu.uvm.ccts.common.sql.SqlStatement;
import edu.uvm.ccts.common.sql.exceptions.TableNotFoundException;
import edu.uvm.ccts.common.sql.model.Field;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 11/6/13.
 */
public class ArdenSqlStatement extends SqlStatement {
    private List<Field> primaryTimeFields = new ArrayList<Field>();
    private List<Field> addedSelectFields = new ArrayList<Field>();
    private Map<String, String> fieldPrimaryTimeFieldMap = new HashMap<String, String>();

    public ArdenSqlStatement(String sql, ArdenDataSource dataSource) throws StandardException,
            PrimaryTimeNotFoundException, TableNotFoundException {

        super(sql);

        buildPrimaryTimeFields(dataSource);

        List<Field> selectedFields = getSelectedFields();

        for (int i = 0; i < primaryTimeFields.size(); i ++) {
            Field ptf = primaryTimeFields.get(i);

            // if it is in selectedFields list already
            //   replace timestampField with already selected matching field in primaryTimeFields list

            // otherwise, add timestampField to selectedFields

            Field matchingField = null;
            for (Field sf : selectedFields) {
                boolean tableMatch = sf.getTable().equalsIgnoreCase(ptf.getTable());
                boolean fieldMatch = sf.getName().equalsIgnoreCase(ptf.getName());

                if (tableMatch && fieldMatch) {
                    // don't care about field alias - maybe it's aliased, and maybe it's not?

                    matchingField = sf;
                    break;
                }
            }

            if (matchingField != null) {
                primaryTimeFields.set(i, matchingField);

            } else {
                addSelectField(ptf);

                // NOTE : at this point, selectedFields list is stale
            }
        }

        for (Field sf : selectedFields) {
            PrimaryTimeField ptf = dataSource.getPrimaryTimeFieldForTable(getActualNameForTable(sf.getTable()));
            Field f = new Field(getAliasOrNameForTable(ptf.getFromTable()), ptf.getField());
            fieldPrimaryTimeFieldMap.put(sf.asResultSetLabel().toLowerCase(), f.asResultSetLabel());
        }
    }

    public List<Field> getAddedSelectFields() {
        return addedSelectFields;
    }

    public List<Field> getPrimaryTimeFields() {
        return primaryTimeFields;
    }

    public String getAssociatedPrimaryTimeFieldLabel(String resultSetLabel) throws PrimaryTimeNotFoundException {
        if (fieldPrimaryTimeFieldMap.containsKey(resultSetLabel.toLowerCase())) {
            return fieldPrimaryTimeFieldMap.get(resultSetLabel.toLowerCase());
        }

        throw new PrimaryTimeNotFoundException("field '" + resultSetLabel + "' has no associated timestamp");
    }

    @Override
    public void addSelectField(Field field) throws StandardException {
        super.addSelectField(field);
        addedSelectFields.add(field);
    }

    private void buildPrimaryTimeFields(ArdenDataSource dataSource) throws TableNotFoundException, PrimaryTimeNotFoundException {
        // identify a list of primary time fields for this particular query.

        List<String> processedTables = new ArrayList<String>();

        for (Field sf : getSelectedFields()) {
            String selectTable = getActualNameForTable(sf.getTable());
            if ( ! processedTables.contains(selectTable) ) {
                PrimaryTimeField ptf = dataSource.getPrimaryTimeFieldForTable(selectTable);
                Field f = new Field(getAliasOrNameForTable(ptf.getFromTable()), ptf.getField());

                if ( ! primaryTimeFields.contains(f) ) {
                    primaryTimeFields.add(f);
                }

                processedTables.add(selectTable);
            }
        }
    }

    public void appendEqualWhereClause(Time time, boolean invert) throws StandardException {
        String op = invert ? "!=" : "=";
        appendSimpleWhereClause(time, op);
    }

    public void appendBeforeWhereClause(Time time, boolean invert) throws StandardException {
        String op = invert ? ">=" : "<";
        appendSimpleWhereClause(time, op);
    }

    public void appendAfterWhereClause(Time time, boolean invert) throws StandardException {
        String op = invert ? "<=" : ">";
        appendSimpleWhereClause(time, op);
    }

    public void appendBetweenWhereClause(Time fromTime, Time toTime, boolean invert) throws StandardException {
        String op = invert ? "not between" : "between";
        List<String> list = new ArrayList<String>();
        for (Field ptf : primaryTimeFields) {
            String sql = "(" + ptf.asWhereClauseField() + " " + op + " ? and ?)";
            list.add(sql);
        }

        List<Object> values = new ArrayList<Object>();
        for (int i = 0; i < primaryTimeFields.size(); i ++) {
            values.add(fromTime.toJavaObject());
            values.add(toTime.toJavaObject());
        }

        addWhereClause(StringUtils.join(list, " and "), values);
    }

    public void appendPrecedingWhereClause(Duration duration, Time time, boolean invert) throws StandardException {
        Time fromTime = time.subtract(duration);
        appendBetweenWhereClause(fromTime, time, invert);
    }

    public void appendFollowingWhereClause(Duration duration, Time time, boolean invert) throws StandardException {
        Time toTime = time.add(duration);
        appendBetweenWhereClause(time, toTime, invert);
    }

    public void appendSurroundingWhereClause(Duration duration, Time time, boolean invert) throws StandardException {
        Time fromTime = time.subtract(duration);
        Time toTime = time.add(duration);
        appendBetweenWhereClause(fromTime, toTime, invert);
    }

    public void appendPastWhereClause(Duration duration, Time now, boolean invert) throws StandardException {
        Time fromTime = now.subtract(duration);
        appendBetweenWhereClause(fromTime, now, invert);
    }

    public void appendSameDayAsWhereClause(Time time, boolean invert) throws StandardException {
        Time fromTime = time.copy();
        fromTime.setHour(0);
        fromTime.setMinute(0);
        fromTime.setSecond(0);

        Time toTime = time.copy();
        toTime.setHour(23);
        toTime.setMinute(59);
        toTime.setSecond(59);

        appendBetweenWhereClause(fromTime, toTime, invert);
    }

    private void appendSimpleWhereClause(Time time, String op) throws StandardException {
        List<String> list = new ArrayList<String>();
        for (Field ptf : primaryTimeFields) {
            String sql = "(" + ptf.asWhereClauseField() + " " + op + " ?)";
            list.add(sql);
        }

        List<Object> values = new ArrayList<Object>();
        for (int i = 0; i < primaryTimeFields.size(); i ++) {
            values.add(time.toJavaObject());
        }

        addWhereClause(StringUtils.join(list, " and "), values);
    }

}
