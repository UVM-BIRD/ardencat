/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.common.db.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mbstorer on 8/7/15.
 */
public class MLMBatchAuthority {
    private static final Log log = LogFactory.getLog(MLMBatchAuthority.class);

    private static MLMBatchAuthority authority = null;

    public static MLMBatchAuthority getInstance() {
        if (authority == null) authority = new MLMBatchAuthority();
        return authority;
    }


////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private Map<String, BatchData> map = new HashMap<String, BatchData>();

    private MLMBatchAuthority() {
        // private to make singleton
    }

    private static final Object sync = new Object();

    public String getBatch(MLM mlm) throws SQLException {
        if (mlm.getExecutionTime() == null) return null;

        BatchData bd = map.get(mlm.getId());

        if ( bd == null || ! bd.getMlmExecutionTime().equals(mlm.getExecutionTime().asDate()) ) {
            // new, or exists but with a different execution time.  in either case, create a new batch

            synchronized(sync) {
                bd = new BatchData(mlm.getExecutionTime().asDate(), generateNewBatch(mlm));
                map.put(mlm.getId(), bd);
            }
        }

        return bd.getBatch();
    }

    private static final Pattern BATCH_PATTERN = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})\\.(\\d+)$");
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    private String generateNewBatch(MLM mlm) throws SQLException {
        log.debug("generateNewBatch : generating a new batch for source {" + mlm.getId() + "}");

        String newBatch;
        String todayDate = SDF.format(new Date());

        DataSource dataSource = DataSourceRegistry.getInstance().getArdencatDataSource();
        String mostRecentBatch = getMostRecentBatch(dataSource, mlm);

        if (mostRecentBatch != null) {
            Matcher matcher = BATCH_PATTERN.matcher(mostRecentBatch);
            if (matcher.find()) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR,          Integer.parseInt(matcher.group(1)));
                cal.set(Calendar.MONTH,         Integer.parseInt(matcher.group(2)) - 1);
                cal.set(Calendar.DAY_OF_MONTH,  Integer.parseInt(matcher.group(3)));

                if (todayDate.equals(SDF.format(cal.getTime()))) {
                    int count = Integer.parseInt(matcher.group(4));
                    newBatch = todayDate + "." + String.format("%03d", count + 1);

                } else {
                    newBatch = todayDate + ".000";
                }

                log.debug("generateNewBatch : found previous batch {" + mostRecentBatch + "}!  creating new batch = {" + newBatch + "}");

                updateMostRecentBatch(dataSource, mlm, newBatch);

            } else {
                throw new RuntimeException("encountered invalid batch : '" + mostRecentBatch + "'");
            }

        } else {
            // this is the first batch!

            newBatch = todayDate + ".000";

            log.debug("generateNewBatch : no previous batches found!  creating new batch {" + newBatch + "}");

            createMostRecentBatch(dataSource, mlm, newBatch);
        }

        return newBatch;
    }

    private String getMostRecentBatch(DataSource dataSource, MLM mlm) throws SQLException {
        String query = "select mostRecentBatch from tblACBatchData where source=?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, mlm.getId());

        ResultSet rs = stmt.executeQuery();

        return rs.next() ?
                rs.getString(1) :
                null;
    }

    private void createMostRecentBatch(DataSource dataSource, MLM mlm, String newBatch) throws SQLException {
        String query = "insert into tblACBatchData (source, mostRecentBatch) values(?, ?)";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, mlm.getId());
        stmt.setString(2, newBatch);

        stmt.executeUpdate();
    }

    private void updateMostRecentBatch(DataSource dataSource, MLM mlm, String newBatch) throws SQLException {
        String query = "update tblACBatchData set mostRecentBatch = ? where source = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, newBatch);
        stmt.setString(2, mlm.getId());

        stmt.executeUpdate();
    }

    private static final class BatchData {
        private Date mlmExecutionTime;
        private String batch;

        public BatchData(Date mlmExecutionTime, String batch) {
            this.mlmExecutionTime = mlmExecutionTime;
            this.batch = batch;
        }

        public Date getMlmExecutionTime() {
            return mlmExecutionTime;
        }

        public String getBatch() {
            return batch;
        }
    }

}
