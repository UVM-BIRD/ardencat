/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.ardencat.mlmengine.MLMEngineConfigRegistry;
import edu.uvm.ccts.common.mail.MailConfig;
import edu.uvm.ccts.common.mail.MailUtil;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 1/16/14.
 */
public class EmailDestination extends Destination<EmailDestination> {
    private static final Log log = LogFactory.getLog(EmailDestination.class);

    private String subject;
    private String to;

    public EmailDestination(String subject, String to) {
        this.subject = subject;
        this.to = to;
    }

    public EmailDestination(EmailDestination d) {
        subject = d.subject;
        to = d.to;
    }

    @Override
    public void write(ADataType obj) throws Exception {
        MailConfig mc = MLMEngineConfigRegistry.getInstance().getConfig().getMailConfig();
        if (mc != null) {
            MailUtil.sendMail(mc, subject, to, obj.toString());

        } else {
            log.warn("mail is not configured!  skipping write -");
        }
    }

    @Override
    public EmailDestination copy() {
        return new EmailDestination(this);
    }

    @Override
    public String toString() {
        return "EmailDestination{subject=" + subject + ", to=" + to + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        EmailDestination d = (EmailDestination) o;
        return new EqualsBuilder()
                .append(subject, d.subject)
                .append(to, d.to)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(257, 499)
                .append(subject)
                .append(to)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
