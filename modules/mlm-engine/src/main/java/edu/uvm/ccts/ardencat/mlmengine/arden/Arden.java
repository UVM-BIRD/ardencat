/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden;

import edu.uvm.ccts.ardencat.arden.AbstractArden;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.AMLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Interface;
import org.apache.commons.logging.Log;

import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 12/18/13.
 */
public class Arden extends AbstractArden<Value> {
    public Arden(Log log, MLM caller) {
        super(log, caller != null ? caller.getExecutionTime() : new Time(new Date()));
    }

    @Override
    protected Value createValue(ADataType obj) {
        return obj == null ? NULL() : new Value(obj);
    }

    @Override
    protected Value NULL() {
        return new Value(ANull.NULL());
    }

    @Override
    protected Value VOID() {
        return new Value(AVoid.VOID());
    }

    @Override
    protected Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    @Override
    protected Value FALSE() {
        return new Value(ABoolean.FALSE());
    }

    // overridden to handle case where object is an Event
    @Override
    public Value id(String id) {
        ADataType obj = evaluateId(id);

        if (obj instanceof Event) {             // event variables are treated as Boolean in the logic-slot
            Event e = (Event) obj;              // spec 11.2.3.1
            return new Value(e.asBoolean());
        }

        return new Value(obj);
    }

    // call may only be used in MLM Engine
    public Value call(String id, List<ADataType> arguments) {
        ADataType adt = evaluateId(id);

        if (adt instanceof AMLM) {                          // spec 10.2.5.5
            MLM mlm = ((AMLM) adt).getMlm();

            try {
                if (mlm.mayBeCalled()) {
                    ADataType rval = mlm.execute(printStream, arguments);
                    return new Value(rval);

                } else {
                    throw new RuntimeException("may not call MLM with id='" + id + "'");
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        } else if (adt instanceof Event) {                  // spec 10.2.5.6
            Event event = (Event) adt;

            try {
                AList list = event.execute(printStream, arguments);
                return new Value(list);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        } else if (adt instanceof Interface) {              // spec 10.2.5.7
            Interface iface = (Interface) adt;

            try {
                ADataType rval = iface.execute(arguments);
                return new Value(rval);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        } else {
            throw new RuntimeException("cannot call object '" + id + "' of type " + adt.getClass().getName());
        }
    }

    // overridden to handle case where value is an Event
    @Override
    public Value timeFunc(Value v) {
        if (v.isEvent()) {
            Event e = v.asEvent();
            if (e.hasExecuted()) {
                return new Value(e.getExecutionTime());

            } else {
                return NULL();
            }

        } else {
            return super.timeFunc(v);
        }
    }
}
