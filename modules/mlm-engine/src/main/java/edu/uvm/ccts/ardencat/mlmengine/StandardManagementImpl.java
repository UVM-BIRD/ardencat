/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMLogUtil;
import edu.uvm.ccts.ardencat.mlmengine.config.StandardConfigurator;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.ScheduleException;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.ScheduleItem;
import edu.uvm.ccts.ardencat.mlmengine.remote.StandardManagement;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.*;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import org.quartz.SchedulerException;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 4/28/14.
 */
public class StandardManagementImpl implements StandardManagement {
    private StandardConfigurator configurator = new StandardConfigurator();

    // todo : integrate this with ServiceManagementImpl

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public void closeConnections() throws RemoteException {
        try {
            DataSourceRegistry.getInstance().closeAll();

        } catch (Exception e) {
            // handle silently
        }
    }

    @Override
    public ConfigVO getConfig() {
        ConfigVO vo = new ConfigVO();

        vo.setConfig(MLMEngineConfigRegistry.getInstance().getConfig());

        return vo;
    }

    @Override
    public void setConfig(ConfigRO configRo) throws ScheduleException, ConfigurationException {
        configurator.loadConfig(configRo.getConfig());
    }

    @Override
    public LogVO getLog(LogRO logRo) {
        LogVO vo = new LogVO();
        vo.setLogItemList(MLMLogUtil.getLog(logRo.getStartDate(), logRo.getEndDate()));
        return vo;
    }

    @Override
    public ScheduleVO getSchedule() throws ScheduleException {
        try {
            ScheduleVO vo = new ScheduleVO();

            List<ScheduleItem> list = new ArrayList<ScheduleItem>();
            for (Job job : MLMScheduler.getInstance().getScheduledJobs()) {
                list.add(new ScheduleItem(job));
            }

            vo.setScheduleItemList(list);

            return vo;

        } catch (SchedulerException e) {
            throw new ScheduleException(e.getMessage());
        }
    }
}
