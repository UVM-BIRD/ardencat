/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.arden.model.Time;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Represents Delayed Event Trigger
 * @see 13.3.2
 */
public class DelEventTrigger extends EvokeCondition<DelEventTrigger> {
    private TimeExpr timeExpr;
    private Event event;

    public DelEventTrigger(TimeExpr timeExpr, Event event) {
        this.timeExpr = timeExpr;
        this.event = event;
    }

    public DelEventTrigger(DelEventTrigger det) {
        this.timeExpr = det.timeExpr.copy();
        this.event = det.event.copy();
    }

    public TimeExpr getTimeExpr() {
        return timeExpr;
    }

    public boolean isTriggeredBy(Event event) {
        return event != null && event.equals(this.event);
    }

    public Time getNextTriggerTime() {
        return timeExpr.getNextTriggerTime(event.getExecutionTime());
    }

    @Override
    public DelEventTrigger copy() {
        return new DelEventTrigger(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        DelEventTrigger det = (DelEventTrigger) o;
        return new EqualsBuilder()
                .append(timeExpr, det.timeExpr)
                .append(event, det.event)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(607, 563)
                .append(timeExpr)
                .append(event)
                .toHashCode();
    }
}
