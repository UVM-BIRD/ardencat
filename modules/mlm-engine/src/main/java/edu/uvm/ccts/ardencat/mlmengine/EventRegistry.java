/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 1/3/14.
 */
public class EventRegistry {
    private static EventRegistry registry = null;

    public static EventRegistry getInstance() {
        if (registry == null) registry = new EventRegistry();
        return registry;
    }

    private Map<String, Event> map = new HashMap<String, Event>();

    private EventRegistry() {
    }

    public boolean isRegistered(String eventName) {
        return map.containsKey(eventName.toLowerCase());
    }

    public void register(Event event) {
        if (isRegistered(event.getName())) {
            throw new RuntimeException("event '" + event.getName() + "' is already registered");
        }

        map.put(event.getName().toLowerCase(), event);
    }

    public Event getEvent(String name) {
        return map.get(name.toLowerCase());
    }
}
