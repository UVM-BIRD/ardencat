/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data;

import edu.uvm.ccts.arden.data.antlr.DataBaseVisitor;
import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * Created by mbstorer on 11/13/15.
 */
public class ValidationDataEvalVisitor extends DataBaseVisitor<Value> {
    private boolean valid = true;
    private String message = null;

    public boolean isValid() {
        return valid;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public Value visitInit(DataParser.InitContext ctx) {
        try {
            return super.visitInit(ctx);

        } catch (ValidationException e) {
            valid = false;
            message = e.getMessage();

            return Value.VOID;
        }
    }

    @Override
    public Value visitReadAs(DataParser.ReadAsContext ctx) {
        validateDataSourceKey(ctx.Mapping());
        return super.visitReadAs(ctx);
    }

    @Override
    public Value visitRead(DataParser.ReadContext ctx) {
        validateDataSourceKey(ctx.Mapping());
        return super.visitRead(ctx);
    }

    private void validateDataSourceKey(TerminalNode mapping) {
        String key = extractDataSourceKey(chop(mapping));
        if ( ! DataSourceRegistry.getInstance().isMLMDataSourceRegistered(key) ) {
            throw new ValidationException("MLM data source '" + key + "' is not defined");
        }
    }

    private String extractDataSourceKey(String mapping) {
        int pos = mapping.indexOf(':');

        if (pos == -1) {
            throw new ValidationException("MLM read mapping is missing data source identifier");

        } else {
            String key = mapping.substring(0, pos).trim();
            if (key.isEmpty()) {
                throw new ValidationException("MLM read mapping data source identifier cannot be blank");
            }

            return key;
        }
    }

    private String chop(TerminalNode tn) {
        String s = tn.getText();
        return s.substring(1, s.length() - 1);
    }

    private static final class ValidationException extends RuntimeException {
        public ValidationException(String message) {
            super(message);
        }
    }
}
