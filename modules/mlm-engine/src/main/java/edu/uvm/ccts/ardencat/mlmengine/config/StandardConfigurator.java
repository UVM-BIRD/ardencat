/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.config;

import edu.uvm.ccts.ardencat.mlmengine.MLMEngine;
import edu.uvm.ccts.ardencat.mlmengine.MLMEngineConfigRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.StandardMLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMEngineConfig;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.JarUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mbstorer on 11/13/15.
 */
public class StandardConfigurator extends Configurator<MLMEngineConfig> {
    private static final Log log = LogFactory.getLog(StandardConfigurator.class);

    private static String CONFIG_FILE;
    static {
        try {
            CONFIG_FILE = FileUtil.buildPath(JarUtil.getJarDir(MLMEngine.class), ".mlm-engine-config");
        } catch (Exception e) {
            CONFIG_FILE = ".mlm-engine-config";
        }
    }

    @Override
    protected MLMBuilder getMLMBuilder() {
        return new StandardMLMBuilder();
    }

    @Override
    protected MLMEngineConfig getActiveConfig() {
        if (FileUtil.exists(CONFIG_FILE)) {
            try {
                return (MLMEngineConfig) FileUtil.deserializeFromFile(CONFIG_FILE);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " reading file '" + CONFIG_FILE + "' - " + e.getMessage(), e);
            }
        }

        return null;
    }

    @Override
    protected void setActiveConfig(MLMEngineConfig cfg) throws Exception {
        FileUtil.serializeToFile(CONFIG_FILE, cfg);
    }

    @Override
    protected void deleteActiveConfig() {
        FileUtil.delete(CONFIG_FILE);
    }

    @Override
    protected void registerConfig(MLMEngineConfig cfg) {
        MLMEngineConfigRegistry.getInstance().registerConfig(cfg);
    }

    @Override
    protected void clearConfigRegistry() {
        MLMEngineConfigRegistry.getInstance().clear();
    }
}
