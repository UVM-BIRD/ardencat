/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.scheduler;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.Duration;
import edu.uvm.ccts.arden.model.Time;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.JobData;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.OneTimeTrigger;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.RecurringTrigger;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.model.OneTimeEventJob;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.model.OneTimeMlmJob;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.model.RecurringMlmJob;
import edu.uvm.ccts.common.exceptions.DataException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import java.util.*;

/**
 * Created by mstorer on 1/8/14.
 */
public class MLMScheduler {
    private static final Log log = LogFactory.getLog(MLMScheduler.class);

    private static MLMScheduler mlmScheduler = null;

    public static MLMScheduler getInstance() {
        if (mlmScheduler == null) mlmScheduler = new MLMScheduler();
        return mlmScheduler;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private Scheduler scheduler = null;
    private final Map<String, List<ADataType>> argumentsMap = new HashMap<String, List<ADataType>>();

    private MLMScheduler() {
        // do not check for updates - see http://quartz-scheduler.org/documentation/best-practices
        System.setProperty("org.terracotta.quartz.skipUpdateCheck", "true");
    }

    public void scheduleTimeBasedTriggers() throws SchedulerException {
        if ( ! isRunning() ) return;

        Time now = new Time(new Date());                            // this is the time that MLMs become executable

        for (MLM mlm : MLMRegistry.getInstance().getMLMs()) {
            for (EvokeCondition ec : mlm.getEvokeConditions()) {
                if (ec instanceof ConstTimeTrigger) {
                    ConstTimeTrigger ctt = (ConstTimeTrigger) ec;

                    // 13.3.3.1
                    // The MLM is triggered at the time specified by the time expression. This is either an absolute
                    // point in time, or a relative date (such as tomorrow or simply a duration). A relative date is
                    // always evaluated relative to the timepoint when the MLM becomes executable in the system. If a
                    // time expression evaluates to a point in time which lies in the past, the MLM is triggered
                    // immediately.

                    // 13.3.3.2
                    // If used with time-of-day-constants and more than one time constant is specified in the
                    // evoke slot, the MLM will be executed at the next scheduled time.

                    // GRAMMAR : <time-expr> | <duration-expr> AFTER <time-expr-simple>

                    // IMPORTANT ASSUMPTION : because <time-expr> may represent multiple <time-expr>s combined using
                    // the OR keyword, and because each <time-expr> may be any of 1) a time constant, 2) a time-of-day
                    // constant (with day-of-week or relative-day-of-week), or 3) a duration constant (formed by using
                    // a number constant with a duration operator) (see 13.3.2) -
                    // - the note above from 13.3.3.2 regarding use of OR for time-of-day constants (which are only
                    // referenced in <time-expr>) LEAVES UNANSWERED what to do when OR is used with NON-time-of-day
                    // constants (time constants and duration constants).
                    // In resolving this unanswered question, IT IS PRESUMED THAT logical consistency is more
                    // important than strict adherence to the letter of a specification (which has been shown to
                    // contain typographical errors).
                    // IT IS THEREFORE ASSUMED THAT the specified use of OR should apply to ALL forms that
                    // <time-expr> might take.

                    if ( ! alreadyExecuted(mlm, ctt) ) {
                        JobDetail job = buildOneTimeJob(mlm, "ConstTime");

                        // ensure that if re-scheduling a time-based trigger, that the existing job and all of its
                        // triggers are removed first.
                        if (scheduler.checkExists(job.getKey())) {
                            scheduler.deleteJob(job.getKey());
                        }

                        Time time = ctt.getNextTriggerTime(now);
                        Trigger t = buildOneTimeTrigger(time.asDate());

                        scheduler.scheduleJob(job, t);
                    }

                } else if (ec instanceof ConstPerTimeTrigger) {
                    ConstPerTimeTrigger cptt = (ConstPerTimeTrigger) ec;

                    JobDetail job = buildRecurringJob(mlm, "ConstPerTime", cptt.getUntilCondition());

                    // ensure that if re-scheduling a time-based trigger, that the existing job and all of its
                    // triggers are removed first.
                    if (scheduler.checkExists(job.getKey())) {
                        scheduler.deleteJob(job.getKey());
                    }

                    int interval = (int) cptt.getInterval().getSeconds();
                    Time startTime = cptt.getStart().getNextTriggerTime(now);
                    Time endTime = startTime.add(cptt.getLifespan());
                    Trigger t = buildRecurringTrigger(startTime.asDate(), endTime.asDate(), interval);

                    scheduler.scheduleJob(job, t);
                }
            }
        }
    }

    public void scheduleEventBasedTriggers(Event event) throws SchedulerException {
        if ( ! isRunning() ) return;

        for (MLM mlm : MLMRegistry.getInstance().getMLMs()) {
            for (EvokeCondition ec : mlm.getEvokeConditions()) {
                if (ec instanceof DelEventTrigger) {
                    DelEventTrigger det = (DelEventTrigger) ec;

                    if (det.isTriggeredBy(event)) {
                        JobDetail job = buildOneTimeJob(mlm, "DelEvent");

                        // 13.3.2.1
                        // Time expressions for the delayed trigger can be combined using OR. In this case the
                        // whole expression is evaluated to find the next earliest trigger time.
                        //
                        //      MONDAY ATTIME 13:00 OR FRIDAY ATTIME 12:00 AFTER TIME OF penicillin_storage
                        //
                        // This triggers the MLM on Monday if the event occurs between Friday after 12:00 and Monday
                        // before 13:00. If the event occurs outside of this time interval, the MLM is triggered on
                        // Friday.

                        // 13.3.2.2
                        // The MLM is triggered at the time specified in the delayed trigger statement. This is
                        // usually some specified duration after the occurrence of an event. In the special case,
                        // that the delay time is given as an absolute point in time, the triggering is delayed to
                        // this timestamp, as soon as the event occurs. If the event occurs after this timestamp,
                        // the MLM triggers immediately.

                        Time time = det.getNextTriggerTime();

                        if (scheduler.checkExists(job.getKey())) {
                            Trigger t = buildOneTimeTrigger(job.getKey(), time.asDate());
                            scheduler.scheduleJob(t);

                        } else {
                            Trigger t = buildOneTimeTrigger(time.asDate());
                            scheduler.scheduleJob(job, t);
                        }
                    }

                } else if (ec instanceof PerEventTrigger) {
                    PerEventTrigger pet = (PerEventTrigger) ec;

                    if (pet.isTriggeredBy(event)) {
                        JobDetail job = buildRecurringJob(mlm, "PerEvent", pet.getUntilCondition());

                        int interval = (int) pet.getInterval().getSeconds();
                        Time startTime = pet.getStart().getNextTriggerTime();
                        Time endTime = startTime.add(pet.getLifespan());

                        if (scheduler.checkExists(job.getKey())) {
                            Trigger t = buildRecurringTrigger(job.getKey(), startTime.asDate(), endTime.asDate(), interval);
                            scheduler.scheduleJob(t);

                        } else {
                            Trigger t = buildRecurringTrigger(startTime.asDate(), endTime.asDate(), interval);
                            scheduler.scheduleJob(job, t);
                        }
                    }
                }
            }
        }
    }

    protected Class<? extends Job> oneTimeMlmJobClass() {
        return OneTimeMlmJob.class;
    }

    protected Class<? extends Job> recurringMlmJobClass() {
        return RecurringMlmJob.class;
    }

    protected Class<? extends Job> oneTimeEventJobClass() {
        return OneTimeEventJob.class;
    }

    public void scheduleMLMForExecution(MLM mlm, List<ADataType> arguments, Duration delay) throws SchedulerException {
        JobBuilder builder = JobBuilder.newJob(oneTimeMlmJobClass())
                .withIdentity("MLM(" + mlm.getId() + ")", "MLM")
                .usingJobData(OneTimeMlmJob.MLM_ID_KEY, mlm.getId());

        if (arguments != null && arguments.size() > 0) {
            builder.usingJobData(OneTimeMlmJob.ARGUMENTS_KEY, storeArguments(arguments));
        }

        JobDetail job = builder.build();

        Time startTime = new Time(new Date());
        if (delay != null) startTime = startTime.add(delay);

        Trigger trigger = buildOneTimeTrigger(startTime.asDate());


        // todo : handle this exception, or, rather, prevent it from happening :
        // Caused by: org.quartz.ObjectAlreadyExistsException: Unable to store Job :
        //   'MLM.MLM(build_anion_gaps:University of Vermont:testing:1.00)', because one already exists with
        //   this identification.

        scheduler.scheduleJob(job, trigger);
    }

    public void scheduleEventForExecution(Event event, Duration delay) throws SchedulerException {
        JobDetail job = JobBuilder.newJob(oneTimeEventJobClass())
                .withIdentity("Event(" + event.getName() + ")", "Event")
                .usingJobData(OneTimeEventJob.EVENT_ID_KEY, event.getName())
                .build();

        Time startTime = new Time(new Date());
        if (delay != null) startTime = startTime.add(delay);

        Trigger trigger = buildOneTimeTrigger(startTime.asDate());

        scheduler.scheduleJob(job, trigger);
    }

    public void unscheduleAllJobs() throws SchedulerException {
        if (scheduler == null) return;

        boolean hadAnyJobs = hasScheduledJobs();

        while (hasScheduledJobs()) {
            List<JobKey> runningJobKeys = new ArrayList<JobKey>();
            for (JobExecutionContext ctx : scheduler.getCurrentlyExecutingJobs()) {
                runningJobKeys.add(ctx.getJobDetail().getKey());
            }

            boolean tryAgain = false;
            for (JobKey key : scheduler.getJobKeys(GroupMatcher.anyJobGroup())) {
                if (runningJobKeys.contains(key)) {
                    log.warn("job " + key + " is currently executing - will wait to unschedule -");
                    tryAgain = true;

                } else {
                    log.info("unscheduling job : " + key);
                    scheduler.deleteJob(key);
                }
            }

            if (tryAgain) {
                try {
                    Thread.sleep(2500);

                } catch (InterruptedException e) {
                    // handle silently
                }
            }
        }

        if (hadAnyJobs) log.info("all jobs successfully unscheduled.");
    }

    public List<ADataType> retrieveArguments(String key) throws DataException {
        if (argumentsMap.containsKey(key)) {
            return argumentsMap.remove(key);

        } else {
            throw new DataException("arguments not found for key '" + key + "'");
        }
    }

    public boolean isRunning() throws SchedulerException {
        return scheduler != null && scheduler.isStarted();
    }

    public void start() throws SchedulerException {
        if (scheduler != null) return;

        scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.start();

        log.info("Quartz scheduler started.");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    if (scheduler != null && scheduler.isStarted()) {
                        scheduler.shutdown(true);       // wait for currently executing jobs to complete
                        scheduler = null;

                        log.info("Quartz scheduler stopped.");
                    }

                } catch (SchedulerException e) {
                    log.error("caught " + e.getClass().getName() + " shutting down scheduler - " + e.getMessage(), e);
                }
            }
        });
    }

    public void stop() throws SchedulerException {
        if (scheduler != null && scheduler.isStarted()) {
            scheduler.shutdown(true);                   // wait for currently executing jobs to complete
            scheduler = null;

            log.info("Quartz scheduler stopped.");
        }
    }

    public List<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job> getScheduledJobs() throws SchedulerException {
        List<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job> list = new ArrayList<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job>();

        for (JobKey key : scheduler.getJobKeys(GroupMatcher.anyJobGroup())) {
            edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job job =
                    new edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job(key.getName(), key.getGroup());

            job.setJobDataList(buildJobDataList(key));
            job.setTriggerList(buildTriggerList(key));

            list.add(job);
        }

        return list;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private boolean hasScheduledJobs() throws SchedulerException {
        return scheduler.getJobKeys(GroupMatcher.anyJobGroup()).size() > 0;
    }

    private boolean alreadyExecuted(MLM mlm, ConstTimeTrigger ctt) {

        // todo : implement this

        return false;
    }

    @SuppressWarnings("unchecked")
    private List<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Trigger> buildTriggerList(JobKey jobKey) throws SchedulerException {
        List<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Trigger> list =
                new ArrayList<edu.uvm.ccts.ardencat.mlmengine.model.schedule.Trigger>();

        for (SimpleTrigger t : (List<SimpleTrigger>) scheduler.getTriggersOfJob(jobKey)) {
            Date start = t.getStartTime();
            Date stop = t.getEndTime();

            if (stop == null) {
                list.add(new OneTimeTrigger(start));

            } else {
                int repeatCount = t.getRepeatCount();
                int interval = (int) (t.getRepeatInterval() / 1000);
                int timesTriggered = t.getTimesTriggered();

                list.add(new RecurringTrigger(start, stop, repeatCount, interval, timesTriggered));
            }
        }

        return list;
    }

    private List<JobData> buildJobDataList(JobKey jobKey) throws SchedulerException {
        List<JobData> list = new ArrayList<JobData>();

        JobDataMap dataMap = scheduler.getJobDetail(jobKey).getJobDataMap();
        for (String key : dataMap.getKeys()) {
            list.add(new JobData(key, dataMap.getString(key)));
        }

        return list;
    }

    private String storeArguments(List<ADataType> arguments) {
        synchronized(argumentsMap) {
            String key = UUID.randomUUID().toString();
            while (argumentsMap.containsKey(key)) {
                key = UUID.randomUUID().toString();
            }

            argumentsMap.put(key, arguments);

            return key;
        }
    }

    private JobDetail buildOneTimeJob(MLM mlm, String group) {
        return JobBuilder.newJob(oneTimeMlmJobClass())
                .withIdentity("MLM(" + mlm.getId() + ")", group)
                .usingJobData(OneTimeMlmJob.MLM_ID_KEY, mlm.getId())
                .build();
    }

    private Trigger buildOneTimeTrigger(Date execTime) {
        return buildOneTimeTrigger(null, execTime);
    }

    private Trigger buildOneTimeTrigger(JobKey jobKey, Date execTime) {
        TriggerBuilder builder = TriggerBuilder.newTrigger()
                .startAt(execTime)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()    // trigger will misfire if it execTime lies in
                        .withMisfireHandlingInstructionFireNow());      // the past.  see specs 13.3.2.2, 13.3.3.1

        if (jobKey != null) {
            builder.forJob(jobKey);
        }

        return builder.build();
    }

    private JobDetail buildRecurringJob(MLM mlm, String group, String untilCondition) {
        return JobBuilder.newJob(recurringMlmJobClass())
                .withIdentity("MLM(" + mlm.getId() + ")", group)
                .usingJobData(RecurringMlmJob.MLM_ID_KEY, mlm.getId())
                .usingJobData(RecurringMlmJob.UNTIL_KEY, untilCondition)
                .build();
    }

    private Trigger buildRecurringTrigger(Date startTime, Date stopTime, int intervalInSeconds) {
        return buildRecurringTrigger(null, startTime, stopTime, intervalInSeconds);
    }

    private Trigger buildRecurringTrigger(JobKey jobKey, Date startTime, Date stopTime, int intervalInSeconds) {
        TriggerBuilder builder = TriggerBuilder.newTrigger()
                .startAt(startTime)
                .endAt(stopTime)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(intervalInSeconds)
                        .withMisfireHandlingInstructionNextWithRemainingCount()
                        .repeatForever());

        if (jobKey != null) {
            builder.forJob(jobKey);
        }

        return builder.build();
    }
}
