/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.config;

import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.ScheduleException;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenCATConfiguration;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMConfigData;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.SchedulerException;

/**
 * Created by mbstorer on 11/13/15.
 */
public abstract class Configurator<C extends ArdenCATConfiguration> {
    private static final Log log = LogFactory.getLog(Configurator.class);

    protected abstract MLMBuilder getMLMBuilder();
    protected abstract C getActiveConfig();
    protected abstract void setActiveConfig(C cfg) throws Exception;
    protected abstract void deleteActiveConfig();
    protected abstract void registerConfig(C cfg);
    protected abstract void clearConfigRegistry();

    public void restoreConfigIfExists() {
        C cfg = getActiveConfig();

        if (cfg != null) {
            try {
                doLoadConfig(cfg);
                // do not save config if loading a config in this context - it's already saved

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " restoring previous configuration - " + e.getMessage(), e);

                try {
                    doUnloadConfig();

                } catch (ScheduleException se) {
                    log.error("caught " + e.getClass().getName() + " restoring previous configuration - " + e.getMessage(), e);
                }

                deleteActiveConfig();
            }
        }
    }

    public void loadConfig(C cfg) throws ScheduleException, ConfigurationException {
        doUnloadConfig();

        if (cfg == null) {
            deleteActiveConfig();

        } else {
            try {
                doLoadConfig(cfg);
                setActiveConfig(cfg);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " loading configuration - " + e.getMessage(), e);

                doUnloadConfig();
                deleteActiveConfig();

                throw (e instanceof ConfigurationException) ?
                        (ConfigurationException) e :
                        new ConfigurationException(e.getMessage(), e);
            }
        }
    }


////////////////////////////////////////////////////////////////////////////////////
// protected methods
//

    protected void doBeforeConfigLoadTasks(C cfg) throws Exception {
        // do nothing
    }

    protected void doAfterConfigLoadedTasks(C cfg) throws Exception {
        // do nothing
    }

    protected void doBeforeConfigUnloadTasks() {
        // do nothing
    }

    protected void doAfterConfigUnloadedTasks() {
        // do nothing
    }


////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void doLoadConfig(C cfg) throws Exception {
        log.info("*** loading MLM Engine config from '" + cfg.getFilename() + "' ***");

        doBeforeConfigLoadTasks(cfg);

        MLMScheduler scheduler = MLMScheduler.getInstance();
        MLMRegistry mlmRegistry = MLMRegistry.getInstance();
        DataSourceRegistry dsRegistry = DataSourceRegistry.getInstance();

        cfg.getArdencatDataSource().loadDriver();
        dsRegistry.registerArdencatDataSource(cfg.getArdencatDataSource());

        for (ArdenDataSource ds : cfg.getMLMDataSources()) {
            ds.loadDriver();        // ensure the appropriate JDBC driver is loaded
            dsRegistry.registerMLMDataSource(ds);
        }

        MLMBuilder builder = getMLMBuilder();
        for (MLMConfigData mlmConfig : cfg.getMLMConfigs()) {
            MLM mlm = builder.build(mlmConfig);
            mlmRegistry.register(mlm);
        }

        mlmRegistry.populateEvokeConditions();

        try {
            scheduler.scheduleTimeBasedTriggers();

        } catch (SchedulerException e) {
            throw new ScheduleException(e.getMessage());
        }

        registerConfig(cfg);

        doAfterConfigLoadedTasks(cfg);

        log.info("*** finished loading MLM Engine config ***");
    }

    private void doUnloadConfig() throws ScheduleException {
        log.info("*** unloading MLM Engine config ***");

        doBeforeConfigUnloadTasks();

        try {
            MLMScheduler.getInstance().unscheduleAllJobs();
            MLMRegistry.getInstance().unregisterAllMLMs();
            DataSourceRegistry.getInstance().reset();

        } catch (SchedulerException e) {
            throw new ScheduleException(e.getMessage());

        } finally {
            clearConfigRegistry();
        }

        doAfterConfigUnloadedTasks();

        log.info("*** finished unloading MLM Engine config ***");
    }
}
