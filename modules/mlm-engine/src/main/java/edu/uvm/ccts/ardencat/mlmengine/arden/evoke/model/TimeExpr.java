/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.ITimeComponent;
import edu.uvm.ccts.arden.model.NonPrimaryTimeDataType;
import edu.uvm.ccts.arden.model.Time;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 1/3/14.
 */
public class TimeExpr extends NonPrimaryTimeDataType<TimeExpr> {
    private List<ITimeComponent> list;

    public TimeExpr(List<ITimeComponent> list) {
        this.list = list;
    }

    public TimeExpr(TimeExpr te) {
        list = new ArrayList<ITimeComponent>();
        for (ITimeComponent item : te.list) {
            list.add(item.copy());
        }
    }

    public Time getNextTriggerTime(Time relativeToTime) {
        Time time = null;

        for (ITimeComponent itc : list) {
            Time t = itc.asTime(relativeToTime);
            if (time == null || t.compareTo(time) < 0) {
                time = t;
            }
        }

        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        TimeExpr te = (TimeExpr) o;
        return new EqualsBuilder()
                .append(list, te.list)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(739, 337)
                .append(list)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }

    @Override
    public TimeExpr copy() {
        return new TimeExpr(this);
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
