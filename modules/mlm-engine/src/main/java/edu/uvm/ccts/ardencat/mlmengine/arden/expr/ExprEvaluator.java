/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.expr;

import edu.uvm.ccts.arden.expr.BailExprLexer;
import edu.uvm.ccts.arden.expr.BailExprParser;
import edu.uvm.ccts.arden.expr.antlr.ExprBaseVisitor;
import edu.uvm.ccts.arden.expr.antlr.ExprLexer;
import edu.uvm.ccts.arden.expr.antlr.ExprParser;
import edu.uvm.ccts.arden.model.Variables;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mstorer on 1/13/14.
 */
public class ExprEvaluator {
    public void validateParse(String ardenExpr) throws IOException {
        new ExprBaseVisitor().visit(buildParseTree(ardenExpr));
    }

    public Object evaluate(String ardenExpr) throws IOException {
        return evaluate(null, ardenExpr);
    }

    public Object evaluate(Variables variables, String ardenExpr) throws IOException {
        ParseTree tree = buildParseTree(ardenExpr);
        ExprEvalVisitor visitor = new ExprEvalVisitor(null, variables);
        visitor.visit(tree);
        return visitor.getValue().toJavaObject();
    }

    private ParseTree buildParseTree(String ardenExpr) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(ardenExpr.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        ExprLexer lexer = new BailExprLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ExprParser parser = new BailExprParser(tokens);
        return parser.init();
    }
}
