/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke;

import edu.uvm.ccts.arden.evoke.antlr.EvokeBaseVisitor;
import edu.uvm.ccts.arden.evoke.antlr.EvokeParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.DurationUtil;
import edu.uvm.ccts.arden.util.NumberUtil;
import edu.uvm.ccts.arden.util.TimeUtil;
import edu.uvm.ccts.ardencat.mlmengine.arden.Arden;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.ardencat.mlmengine.arden.util.AntlrUtil;
import edu.uvm.ccts.common.exceptions.InvalidAttributeException;
import edu.uvm.ccts.common.exceptions.antlr.GrammarException;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 12/19/13.
 */
public class EvokeEvalVisitor extends EvokeBaseVisitor<Value> {
    private static final Log log = LogFactory.getLog(EvokeEvalVisitor.class);

    private Arden arden;
    private List<EvokeCondition> evokeConditions = new ArrayList<EvokeCondition>();

    public EvokeEvalVisitor(MLM caller, Variables variables) {
        arden = new Arden(log, caller);

        if (variables != null) {
            for (String id : variables.getVariableNames()) {
                assign(id, variables.getVariable(id));
            }
        }
    }

    public void setPrintStream(PrintStream printStream) {
        arden.setPrintStream(printStream);
    }

    public List<EvokeCondition> getEvokeConditions() {
        return evokeConditions;
    }

    private void assign(String id, ADataType adt) {
        arden.assign(id, adt);
    }

    private ADataType evaluateId(String id) {
        return arden.evaluateId(id);
    }

    @Override
    public Value visitStmt(@NotNull EvokeParser.StmtContext ctx) {
        if (ctx.simpleTrigger() != null) {
            Value v = visit(ctx.simpleTrigger());

            List<Event> triggerEvents = new ArrayList<Event>();
            boolean mayCall = false;

            for (AString as : v.asStringList()) {
                String id = as.toString();
                if (id.equalsIgnoreCase(SimpleTrigger.CALL_TOKEN)) {
                    mayCall = true;

                } else {
                    ADataType obj = evaluateId(id);
                    if (obj instanceof Event) {
                        triggerEvents.add((Event) obj);

                    } else {
                        throw new RuntimeException("event '" + id + "' is not defined");
                    }
                }
            }

            evokeConditions.add(new SimpleTrigger(triggerEvents, mayCall));

        } else if (ctx.delEventTrigger() != null) {
            Value v = visit(ctx.delEventTrigger());

            if (v.isDelEventTrigger()) {
                evokeConditions.add(v.asDelEventTrigger());
            }

        } else if (ctx.constTimeTrigger() != null) {
            Value v = visit(ctx.constTimeTrigger());

            if (v.isConstTimeTrigger()) {
                evokeConditions.add(v.asConstTimeTrigger());
            }

        } else if (ctx.perEventTrigger() != null) {
            Value v = visit(ctx.perEventTrigger());

            if (v.isPerEventTrigger()) {
                evokeConditions.add(v.asPerEventTrigger());
            }

        } else if (ctx.constPerTimeTrigger() != null) {
            Value v = visit(ctx.constPerTimeTrigger());

            if (v.isConstPerTimeTrigger()) {
                evokeConditions.add(v.asConstPerTimeTrigger());
            }

        } else {
            throw new GrammarException("unknown statement type");
        }

        return null;
    }

    @Override
    public Value visitSimpleTrigger(@NotNull EvokeParser.SimpleTriggerContext ctx) {
        if (ctx.Or() != null) {
            Value vLeft = visit(ctx.simpleTrigger(0));
            Value vRight = visit(ctx.simpleTrigger(1));

            AList list = vLeft.asList();
            list.addAll(vRight.asList());
            return new Value(list);

        } else if (ctx.ID().size() > 1) {
            AList list = new AList();
            for (TerminalNode id : ctx.ID()) {
                list.add(new AString(id.getText()));
            }
            return new Value(list);

        } else if (ctx.Call() != null) {
            return new Value(new AString(SimpleTrigger.CALL_TOKEN));

        } else {
            return new Value(new AString(ctx.ID(0).getText()));
        }
    }

    @Override
    public Value visitDelEventTrigger(@NotNull EvokeParser.DelEventTriggerContext ctx) {
        Value vTimeExpr = visit(ctx.timeExpr());
        ADataType obj = evaluateId(ctx.ID().getText());

        if (vTimeExpr.isTimeExpr() && obj instanceof Event) {
            DelEventTrigger det = new DelEventTrigger(vTimeExpr.asTimeExpr(), ((Event) obj));
            return new Value(det);

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitConstTimeTrigger(@NotNull EvokeParser.ConstTimeTriggerContext ctx) {
        if (ctx.timeExpr() != null) {
            Value v = visit(ctx.timeExpr());
            if (v.isTimeExpr()) {
                return new Value(new ConstTimeTrigger(v.asTimeExpr()));
            }

        } else if (ctx.After() != null) {
            Value vDur = visit(ctx.durationExpr());
            Value vTes = visit(ctx.timeExprSimple());

            if (vDur.isDuration() && vTes.isTimeExprSimple()) {
                return new Value(new ConstTimeTrigger(vDur.asDuration(), vTes.asTimeExprSimple()));
            }

        } else {
            throw new GrammarException("unknown grammar for const time trigger");
        }

        return NULL();
    }

    @Override
    public Value visitPerEventTrigger(@NotNull EvokeParser.PerEventTriggerContext ctx) {
        Value vInterval = visit(ctx.durationExpr(0));
        Value vLifespan = visit(ctx.durationExpr(1));
        Value vStart = visit(ctx.delEventTrigger());

        String untilCondition = ctx.Until() != null ?
                AntlrUtil.getText(ctx.expr()) :
                null;

        if (vInterval.isDuration() && vLifespan.isDuration() && vStart.isDelEventTrigger()) {
            Duration interval = vInterval.asDuration();
            Duration lifespan = vLifespan.asDuration();
            DelEventTrigger start = vStart.asDelEventTrigger();

            PerEventTrigger pet = untilCondition != null ?
                    new PerEventTrigger(interval, lifespan, start, untilCondition) :
                    new PerEventTrigger(interval, lifespan, start);

            return new Value(pet);
        }

        return NULL();
    }

    @Override
    public Value visitConstPerTimeTrigger(@NotNull EvokeParser.ConstPerTimeTriggerContext ctx) {
        Value vInterval = visit(ctx.durationExpr(0));
        Value vLifespan = visit(ctx.durationExpr(1));
        Value vStart = visit(ctx.constTimeTrigger());

        String untilCondition = ctx.Until() != null ?
                AntlrUtil.getText(ctx.expr()) :
                null;

        if (vInterval.isDuration() && vLifespan.isDuration() && vStart.isConstTimeTrigger()) {
            Duration interval = vInterval.asDuration();
            Duration lifespan = vLifespan.asDuration();
            ConstTimeTrigger start = vStart.asConstTimeTrigger();

            ConstPerTimeTrigger cptt = untilCondition != null ?
                    new ConstPerTimeTrigger(interval, lifespan, start, untilCondition) :
                    new ConstPerTimeTrigger(interval, lifespan, start);

            return new Value(cptt);
        }

        return NULL();
    }

    @Override
    public Value visitTimeExpr(@NotNull EvokeParser.TimeExprContext ctx) {
        List<ITimeComponent> list = new ArrayList<ITimeComponent>();

        for (EvokeParser.TimeExprComponentContext tCtx : ctx.timeExprComponent()) {
            Value v = visit(tCtx);
            ADataType obj = v.getBackingObject();
            if (obj instanceof ITimeComponent) {
                list.add((ITimeComponent) obj);
            }
        }

        return new Value(new TimeExpr(list));
    }

    @Override
    public Value visitTimeExprSimple(@NotNull EvokeParser.TimeExprSimpleContext ctx) {
        List<ISimpleTimeComponent> list = new ArrayList<ISimpleTimeComponent>();

        for (EvokeParser.TimeExprSimpleComponentContext tCtx : ctx.timeExprSimpleComponent()) {
            Value v = visit(tCtx);
            ADataType obj = v.getBackingObject();
            if (obj instanceof ISimpleTimeComponent) {
                list.add((ISimpleTimeComponent) obj);
            }
        }

        return new Value(new TimeExprSimple(list));
    }

    @Override
    public Value visitDurationExpr(@NotNull EvokeParser.DurationExprContext ctx) {
        return new Value(buildDuration(ctx));
    }

    @Override
    public Value visitConstTimeVal(@NotNull EvokeParser.ConstTimeValContext ctx) {
        return arden.timeVal(ctx.getText());
    }

    @Override
    public Value visitRelTimeVal(@NotNull EvokeParser.RelTimeValContext ctx) {
        TimeOfDay tod;

        try {
            tod = TimeUtil.parseTimeOfDay(ctx.TimeOfDayVal().getText());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        RelativeTime rt;
        if (ctx.DayOfWeek() != null) {
            String dowStr = ctx.DayOfWeek().getText();
            DayOfWeek dow = DayOfWeek.valueOf(dowStr.toUpperCase());
            rt = new RelativeTime(dow, tod);

        } else {
            String rdowStr = ctx.RelDayOfWeek().getText();
            RelativeDayOfWeek rdow = RelativeDayOfWeek.valueOf(rdowStr.toUpperCase());
            rt = new RelativeTime(rdow, tod);
        }

        return new Value(rt);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// from LogicEvalVisitor
//

    @Override
    public Value visitParens(@NotNull EvokeParser.ParensContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Value visitId(@NotNull EvokeParser.IdContext ctx) {
        String id = ctx.ID().getText();
        return arden.id(id);
    }

    @Override
    public Value visitNullVal(@NotNull EvokeParser.NullValContext ctx) {
        return arden.nullVal();
    }

    @Override
    public Value visitStringVal(@NotNull EvokeParser.StringValContext ctx) {
        String str = ctx.StringVal().getText();
        return arden.stringVal(str);
    }

    @Override
    public Value visitNumberVal(@NotNull EvokeParser.NumberValContext ctx) {
        String numStr = ctx.NumberVal().getText();
        return arden.numberVal(numStr);
    }

    @Override
    public Value visitBooleanVal(@NotNull EvokeParser.BooleanValContext ctx) {
        String boolStr = ctx.BooleanVal().getText();
        return arden.booleanVal(boolStr);
    }

    @Override
    public Value visitBinaryList(@NotNull EvokeParser.BinaryListContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.binaryList(left, right);
    }

    @Override
    public Value visitUnaryList(@NotNull EvokeParser.UnaryListContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryList(v);
    }

    @Override
    public Value visitEmptyList(@NotNull EvokeParser.EmptyListContext ctx) {
        return arden.emptyList();
    }

    @Override
    public Value visitNow(@NotNull EvokeParser.NowContext ctx) {
        return arden.now();
    }

    @Override
    public Value visitCurrentTime(@NotNull EvokeParser.CurrentTimeContext ctx) {
        return arden.currentTime();
    }

    @Override
    public Value visitTimeVal(@NotNull EvokeParser.TimeValContext ctx) {
        return arden.timeVal(ctx.getText());
    }

    @Override
    public Value visitTimeOfDayVal(@NotNull EvokeParser.TimeOfDayValContext ctx) {
        return arden.timeOfDayVal(ctx.getText());
    }

    /**
     * The duration data type signifies an interval of time that is not anchored to any particular point in absolute
     * time. There are no duration constants. Instead one builds durations using the duration operators (see Section
     * 9.10.7). For example, <strong>1 day</strong>, <strong>45 seconds</strong>, and <strong>3.2 months</strong> are
     * durations.
     * @see {@link edu.uvm.ccts.arden.model.Duration}, {@link edu.uvm.ccts.arden.model.DurationUnit}, {@link edu.uvm.ccts.arden.model.SecondsDuration}, {@link edu.uvm.ccts.arden.model.MonthsDuration}, 8.5, 9.10.7, 9.11
     * @param ctx
     * @return
     */
    @Override
    public Value visitDuration(@NotNull EvokeParser.DurationContext ctx) {
        return new Value(buildDuration(ctx.durationExpr()));
    }

    private Duration buildDuration(EvokeParser.DurationExprContext durExprCtx) {
        // first, split duration ("3 days 2 hours") into component durations ("3 days", "2 hours").

        List<Duration> componentDurations = new ArrayList<Duration>();
        for (int i = 0; i < durExprCtx.NumberVal().size(); i ++) {
            ANumber n = NumberUtil.parseNumber(durExprCtx.NumberVal(i).getText());
            String s = durExprCtx.durationUnit(i).getText();

            try {
                componentDurations.add(DurationUtil.buildDuration(n, s));

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // next, build a Duration object that represents the sum of each component duration, generated above

        Duration duration = null;
        for (Duration d : componentDurations) {
            if (duration == null) {
                duration = d;

            } else {
                duration = duration.add(d);     // note: might change form from MonthsDuration to SecondsDuration
            }
        }

        return duration;
    }

    @Override
    public Value visitDayOfWeek(@NotNull EvokeParser.DayOfWeekContext ctx) {
        String dayOfWeekStr = ctx.DayOfWeek().getText();
        return arden.dayOfWeek(dayOfWeekStr);
    }

    /**
     * The <strong>new</strong> statement causes a new object to be created, and assigns it to the named variable.
     * <br>
     * In the simple case (without the <strong>with</strong> clause) all attributes of the object are initialized to
     * {@code null}. In the full statement, a set of 1 or more comma-separated expressions should follow the
     * <strong>with</strong> reserved word. Each expression is evaluated and assigned as a value of an attribute of
     * the object. They are assigned in the order the attributes were declared in the <strong>object</strong> statement.
     * If the number of expressions is less than the number of attributes, remaining attributes are initialized to
     * {@code null}. If the number of expressions is greater than the number of attributes, the extra expressions are
     * evaluated but the results are silently discarded.
     * @see {@link AObject}, 10.2.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitNewObject(@NotNull EvokeParser.NewObjectContext ctx) {
        String typeName = ctx.ID().getText();

        AObjectType type = arden.getObjectType(typeName);
        if (type == null) throw new RuntimeException(typeName + " does not refer to a valid Arden object type");

        AObject obj = new AObject(type);

        // ordered-with refers to e.g., "new <object> with <attrVal1>, <attrVal2>"
        if (ctx.objOrderedWith() != null) {
            EvokeParser.ObjOrderedWithContext octx = ctx.objOrderedWith();
            List<ADataType> list = new ArrayList<ADataType>();

            for (EvokeParser.ExprContext ectx : octx.expr()) {
                Value v = visit(ectx);
                list.add(v.getBackingObject());
            }

            obj.populate(list);
        }

        // named-with refers to e.g., "new <object> with <attrName1> := <attrVal1>, <attrName2> := <attrVal2>"
        if (ctx.objNamedWith() != null) {
            EvokeParser.ObjNamedWithContext nctx = ctx.objNamedWith();
            int max = nctx.ID().size();

            for (int i = 0; i < max; i ++) {
                String attrName = nctx.ID(i).getText();
                Value v = visit(nctx.expr(i));

                try {
                    obj.setAttribute(attrName, v.getBackingObject());

                } catch (InvalidAttributeException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return new Value(obj);
    }

    /**
     * The <strong>dot</strong> operator ("{@code .}") selects an attribute from an object based on the name following
     * the dot. It takes an expression and an identifier. The expression typically evaluates to an object or a list of
     * objects.
     * <br>
     * If the expression does not evaluate to an object, or if the object does not contain the named attribute, then
     * {@code null} is returned. If the expression evaluates to a list, normal Arden list handling is used and a list is
     * returned. Therefore, if the expression is a list of objects, then a list (of the same length) of the attribute
     * values named by the identifier is returned (a common usage).
     * @see {@link AObject}, 9.18.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitDot(@NotNull EvokeParser.DotContext ctx) {
        Value v = visit(ctx.expr());

        if (v.isObject()) {                                                         // if its an object, return the
            return new Value(visitDotHelper(v.asObject(), ctx));                    // requested attribute

        } else if (v.isList() && v.asList().getListType() == ListType.OBJECT) {     // if it's a list of objects,
            AList list = new AList();                                               // return a list containing the
            for (ADataType adt : v.asList()) {                                      // requested attribute, one for
                list.add(visitDotHelper((AObject) adt, ctx));                       // each object in the source list
            }
            return new Value(list);
        }

        return NULL();
    }

    private ADataType visitDotHelper(AObject ao, EvokeParser.DotContext ctx) {
        String attrName = ctx.ID().getText();

        if (ao.hasAttribute(attrName)) {
            try {
                return ao.getAttribute(attrName);

            } catch (InvalidAttributeException e) {
                throw new RuntimeException(e);          // should never get here, for call to hasAttribute() above
            }
        }

        return new ANull();
    }

    @Override
    public Value visitClone(@NotNull EvokeParser.CloneContext ctx) {
        Value v = visit(ctx.expr());
        return arden.clone(v);
    }

    @Override
    public Value visitExtractAttrNames(@NotNull EvokeParser.ExtractAttrNamesContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractAttrNames(v);
    }

    @Override
    public Value visitAttributeFrom(@NotNull EvokeParser.AttributeFromContext ctx) {
        Value vAttrName = visit(ctx.expr(0));
        Value vObj = visit(ctx.expr(1));
        return arden.attributeFrom(vAttrName, vObj);
    }

    @Override
    public Value visitSort(@NotNull EvokeParser.SortContext ctx) {
        Value v = visit(ctx.expr());
        boolean sortByTime = ctx.Time() != null;
        return arden.sort(v, sortByTime);
    }

    @Override
    public Value visitMerge(@NotNull EvokeParser.MergeContext ctx) {
        Value v1 = visit(ctx.expr(0));
        Value v2 = visit(ctx.expr(1));
        return arden.merge(v1, v2);
    }

    @Override
    public Value visitWhereTimeIsPresent(@NotNull EvokeParser.WhereTimeIsPresentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.whereTimeIsPresent(v);
    }

    @Override
    public Value visitAddToList(@NotNull EvokeParser.AddToListContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if (ctx.At() != null) {
            Value vAt = visit(ctx.expr(2));
            return arden.addToList(vObj, vList, vAt);

        } else {
            return arden.addToList(vObj, vList);
        }
    }

    @Override
    public Value visitRemoveFromList(@NotNull EvokeParser.RemoveFromListContext ctx) {
        Value vPos = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.removeFromList(vPos, vList);
    }

    @Override
    public Value visitWhere(@NotNull EvokeParser.WhereContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTest = visit(ctx.expr(1));
        return arden.where(vObj, vTest);
    }

    @Override
    public Value visitPrint(@NotNull EvokeParser.PrintContext ctx) {
        Value v = visit(ctx.expr());
        return arden.print(v);
    }

    @Override
    public Value visitConcat(@NotNull EvokeParser.ConcatContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.concat(left, right);
    }

    @Override
    public Value visitBuildString(@NotNull EvokeParser.BuildStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.string(v);
    }

    @Override
    public Value visitMatches(@NotNull EvokeParser.MatchesContext ctx) {
        Value vStr = visit(ctx.expr(0));
        Value vPattern = visit(ctx.expr(1));
        return arden.matches(vStr, vPattern);
    }

    @Override
    public Value visitLength(@NotNull EvokeParser.LengthContext ctx) {
        Value v = visit(ctx.expr());
        return arden.length(v);
    }

    @Override
    public Value visitUppercase(@NotNull EvokeParser.UppercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.uppercase(v);
    }

    @Override
    public Value visitLowercase(@NotNull EvokeParser.LowercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.lowercase(v);
    }

    /**
     * The <strong>trim</strong> operator removes leading and trailing white space from a string (see Section 7.1.10).
     * The optional <strong>left</strong> or <strong>right</strong> modifier can be applied to remove leading or
     * trailing white space respectively. Printable characters and embedded white space characters are not affected.
     * The trim of a non-string data type or empty list is {@code null}. Primary times are preserved.
     * @see 9.8.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitTrim(@NotNull EvokeParser.TrimContext ctx) {
        Value v = visit(ctx.expr());
        TrimType type;
        if      (ctx.Left() != null)    type = TrimType.LEFT;
        else if (ctx.Right() != null)   type = TrimType.RIGHT;
        else                            type = TrimType.LEFT_AND_RIGHT;
        return arden.trim(v, type);
    }

    /**
     * The <strong>find ... string</strong> operator locates a substring within a target string, and returns a number
     * that represents the starting position of the substring. <strong>Find ... string</strong> is similar to
     * <strong>matches pattern</strong>, but returns a number (rather than a boolean), and does not support wildcards.
     * <strong>Find ... string</strong> is case-sensitive, and returns a zero if the target string does not contain the
     * exact substring. If either the substring or target is not a string data type, {@code null} is returned. Primary
     * times are not preserved.
     * <br>
     * The optional modifier <strong>starting at...</strong> can be appended to the <strong>find ... string</strong>
     * operator to control where the search for the substring begins. Omitting the modifier causes the search to begin
     * at the first character of the string. The value following <strong>starting at...</strong> must be an integer,
     * otherwise {@code null} is returned. If the value following <strong>starting at...</strong> is an integer beyond
     * the length of the target string (i.e. less than 1 or greater than length target), zero is returned.
     * @see 9.8.9
     * @param ctx
     * @return
     */
    @Override
    public Value visitFindInString(@NotNull EvokeParser.FindInStringContext ctx) {
        Value vNeedle = visit(ctx.expr(0));
        Value vHaystack = visit(ctx.expr(1));

        if (vNeedle.isString() && vHaystack.isString()) {
            int start = 1;
            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(2));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    start = vStartAt.asInteger();

                } else {
                    return NULL();
                }
            }

            return new Value(vHaystack.asString().findPositionOf(vNeedle.asString(), start));
        }

        return NULL();
    }

    /**
     * The <strong>substring ... characters [starting at ...] from ...</strong> operator returns a substring of
     * characters from a designated target string. This substring consists of the specified number of characters from
     * the source string beginning with the starting position (either the first character of the string or the specified
     * location within the string). For example <strong>substring 3 characters starting at 2 from "Example"</strong>
     * would return "xam" - a 3 character string beginning with the second character in the source string "Example".
     * <br>
     * The target string must be a string data type, the starting location within the string must be a positive
     * integer, and the number of characters to be returned must be an integer, or the operator returns {@code null}. If a
     * starting position is specified, its value must be an integer between 1 and the length of the string, otherwise
     * an empty string is returned. If the requested number of characters is greater than the length of the string,
     * the entire string is returned. If a starting point is specified, and the requested number of characters is
     * greater than the length of the string minus the starting point, the resulting string is the original string
     * to the right of and including the starting position. If the number of characters requested is positive the
     * characters are counted from left to right. If the number of characters requested is negative, the characters
     * are counted from right to left. The characters in a substring are always returned in the order that they
     * appear in the string. Default list handling is observed. Primary times are preserved.
     * @see 9.8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubstring(@NotNull EvokeParser.SubstringContext ctx) {
        Value vNumChars = visit(ctx.expr(0));

        if (vNumChars.isNumber() && vNumChars.asNumber().isWholeNumber()) {
            Value vObj;
            int numChars = vNumChars.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vObj = visit(ctx.expr(2));

            } else {
                vObj = visit(ctx.expr(1));
            }

            if (vObj.isString()) {
                return new Value(vObj.asString().substring(startAt, numChars));

            } else if (vObj.isList()) {
                AList list = new AList();
                for (ADataType obj : vObj.asList()) {
                    if (obj instanceof AString) {
                        list.add(((AString) obj).substring(startAt, numChars));

                    } else {
                        ANull n = new ANull();
                        n.setPrimaryTime(obj.getPrimaryTime());                     // preserve primary time
                        list.add(n);
                    }
                }
                return new Value(list);

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitAsString(@NotNull EvokeParser.AsStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asString(v);
    }

    @Override
    public Value visitCount(@NotNull EvokeParser.CountContext ctx) {
        Value v = visit(ctx.expr());
        return arden.count(v);
    }

    @Override
    public Value visitExist(@NotNull EvokeParser.ExistContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exist(v);
    }

    @Override
    public Value visitAverage(@NotNull EvokeParser.AverageContext ctx) {
        Value v = visit(ctx.expr());
        return arden.average(v);
    }

    @Override
    public Value visitMedian(@NotNull EvokeParser.MedianContext ctx) {
        Value v = visit(ctx.expr());
        return arden.median(v);
    }

    @Override
    public Value visitSum(@NotNull EvokeParser.SumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sum(v);
    }

    @Override
    public Value visitStdDev(@NotNull EvokeParser.StdDevContext ctx) {
        Value v = visit(ctx.expr());
        return arden.stdDev(v);
    }

    @Override
    public Value visitVariance(@NotNull EvokeParser.VarianceContext ctx) {
        Value v = visit(ctx.expr());
        return arden.variance(v);
    }

    @Override
    public Value visitMinimum(@NotNull EvokeParser.MinimumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.minimum(v);
    }

    @Override
    public Value visitMaximum(@NotNull EvokeParser.MaximumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.maximum(v);
    }

    @Override
    public Value visitFirst(@NotNull EvokeParser.FirstContext ctx) {
        Value v = visit(ctx.expr());
        return arden.first(v);
    }

    @Override
    public Value visitLast(@NotNull EvokeParser.LastContext ctx) {
        Value v = visit(ctx.expr());
        return arden.last(v);
    }

    @Override
    public Value visitAny(@NotNull EvokeParser.AnyContext ctx) {
        Value v = visit(ctx.expr());
        return arden.any(v);
    }

    @Override
    public Value visitAll(@NotNull EvokeParser.AllContext ctx) {
        Value v = visit(ctx.expr());
        return arden.all(v);
    }

    @Override
    public Value visitNo(@NotNull EvokeParser.NoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.no(v);
    }

    @Override
    public Value visitElement(@NotNull EvokeParser.ElementContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vIndex = visit(ctx.expr(1));
        return arden.element(vObj, vIndex);
    }

    @Override
    public Value visitEarliest(@NotNull EvokeParser.EarliestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.earliest(v);
    }

    @Override
    public Value visitLatest(@NotNull EvokeParser.LatestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.latest(v);
    }

    @Override
    public Value visitExtractChars(@NotNull EvokeParser.ExtractCharsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractChars(v);
    }

    @Override
    public Value visitSeqto(@NotNull EvokeParser.SeqtoContext ctx) {
        Value vFrom = visit(ctx.expr(0));
        Value vTo = visit(ctx.expr(1));
        return arden.seqto(vFrom, vTo);
    }

    @Override
    public Value visitReverse(@NotNull EvokeParser.ReverseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.reverse(v);
    }

    @Override
    public Value visitIndex(@NotNull EvokeParser.IndexContext ctx) {
        Value vList = visit(ctx.expr());
        String indexTypeStr = ctx.indexType().getText();
        return arden.index(vList, indexTypeStr);
    }

    @Override
    public Value visitIndexFrom(@NotNull EvokeParser.IndexFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        String indexTypeStr = ctx.indexType().getText();
        return arden.indexFrom(vNum, vList, indexTypeStr);
    }

    @Override
    public Value visitIndexOfFrom(@NotNull EvokeParser.IndexOfFromContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.indexOfFrom(vObj, vList);
    }

    @Override
    public Value visitAfter(@NotNull EvokeParser.AfterContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.after(vDur, vTime);
    }

    @Override
    public Value visitBefore(@NotNull EvokeParser.BeforeContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.before(vDur, vTime);
    }

    @Override
    public Value visitAgo(@NotNull EvokeParser.AgoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ago(v);
    }

    @Override
    public Value visitTimeOfDayFunc(@NotNull EvokeParser.TimeOfDayFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeOfDayFunc(v);
    }

    @Override
    public Value visitTimeFunc(@NotNull EvokeParser.TimeFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeFunc(v);
    }

    @Override
    public Value visitAtTime(@NotNull EvokeParser.AtTimeContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vTimeOfDay = visit(ctx.expr(1));
        return arden.atTime(vTime, vTimeOfDay);
    }

    @Override
    public Value visitAsTime(@NotNull EvokeParser.AsTimeContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asTime(v);
    }

    @Override
    public Value visitDayOfWeekFunc(@NotNull EvokeParser.DayOfWeekFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.dayOfWeekFunc(v);
    }

    @Override
    public Value visitExtract(@NotNull EvokeParser.ExtractContext ctx) {
        Value v = visit(ctx.expr());
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.extract(v, temporalUnitStr);
    }

    @Override
    public Value visitReplace(@NotNull EvokeParser.ReplaceContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vNumber = visit(ctx.expr(1));
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.replace(vTime, vNumber, temporalUnitStr);
    }

    @Override
    public Value visitUnaryPlus(@NotNull EvokeParser.UnaryPlusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryPlus(v);
    }

    @Override
    public Value visitUnaryMinus(@NotNull EvokeParser.UnaryMinusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryMinus(v);
    }

    @Override
    public Value visitAdd(@NotNull EvokeParser.AddContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.add(left, right);
    }

    @Override
    public Value visitSubtract(@NotNull EvokeParser.SubtractContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.subtract(left, right);
    }

    @Override
    public Value visitMultiply(@NotNull EvokeParser.MultiplyContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.multiply(left, right);
    }

    @Override
    public Value visitDivide(@NotNull EvokeParser.DivideContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.divide(left, right);
    }

    @Override
    public Value visitRaiseToPower(@NotNull EvokeParser.RaiseToPowerContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.raiseToPower(left, right);
    }

    @Override
    public Value visitArccos(@NotNull EvokeParser.ArccosContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arccos(v);
    }

    @Override
    public Value visitArcsin(@NotNull EvokeParser.ArcsinContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arcsin(v);
    }

    @Override
    public Value visitArctan(@NotNull EvokeParser.ArctanContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arctan(v);
    }

    @Override
    public Value visitCosine(@NotNull EvokeParser.CosineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.cosine(v);
    }

    @Override
    public Value visitSine(@NotNull EvokeParser.SineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sine(v);
    }

    @Override
    public Value visitTangent(@NotNull EvokeParser.TangentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.tangent(v);
    }

    @Override
    public Value visitExp(@NotNull EvokeParser.ExpContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exp(v);
    }

    @Override
    public Value visitLog(@NotNull EvokeParser.LogContext ctx) {
        Value v = visit(ctx.expr());
        return arden.log(v);
    }

    @Override
    public Value visitLog10(@NotNull EvokeParser.Log10Context ctx) {
        Value v = visit(ctx.expr());
        return arden.log10(v);
    }

    @Override
    public Value visitFloor(@NotNull EvokeParser.FloorContext ctx) {
        Value v = visit(ctx.expr());
        return arden.floor(v);
    }

    @Override
    public Value visitCeiling(@NotNull EvokeParser.CeilingContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ceiling(v);
    }

    @Override
    public Value visitTruncate(@NotNull EvokeParser.TruncateContext ctx) {
        Value v = visit(ctx.expr());
        return arden.truncate(v);
    }

    @Override
    public Value visitRound(@NotNull EvokeParser.RoundContext ctx) {
        Value v = visit(ctx.expr());
        return arden.round(v);
    }

    @Override
    public Value visitAbs(@NotNull EvokeParser.AbsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.abs(v);
    }

    @Override
    public Value visitSqrt(@NotNull EvokeParser.SqrtContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sqrt(v);
    }

    @Override
    public Value visitAsNumber(@NotNull EvokeParser.AsNumberContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asNumber(v);
    }

    @Override
    public Value visitAnd(@NotNull EvokeParser.AndContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.and(vLeft, vRight);
    }

    @Override
    public Value visitOr(@NotNull EvokeParser.OrContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.or(vLeft, vRight);
    }

    @Override
    public Value visitNot(@NotNull EvokeParser.NotContext ctx) {
        Value v = visit(ctx.expr());
        return arden.not(v);
    }

    @Override
    public Value visitIsEqual(@NotNull EvokeParser.IsEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isEqual(left, right);
    }

    @Override
    public Value visitIsNotEqual(@NotNull EvokeParser.IsNotEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isNotEqual(left, right);
    }

    @Override
    public Value visitIsLessThan(@NotNull EvokeParser.IsLessThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThan(left, right);
    }

    @Override
    public Value visitIsLessThanOrEqual(@NotNull EvokeParser.IsLessThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThanOrEqual(left, right);
    }

    @Override
    public Value visitIsGreaterThan(@NotNull EvokeParser.IsGreaterThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThan(left, right);
    }

    @Override
    public Value visitIsGreaterThanOrEqual(@NotNull EvokeParser.IsGreaterThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThanOrEqual(left, right);
    }

    @Override
    public Value visitIsWithinTo(@NotNull EvokeParser.IsWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitIsWithinPreceding(@NotNull EvokeParser.IsWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinFollowing(@NotNull EvokeParser.IsWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinSurrounding(@NotNull EvokeParser.IsWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinPast(@NotNull EvokeParser.IsWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitIsWithinSameDay(@NotNull EvokeParser.IsWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitIsBefore(@NotNull EvokeParser.IsBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitIsAfter(@NotNull EvokeParser.IsAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitIsIn(@NotNull EvokeParser.IsInContext ctx) {
        Value left = visit(ctx.expr(0));
        boolean flip = ctx.Not() != null;

        if (left.isNull()) {
            return flip ? FALSE() : TRUE();

        } else {
            Value right = visit(ctx.expr(1));
            return arden.isIn(left, right, flip);
        }
    }

    @Override
    public Value visitIsDataType(@NotNull EvokeParser.IsDataTypeContext ctx) {
        Value v = visit(ctx.expr());
        String dataTypeStr = ctx.dataType().getText();
        boolean flip = ctx.Not() != null;
        return arden.isDataType(v, dataTypeStr, flip);
    }

    @Override
    public Value visitIsObjectType(@NotNull EvokeParser.IsObjectTypeContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vType = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isObjectType(vObj, vType, flip);
    }

    @Override
    public Value visitOccurEqual(@NotNull EvokeParser.OccurEqualContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurEqual(vObj, vTime, flip);
    }

    @Override
    public Value visitOccurWithinTo(@NotNull EvokeParser.OccurWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitOccurWithinPreceding(@NotNull EvokeParser.OccurWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinFollowing(@NotNull EvokeParser.OccurWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinSurrounding(@NotNull EvokeParser.OccurWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinPast(@NotNull EvokeParser.OccurWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitOccurWithinSameDay(@NotNull EvokeParser.OccurWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurBefore(@NotNull EvokeParser.OccurBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurAfter(@NotNull EvokeParser.OccurAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitMinimumFrom(@NotNull EvokeParser.MinimumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.minimumFrom(vNum, vList);
    }

    @Override
    public Value visitMaximumFrom(@NotNull EvokeParser.MaximumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.maximumFrom(vNum, vList);
    }

    @Override
    public Value visitFirstFrom(@NotNull EvokeParser.FirstFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.firstFrom(vNum, vList);
    }

    @Override
    public Value visitLastFrom(@NotNull EvokeParser.LastFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.lastFrom(vNum, vList);
    }

    @Override
    public Value visitNearest(@NotNull EvokeParser.NearestContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return ctx.Index() != null ?
                arden.indexNearest(vTime, vList) :
                arden.nearest(vTime, vList);
    }

    @Override
    public Value visitAtMostOrLeast(@NotNull EvokeParser.AtMostOrLeastContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if      (ctx.Least() != null)   return arden.atLeast(vNum, vList);
        else if (ctx.Most() != null)    return arden.atMost(vNum, vList);
        else                            throw new GrammarException("expected 'least' or 'most'");
    }

    /**
     * The <strong>sublist ... elements [starting at ...] from</strong> operator returns a sublist of elements from a
     * designated target list and is similar to the <strong>substring</strong> operator (see 9.8.10). This sublist
     * consists of the specified number of elements from the source list beginning with the starting position (either
     * the first elements of the list or the specified location within the list).
     * <br>
     * The target list must be a list data type, the starting location within the list must be a positive integer,
     * and the number of elements to be returned must be an integer, or the operator returns {@code null}. If target is not a
     * list data type, a list with one element is assumed. If a starting position is specified, its value must be an
     * integer between 1 and the length of the list, otherwise an empty list is returned. If the requested number of
     * elements is greater than the length of the list, the entire list is returned. If a starting point is specified,
     * and the requested number of elements is greater than the size of the list minus the starting point, the
     * resulting list is the original list to the right of and including the starting position. If the number of
     * elements requested is positive the elements are counted from left to right. If the number of elements requested
     * is negative, the elements are counted from right to left. The elements in a sublist are always returned in the
     * order that they appear in the original list. Default list handling is observed. Primary times are preserved.
     * @see 9.14.6
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubList(@NotNull EvokeParser.SubListContext ctx) {
        Value vNumElements = visit(ctx.expr(0));

        if (vNumElements.isNumber() && vNumElements.asNumber().isWholeNumber()) {
            Value vList;
            int numElements = vNumElements.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vList = visit(ctx.expr(2));

            } else {
                vList = visit(ctx.expr(1));
            }

            if (vList.isList()) {
                return new Value(vList.asList().sublist(startAt, numElements));

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitIncrease(@NotNull EvokeParser.IncreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.increase(v, pct);
    }

    @Override
    public Value visitDecrease(@NotNull EvokeParser.DecreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.decrease(v, pct);
    }

    @Override
    public Value visitInterval(@NotNull EvokeParser.IntervalContext ctx) {
        Value v = visit(ctx.expr());
        return arden.interval(v);
    }

    @Override
    public Value visitEarliestFrom(@NotNull EvokeParser.EarliestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.earliestFrom(vNum, vList);
    }

    @Override
    public Value visitLatestFrom(@NotNull EvokeParser.LatestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.latestFrom(vNum, vList);
    }

    private Value NULL() {
        return new Value(ANull.NULL());
    }

    private Value VOID() {
        return new Value(AVoid.VOID());
    }

    private Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    private Value FALSE() {
        return new Value(ABoolean.FALSE());
    }
}
