/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data;

import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.AObjectType;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.AMLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Interface;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * Created by mstorer on 11/22/13.
 */
public class IncludeDataEvalVisitor extends DataEvalVisitor {
    public IncludeDataEvalVisitor(MLM caller) {
        super(caller, null);
    }

    @Override
    protected boolean shouldIncludeVariable(ADataType obj) {

        // when evaluating the data slot for getting variables to include in other MLMs, we only want to
        // include variables of particular types.

        return obj instanceof AObjectType ||
                obj instanceof AMLM ||
                obj instanceof Event ||
                obj instanceof Interface;
    }


    @Override
    public Value visitRead(@NotNull DataParser.ReadContext ctx) {
        return NULL();
    }

    @Override
    public Value visitReadAs(@NotNull DataParser.ReadAsContext ctx) {
        return NULL();
    }

    @Override
    public Value visitCall(@NotNull DataParser.CallContext ctx) {
        return NULL();
    }
}
