/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.mlm;

import edu.uvm.ccts.arden.mlm.antlr.MLMBaseVisitor;
import edu.uvm.ccts.arden.mlm.antlr.MLMParser;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.Category;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.Slot;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.*;

/**
 * Created by mstorer on 8/9/13.
 */
public class MLMEvalVisitor extends MLMBaseVisitor<Boolean> {
    private static final Map<String, List<String>> catSlotMap;

    static {
        catSlotMap = new HashMap<String, List<String>>();
        catSlotMap.put("maintenance", Arrays.asList("title", "mlmname", "arden", "version", "institution", "author",
                                                    "specialist", "date", "validation", "license"));
        catSlotMap.put("library", Arrays.asList("purpose", "explanation", "keywords", "citations", "links"));
        catSlotMap.put("knowledge", Arrays.asList("type", "data", "priority", "evoke", "logic", "action", "urgency"));
        catSlotMap.put("resources", Arrays.asList("default", "language"));
    }

    protected List<Category> categories;

    public MLMEvalVisitor() {
        categories = new ArrayList<Category>();
    }

    public MLM getMLM() {
        return new MLM(categories);
    }

    @Override
    public Boolean visitCategory(@NotNull MLMParser.CategoryContext ctx) {
        String categoryId = stripTrailing(ctx.CategoryID().getText(), ":");

        if ( ! catSlotMap.containsKey(categoryId) ) {
            throw new RuntimeException("invalid category : " + categoryId);
        }

        categories.add(new Category(categoryId));

        visitChildren(ctx);

        return super.visitCategory(ctx);
    }

    @Override
    public Boolean visitSlot(@NotNull MLMParser.SlotContext ctx) {
        String slotId = stripTrailing(ctx.SlotID().getText(), ":");
        String data = stripTrailing(ctx.Data().getText(), ";;");

        Category currentCategory = categories.get(categories.size() - 1);

        if ( ! catSlotMap.get(currentCategory.getName()).contains(slotId) ) {
            throw new RuntimeException("invalid slot for category '" + currentCategory.getName() + "' : " + slotId);
        }

        currentCategory.addSlot(new Slot(slotId, data));

        return super.visitSlot(ctx);
    }

    private String stripTrailing(String s, String toRemove) {
        if (s != null && s.endsWith(toRemove)) {
            return s.substring(0, s.length() - toRemove.length());

        } else {
            return s;
        }
    }
}
