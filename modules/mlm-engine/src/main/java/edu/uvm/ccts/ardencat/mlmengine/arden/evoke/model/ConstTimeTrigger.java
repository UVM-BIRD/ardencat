/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.arden.model.Duration;
import edu.uvm.ccts.arden.model.Time;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 1/6/14.
 */
public class ConstTimeTrigger extends EvokeCondition<ConstTimeTrigger> {
    private TimeExpr timeExpr = null;
    private Duration duration = null;
    private TimeExprSimple timeExprSimple = null;

    public ConstTimeTrigger(TimeExpr timeExpr) {
        this.timeExpr = timeExpr;
    }

    public ConstTimeTrigger(Duration duration, TimeExprSimple timeExprSimple) {
        this.duration = duration;
        this.timeExprSimple = timeExprSimple;
    }

    public ConstTimeTrigger(ConstTimeTrigger ctt) {
        this.timeExpr = ctt.timeExpr != null ? ctt.timeExpr.copy() : null;
        this.duration = Duration.copyOf(ctt.duration);
        this.timeExprSimple = ctt.timeExprSimple != null ? ctt.timeExprSimple.copy() : null;
    }

    public Time getNextTriggerTime(Time relativeToTime) {
        if (timeExpr != null) {
            return timeExpr.getNextTriggerTime(relativeToTime);

        } else {
            Time time = timeExprSimple.getNextTriggerTime(relativeToTime);
            return time.add(duration);
        }
    }

    @Override
    public ConstTimeTrigger copy() {
        return new ConstTimeTrigger(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ConstTimeTrigger ctt = (ConstTimeTrigger) o;
        return new EqualsBuilder()
                .append(timeExpr, ctt.timeExpr)
                .append(duration, ctt.duration)
                .append(timeExprSimple, ctt.timeExprSimple)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(607, 463)
                .append(timeExpr)
                .append(duration)
                .append(timeExprSimple)
                .toHashCode();
    }
}
