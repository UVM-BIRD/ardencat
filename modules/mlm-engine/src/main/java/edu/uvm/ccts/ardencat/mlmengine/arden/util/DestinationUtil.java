/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.util;

import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.*;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;

import java.util.Properties;

/**
 * Created by mstorer on 1/16/14.
 */
public class DestinationUtil {

    public static Destination buildDestination(MLM caller, String text) throws ValidationException, DataException, ConfigurationException {
        if (text.toLowerCase().trim().equals("stdout")) {
            return new StdOutDestination();

        } else if (text.toLowerCase().startsWith("email:")) {
            String str = text.substring(6).trim();

            String subject = "";
            String to = null;

            for (String part : str.split("\\s*,\\s*")) {
                String[] kvPair = part.split("\\s*=\\s*");
                String key = kvPair[0].toLowerCase();

                if      (key.equals("subject")) subject = kvPair[1];
                else if (key.equals("to"))      to = kvPair[1];
            }

            if (to == null) throw new ValidationException("field 'to' is required for email destination");

            return new EmailDestination(subject, to);

        } else if (text.toLowerCase().startsWith("mysql:")) {
            String str = text.substring(6).trim();

            String user = "";
            String pass = null;
            String host = "localhost";
            int port = 3306;
            String db = null;
            String table = null;
            String col = null;

            for (String part : str.split("\\s*,\\s*")) {
                String[] kvPair = part.split("\\s*=\\s*");
                String key = kvPair[0].toLowerCase();

                if      (key.equals("user"))    user = kvPair[1];
                else if (key.equals("pass"))    pass = kvPair[1];
                else if (key.equals("host"))    host = kvPair[1];
                else if (key.equals("port"))    port = Integer.parseInt(kvPair[1]);
                else if (key.equals("db"))      db = kvPair[1];
                else if (key.equals("table"))   table = kvPair[1];
                else if (key.equals("col"))     col = kvPair[1];
                else throw new DataException("invalid property '" + kvPair[0] + "' for database table destination");
            }

            if (db == null)     throw new ValidationException("field 'db' is required for database table destination");
            if (table == null)  throw new ValidationException("field 'table' is required for database table destination");
            if (col == null)    throw new ValidationException("field 'col' is required for database table destination");

            String key = "mysql." + host + "." + user + "." + db;

            if ( ! DataSourceRegistry.getInstance().isDestinationDataSourceRegistered(key) ) {
                String url = "jdbc:mysql://" + host + ":" + port + "/" + db;

                Properties info = new Properties();
                info.setProperty("user", user);
                if (pass != null) info.setProperty("password", pass);

                DataSource dataSource = new DataSource(key, "com.mysql.jdbc.Driver", url, info);

                DataSourceRegistry.getInstance().registerDestinationDataSource(dataSource);
            }

            return new TableDestination(key, table, col);

        } else if (text.toLowerCase().startsWith("ardencat:")) {
            String str = text.substring(9).trim();

            String source = caller.getId();
            EligibilityStatus status = EligibilityStatus.valueOf(str.toUpperCase());

            if (status == null) throw new ValidationException("status is required for ArdenCAT destination");

            return new ArdenCatDestination(caller, source, status);

        } else {
            throw new ValidationException("invalid destination mapping: '" + text + "'");
        }
    }
}
