/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.logic;

import edu.uvm.ccts.ardencat.arden.logic.AbstractLogicEvalVisitor;
import edu.uvm.ccts.arden.logic.antlr.LogicParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.Arden;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import org.antlr.v4.runtime.misc.NotNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * This visitor operates on the Arden Logic slot
 * @author Matt Storer
 */
public class LogicEvalVisitor extends AbstractLogicEvalVisitor<Value, Arden> {
    private static final Log log = LogFactory.getLog(LogicEvalVisitor.class);

    public LogicEvalVisitor(MLM caller, Variables variables) {
        super(variables, new Arden(log, caller));
    }

    @Override
    protected Value createValue(ADataType obj) {
        return new Value(obj);
    }

    @Override
    protected Value NULL() {
        return new Value(ANull.NULL());
    }

    @Override
    protected Value VOID() {
        return new Value(AVoid.VOID());
    }

    @Override
    protected Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    @Override
    protected Value FALSE() {
        return new Value(ABoolean.FALSE());
    }


    /**
     * The call statement permits nesting of MLMs. Given an MLM filename, the MLM can be called directly with optional
     * parameters and return zero or more results. Given an event definition, all the MLMs that are normally evoked by
     * that event can be called; the called MLMs can be given optional parameters and optionally return results. Given
     * an interface definition, the foreign function can be called directly with optional parameters and return zero
     * or more results.
     * @see 10.2.5
     * @param ctx
     * @return
     */
    @Override
    public Value visitCall(@NotNull LogicParser.CallContext ctx) {
        String id = ctx.ID().getText();

        List<ADataType> arguments = null;
        if (ctx.With() != null) {
            arguments = new ArrayList<ADataType>();
            for (LogicParser.ExprContext expr : ctx.expr()) {
                Value v = visit(expr);
                arguments.add(v.getBackingObject());
            }
        }

        return arden.call(id, arguments);
    }
}
