/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 1/16/14.
 */
public class StdOutDestination extends Destination<StdOutDestination> {
    @Override
    public void write(ADataType obj) throws Exception {
        System.out.println(obj.toString());
    }

    @Override
    public String toString() {
        return "StdOutDestination{}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        return o.getClass() == getClass();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(257, 727)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }

    @Override
    public StdOutDestination copy() {
        return this;                        // no need to copy, since this class has no instance variables
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
