/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.exceptions.MLMNotFoundException;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.Category;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 1/3/14.
 */
public class MLMRegistry {
    private static final Log log = LogFactory.getLog(MLMRegistry.class);
    private static MLMRegistry registry = null;

    public static MLMRegistry getInstance() {
        if (registry == null) registry = new MLMRegistry();
        return registry;
    }

    private static final FilenameFilter mlmFilter;
    static {
        mlmFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.matches("(?i:^.+\\.mlm$)");
            }
        };
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private final Map<String, MLM> map = new HashMap<String, MLM>();

    private MLMRegistry() {
    }

    public void unregisterAllMLMs() {
        map.clear();
    }

    public void register(MLM mlm) throws IOException, ConfigurationException {
        try {
            mlm.validateParse();

        } catch (AntlrException e) {
            log.error("register : couldn't parse MLM : " + mlm.getId() + " - " + e.getMessage());
            throw e;
        }

        if (map.containsKey(mlm.getId())) {
            String message = "cannot register MLM with duplicate key '" + mlm.getId() + "'";
            log.error("register : " + message);
            throw new ConfigurationException(message);
        }

        map.put(mlm.getId(), mlm);
        log.info("register : registered MLM : " + mlm.getId());
    }

    public Collection<MLM> getMLMs() {
        return Collections.unmodifiableCollection(map.values());
    }

    public MLM getMLM(String id) {
        return map.get(id);
    }

    public MLM locateMLM(String mlmName, String institution) throws ConfigurationException {
        return locateMLM(mlmName, institution, null);
    }

    public MLM locateMLM(String mlmName, String institution, String validation) throws ConfigurationException {
        MLM mlm = null;
        boolean validationSpecified = validation != null && ! validation.isEmpty();

        for (MLM m : map.values()) {
            Category cat = m.getCategory("maintenance");

            if (mlmName.equalsIgnoreCase(cat.getSlot("mlmname").getData()) &&
                    institution.equalsIgnoreCase(cat.getSlot("institution").getData())) {

                boolean update = false;

                if (validationSpecified) {
                    if (validation.equalsIgnoreCase(cat.getSlot("validation").getData())) {
                        if (mlm == null) {
                            update = true;

                        } else {
                            update = compareVersion(m, mlm) > 0;
                        }
                    }

                } else if (mlm == null) {
                    update = true;

                } else {
                    update = compareVersion(m, mlm) > 0;
                }

                if (update) {
                    mlm = m;
                }
            }
        }

        if (mlm == null) {
            throw new MLMNotFoundException("could not find MLM with name '" + mlmName + "'");
        }

        return mlm;
    }

    public void populateEvokeConditions() throws IOException, ConfigurationException {
        // note : this has to occur AFTER all MLMs have been identified and added to the list; otherwise, MLMs may
        //        not be found (and trigger an exception) while evaluating the MLM for evoke conditions and for data
        //        variables.  I recognize this is sorta hacky, but there's no real clean solution to any chicken-egg
        //        problem, which this is.

        for (MLM mlm : map.values()) {
            mlm.populateEvokeConditions();
        }
    }


///////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private static int compareVersion(MLM mlm1, MLM mlm2) throws ConfigurationException {
        String m1 = mlm1.getCategory("maintenance").getSlot("version").getData();
        String m2 = mlm2.getCategory("maintenance").getSlot("version").getData();

        try {
            Float f1 = Float.parseFloat(m1);
            Float f2 = Float.parseFloat(m2);

            return f1.compareTo(f2);

        } catch (NumberFormatException nfe) {
            return m1.compareTo(m2);
        }
    }
}
