/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data;

import com.foundationdb.sql.StandardException;
import edu.uvm.ccts.arden.data.antlr.DataBaseVisitor;
import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.DurationUtil;
import edu.uvm.ccts.arden.util.NumberUtil;
import edu.uvm.ccts.ardencat.arden.Scope;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.EventRegistry;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.Arden;
import edu.uvm.ccts.ardencat.mlmengine.arden.Value;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.model.ARecord;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.model.ArdenSqlStatement;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.Category;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.util.DestinationUtil;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.exceptions.InvalidAttributeException;
import edu.uvm.ccts.common.exceptions.antlr.GrammarException;
import edu.uvm.ccts.jmethodsig.JMethodSigBuilder;
import edu.uvm.ccts.jmethodsig.model.JMethodSig;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This visitor operates on the Arden Data slot
 * @author Matt Storer
 */
public class DataEvalVisitor extends DataBaseVisitor<Value> {
    private static final Log log = LogFactory.getLog(DataEvalVisitor.class);

    private Arden arden;
    private int loopDepth = 0;
    private boolean breakLoop = false;
    private boolean continueLoop = false;
    private MLM caller;
    private List<ADataType> arguments;

    public DataEvalVisitor(MLM caller, List<ADataType> arguments) {
        arden = new Arden(log, caller);
        this.caller = caller;
        this.arguments = arguments;
    }

    public Variables getVariables() {
        Variables variables = new Variables();
        for (Map.Entry<String, ADataType> entry : arden.getVariablesAndObjectTypes().entrySet()) {
            if (shouldIncludeVariable(entry.getValue())) {
                variables.putVariable(entry.getKey(), entry.getValue());
            }
        }
        return variables;
    }

    private ADataType evaluateId(String id) {
        return arden.evaluateId(id);
    }

    private AObjectType getObjectTypeByName(String typeName) {
        return arden.getObjectTypeByName(typeName);
    }

    private void includeVariable(String id, ADataType obj) {
        arden.includeVariable(id, obj);
    }

    private void includeObjectType(AObjectType objType) {
        arden.includeObjectType(objType);
    }

    public void setPrintStream(PrintStream printStream) {
        arden.setPrintStream(printStream);
    }

    /**
     * Determines whether or not to continue parse-tree traversal.  Will return {@code true} unless a {@code conclude}
     * statement, or the end of the file has been reached.
     * @param node
     * @param currentResult
     * @return
     */
    @Override
    protected boolean shouldVisitNextChild(@NotNull RuleNode node, Value currentResult) {
        return ! breakLoop &&
                ! continueLoop &&
                super.shouldVisitNextChild(node, currentResult);
    }

    protected boolean shouldIncludeVariable(ADataType obj) {
        return true;
    }

    /**
     * The main source of data is the patient database. Each institution will need to do its own queries; databases may
     * be hierarchical, relational, object oriented, etc. The vocabulary used to represent entities in the database
     * will vary from institution to institution. (No attempt was made to select a standard vocabulary in this version
     * of this specification.) The read statement is designed to isolate those parts of a database query that are
     * specific to an institution from those parts that are universal.
     * <br>
     * There is no restriction that a read statement must derive its input from the patient database. A read statement
     * might access a medical dictionary, for example; or it might interactively request information from somebody
     * (and, if the compiler does on-demand optimization, the interaction might happen only if needed). How this is
     * done is implementation defined.
     * @see 11.2.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitRead(@NotNull DataParser.ReadContext ctx) {
        return new Value(read(ctx.Mapping(), ctx.constraint(), ctx.aggregation(), ctx.transformation()));
    }

    /**
     * The read as statement is very similar to the read statement (11.2.1.1). However, rather than returning query
     * results as a set of lists, where each list represents a collection of values for a particular query field (or
     * column), it returns a single list of objects, each of which consist of named attributes (fields) and values.
     * The attribute names are specified in the object declaration, which should have been declared previously (see
     * Section 11.2.17).
     * @see 11.2.2, 8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitReadAs(@NotNull DataParser.ReadAsContext ctx) {
        ADataType adt = read(ctx.Mapping(), ctx.constraint(), ctx.aggregation(), ctx.transformation());

        String typeName = ctx.ID().getText();
        AObjectType type = getObjectTypeByName(typeName);

        if (adt instanceof RowIterable) {
            RowIterable ri = (RowIterable) adt;

            if (ri.getRowCount() == 0) return NULL();

            List<ADataType> adtList = new ArrayList<ADataType>();
            if (type.getAttributes().size() == ri.getColCount()) {
                for (int i = 0; i < ri.getRowCount(); i ++) {
                    AObject obj = new AObject(type);
                    obj.populate(ri.getRow(i));
                    adtList.add(obj);
                }

            } else {
                throw new RuntimeException("attribute count mismatch : object " + typeName + " has " +
                        type.getAttributes().size() + " attributes, but mapping generated " + ri.getColCount() + " fields.");
            }

            return adtList.size() == 1 ?
                    new Value(adtList.get(0)) :
                    new Value(new AList(adtList));

        } else if (type.getAttributes().size() == 1) {
            AObject obj = new AObject(type);
            obj.populate(Arrays.asList(adt));
            return new Value(obj);

        } else {
            throw new RuntimeException("cannot process type " + adt.getClass().getName());
        }
    }

    /**
     * The event statement assigns an institution-specific event definition to a variable. An event can be an
     * insertion or update in the patient database, or any other medically relevant occurrence. The variable is
     * currently used in the evoke slot (see Section 13), as part of the call statement to call other MLMs (see
     * Section 10.2.5), and as a Boolean value in a logic or action slot.
     * @see 11.2.3
     * <br>
     * It can only be used in the evoke slot or as part of a call statement.
     * <br>
     * The variable that represents the event can be treated like a Boolean in the logic or action slots. The Boolean
     * value of the variable is false until the MLM is called by the referred event.
     * <br>
     * The time operator (see Section 9.17) can be applied to an event variable. It yields the clinically relevant
     * time of the event. This may be different from the eventtime variable, which refers to the time that the event
     * was recorded in the database (see Section 8.4.4).
     * @see 11.2.3.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareEvent(@NotNull DataParser.DeclareEventContext ctx) {
        String eventName = chop(ctx.Mapping());
        EventRegistry registry = EventRegistry.getInstance();

        // todo : should this throw an exception if the specified event has already been registered?

        Event event;
        if (registry.isRegistered(eventName)) {
            event = registry.getEvent(eventName);

        } else {
            event = new Event(eventName);
            registry.register(event);
        }

        return new Value(event);
    }

    /**
     * The MLM statement assigns a valid mlmname to a variable. That variable is currently used only as part of
     * the call statement to call another MLM, as defined in Section 10.2.5.
     * <br>
     * If the institution is specified, then a unique MLM is found using the institution name, the mlmname, and
     * the latest version number. If the institution is not specified, then a unique MLM is found using the same
     * institution as the main (calling) MLM, the mlmname, the MLM's validation, and the latest version number.
     * Although the exact form of the version is institution-specific, within an institution it is possible to
     * determine the latest version of an MLM (see Section 6.1.4).
     * @see 11.2.4
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareMlm(@NotNull DataParser.DeclareMlmContext ctx) {
        String name = chop(ctx.Term());
        String institution = ctx.Institution() != null ? chop(ctx.Institution()) : "" ;

        try {
            MLM mlm = null;
            MLMRegistry registry = MLMRegistry.getInstance();

            if (institution.isEmpty()) {
                Category cat = caller.getCategory("maintenance");
                institution = cat.getSlot("institution").getData();
                String validation = cat.getSlot("validation").getData();
                mlm = registry.locateMLM(name, institution, validation);

            } else {
                mlm = registry.locateMLM(name, institution);
            }

            AMLM amlm = new AMLM(name, institution, mlm);
            return new Value(amlm);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The argument statement is used by an MLM that is called by another MLM, as defined in Section 10.2.5. If the
     * main MLM passes parameters to the called MLM, then the called MLM retrieves the parameters via the argument
     * statement. The argument statements access the corresponding passed arguments. Thus, the first variable <var1>
     * refers to the first passed argument, the second variable <var2>to the second argument, etc.
     * <br>
     * If there is a mismatch of variables where the number of variables is greater than the number of arguments
     * passed from the CALL, null is assigned to the extra left-hand-side variable(s). If the MLM is evoked instead
     * of called, all the arguments are treated as null (just like any other uninitialized variable).
     * @see 11.2.5
     * @param ctx
     * @return
     */
    @Override
    public Value visitArgument(@NotNull DataParser.ArgumentContext ctx) {
        ArgumentList list = arguments != null && arguments.size() > 0 ?
                new ArgumentList(arguments) :
                new ArgumentList();

        return new Value(list);
    }


    /**
     * The interface statement assigns an institution-specific foreign function interface definition to a variable.
     * <br>
     * The interface statement permits specification of a foreign function, i.e., a function written in another
     * programming language. Sometimes medical logic requires information not directly available from the database
     * (via read statements). It may be desirable to call operating system functions or libraries obtained from
     * other vendors. A foreign function, when specified, can then be called with the call statement (see Section
     * 10.2.5). Curly braces ({}) are used to specify the foreign function. The specification within the curly braces
     * is implementation specific.
     * @see 11.2.16
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareInterface(@NotNull DataParser.DeclareInterfaceContext ctx) {
        String m = chop(ctx.Mapping());

        Interface iface = null;

        try {
            JMethodSig sig = JMethodSigBuilder.build(m);
            iface = new MethodInterface(sig);

        } catch (Throwable t) {
            log.debug("couldn't parse Java method signature from '" + m + "' - treating as a script filename");
        }

        if (iface == null) {
            File f = new File(m);
            if (f.canExecute()) {
                iface = new ScriptInterface(m);
            }
        }

        if (iface == null) {
            throw new RuntimeException("invalid interface mapping '" + m + "'");
        }

        return new Value(iface);
    }

    /**
     * The message statement assigns an institution-specific message (for example, an alert) to a variable. It allows
     * an institution to write coded messages in the patient database (see Section 12.2).
     * @see 10.2.6
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareMessage(@NotNull DataParser.DeclareMessageContext ctx) {
        String m = chop(ctx.Mapping());

        // todo : implement this

        return new Value(new Message(m));
    }

    /**
     * The message as statement is very similar to the message statement (11.2.5). However, rather than returning a
     * variable, it returns a single object, which consists of named attributes (fields) and values. The attribute
     * names are specified in the object statement, which should have occurred previously in the MLM (see Section
     * 11.2.13). If the mapping clause is empty, it may be omitted in this statement. However, it is up to the
     * implementation if a non-empty mapping clause is allowed.
     * @see 11.2.7, 8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareMessageAs(@NotNull DataParser.DeclareMessageAsContext ctx) {
        AObjectType type = getObjectTypeByName(ctx.ID().getText());

        // todo : implement this

        // todo : do something with mapping?

        return new Value(new AObject(type));
    }

    /**
     * The destination statement assigns an institution-specific destination to a variable. It allows one to write a
     * message to an institution-specific destination (see Section 12.2.1).
     * @see 11.2.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareDestination(@NotNull DataParser.DeclareDestinationContext ctx) {
        String m = chop(ctx.Mapping());

        try {
            return new Value(DestinationUtil.buildDestination(caller, m));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The destination as statement is very similar to the destination statement (11.2.6.1). However, rather than
     * returning a variable, it returns a single object, which consists of named attributes (fields) and values. The
     * attribute names are specified in the object statement, which should have occurred previously in the MLM (see
     * Section 11.2.17). If the mapping clause is empty, it may be omitted in this statement. However, it is up to the
     * implementation if a non-empty mapping clause is allowed.
     * @see 11.2.9, 8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareDestinationAs(@NotNull DataParser.DeclareDestinationAsContext ctx) {

        // todo : implement this - but how?  documentation and examples are confusing -

        throw new UnsupportedOperationException("destination-as is not implemented");
    }

    /**
     * The include statement is analogous to the include statement in C-based languages in that indicates an external
     * MLM may be consulted for object, MLM, event, interface variable and resource definitions. The include statement
     * references a variable previously assigned in an MLM statement (11.2.3). When object definitions or resource
     * definitions occur in both the local MLM and a remote MLM, the definition in the local scope always takes
     * precedence. If two remote MLMs define objects or resource definitions with the same name or key, the definitions
     * in MLMs referred to later in the local MLM take precedence.
     * @see 11.2.19
     * @param ctx
     * @return
     */
    @Override
    public Value visitInclude(@NotNull DataParser.IncludeContext ctx) {
        ADataType adt = evaluateId(ctx.ID().getText());

        try {
            if (adt instanceof AMLM) {
                MLM mlm = ((AMLM) adt).getMlm();

                Variables vars = mlm.getVariablesForInclusion();
                for (String name : vars.getVariableNames()) {
                    ADataType obj = vars.getVariable(name);

                    if (obj instanceof AObjectType) {
                        AObjectType objType = (AObjectType) obj;
                        includeObjectType(objType);

                    } else {
                        includeVariable(name, obj);
                    }
                }

                return Value.VOID;

            } else {
                throw new RuntimeException("cannot include object of type " + adt.getClass().getName());
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The <strong>object</strong> statement assigns object declaration to a variable.
     * <br>
     * This variable should not be reassigned in another statement, and the variable name becomes the object type name
     * (as used in a read as statement (Section 11.2.2) or new statement (Section 10.2.8). The <strong>object</strong>
     * statement permits specification of the attributes and attribute ordering of an object type.
     * @see {@link AObject}, {@link AObjectType}, 11.2.17
     * @param ctx
     * @return
     */
    @Override
    public Value visitDeclareObject(@NotNull DataParser.DeclareObjectContext ctx) {
        List<String> attrNames = new ArrayList<String>();
        for (TerminalNode tn : ctx.ID()) {
            attrNames.add(tn.getText());
        }
        return new Value(new AObjectType(attrNames));
    }


/////////////////////////////////////////////////////////////////////////////////////////////////
// Private methods
//

    private ADataType read(TerminalNode mapping,
                           DataParser.ConstraintContext constraint,
                           DataParser.AggregationContext aggregation,
                           DataParser.TransformationContext transformation) {

        List<ARecord> records = buildRecords(mapping, constraint);

        try {
            if (aggregation != null) {
                return aggregate(records, aggregation);

            } else if (transformation != null) {
                return transform(records, transformation);

            } else if (hasMultipleFields(records)) {
                return convertRecordsToListOfLists(records);

            } else {
                return convertFirstRecordValuesToList(records);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String chop(TerminalNode tn) {
        String s = tn.getText();
        return s.substring(1, s.length() - 1);                   // strip opening and closing braces
    }

    private List<ARecord> buildRecords(TerminalNode mapping, DataParser.ConstraintContext constraintCtx) {
        String m = chop(mapping);

        ArdenDataSource ds;
        try {
            String key = extractDataSourceKey(m);

            DataSourceRegistry registry = DataSourceRegistry.getInstance();
            if (registry.isMLMDataSourceRegistered(key)) {
                ds = registry.getMLMDataSource(key);

            } else {
                throw new ConfigurationException("MLM data source '" + key + "' is not defined");
            }

        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }

        ArdenSqlStatement stmt;
        try {
            stmt = new ArdenSqlStatement(extractSql(m), ds);
            updateWhereClauseWithConstraint(stmt, constraintCtx);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        log.debug("buildRecords : will execute SQL: " + stmt.getSql());

        ArdenRecordBuilder recordBuilder = new ArdenRecordBuilder(ds, stmt);

        return recordBuilder.execute();
    }

    private String extractDataSourceKey(String mapping) throws ConfigurationException {
        int pos = mapping.indexOf(':');

        if (pos == -1) {
            throw new ConfigurationException("read mapping is missing data source identifier");

        } else {
            String key = mapping.substring(0, pos).trim();
            if (key.isEmpty()) {
                throw new ConfigurationException("read mapping data source identifier cannot be blank");
            }

            return key;
        }
    }

    private String extractSql(String mapping) {
        int pos = mapping.indexOf(':');
        return pos >= 0 ?
                mapping.substring(pos + 1) :
                mapping;
    }

    private boolean hasMultipleFields(List<ARecord> records) {
        return getFieldCount(records) > 1;
    }

    private int getFieldCount(List<ARecord> records) {
        return records.isEmpty() ? 0 : records.get(0).getFieldNames().size();
    }

    private AListOfLists convertRecordsToListOfLists(List<ARecord> records) {
        if (records.isEmpty()) return new AListOfLists();

        int listsToBuild = records.get(0).getFieldNames().size();

        AListOfLists listOfLists = new AListOfLists(listsToBuild);
        for (ARecord record : records) {
            int i = 0;
            for (ADataType adt : record.asList()) {
                listOfLists.getList(i++).add(adt);
            }
        }

        return listOfLists;
    }

    private AList convertFirstRecordValuesToList(List<ARecord> records) {
        AList list = new AList();

        String fieldName = null;
        for (ARecord record : records) {
            if (fieldName == null) {
                fieldName = record.getFieldNames().iterator().next();
            }
            list.add(record.getField(fieldName));
        }

        return list;
    }

    private ADataType aggregate(List<ARecord> records, DataParser.AggregationContext ctx) {
        if (hasMultipleFields(records)) {
            AListOfLists listOfLists = new AListOfLists();
            for (ADataType subList : convertRecordsToListOfLists(records)) {
                AList list = new AList();
                list.add(applyAggregation(ctx, (AList) subList));
                listOfLists.add(list);
            }
            return listOfLists;

        } else {
            return applyAggregation(ctx, convertFirstRecordValuesToList(records));
        }
    }

    private ADataType transform(List<ARecord> records, DataParser.TransformationContext ctx) {
        if (hasMultipleFields(records)) {
            AListOfLists listOfLists = new AListOfLists();
            for (ADataType subList : convertRecordsToListOfLists(records)) {
                listOfLists.add(applyTransformation(ctx, (AList) subList));
            }
            return listOfLists;

        } else {
            return applyTransformation(ctx, convertFirstRecordValuesToList(records));
        }
    }

    private ADataType applyAggregation(DataParser.AggregationContext ctx, AList list) {
        Value vList = new Value(list);
        Value v;

        if (ctx.Exist() != null)            v = arden.exist(vList);
        else if (ctx.Sum() != null)         v = arden.sum(vList);
        else if (ctx.Avg() != null)         v = arden.average(vList);
        else if (ctx.Min() != null)         v = arden.minimum(vList);
        else if (ctx.Max() != null)         v = arden.maximum(vList);
        else if (ctx.Last() != null)        v = arden.last(vList);
        else if (ctx.First() != null)       v = arden.first(vList);
        else if (ctx.Earliest() != null)    v = arden.earliest(vList);
        else if (ctx.Latest() != null)      v = arden.latest(vList);
        else throw new GrammarException("unknown read-statement aggregation");

        return v.getBackingObject();
    }

    private ADataType applyTransformation(DataParser.TransformationContext ctx, AList list) {
        Value vList = new Value(list);
        Value v;

        if (ctx.Min() != null)              v = arden.minimumFrom(visit(ctx.expr()), vList);
        else if (ctx.Max() != null)         v = arden.maximumFrom(visit(ctx.expr()), vList);
        else if (ctx.Last() != null)        v = arden.lastFrom(visit(ctx.expr()), vList);
        else if (ctx.First() != null)       v = arden.firstFrom(visit(ctx.expr()), vList);
        else if (ctx.Earliest() != null)    v = arden.earliestFrom(visit(ctx.expr()), vList);
        else if (ctx.Latest() != null)      v = arden.latestFrom(visit(ctx.expr()), vList);
        else throw new GrammarException("unknown read-statement transformation");

        return v.getBackingObject();
    }

    private void updateWhereClauseWithConstraint(ArdenSqlStatement stmt, @NotNull DataParser.ConstraintContext rscCtx) throws StandardException {
        if (rscCtx != null) {
            boolean invert = rscCtx.Not() != null;

            if (rscCtx.Equal() != null || rscCtx.At() != null) {
                Value v = visit(rscCtx.expr(0));
                if (v.isTime()) {
                    stmt.appendEqualWhereClause(v.asTime(), invert);
                }

            } else if (rscCtx.Before() != null) {
                Value v = visit(rscCtx.expr(0));
                if (v.isTime()) {
                    stmt.appendBeforeWhereClause(v.asTime(), invert);
                }

            } else if (rscCtx.After() != null) {
                Value v = visit(rscCtx.expr(0));
                if (v.isTime()) {
                    stmt.appendAfterWhereClause(v.asTime(), invert);
                }

            } else if (rscCtx.To() != null) {
                Value vLeft = visit(rscCtx.expr(0));
                Value vRight = visit(rscCtx.expr(1));
                if (vLeft.isTime() && vRight.isTime()) {
                    stmt.appendBetweenWhereClause(vLeft.asTime(), vRight.asTime(), invert);
                }

            } else if (rscCtx.Preceding() != null) {
                Value vLeft = visit(rscCtx.expr(0));
                Value vRight = visit(rscCtx.expr(1));
                if (vLeft.isDuration() && vRight.isTime()) {
                    stmt.appendPrecedingWhereClause(vLeft.asDuration(), vRight.asTime(), invert);
                }

            } else if (rscCtx.Following() != null) {
                Value vLeft = visit(rscCtx.expr(0));
                Value vRight = visit(rscCtx.expr(1));
                if (vLeft.isDuration() && vRight.isTime()) {
                    stmt.appendFollowingWhereClause(vLeft.asDuration(), vRight.asTime(), invert);
                }

            } else if (rscCtx.Surrounding() != null) {
                Value vLeft = visit(rscCtx.expr(0));
                Value vRight = visit(rscCtx.expr(1));
                if (vLeft.isDuration() && vRight.isTime()) {
                    stmt.appendSurroundingWhereClause(vLeft.asDuration(), vRight.asTime(), invert);
                }

            } else if (rscCtx.Past() != null) {
                Value v = visit(rscCtx.expr(0));
                if (v.isDuration()) {
                    stmt.appendPastWhereClause(v.asDuration(), arden.getNow(), invert);
                }

            } else if (rscCtx.SameDayAs() != null) {
                Value v = visit(rscCtx.expr(0));
                if (v.isTime()) {
                    stmt.appendSameDayAsWhereClause(v.asTime(), invert);
                }

            } else {
                throw new GrammarException("unknown read-statement constraint");
            }
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// from LogicEvalVisitor
//

    /**
     * A block represents a part of the codebase that has its own scope.
     * @see {@link Scope}
     * @param ctx
     * @return
     */
    @Override
    public Value visitBlock(@NotNull DataParser.BlockContext ctx) {
        arden.getScope().shift();
        try {
            return visitChildren(ctx);

        } finally {
            arden.getScope().pop();
        }
    }

    /**
     * The <strong>if-then</strong> statement permits conditional execution based upon the value of an expression. It
     * tests whether the expression (&lt;expr&gt;) is equal to a single Boolean {@code true}. If it is, then a block of
     * statements (&lt;block&gt;) is executed. (A block of statements is simply a collection of valid statements possibly
     * including other if-then statements; thus the if-then statement is a nested structure.) If the expression is a
     * list, or if it is any single item other than {@code true}, then the block of statements is <strong>not</strong>
     * executed. The flow of control then continues with subsequent statements.
     * <br>
     * It is important to emphasize that non-{@code true} is different from {@code false}. That is, the
     * <strong>else</strong> portion of the if-then-else statement is executed whether the expression is {@code false},
     * or {@code null}, or anything other than {@code true}.
     * @see 10.2.2
     * @param ctx
     * @return
     */
    @Override
    public Value visitIfStatement(@NotNull DataParser.IfStatementContext ctx) {
        int exprSize = ctx.expr().size();
        int blockSize = ctx.block().size();
        boolean found = false;

        for (int i = 0; i < exprSize; i ++) {
            Value v = visit(ctx.expr(i));
            if (v.asBoolean().hasValue(ABoolean.TRUE())) {
                found = true;
                visit(ctx.block(i));
                break;
            }
        }

        if ( ! found && blockSize == exprSize + 1 ) {       // has else clause
            visit(ctx.block(blockSize - 1));
        }

        return Value.VOID;
    }

    /**
     * The <strong>switch-case</strong> statement permits conditional execution based on the value of an expression. It
     * tests whether an expression (&lt;expr1&gt;, &lt;expr2&gt;, &lt;expr3&gt; ...) is equal to the value of the provided variable
     * (&lt;var&gt;). If they are equal, the corresponding block of statements (&lt;block1&gt;, &lt;block2&gt;, &lt;block3&gt; ...) is executed.
     * A block of statements is simply a collection of valid statements, possibly including other switch- case
     * statements; thus the switch-case statement is a nested structure. If the expression does not match the value of
     * the provided variable, then the corresponding block of statements is not executed. The flow of control then
     * continues with subsequent statements.
     * @see 10.2.3
     * @param ctx
     * @return
     */
    @Override
    public Value visitSwitchStatement(@NotNull DataParser.SwitchStatementContext ctx) {
        ADataType obj = evaluateId(ctx.ID().getText());

        int exprSize = ctx.expr().size();
        int blockSize = ctx.block().size();
        boolean found = false;

        for (int i = 0; i < exprSize; i ++) {
            Value v = visit(ctx.expr(i));
            if (obj.equals(v.getBackingObject())) {
                found = true;
                visit(ctx.block(i));
                break;
            }
        }

        if ( ! found && blockSize == exprSize + 1 ) {       // has default clause
            visit(ctx.block(blockSize - 1));
        }

        return Value.VOID;
    }

    /**
     * The <strong>while loop</strong> tests whether an expression (&lt;expr&gt;) is equal to a single Boolean 
     * {@code true} (similar to the conditional execution introduced in the if ... then syntax - see Section 10.2.1.2). 
     * If it is, the block of statements (&lt;block&gt;) is executed repeatedly until &lt;expr&gt; is not {@code true}. 
     * If &lt;expr&gt; is not {@code true}, the block is not executed.
     * @see 10.2.6
     * @param ctx
     * @return
     */
    @Override
    public Value visitWhileLoop(@NotNull DataParser.WhileLoopContext ctx) {
        Value exprVal = visit(ctx.expr());

        try {
            enterLoop();
            while (exprVal.asBoolean().hasValue(ABoolean.TRUE())) {
                beginLoopIteration();
                visit(ctx.block());
                exprVal = visit(ctx.expr());
            }

        } finally {
            exitLoop();
        }

        return Value.VOID;
    }

    /**
     * Another form of looping is provided by the <strong>for loop</strong>.  The &lt;expr&gt; will usually be a list
     * generator. If &lt;expr&gt; is empty or {@code null}, the block is not executed. Otherwise the block is executed
     * with the &lt;identifier&gt; taking on consecutive elements in &lt;expr&gt;. The &lt;identifier&gt; cannot be
     * assigned to inside the &lt;block&gt; (the compiler must produce a compilation error if this is attempted). After
     * the <strong>enddo</strong>, the &lt;identifier&gt; becomes undefined and its value should not be used. A compiler
     * may flag this as an error.
     * @see 10.2.7
     * @param ctx
     * @return
     */
    @Override
    public Value visitForLoop(@NotNull DataParser.ForLoopContext ctx) {
        String id = ctx.ID().getText();
        Value v = visit(ctx.expr());
        AList list = v.asList();

        boolean retainValue = arden.getScope().isDefined(id);

        try {
            enterLoop();
            for (ADataType item : list) {
                beginLoopIteration();
                try {
                    arden.getScope().put(id, item);        // todo : do not permit re-assignment of id within for-loop scope
                    arden.getScope().freeze(id);

                    visit(ctx.block());

                } finally {
                    arden.getScope().thaw(id);
                }
            }

        } finally {
            exitLoop();
        }

        if ( ! retainValue ) {
            arden.getScope().remove(id);
        }

        return Value.VOID;
    }

    /**
     * The block of statements (&lt;block&gt;) of a while loop may contain a <strong>breakloop</strong> statement. If 
     * the execution reaches such a breakloop statement, the direct superior loop will be aborted immediately. If the 
     * breakloop statement occurs within a nested loop, it will always apply to the innermost loop only. Breakloop 
     * statements are only allowed inside of loops.
     * @see 10.2.6.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitBreakLoop(@NotNull DataParser.BreakLoopContext ctx) {
        if (inLoop()) {
            breakLoop = true;
            return Value.VOID;

        } else {
            throw new RuntimeException("breakloop only permitted within a loop");
        }
    }

    @Override
    public Value visitContinueLoop(@NotNull DataParser.ContinueLoopContext ctx) {
        if (inLoop()) {
            continueLoop = true;
            return Value.VOID;

        } else {
            throw new RuntimeException("continue only permitted within a loop");
        }
    }

    private void enterLoop() {
        loopDepth ++;
    }

    private void exitLoop() {
        if (breakLoop) breakLoop = false;
        if (continueLoop) continueLoop = false;
        loopDepth --;
    }

    private boolean inLoop() {
        return loopDepth > 0;
    }

    private void beginLoopIteration() {
        if (continueLoop) continueLoop = false;
    }

    @Override
    public Value visitParens(@NotNull DataParser.ParensContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Value visitId(@NotNull DataParser.IdContext ctx) {
        String id = ctx.ID().getText();
        return arden.id(id);
    }

    @Override
    public Value visitNullVal(@NotNull DataParser.NullValContext ctx) {
        return arden.nullVal();
    }

    @Override
    public Value visitStringVal(@NotNull DataParser.StringValContext ctx) {
        String str = ctx.StringVal().getText();
        return arden.stringVal(str);
    }

    @Override
    public Value visitNumberVal(@NotNull DataParser.NumberValContext ctx) {
        String numStr = ctx.NumberVal().getText();
        return arden.numberVal(numStr);
    }

    @Override
    public Value visitBooleanVal(@NotNull DataParser.BooleanValContext ctx) {
        String boolStr = ctx.BooleanVal().getText();
        return arden.booleanVal(boolStr);
    }

    @Override
    public Value visitBinaryList(@NotNull DataParser.BinaryListContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.binaryList(left, right);
    }

    @Override
    public Value visitUnaryList(@NotNull DataParser.UnaryListContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryList(v);
    }

    @Override
    public Value visitEmptyList(@NotNull DataParser.EmptyListContext ctx) {
        return arden.emptyList();
    }

    @Override
    public Value visitNow(@NotNull DataParser.NowContext ctx) {
        return arden.now();
    }

    @Override
    public Value visitCurrentTime(@NotNull DataParser.CurrentTimeContext ctx) {
        return arden.currentTime();
    }

    @Override
    public Value visitTimeVal(@NotNull DataParser.TimeValContext ctx) {
        return arden.timeVal(ctx.getText());
    }

    @Override
    public Value visitTimeOfDayVal(@NotNull DataParser.TimeOfDayValContext ctx) {
        return arden.timeOfDayVal(ctx.getText());
    }

    /**
     * The duration data type signifies an interval of time that is not anchored to any particular point in absolute
     * time. There are no duration constants. Instead one builds durations using the duration operators (see Section
     * 9.10.7). For example, <strong>1 day</strong>, <strong>45 seconds</strong>, and <strong>3.2 months</strong> are
     * durations.
     * @see {@link Duration}, {@link DurationUnit}, {@link SecondsDuration}, {@link MonthsDuration}, 8.5, 9.10.7, 9.11
     * @param ctx
     * @return
     */
    @Override
    public Value visitDuration(@NotNull DataParser.DurationContext ctx) {
        return new Value(buildDuration(ctx.durationExpr()));
    }

    private Duration buildDuration(DataParser.DurationExprContext durExprCtx) {
        // first, split duration ("3 days 2 hours") into component durations ("3 days", "2 hours").

        List<Duration> componentDurations = new ArrayList<Duration>();
        for (int i = 0; i < durExprCtx.NumberVal().size(); i ++) {
            ANumber n = NumberUtil.parseNumber(durExprCtx.NumberVal(i).getText());
            String s = durExprCtx.durationUnit(i).getText();

            try {
                componentDurations.add(DurationUtil.buildDuration(n, s));

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // next, build a Duration object that represents the sum of each component duration, generated above

        Duration duration = null;
        for (Duration d : componentDurations) {
            if (duration == null) {
                duration = d;

            } else {
                duration = duration.add(d);     // note: might change form from MonthsDuration to SecondsDuration
            }
        }

        return duration;
    }

    @Override
    public Value visitDayOfWeek(@NotNull DataParser.DayOfWeekContext ctx) {
        String dayOfWeekStr = ctx.DayOfWeek().getText();
        return arden.dayOfWeek(dayOfWeekStr);
    }

    /**
     * The <strong>new</strong> statement causes a new object to be created, and assigns it to the named variable.
     * <br>
     * In the simple case (without the <strong>with</strong> clause) all attributes of the object are initialized to
     * {@code null}. In the full statement, a set of 1 or more comma-separated expressions should follow the
     * <strong>with</strong> reserved word. Each expression is evaluated and assigned as a value of an attribute of
     * the object. They are assigned in the order the attributes were declared in the <strong>object</strong> statement.
     * If the number of expressions is less than the number of attributes, remaining attributes are initialized to
     * {@code null}. If the number of expressions is greater than the number of attributes, the extra expressions are
     * evaluated but the results are silently discarded.
     * @see {@link AObject}, 10.2.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitNewObject(@NotNull DataParser.NewObjectContext ctx) {
        String typeName = ctx.ID().getText();

        AObjectType type = arden.getObjectType(typeName);
        if (type == null) throw new RuntimeException(typeName + " does not refer to a valid Arden object type");

        AObject obj = new AObject(type);

        // ordered-with refers to e.g., "new <object> with <attrVal1>, <attrVal2>"
        if (ctx.objOrderedWith() != null) {
            DataParser.ObjOrderedWithContext octx = ctx.objOrderedWith();
            List<ADataType> list = new ArrayList<ADataType>();

            for (DataParser.ExprContext ectx : octx.expr()) {
                Value v = visit(ectx);
                list.add(v.getBackingObject());
            }

            obj.populate(list);
        }

        // named-with refers to e.g., "new <object> with <attrName1> := <attrVal1>, <attrName2> := <attrVal2>"
        if (ctx.objNamedWith() != null) {
            DataParser.ObjNamedWithContext nctx = ctx.objNamedWith();
            int max = nctx.ID().size();

            for (int i = 0; i < max; i ++) {
                String attrName = nctx.ID(i).getText();
                Value v = visit(nctx.expr(i));

                try {
                    obj.setAttribute(attrName, v.getBackingObject());

                } catch (InvalidAttributeException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return new Value(obj);
    }

    /**
     * The <strong>dot</strong> operator ("{@code .}") selects an attribute from an object based on the name following
     * the dot. It takes an expression and an identifier. The expression typically evaluates to an object or a list of
     * objects.
     * <br>
     * If the expression does not evaluate to an object, or if the object does not contain the named attribute, then
     * {@code null} is returned. If the expression evaluates to a list, normal Arden list handling is used and a list is
     * returned. Therefore, if the expression is a list of objects, then a list (of the same length) of the attribute
     * values named by the identifier is returned (a common usage).
     * @see {@link AObject}, 9.18.1
     * @param ctx
     * @return
     */
    @Override
    public Value visitDot(@NotNull DataParser.DotContext ctx) {
        Value v = visit(ctx.expr());

        if (v.isObject()) {                                                         // if its an object, return the
            return new Value(visitDotHelper(v.asObject(), ctx));                    // requested attribute

        } else if (v.isList() && v.asList().getListType() == ListType.OBJECT) {     // if it's a list of objects,
            AList list = new AList();                                               // return a list containing the
            for (ADataType adt : v.asList()) {                                      // requested attribute, one for
                list.add(visitDotHelper((AObject) adt, ctx));                       // each object in the source list
            }
            return new Value(list);
        }

        return NULL();
    }

    private ADataType visitDotHelper(AObject ao, DataParser.DotContext ctx) {
        String attrName = ctx.ID().getText();

        if (ao.hasAttribute(attrName)) {
            try {
                return ao.getAttribute(attrName);

            } catch (InvalidAttributeException e) {
                throw new RuntimeException(e);          // should never get here, for call to hasAttribute() above
            }
        }

        return new ANull();
    }

    /**
     * The call statement permits nesting of MLMs. Given an MLM filename, the MLM can be called directly with optional
     * parameters and return zero or more results. Given an event definition, all the MLMs that are normally evoked by
     * that event can be called; the called MLMs can be given optional parameters and optionally return results. Given
     * an interface definition, the foreign function can be called directly with optional parameters and return zero
     * or more results.
     * @see 10.2.5
     * @param ctx
     * @return
     */
    @Override
    public Value visitCall(@NotNull DataParser.CallContext ctx) {
        String id = ctx.ID().getText();

        List<ADataType> arguments = null;
        if (ctx.With() != null) {
            arguments = new ArrayList<ADataType>();
            for (DataParser.ExprContext expr : ctx.expr()) {
                Value v = visit(expr);
                arguments.add(v.getBackingObject());
            }
        }

        return arden.call(id, arguments);
    }

    @Override
    public Value visitClone(@NotNull DataParser.CloneContext ctx) {
        Value v = visit(ctx.expr());
        return arden.clone(v);
    }

    @Override
    public Value visitExtractAttrNames(@NotNull DataParser.ExtractAttrNamesContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractAttrNames(v);
    }

    @Override
    public Value visitAttributeFrom(@NotNull DataParser.AttributeFromContext ctx) {
        Value vAttrName = visit(ctx.expr(0));
        Value vObj = visit(ctx.expr(1));
        return arden.attributeFrom(vAttrName, vObj);
    }

    @Override
    public Value visitSort(@NotNull DataParser.SortContext ctx) {
        Value v = visit(ctx.expr());
        boolean sortByTime = ctx.Time() != null;
        return arden.sort(v, sortByTime);
    }

    @Override
    public Value visitMerge(@NotNull DataParser.MergeContext ctx) {
        Value v1 = visit(ctx.expr(0));
        Value v2 = visit(ctx.expr(1));
        return arden.merge(v1, v2);
    }

    @Override
    public Value visitWhereTimeIsPresent(@NotNull DataParser.WhereTimeIsPresentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.whereTimeIsPresent(v);
    }

    @Override
    public Value visitAddToList(@NotNull DataParser.AddToListContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if (ctx.At() != null) {
            Value vAt = visit(ctx.expr(2));
            return arden.addToList(vObj, vList, vAt);

        } else {
            return arden.addToList(vObj, vList);
        }
    }

    @Override
    public Value visitRemoveFromList(@NotNull DataParser.RemoveFromListContext ctx) {
        Value vPos = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.removeFromList(vPos, vList);
    }

    @Override
    public Value visitWhere(@NotNull DataParser.WhereContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTest = visit(ctx.expr(1));
        return arden.where(vObj, vTest);
    }

    @Override
    public Value visitAssignment(@NotNull DataParser.AssignmentContext ctx) {
        String id = ctx.ID().getText();
        Value v = visit(ctx.assignable());
        return arden.assignment(id, v);
    }

    @Override
    public Value visitMultipleAssignment(@NotNull DataParser.MultipleAssignmentContext ctx) {
        List<String> idList = new ArrayList<String>();
        for (TerminalNode tn : ctx.ID()) {
            idList.add(tn.getText());
        }

        Value v = visit(ctx.assignable());

        return arden.multipleAssignment(idList, v);
    }

    /**
     * Permit the setting of object attributes, and the setting of list elements, even if they are deeply nested.
     * @see 10.2.1.1, 10.2.1.2
     * @param ctx
     * @return
     */
    @Override
    public Value visitEnhancedAssignment(@NotNull DataParser.EnhancedAssignmentContext ctx) {
        String id = ctx.ID().getText();

        ADataType newValue = visit(ctx.assignable()).getBackingObject();

        ADataType obj = evaluateId(id);

        for (int i = 0; i < ctx.objOrIndexRule().size(); i ++) {
            DataParser.ObjOrIndexRuleContext octx = ctx.objOrIndexRule(i);
            boolean isLast = i == ctx.objOrIndexRule().size() - 1;

            if (octx.getChild(0).getText().equals(".")) {
                String attr = octx.ID().getText();

                if (obj instanceof AObject) {
                    if (((AObject) obj).hasAttribute(attr)) {
                        try {
                            if (isLast) {
                                ((AObject) obj).setAttribute(attr, newValue);

                            } else {
                                obj = ((AObject) obj).getAttribute(attr);
                            }

                        } catch (InvalidAttributeException e) {
                            throw new RuntimeException(e);      // should never get here for call to hasAttribute() above
                        }

                    } else {
                        throw new RuntimeException("object '" + id + "' does not have attribute '" + attr + "'");
                    }

                } else {
                    throw new RuntimeException("cannot access attribute '" + attr + "' from non-object variable");
                }

            } else {                                                    // index into a list
                Value v = visit(octx.expr());

                if (v.isInteger()) {
                    int index = v.asInteger();

                    if (obj instanceof AList) {
                        AList list = (AList) obj;
                        if (index > 0 && index <= list.size()) {
                            if (isLast) {
                                list.set(index, newValue);

                            } else {
                                obj = list.get(index);
                            }

                        } else {
                            throw new RuntimeException("index out of bounds: cannot reference index " + index +
                                    " of list " + list);
                        }

                    } else {
                        throw new RuntimeException("cannot index into " + obj.getClass().getName());
                    }

                } else {
                    throw new RuntimeException("invalid index: " + v.getBackingObject());
                }
            }
        }

        return Value.VOID;
    }

    @Override
    public Value visitPrint(@NotNull DataParser.PrintContext ctx) {
        Value v = visit(ctx.expr());
        return arden.print(v);
    }

    @Override
    public Value visitConcat(@NotNull DataParser.ConcatContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.concat(left, right);
    }

    @Override
    public Value visitBuildString(@NotNull DataParser.BuildStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.string(v);
    }

    @Override
    public Value visitMatches(@NotNull DataParser.MatchesContext ctx) {
        Value vStr = visit(ctx.expr(0));
        Value vPattern = visit(ctx.expr(1));
        return arden.matches(vStr, vPattern);
    }

    @Override
    public Value visitLength(@NotNull DataParser.LengthContext ctx) {
        Value v = visit(ctx.expr());
        return arden.length(v);
    }

    @Override
    public Value visitUppercase(@NotNull DataParser.UppercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.uppercase(v);
    }

    @Override
    public Value visitLowercase(@NotNull DataParser.LowercaseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.lowercase(v);
    }

    /**
     * The <strong>trim</strong> operator removes leading and trailing white space from a string (see Section 7.1.10).
     * The optional <strong>left</strong> or <strong>right</strong> modifier can be applied to remove leading or
     * trailing white space respectively. Printable characters and embedded white space characters are not affected.
     * The trim of a non-string data type or empty list is {@code null}. Primary times are preserved.
     * @see 9.8.8
     * @param ctx
     * @return
     */
    @Override
    public Value visitTrim(@NotNull DataParser.TrimContext ctx) {
        Value v = visit(ctx.expr());
        TrimType type;
        if      (ctx.Left() != null)    type = TrimType.LEFT;
        else if (ctx.Right() != null)   type = TrimType.RIGHT;
        else                            type = TrimType.LEFT_AND_RIGHT;
        return arden.trim(v, type);
    }

    /**
     * The <strong>find ... string</strong> operator locates a substring within a target string, and returns a number
     * that represents the starting position of the substring. <strong>Find ... string</strong> is similar to
     * <strong>matches pattern</strong>, but returns a number (rather than a boolean), and does not support wildcards.
     * <strong>Find ... string</strong> is case-sensitive, and returns a zero if the target string does not contain the
     * exact substring. If either the substring or target is not a string data type, {@code null} is returned. Primary
     * times are not preserved.
     * <br>
     * The optional modifier <strong>starting at...</strong> can be appended to the <strong>find ... string</strong>
     * operator to control where the search for the substring begins. Omitting the modifier causes the search to begin
     * at the first character of the string. The value following <strong>starting at...</strong> must be an integer,
     * otherwise {@code null} is returned. If the value following <strong>starting at...</strong> is an integer beyond
     * the length of the target string (i.e. less than 1 or greater than length target), zero is returned.
     * @see 9.8.9
     * @param ctx
     * @return
     */
    @Override
    public Value visitFindInString(@NotNull DataParser.FindInStringContext ctx) {
        Value vNeedle = visit(ctx.expr(0));
        Value vHaystack = visit(ctx.expr(1));

        if (vNeedle.isString() && vHaystack.isString()) {
            int start = 1;
            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(2));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    start = vStartAt.asInteger();

                } else {
                    return NULL();
                }
            }

            return new Value(vHaystack.asString().findPositionOf(vNeedle.asString(), start));
        }

        return NULL();
    }

    /**
     * The <strong>substring ... characters [starting at ...] from ...</strong> operator returns a substring of
     * characters from a designated target string. This substring consists of the specified number of characters from
     * the source string beginning with the starting position (either the first character of the string or the specified
     * location within the string). For example <strong>substring 3 characters starting at 2 from "Example"</strong>
     * would return "xam" - a 3 character string beginning with the second character in the source string "Example".
     * <br>
     * The target string must be a string data type, the starting location within the string must be a positive
     * integer, and the number of characters to be returned must be an integer, or the operator returns {@code null}. If a
     * starting position is specified, its value must be an integer between 1 and the length of the string, otherwise
     * an empty string is returned. If the requested number of characters is greater than the length of the string,
     * the entire string is returned. If a starting point is specified, and the requested number of characters is
     * greater than the length of the string minus the starting point, the resulting string is the original string
     * to the right of and including the starting position. If the number of characters requested is positive the
     * characters are counted from left to right. If the number of characters requested is negative, the characters
     * are counted from right to left. The characters in a substring are always returned in the order that they
     * appear in the string. Default list handling is observed. Primary times are preserved.
     * @see 9.8.10
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubstring(@NotNull DataParser.SubstringContext ctx) {
        Value vNumChars = visit(ctx.expr(0));

        if (vNumChars.isNumber() && vNumChars.asNumber().isWholeNumber()) {
            Value vObj;
            int numChars = vNumChars.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vObj = visit(ctx.expr(2));

            } else {
                vObj = visit(ctx.expr(1));
            }

            if (vObj.isString()) {
                return new Value(vObj.asString().substring(startAt, numChars));

            } else if (vObj.isList()) {
                AList list = new AList();
                for (ADataType obj : vObj.asList()) {
                    if (obj instanceof AString) {
                        list.add(((AString) obj).substring(startAt, numChars));

                    } else {
                        ANull n = new ANull();
                        n.setPrimaryTime(obj.getPrimaryTime());                     // preserve primary time
                        list.add(n);
                    }
                }
                return new Value(list);

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitAsString(@NotNull DataParser.AsStringContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asString(v);
    }

    @Override
    public Value visitCount(@NotNull DataParser.CountContext ctx) {
        Value v = visit(ctx.expr());
        return arden.count(v);
    }

    @Override
    public Value visitExist(@NotNull DataParser.ExistContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exist(v);
    }

    @Override
    public Value visitAverage(@NotNull DataParser.AverageContext ctx) {
        Value v = visit(ctx.expr());
        return arden.average(v);
    }

    @Override
    public Value visitMedian(@NotNull DataParser.MedianContext ctx) {
        Value v = visit(ctx.expr());
        return arden.median(v);
    }

    @Override
    public Value visitSum(@NotNull DataParser.SumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sum(v);
    }

    @Override
    public Value visitStdDev(@NotNull DataParser.StdDevContext ctx) {
        Value v = visit(ctx.expr());
        return arden.stdDev(v);
    }

    @Override
    public Value visitVariance(@NotNull DataParser.VarianceContext ctx) {
        Value v = visit(ctx.expr());
        return arden.variance(v);
    }

    @Override
    public Value visitMinimum(@NotNull DataParser.MinimumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.minimum(v);
    }

    @Override
    public Value visitMaximum(@NotNull DataParser.MaximumContext ctx) {
        Value v = visit(ctx.expr());
        return arden.maximum(v);
    }

    @Override
    public Value visitFirst(@NotNull DataParser.FirstContext ctx) {
        Value v = visit(ctx.expr());
        return arden.first(v);
    }

    @Override
    public Value visitLast(@NotNull DataParser.LastContext ctx) {
        Value v = visit(ctx.expr());
        return arden.last(v);
    }

    @Override
    public Value visitAny(@NotNull DataParser.AnyContext ctx) {
        Value v = visit(ctx.expr());
        return arden.any(v);
    }

    @Override
    public Value visitAll(@NotNull DataParser.AllContext ctx) {
        Value v = visit(ctx.expr());
        return arden.all(v);
    }

    @Override
    public Value visitNo(@NotNull DataParser.NoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.no(v);
    }

    @Override
    public Value visitElement(@NotNull DataParser.ElementContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vIndex = visit(ctx.expr(1));
        return arden.element(vObj, vIndex);
    }

    @Override
    public Value visitEarliest(@NotNull DataParser.EarliestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.earliest(v);
    }

    @Override
    public Value visitLatest(@NotNull DataParser.LatestContext ctx) {
        Value v = visit(ctx.expr());
        return arden.latest(v);
    }

    @Override
    public Value visitExtractChars(@NotNull DataParser.ExtractCharsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.extractChars(v);
    }

    @Override
    public Value visitSeqto(@NotNull DataParser.SeqtoContext ctx) {
        Value vFrom = visit(ctx.expr(0));
        Value vTo = visit(ctx.expr(1));
        return arden.seqto(vFrom, vTo);
    }

    @Override
    public Value visitReverse(@NotNull DataParser.ReverseContext ctx) {
        Value v = visit(ctx.expr());
        return arden.reverse(v);
    }

    @Override
    public Value visitIndex(@NotNull DataParser.IndexContext ctx) {
        Value vList = visit(ctx.expr());
        String indexTypeStr = ctx.indexType().getText();
        return arden.index(vList, indexTypeStr);
    }

    @Override
    public Value visitIndexFrom(@NotNull DataParser.IndexFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        String indexTypeStr = ctx.indexType().getText();
        return arden.indexFrom(vNum, vList, indexTypeStr);
    }

    @Override
    public Value visitIndexOfFrom(@NotNull DataParser.IndexOfFromContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.indexOfFrom(vObj, vList);
    }

    @Override
    public Value visitAfter(@NotNull DataParser.AfterContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.after(vDur, vTime);
    }

    @Override
    public Value visitBefore(@NotNull DataParser.BeforeContext ctx) {
        Value vDur = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        return arden.before(vDur, vTime);
    }

    @Override
    public Value visitAgo(@NotNull DataParser.AgoContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ago(v);
    }

    @Override
    public Value visitTimeOfDayFunc(@NotNull DataParser.TimeOfDayFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeOfDayFunc(v);
    }

    @Override
    public Value visitTimeFunc(@NotNull DataParser.TimeFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.timeFunc(v);
    }

    @Override
    public Value visitAtTime(@NotNull DataParser.AtTimeContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vTimeOfDay = visit(ctx.expr(1));
        return arden.atTime(vTime, vTimeOfDay);
    }

    @Override
    public Value visitAsTime(@NotNull DataParser.AsTimeContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asTime(v);
    }

    @Override
    public Value visitDayOfWeekFunc(@NotNull DataParser.DayOfWeekFuncContext ctx) {
        Value v = visit(ctx.expr());
        return arden.dayOfWeekFunc(v);
    }

    @Override
    public Value visitExtract(@NotNull DataParser.ExtractContext ctx) {
        Value v = visit(ctx.expr());
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.extract(v, temporalUnitStr);
    }

    @Override
    public Value visitReplace(@NotNull DataParser.ReplaceContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vNumber = visit(ctx.expr(1));
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.replace(vTime, vNumber, temporalUnitStr);
    }

    @Override
    public Value visitUnaryPlus(@NotNull DataParser.UnaryPlusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryPlus(v);
    }

    @Override
    public Value visitUnaryMinus(@NotNull DataParser.UnaryMinusContext ctx) {
        Value v = visit(ctx.expr());
        return arden.unaryMinus(v);
    }

    @Override
    public Value visitAdd(@NotNull DataParser.AddContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.add(left, right);
    }

    @Override
    public Value visitSubtract(@NotNull DataParser.SubtractContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.subtract(left, right);
    }

    @Override
    public Value visitMultiply(@NotNull DataParser.MultiplyContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.multiply(left, right);
    }

    @Override
    public Value visitDivide(@NotNull DataParser.DivideContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.divide(left, right);
    }

    @Override
    public Value visitRaiseToPower(@NotNull DataParser.RaiseToPowerContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.raiseToPower(left, right);
    }

    @Override
    public Value visitArccos(@NotNull DataParser.ArccosContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arccos(v);
    }

    @Override
    public Value visitArcsin(@NotNull DataParser.ArcsinContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arcsin(v);
    }

    @Override
    public Value visitArctan(@NotNull DataParser.ArctanContext ctx) {
        Value v = visit(ctx.expr());
        return arden.arctan(v);
    }

    @Override
    public Value visitCosine(@NotNull DataParser.CosineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.cosine(v);
    }

    @Override
    public Value visitSine(@NotNull DataParser.SineContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sine(v);
    }

    @Override
    public Value visitTangent(@NotNull DataParser.TangentContext ctx) {
        Value v = visit(ctx.expr());
        return arden.tangent(v);
    }

    @Override
    public Value visitExp(@NotNull DataParser.ExpContext ctx) {
        Value v = visit(ctx.expr());
        return arden.exp(v);
    }

    @Override
    public Value visitLog(@NotNull DataParser.LogContext ctx) {
        Value v = visit(ctx.expr());
        return arden.log(v);
    }

    @Override
    public Value visitLog10(@NotNull DataParser.Log10Context ctx) {
        Value v = visit(ctx.expr());
        return arden.log10(v);
    }

    @Override
    public Value visitFloor(@NotNull DataParser.FloorContext ctx) {
        Value v = visit(ctx.expr());
        return arden.floor(v);
    }

    @Override
    public Value visitCeiling(@NotNull DataParser.CeilingContext ctx) {
        Value v = visit(ctx.expr());
        return arden.ceiling(v);
    }

    @Override
    public Value visitTruncate(@NotNull DataParser.TruncateContext ctx) {
        Value v = visit(ctx.expr());
        return arden.truncate(v);
    }

    @Override
    public Value visitRound(@NotNull DataParser.RoundContext ctx) {
        Value v = visit(ctx.expr());
        return arden.round(v);
    }

    @Override
    public Value visitAbs(@NotNull DataParser.AbsContext ctx) {
        Value v = visit(ctx.expr());
        return arden.abs(v);
    }

    @Override
    public Value visitSqrt(@NotNull DataParser.SqrtContext ctx) {
        Value v = visit(ctx.expr());
        return arden.sqrt(v);
    }

    @Override
    public Value visitAsNumber(@NotNull DataParser.AsNumberContext ctx) {
        Value v = visit(ctx.expr());
        return arden.asNumber(v);
    }

    @Override
    public Value visitAnd(@NotNull DataParser.AndContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.and(vLeft, vRight);
    }

    @Override
    public Value visitOr(@NotNull DataParser.OrContext ctx) {
        Value vLeft = visit(ctx.expr(0));
        Value vRight = visit(ctx.expr(1));
        return arden.or(vLeft, vRight);
    }

    @Override
    public Value visitNot(@NotNull DataParser.NotContext ctx) {
        Value v = visit(ctx.expr());
        return arden.not(v);
    }

    @Override
    public Value visitIsEqual(@NotNull DataParser.IsEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isEqual(left, right);
    }

    @Override
    public Value visitIsNotEqual(@NotNull DataParser.IsNotEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isNotEqual(left, right);
    }

    @Override
    public Value visitIsLessThan(@NotNull DataParser.IsLessThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThan(left, right);
    }

    @Override
    public Value visitIsLessThanOrEqual(@NotNull DataParser.IsLessThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isLessThanOrEqual(left, right);
    }

    @Override
    public Value visitIsGreaterThan(@NotNull DataParser.IsGreaterThanContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThan(left, right);
    }

    @Override
    public Value visitIsGreaterThanOrEqual(@NotNull DataParser.IsGreaterThanOrEqualContext ctx) {
        Value left = visit(ctx.expr(0));
        Value right = visit(ctx.expr(1));
        return arden.isGreaterThanOrEqual(left, right);
    }

    @Override
    public Value visitIsWithinTo(@NotNull DataParser.IsWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitIsWithinPreceding(@NotNull DataParser.IsWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinFollowing(@NotNull DataParser.IsWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinSurrounding(@NotNull DataParser.IsWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitIsWithinPast(@NotNull DataParser.IsWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitIsWithinSameDay(@NotNull DataParser.IsWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitIsBefore(@NotNull DataParser.IsBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitIsAfter(@NotNull DataParser.IsAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitIsIn(@NotNull DataParser.IsInContext ctx) {
        Value left = visit(ctx.expr(0));
        boolean flip = ctx.Not() != null;

        if (left.isNull()) {
            return flip ? FALSE() : TRUE();

        } else {
            Value right = visit(ctx.expr(1));
            return arden.isIn(left, right, flip);
        }
    }

    @Override
    public Value visitIsDataType(@NotNull DataParser.IsDataTypeContext ctx) {
        Value v = visit(ctx.expr());
        String dataTypeStr = ctx.dataType().getText();
        boolean flip = ctx.Not() != null;
        return arden.isDataType(v, dataTypeStr, flip);
    }

    @Override
    public Value visitIsObjectType(@NotNull DataParser.IsObjectTypeContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vType = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isObjectType(vObj, vType, flip);
    }

    @Override
    public Value visitOccurEqual(@NotNull DataParser.OccurEqualContext ctx) {
        Value vObj = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurEqual(vObj, vTime, flip);
    }

    @Override
    public Value visitOccurWithinTo(@NotNull DataParser.OccurWithinToContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vLow = visit(ctx.expr(1));
        Value vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public Value visitOccurWithinPreceding(@NotNull DataParser.OccurWithinPrecedingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinFollowing(@NotNull DataParser.OccurWithinFollowingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinSurrounding(@NotNull DataParser.OccurWithinSurroundingContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        Value vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public Value visitOccurWithinPast(@NotNull DataParser.OccurWithinPastContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPast(vTest, vDur, flip);
    }

    @Override
    public Value visitOccurWithinSameDay(@NotNull DataParser.OccurWithinSameDayContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurBefore(@NotNull DataParser.OccurBeforeContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurBefore(vTest, vTime, flip);
    }

    @Override
    public Value visitOccurAfter(@NotNull DataParser.OccurAfterContext ctx) {
        Value vTest = visit(ctx.expr(0));
        Value vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurAfter(vTest, vTime, flip);
    }

    @Override
    public Value visitMinimumFrom(@NotNull DataParser.MinimumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.minimumFrom(vNum, vList);
    }

    @Override
    public Value visitMaximumFrom(@NotNull DataParser.MaximumFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.maximumFrom(vNum, vList);
    }

    @Override
    public Value visitFirstFrom(@NotNull DataParser.FirstFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.firstFrom(vNum, vList);
    }

    @Override
    public Value visitLastFrom(@NotNull DataParser.LastFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.lastFrom(vNum, vList);
    }

    @Override
    public Value visitNearest(@NotNull DataParser.NearestContext ctx) {
        Value vTime = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return ctx.Index() != null ?
                arden.indexNearest(vTime, vList) :
                arden.nearest(vTime, vList);
    }

    @Override
    public Value visitAtMostOrLeast(@NotNull DataParser.AtMostOrLeastContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));

        if      (ctx.Least() != null)   return arden.atLeast(vNum, vList);
        else if (ctx.Most() != null)    return arden.atMost(vNum, vList);
        else                            throw new GrammarException("expected 'least' or 'most'");
    }

    /**
     * The <strong>sublist ... elements [starting at ...] from</strong> operator returns a sublist of elements from a
     * designated target list and is similar to the <strong>substring</strong> operator (see 9.8.10). This sublist
     * consists of the specified number of elements from the source list beginning with the starting position (either
     * the first elements of the list or the specified location within the list).
     * <br>
     * The target list must be a list data type, the starting location within the list must be a positive integer,
     * and the number of elements to be returned must be an integer, or the operator returns {@code null}. If target is not a
     * list data type, a list with one element is assumed. If a starting position is specified, its value must be an
     * integer between 1 and the length of the list, otherwise an empty list is returned. If the requested number of
     * elements is greater than the length of the list, the entire list is returned. If a starting point is specified,
     * and the requested number of elements is greater than the size of the list minus the starting point, the
     * resulting list is the original list to the right of and including the starting position. If the number of
     * elements requested is positive the elements are counted from left to right. If the number of elements requested
     * is negative, the elements are counted from right to left. The elements in a sublist are always returned in the
     * order that they appear in the original list. Default list handling is observed. Primary times are preserved.
     * @see 9.14.6
     * @param ctx
     * @return
     */
    @Override
    public Value visitSubList(@NotNull DataParser.SubListContext ctx) {
        Value vNumElements = visit(ctx.expr(0));

        if (vNumElements.isNumber() && vNumElements.asNumber().isWholeNumber()) {
            Value vList;
            int numElements = vNumElements.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                Value vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vList = visit(ctx.expr(2));

            } else {
                vList = visit(ctx.expr(1));
            }

            if (vList.isList()) {
                return new Value(vList.asList().sublist(startAt, numElements));

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public Value visitIncrease(@NotNull DataParser.IncreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.increase(v, pct);
    }

    @Override
    public Value visitDecrease(@NotNull DataParser.DecreaseContext ctx) {
        Value v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.decrease(v, pct);
    }

    @Override
    public Value visitInterval(@NotNull DataParser.IntervalContext ctx) {
        Value v = visit(ctx.expr());
        return arden.interval(v);
    }

    @Override
    public Value visitEarliestFrom(@NotNull DataParser.EarliestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.earliestFrom(vNum, vList);
    }

    @Override
    public Value visitLatestFrom(@NotNull DataParser.LatestFromContext ctx) {
        Value vNum = visit(ctx.expr(0));
        Value vList = visit(ctx.expr(1));
        return arden.latestFrom(vNum, vList);
    }

    protected Value NULL() {
        return new Value(ANull.NULL());
    }

    protected Value VOID() {
        return new Value(AVoid.VOID());
    }

    protected Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    protected Value FALSE() {
        return new Value(ABoolean.FALSE());
    }
}
