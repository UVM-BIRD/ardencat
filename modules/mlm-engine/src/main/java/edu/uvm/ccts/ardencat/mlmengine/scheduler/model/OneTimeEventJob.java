/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.scheduler.model;

import edu.uvm.ccts.ardencat.mlmengine.EventRegistry;
import org.quartz.JobDataMap;

/**
 * Created by mstorer on 1/14/14.
 */
public class OneTimeEventJob extends AbstractJob {
    public static final String EVENT_ID_KEY = "eventId";

    @Override
    protected void execute(JobDataMap dataMap) throws Exception {
        String eventId = dataMap.getString(EVENT_ID_KEY);

        EventRegistry.getInstance().getEvent(eventId).execute(null, null);
    }
}
