/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.model.ARecord;
import edu.uvm.ccts.ardencat.mlmengine.arden.data.model.ArdenSqlStatement;
import edu.uvm.ccts.ardencat.mlmengine.arden.exceptions.NullPrimaryTimeException;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.PrimaryTimeNotFoundException;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.common.exceptions.UnhandledClassException;
import edu.uvm.ccts.common.sql.model.Field;
import edu.uvm.ccts.common.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 11/6/13.
 */
public class ArdenRecordBuilder {
    private static final Log log = LogFactory.getLog(ArdenRecordBuilder.class);

    private ArdenDataSource dataSource = null;
    private ArdenSqlStatement ardenSqlStatement = null;

    public ArdenRecordBuilder(ArdenDataSource dataSource, ArdenSqlStatement ardenSqlStatement) {
        this.dataSource = dataSource;
        this.ardenSqlStatement = ardenSqlStatement;
    }

    public List<ARecord> execute() {
        try {
            List<ARecord> records = new ArrayList<ARecord>();

            ResultSet rs = null;
            PreparedStatement stmt = null;

            try {
                stmt = buildSelectStatement(ardenSqlStatement.getWhereClauseValues());
                rs = stmt.executeQuery();

                // do not include select fields (timestamps) that were added by the system.
                //  - need to know result set aliases of those fields

                List<String> fieldsToSkip = new ArrayList<String>();
                for (Field f : ardenSqlStatement.getAddedSelectFields()) {
                    fieldsToSkip.add(f.asResultSetLabel().toLowerCase());
                }

                while (rs.next()) {
                    try {
                        records.add(buildRecord(rs, fieldsToSkip));

                    } catch (NullPrimaryTimeException e) {
                        log.warn("caught " + e.getClass().getName() + " building record - " + e.getMessage() + " - skipping -");
                    }
                }

            } finally {
                try { if (rs   != null) rs.close();   } catch (Exception e) {}
                try { if (stmt != null) stmt.close(); } catch (Exception e) {}
            }

            return records;

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " locating base record - " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private PreparedStatement buildSelectStatement(List<Object> values) throws SQLException {
        PreparedStatement stmt = dataSource.getConnection().prepareStatement(ardenSqlStatement.getSql());

        if (values != null && values.size() > 0) {
            int i = 1;
            for (Object value : values) stmt.setObject(i++, value);
        }

        return stmt;
    }

    private ARecord buildRecord(ResultSet rs, List<String> fieldsToSkip) throws SQLException,
            PrimaryTimeNotFoundException, NullPrimaryTimeException, ParseException {

        ARecord r = new ARecord();

        ResultSetMetaData meta = rs.getMetaData();

        int anonCount = 0;

        for (int i = 1; i <= meta.getColumnCount(); i ++) {
            String fieldName = meta.getColumnLabel(i);

            // strip table alias from ResultSet column labels if they exist - only seems to
            // happen when subqueries are used.  weird, but also easy to handle
            int dotIndex = fieldName.indexOf('.');
            if (dotIndex > 0) {
                fieldName = fieldName.substring(dotIndex + 1);
            }

            if (fieldName.equals("") || Field.NULL.equalsIgnoreCase(fieldName)) {
                PrimaryTimeDataType obj = convertToArdenObject(rs.getObject(i), null);
                r.setField("ANON_" + anonCount++, obj);

            } else if ( ! fieldsToSkip.contains(fieldName.toLowerCase()) ) {
                String tsFieldName = ardenSqlStatement.getAssociatedPrimaryTimeFieldLabel(fieldName);
                Date d = DateUtil.castToDate(rs.getObject(tsFieldName));

                if (d == null) {
                    throw new NullPrimaryTimeException("primary time cannot be null for field=" + fieldName);
                }

                Time timestamp = new Time(d.getTime() / 1000);

                PrimaryTimeDataType obj = convertToArdenObject(rs.getObject(i), timestamp);

                r.setField(fieldName, obj);
            }
        }

        return r;
    }

    private PrimaryTimeDataType convertToArdenObject(Object obj, Time primaryTime) {
        PrimaryTimeDataType adt;

        if (obj == null)                    adt = new ANull();
        else if (obj instanceof Boolean)    adt = new ABoolean((Boolean) obj);
        else if (obj instanceof Number)     adt = new ANumber((Number) obj);
        else if (obj instanceof String)     adt = new AString((String) obj);
        else if (obj instanceof Date)       adt = new Time((Date) obj);
        else throw new UnhandledClassException("unhandled class : " + obj.getClass().getCanonicalName());

        adt.setPrimaryTime(primaryTime);

        return adt;
    }
}
