/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.mlm;

import edu.uvm.ccts.arden.model.Time;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.model.log.LogItem;
import edu.uvm.ccts.ardencat.mlmengine.model.log.Status;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.util.DateUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.StringWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 5/1/14.
 */
public class MLMLogUtil {
    private static final Log log = LogFactory.getLog(MLMLogUtil.class);

    public static List<LogItem> getLog(Date startDate, Date endDate) {
        try {
            DataSource ds = DataSourceRegistry.getInstance().getArdencatDataSource();
            if (ds == null) return null;

            StringBuilder sb = new StringBuilder();
            sb.append("select mlmId, executionTime, status, message from tblACMLMExecLog");

            if (startDate != null || endDate != null) {
                sb.append(" where");
                if (startDate != null)                      sb.append(" executionTime >= ?");
                if (startDate != null && endDate != null)   sb.append(" and");
                if (endDate != null)                        sb.append(" executionTime <= ?");
            }

            sb.append(" order by executionTime desc");

            PreparedStatement stmt = ds.getConnection().prepareStatement(sb.toString());

            if (startDate != null) {
                stmt.setTimestamp(1, new Timestamp(startDate.getTime()));
                if (endDate != null) stmt.setTimestamp(2, new Timestamp(endDate.getTime()));

            } else if (endDate != null) {
                stmt.setTimestamp(1, new Timestamp(endDate.getTime()));
            }

            ResultSet rs = stmt.executeQuery();

            List<LogItem> list = new ArrayList<LogItem>();
            while (rs.next()) {
                String mlmId = rs.getString(1);
                Date executionDate = DateUtil.castToDate(rs.getObject(2));
                Status status = Status.valueOf(rs.getString(3));

                String message = null;
                try {
                    Clob clob = rs.getClob(4);
                    if (clob != null) {
                        InputStream in = clob.getAsciiStream();
                        StringWriter writer = new StringWriter();
                        IOUtils.copy(in, writer, "UTF-8");
                        message = writer.toString();
                    }

                } catch (SQLException e) {
                    // cannot get CLOB - try getting as String instead
                    message = rs.getString(4);
                }

                list.add(new LogItem(mlmId, executionDate, status, message));
            }

            return list;

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " getting log with startDate={" +
                    startDate + "}, endDate={" + endDate + "} - " + e.getMessage(), e);

            return null;
        }
    }

    public static Integer createRunningExecutionLog(String mlmId, Time executionTime) {
        try {
            String query = "insert into tblACMLMExecLog (mlmId, executionTime, status) values(?, ?, ?)";

            DataSource ds = DataSourceRegistry.getInstance().getArdencatDataSource();
            PreparedStatement stmt = ds.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, mlmId);
            stmt.setTimestamp(2, new Timestamp(executionTime.getTimeInMillis()));
            stmt.setString(3, Status.RUNNING.toString());

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("log insert failed - no records affected");
            }

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);

            } else {
                throw new SQLException("no auto-generated keys found");
            }

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " attempting to create MLM execution log - " + e.getMessage(), e);
            return null;
        }
    }

    public static void updateExecutionLog(Integer logId, Status status, String message) {
        if (logId == null) return;

        try {
            String query = "update tblACMLMExecLog set status=?, message=? where id=?";

            DataSource ds = DataSourceRegistry.getInstance().getArdencatDataSource();
            PreparedStatement stmt = ds.getConnection().prepareStatement(query);

            stmt.setString(1, status.toString());

            if (message != null) {
                try {
                    Clob clob = ds.getConnection().createClob();
                    clob.setString(1, message);
                    stmt.setClob(2, clob);

                } catch (Throwable t) {
                    log.debug("caught " + t.getClass().getName() + " updating execution log w/ Clob - will try String instead");
                    stmt.setString(2, message);
                }

            } else {
                stmt.setNull(2, Types.CLOB);
            }

            stmt.setInt(3, logId);

            stmt.executeUpdate();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " updating MLM execution log with id=" + logId +
                    " - " + e.getMessage(), e);
        }
    }
}
