/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.util;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class related to ANTLR functionality
 */
public class AntlrUtil {
    public static String getText(RuleContext payload) {
        return getText(payload, " ");
    }

    /**
     * Utility function to reintroduce whitespace delimiters into the text from an ANTLR parse tree.  This is required
     * since whitespace is ignored by the ANTLR lexer, and {@link RuleContext#getText} method simply strings all tokens
     * together without any separating whitespace, making the resulting text impossible to re-parse.
     * @param payload
     * @param delimiter
     * @return
     */
    public static String getText(RuleContext payload, String delimiter) {
        List<String> parts = new ArrayList<String>();
        for (int i = 0; i < payload.getChildCount(); i ++) {
            buildParts(parts, payload.getChild(i));
        }
        return StringUtils.join(parts, delimiter);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private static void buildParts(List<String> parts, ParseTree tree) {
        if (tree instanceof TerminalNode) {
            parts.add(tree.getText());

        } else {
            for (int i = 0; i < tree.getChildCount(); i ++) {
                buildParts(parts, tree.getChild(i));
            }
        }
    }
}
