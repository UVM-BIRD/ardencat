/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.NonPrimaryTimeDataType;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 11/18/13.
 */
public class AMLM extends NonPrimaryTimeDataType<AMLM> {
    private String name;
    private String institution;
    private MLM mlm;

    public AMLM(String name, String institution, MLM mlm) {
        this.name = name;
        this.institution = institution;
        this.mlm = mlm;
    }

    public AMLM(AMLM amlm) {
        name = amlm.name;
        institution = amlm.institution;
        mlm = amlm.mlm;
    }

    public String getName() {
        return name;
    }

    public String getInstitution() {
        return institution;
    }

    public MLM getMlm() {
        return mlm;
    }

    @Override
    public String toString() {
        return "MLM{name=" + name + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        AMLM amlm = (AMLM) o;
        return new EqualsBuilder()
                .append(name, amlm.name)
                .append(institution, amlm.institution)
                .append(amlm, amlm.mlm)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(443, 79)
                .append(name)
                .append(institution)
                .append(mlm)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (name == null)               return ((AMLM) o).name == null;
        if (institution == null)        return ((AMLM) o).institution == null;
        if (mlm == null)                return ((AMLM) o).mlm == null;

        return name.equalsIgnoreCase(((AMLM) o).name) &&
                institution.equalsIgnoreCase(((AMLM) o).institution);
    }

    @Override
    public AMLM copy() {
        return new AMLM(this);
    }

    @Override
    public Object toJavaObject() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("institution", institution);
        map.put("mlm", mlm);
        return map;
    }
}
