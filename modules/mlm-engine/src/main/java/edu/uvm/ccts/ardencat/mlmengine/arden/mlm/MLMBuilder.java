/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.mlm;

import edu.uvm.ccts.arden.mlm.BailMLMLexer;
import edu.uvm.ccts.arden.mlm.BailMLMParser;
import edu.uvm.ccts.arden.mlm.antlr.MLMLexer;
import edu.uvm.ccts.arden.mlm.antlr.MLMParser;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMConfigData;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by mbstorer on 10/12/15.
 */
public abstract class MLMBuilder<M extends MLM> {
    public abstract M build(MLMConfigData mlmConfig) throws IOException;

    protected ParseTree buildParseTree(String filename) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(filename));
        MLMLexer lexer = new BailMLMLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MLMParser parser = new BailMLMParser(tokens);
        return parser.init();
    }
}
