/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.util;

import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.MLMEngine;
import edu.uvm.ccts.ardencat.mlmengine.MLMEngineConfigRegistry;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.ScheduleException;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMEngineConfig;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMConfigData;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.JarUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.SchedulerException;

import java.io.IOException;

/**
 * Created by mstorer on 10/30/14.
 */
@Deprecated
public class MLMEngineConfigUtil {
    private static final Log log = LogFactory.getLog(MLMEngineConfigUtil.class);

    private static String CONFIG_FILE;
    static {
        try {
            CONFIG_FILE = FileUtil.buildPath(JarUtil.getJarDir(MLMEngine.class), ".mlm-engine-config");
        } catch (Exception e) {
            CONFIG_FILE = ".mlm-engine-config";
        }
    }

    public static void restoreConfigIfExists(MLMBuilder builder) {
        MLMEngineConfig cfg = getActiveConfig();

        if (cfg != null) {
            try {
                doLoadConfig(cfg, builder);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " restoring previous configuration - " + e.getMessage(), e);

                try {
                    doUnloadConfig();

                } catch (ScheduleException se) {
                    log.error("caught " + e.getClass().getName() + " restoring previous configuration - " + e.getMessage(), e);
                }

                deleteActiveConfig();
            }
        }
    }

    public static void loadConfig(MLMEngineConfig cfg, MLMBuilder builder) throws ScheduleException, ConfigurationException {
        doUnloadConfig();

        if (cfg == null) {
            deleteActiveConfig();

        } else {
            try {
                doLoadConfig(cfg, builder);

                MLMEngineConfigRegistry.getInstance().registerConfig(cfg);
                setActiveConfig(cfg);

                log.info("*** done loading config ***");

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " loading configuration - " + e.getMessage(), e);

                doUnloadConfig();
                deleteActiveConfig();

                throw (e instanceof ConfigurationException) ?
                        (ConfigurationException) e :
                        new ConfigurationException(e.getMessage(), e);
            }
        }
    }

    public static void doUnloadConfig() throws ScheduleException {
        try {
            MLMScheduler.getInstance().unscheduleAllJobs();
            MLMRegistry.getInstance().unregisterAllMLMs();
            MLMEngineConfigRegistry.getInstance().clear();

            DataSourceRegistry.getInstance().reset();

        } catch (SchedulerException e) {
            throw new ScheduleException(e.getMessage());
        }
    }

    public static void doLoadConfig(MLMEngineConfig cfg, MLMBuilder builder) throws ConfigurationException,
            IOException, ScheduleException {

        MLMScheduler scheduler = MLMScheduler.getInstance();
        MLMRegistry mlmRegistry = MLMRegistry.getInstance();
        DataSourceRegistry dsRegistry = DataSourceRegistry.getInstance();

        log.info("*** loading MLM Engine config from '" + cfg.getFilename() + "' ***");

        cfg.getArdencatDataSource().loadDriver();
        dsRegistry.registerArdencatDataSource(cfg.getArdencatDataSource());

        for (ArdenDataSource ds : cfg.getMLMDataSources()) {
            ds.loadDriver();        // ensure the appropriate JDBC driver is loaded
            dsRegistry.registerMLMDataSource(ds);
        }

        for (MLMConfigData mlmConfig : cfg.getMLMConfigs()) {
            MLM mlm = builder.build(mlmConfig);
            mlmRegistry.register(mlm);
        }

        mlmRegistry.populateEvokeConditions();

        try {
            scheduler.scheduleTimeBasedTriggers();

        } catch (SchedulerException e) {
            throw new ScheduleException(e.getMessage());
        }

        log.info("*** done loading MLM Engine config ***");
    }


/////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private static MLMEngineConfig getActiveConfig() {
        if (FileUtil.exists(CONFIG_FILE)) {
            try {
                return (MLMEngineConfig) FileUtil.deserializeFromFile(CONFIG_FILE);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " reading file '" + CONFIG_FILE + "' - " + e.getMessage(), e);
            }
        }

        return null;
    }

    private static void deleteActiveConfig() {
        FileUtil.delete(CONFIG_FILE);
    }

    private static void setActiveConfig(MLMEngineConfig cfg) throws IOException {
        FileUtil.serializeToFile(CONFIG_FILE, cfg);
    }
}
