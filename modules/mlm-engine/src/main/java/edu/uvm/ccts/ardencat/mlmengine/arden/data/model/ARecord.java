/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.data.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.AList;

import java.util.*;

/**
 * This class represents a single database record with timestamp-aware fields
 */
public class ARecord {
    private Map<String, ADataType> fields = new LinkedHashMap<String, ADataType>();

    public AList asList() {
        List<ADataType> list = new ArrayList<ADataType>();
        for (ADataType adt : fields.values()) {
            list.add(adt);
        }
        return new AList(list);
    }

    public Collection<String> getFieldNames() {
        return fields.keySet();
    }

    public void setField(String field, ADataType value) {
        fields.put(field, value);
    }

    public ADataType getField(String field) {
        return fields.get(field);
    }

    public boolean hasField(String field) {
        return fields.containsKey(field);
    }
}
