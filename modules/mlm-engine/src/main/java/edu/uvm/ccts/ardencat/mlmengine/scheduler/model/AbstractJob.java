/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.scheduler.model;

import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.exceptions.MetUntilConditionException;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by mstorer on 8/27/14.
 */
public abstract class AbstractJob implements Job {

    protected abstract void execute(JobDataMap dataMap) throws Exception;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            execute(context.getMergedJobDataMap());

        } catch (MetUntilConditionException e) {
            JobExecutionException jee = new JobExecutionException(e);
            jee.setUnscheduleAllTriggers(true);
            throw jee;

        } catch (Exception e) {
            throw new JobExecutionException(e);

        } finally {
            DataSourceRegistry.getInstance().closeAll();
        }
    }
}
