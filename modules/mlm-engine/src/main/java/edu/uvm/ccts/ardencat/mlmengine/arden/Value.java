/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden;

import edu.uvm.ccts.ardencat.arden.AbstractValue;
import edu.uvm.ccts.arden.model.ABoolean;
import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.ANull;
import edu.uvm.ccts.arden.model.AVoid;
import edu.uvm.ccts.arden.util.ObjectUtil;
import edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model.*;
import edu.uvm.ccts.ardencat.mlmengine.arden.model.Event;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A wrapper-class that represents an abstract value as referenced by DataEvalVisitor.
 * @author Matt Storer
 */
public class Value extends AbstractValue<Value> {
    public static final Value VOID = new Value(AVoid.VOID());

    public Value(ADataType obj) {
        super(obj);
    }

    @Override
    public Value VOID() {
        return VOID;
    }

    @Override
    public Value NULL() {
        return new Value(ANull.NULL());
    }

    @Override
    public Value TRUE() {
        return new Value(ABoolean.TRUE());
    }

    @Override
    public Value FALSE() {
        return new Value(ABoolean.FALSE());
    }


//////////////////////////////////////////////////////////////////////////////////


    /**
     * Determines whether or not one Value's backing Arden-object is of the same type as another's.
     *
     * @param v a Value object
     * @return <code>true</code> if this Value's backing Arden-object's class is an instance of the same class
     * as the parameter <code>v</code>'s backing Arden-object's class; <code>false</code> otherwise.
     */
    public boolean isLike(Value v) {
        return ObjectUtil.isLike(obj, v.getBackingObject());
    }

    public boolean isEvent() {
        return obj instanceof Event;
    }

    public Event asEvent() {
        if (obj instanceof Event)   return (Event) obj;
        else                        return null;
    }

    public boolean isRelativeTime() {
        return obj instanceof RelativeTime;
    }

    public RelativeTime asRelativeTime() {
        if (obj instanceof RelativeTime)    return (RelativeTime) obj;
        else                                return null;
    }

    public boolean isTimeExprSimple() {
        return obj instanceof TimeExprSimple;
    }

    public TimeExprSimple asTimeExprSimple() {
        if (obj instanceof TimeExprSimple)  return (TimeExprSimple) obj;
        else                                return null;
    }

    public boolean isTimeExpr() {
        return obj instanceof TimeExpr;
    }

    public TimeExpr asTimeExpr() {
        if (obj instanceof TimeExpr)    return (TimeExpr) obj;
        else                            return null;
    }

    public boolean isDelEventTrigger() {
        return obj instanceof DelEventTrigger;
    }

    public DelEventTrigger asDelEventTrigger() {
        if (obj instanceof DelEventTrigger) return (DelEventTrigger) obj;
        else                                return null;
    }

    public boolean isConstTimeTrigger() {
        return obj instanceof ConstTimeTrigger;
    }

    public ConstTimeTrigger asConstTimeTrigger() {
        if (obj instanceof ConstTimeTrigger)    return (ConstTimeTrigger) obj;
        else                                    return null;
    }

    public boolean isPerEventTrigger() {
        return obj instanceof PerEventTrigger;
    }

    public PerEventTrigger asPerEventTrigger() {
        if (obj instanceof PerEventTrigger) return (PerEventTrigger) obj;
        else                                return null;
    }

    public boolean isConstPerTimeTrigger() {
        return obj instanceof ConstPerTimeTrigger;
    }

    public ConstPerTimeTrigger asConstPerTimeTrigger() {
        if (obj instanceof ConstPerTimeTrigger) return (ConstPerTimeTrigger) obj;
        else                                    return null;
    }

    @Override
    public boolean equals(Object o) {
        if      (o == null)                  return false;
        else if (o == this)                  return true;
        else if (o.getClass() != getClass()) return false;
        else {
            Value v = (Value) o;
            return new EqualsBuilder()
                    .append(obj, v)
                    .isEquals();
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(919, 503)
                .append(obj)
                .toHashCode();
    }
}
