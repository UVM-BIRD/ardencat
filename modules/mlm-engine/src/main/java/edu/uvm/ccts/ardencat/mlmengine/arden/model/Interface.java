/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ABoolean;
import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.NonPrimaryTimeDataType;
import edu.uvm.ccts.arden.model.Time;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public abstract class Interface extends NonPrimaryTimeDataType<Interface> {
    private static final Log log = LogFactory.getLog(Interface.class);

    protected Time executionTime = null;

    public ADataType execute(List<ADataType> arguments) throws Exception {
        executionTime = new Time(System.currentTimeMillis() / 1000);
        return doExecute(arguments);
    }

    public void executeAsynchronously(final List<ADataType> arguments) {
        executionTime = new Time(System.currentTimeMillis() / 1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    doExecute(arguments);

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " calling interface - " + e.getMessage(), e);

                } finally {
                    DataSourceRegistry.getInstance().closeAll();
                }
            }
        }).start();
    }

    protected abstract ADataType doExecute(List<ADataType> arguments) throws Exception;

    /**
     * The variable that represents the event can be treated like a {@code Boolean} in the logic or action slots.
     * The Boolean value of the variable is {@code false} until the MLM is called by the referred event.
     * @spec 11.2.3.1
     * @return
     */
    public ABoolean asBoolean() {
        return new ABoolean(hasExecuted());
    }


    public boolean hasExecuted() {
        return executionTime != null;
    }

    public Time getExecutionTime() {
        return executionTime;
    }
}
