/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.scheduler.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.ardencat.mlmengine.MLMRegistry;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.mlmengine.scheduler.MLMScheduler;
import org.quartz.JobDataMap;

import java.util.List;

/**
 * Created by mstorer on 1/8/14.
 */
public class OneTimeMlmJob extends AbstractJob {
    public static final String MLM_ID_KEY = "mlmId";
    public static final String ARGUMENTS_KEY = "arguments";

    @Override
    protected void execute(JobDataMap dataMap) throws Exception {
        String mlmId = dataMap.getString(MLM_ID_KEY);

        MLM mlm = MLMRegistry.getInstance().getMLM(mlmId);

        if (dataMap.containsKey(ARGUMENTS_KEY)) {
            List<ADataType> arguments = MLMScheduler.getInstance().retrieveArguments(dataMap.getString(ARGUMENTS_KEY));
            mlm.execute(System.out, arguments);

        } else {
            mlm.execute(System.out);
        }
    }
}
