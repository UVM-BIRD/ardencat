/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.common.db.DataSource;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.sql.PreparedStatement;

/**
 * Created by mstorer on 1/16/14.
 */
public class TableDestination extends Destination<TableDestination> {
    private String dataSourceName;
    private String table;
    private String col;


    public TableDestination(String dataSourceName, String table, String col) {
        this.dataSourceName = dataSourceName;
        this.table = table;
        this.col = col;
    }

    public TableDestination(TableDestination d) {
        this.dataSourceName = d.dataSourceName;
        this.table = d.table;
        this.col = d.col;
    }

    @Override
    public void write(ADataType obj) throws Exception {
        DataSource ds = DataSourceRegistry.getInstance().getDestinationDataSource(dataSourceName);

        String query = "insert into " + table + " (" + col + ") values(?)";

        PreparedStatement stmt = ds.getConnection().prepareStatement(query);

        stmt.setObject(1, obj.toJavaObject());

        stmt.executeUpdate();
    }

    @Override
    public TableDestination copy() {
        return new TableDestination(this);
    }

    @Override
    public String toString() {
        return "TableDestination{dataSourceName=" + dataSourceName + ", table=" + table + ", col=" + col + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        TableDestination d = (TableDestination) o;
        return new EqualsBuilder()
                .append(dataSourceName, d.dataSourceName)
                .append(table, d.table)
                .append(col, d.col)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(257, 881)
                .append(dataSourceName)
                .append(table)
                .append(col)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }

    @Override
    public Object toJavaObject() {
        return dataSourceName;
    }
}
