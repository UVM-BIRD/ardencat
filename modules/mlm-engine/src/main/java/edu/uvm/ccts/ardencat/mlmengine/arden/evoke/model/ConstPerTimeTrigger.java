/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.evoke.model;

import edu.uvm.ccts.arden.model.Duration;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 1/6/14.
 */
public class ConstPerTimeTrigger extends EvokeCondition<ConstPerTimeTrigger> {
    private Duration interval;
    private Duration lifespan;
    private ConstTimeTrigger start;
    private String untilCondition = null;

    public ConstPerTimeTrigger(Duration interval, Duration lifespan, ConstTimeTrigger start) {
        this.interval = interval;
        this.lifespan = lifespan;
        this.start = start;
    }

    public ConstPerTimeTrigger(Duration interval, Duration lifespan, ConstTimeTrigger start, String untilCondition) {
        this.interval = interval;
        this.lifespan = lifespan;
        this.start = start;
        this.untilCondition = untilCondition;
    }

    public ConstPerTimeTrigger(ConstPerTimeTrigger pet) {
        this.interval = Duration.copyOf(pet.interval);
        this.lifespan = Duration.copyOf(pet.lifespan);
        this.start = pet.start.copy();
        this.untilCondition = pet.untilCondition;
    }

    public Duration getInterval() {
        return interval;
    }

    public Duration getLifespan() {
        return lifespan;
    }

    public ConstTimeTrigger getStart() {
        return start;
    }

    public String getUntilCondition() {
        return untilCondition;
    }

    @Override
    public ConstPerTimeTrigger copy() {
        return new ConstPerTimeTrigger(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ConstPerTimeTrigger cptt = (ConstPerTimeTrigger) o;
        return new EqualsBuilder()
                .append(interval, cptt.interval)
                .append(lifespan, cptt.lifespan)
                .append(start, cptt.start)
                .append(untilCondition, cptt.untilCondition)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(617, 101)
                .append(interval)
                .append(lifespan)
                .append(start)
                .append(untilCondition)
                .toHashCode();
    }
}
