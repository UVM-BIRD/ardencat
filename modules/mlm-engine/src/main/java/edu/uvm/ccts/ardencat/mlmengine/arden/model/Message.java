/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.NonPrimaryTimeDataType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 11/21/13.
 */
public class Message extends NonPrimaryTimeDataType<Message> {
    private String text;

    public Message(String text) {
        this.text = text;
    }

    public Message(Message text) {
        this.text = text.text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Message{text=" + text + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        Message m = (Message) o;
        return new EqualsBuilder()
                .append(text, m.text)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(193, 331)
                .append(text)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (text == null)               return ((Message) o).text == null;

        return text.equalsIgnoreCase(((Message) o).text);
    }

    @Override
    public Message copy() {
        return new Message(this);
    }

    @Override
    public Object toJavaObject() {
        return text;
    }
}
