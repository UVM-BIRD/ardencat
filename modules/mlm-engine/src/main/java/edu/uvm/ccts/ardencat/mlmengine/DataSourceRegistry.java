/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine;

import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 1/16/14.
 */
public class DataSourceRegistry {
    private static DataSourceRegistry registry = null;

    public static DataSourceRegistry getInstance() {
        if (registry == null) registry = new DataSourceRegistry();
        return registry;
    }

    private DataSource ads = null;
    private Map<String, ArdenDataSource> mlmdsMap = new HashMap<String, ArdenDataSource>();
    private Map<String, DataSource> ddsMap = new HashMap<String, DataSource>();

    private DataSourceRegistry() {
    }

    public void registerArdencatDataSource(DataSource dataSource) {
        ads = dataSource;
    }

    public DataSource getArdencatDataSource() {
        return ads;
    }

    public void registerMLMDataSource(ArdenDataSource dataSource) throws ConfigurationException {
        if (dataSource != null) {
            String key = dataSource.getName().toLowerCase();
            if (mlmdsMap.containsKey(key)) {
                throw new ConfigurationException("attempted to register multiple MLM data sources with id '" +
                        dataSource.getName() + "'");
            }
            mlmdsMap.put(key, dataSource);
        }
    }

    public boolean isMLMDataSourceRegistered(String key) {
        return key != null && mlmdsMap.containsKey(key.toLowerCase());
    }

    public ArdenDataSource getMLMDataSource(String key) {
        return key != null ?
                mlmdsMap.get(key.toLowerCase()) :
                null;
    }

    public void registerDestinationDataSource(DataSource dataSource) throws ConfigurationException {
        if (dataSource != null) {
            String key = dataSource.getName().toLowerCase();
            if (ddsMap.containsKey(key)) {
                throw new ConfigurationException("attempted to register multiple destination data sources with id '" +
                        dataSource.getName() + "'");
            }
            ddsMap.put(key, dataSource);
        }
    }

    public boolean isDestinationDataSourceRegistered(String key) {  // key is in the form <protocol>.<host>.<user>.<db>
        return key != null && ddsMap.containsKey(key.toLowerCase());
    }

    public DataSource getDestinationDataSource(String key) {
        return key != null ?
                ddsMap.get(key.toLowerCase()) :
                null;
    }

    public void closeAll() {
        if (ads != null) ads.close();

        for (DataSource ds : mlmdsMap.values()) {
            ds.close();
        }

        for (DataSource ds : ddsMap.values()) {
            ds.close();
        }
    }

    public void reset() {
        closeAll();
        ads = null;
        mlmdsMap.clear();
        ddsMap.clear();
    }
}
