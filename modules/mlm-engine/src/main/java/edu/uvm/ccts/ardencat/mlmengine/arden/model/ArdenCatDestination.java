/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.arden.model;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.AObject;
import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.mlmengine.MLMBatchAuthority;
import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.model.MLM;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.UnhandledClassException;
import edu.uvm.ccts.common.util.Base64Util;

import java.sql.PreparedStatement;
import java.util.Map;

/**
 * Created by mstorer on 1/22/14.
 */
public class ArdenCatDestination extends Destination<ArdenCatDestination> {
    private MLM caller;
    private String source;
    private EligibilityStatus type;
    private String batch;

    public ArdenCatDestination(MLM caller, String source, EligibilityStatus type) {
        this.caller = caller;
        this.source = source;
        this.type = type;
        this.batch = null;          // start off null - will be set on first use during write
    }

    public ArdenCatDestination(ArdenCatDestination acd) {
        this.caller = acd.caller;
        this.source = acd.source;
        this.type = acd.type;
        this.batch = acd.batch;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void write(ADataType obj) throws Exception {
        if (obj instanceof AObject) {
            Map<String, Object> map = (Map<String, Object>) obj.toJavaObject();
            if (map.containsKey("id")) {
                String query = "insert into tblACEligibility (source, status, batch, data) values(?, ?, ?, ?)";

                DataSource dataSource = DataSourceRegistry.getInstance().getArdencatDataSource();
                PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

                stmt.setString(1, source);
                stmt.setString(2, type.getStringVal());
                stmt.setString(3, MLMBatchAuthority.getInstance().getBatch(caller));
                stmt.setString(4, Base64Util.serialize(map));

                stmt.executeUpdate();

            } else {
                throw new DataException("object written to ArdenCAT destination must contain an element with key='id'");
            }

        } else {
            throw new UnhandledClassException("cannot write " + obj.getClass().getName() + " to ArdenCAT destination");
        }
    }

    @Override
    public boolean hasValue(ADataType o) {
        return false;
    }

    @Override
    public ArdenCatDestination copy() {
        return new ArdenCatDestination(this);
    }

    @Override
    public String toString() {
        return "ArdenCatDestination{type=" + type + ", source=" + source + "}";
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
