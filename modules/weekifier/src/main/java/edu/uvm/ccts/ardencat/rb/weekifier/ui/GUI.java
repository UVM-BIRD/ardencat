/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Wed Oct 01 12:56:46 EDT 2014
 */

package edu.uvm.ccts.ardencat.rb.weekifier.ui;

import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.util.Base64Util;
import edu.uvm.ccts.common.util.DateUtil;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import electric.xml.Document;
import electric.xml.Element;
import electric.xml.XPath;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @author Matthew Storer
 */
public class GUI extends JFrame {
    private static final Log log = LogFactory.getLog(GUI.class);

    private DataSource dataSource = null;

    public GUI() {
        super();
        initComponents();
    }

    private void btnSelectFileActionPerformed() {
        File file = FileUtil.selectFileToOpen(this,
                new FileNameExtensionFilter("XML Files", "xml"),
                System.getProperty("user.dir"));

        if (file != null) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            try {
                txtFile.setText(file.getCanonicalPath());

                dataSource = buildArdenCatDataSource(file);

                populateSourceList();
                cboSource.setEnabled(true);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
            }

            this.setCursor(Cursor.getDefaultCursor());
        }
    }

    private DataSource buildArdenCatDataSource(File file) throws electric.xml.ParseException, ConfigurationException {
        Document doc = new Document(file);

        Element xmlSystemDataSource;
        if (doc.hasElement("recordBuilder")) {
            xmlSystemDataSource = doc.getElement(new XPath("recordBuilder/ardencatDataSource"));

        } else if (doc.hasElement("mlmEngine")) {
            xmlSystemDataSource = doc.getElement(new XPath("mlmEngine/ardencatDataSource"));

        } else if (doc.hasElement("ardencat")) {
            xmlSystemDataSource = doc.getElement(new XPath("ardencat/ardencatDataSource"));

        } else {
            throw new ConfigurationException("invalid configuration file");
        }

        return DataSource.buildFromXml("ardencat", xmlSystemDataSource, this);
    }

    @SuppressWarnings("unchecked")
    private void populateSourceList() throws SQLException {
        cboSource.removeAllItems();

        String query = "select distinct source from tblACEligibility order by source";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            cboSource.addItem(rs.getString(1));
        }
    }

    private void cboSourceItemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            try {
                populateBatchList();
                cboBatch.setEnabled(true);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void populateBatchList() throws SQLException {
        cboBatch.removeAllItems();

        String source = cboSource.getSelectedItem().toString();

        String query = "select distinct batch from tblACEligibility where source = ? order by batch";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, source);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            cboBatch.addItem(rs.getString(1));
        }
    }

    private void cboBatchItemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            try {
                populateFieldList();
                cboField.setEnabled(true);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void populateFieldList() throws SQLException, IOException, ClassNotFoundException {
        cboField.removeAllItems();

        String source = cboSource.getSelectedItem().toString();
        String batch = cboBatch.getSelectedItem().toString();

        String query = "select data from tblACEligibility where source = ? and batch = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, source);
        stmt.setString(2, batch);

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            String dataStr = rs.getString(1);
            Map<String, Object> m = (Map<String, Object>) Base64Util.deserialize(dataStr);

            for (String field : m.keySet()) {
                cboField.addItem(field);
            }
        }
    }

    private void cboFieldItemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            try {
                btnWeekify.setEnabled(true);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
            }
        }
    }

    private void btnWeekifyActionPerformed() {
        String source = (String) cboSource.getSelectedItem();
        String batch = (String) cboBatch.getSelectedItem();
        String field = (String) cboField.getSelectedItem();

        try {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            weekify(source, batch, field);
            populateBatchList();
            cboField.setSelectedItem(field);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " weekifying {source=" + source + ", batch=" + batch +
                    "} with field=" + field + " - " + e.getMessage(), e);

            DialogUtil.showException(this, "Error", e);

        } finally {
            this.setCursor(Cursor.getDefaultCursor());
        }
    }

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @SuppressWarnings("unchecked")
    private void weekify(String source, String batch, String field) throws SQLException, IOException, ClassNotFoundException, ParseException {
        log.info("weekifying {source=" + source + ", batch=" + batch + ", field=" + field + "}");

        String query = "select id, data from tblACEligibility where source = ? and batch = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);
        stmt.setString(1, source);
        stmt.setString(2, batch);

        ResultSet rs = stmt.executeQuery();

        String mostRecentBatch = null;

        while (rs.next()) {
            int id = rs.getInt(1);

            String dataStr = rs.getString(2);
            Map<String, Object> m = (Map<String, Object>) Base64Util.deserialize(dataStr);

            Object obj = m.get(field);
            Date d = nextSunday(DateUtil.castToDate(obj));
            String newBatch = SDF.format(d) + ".000";

            if (mostRecentBatch == null || newBatch.compareTo(mostRecentBatch) > 0) {
                mostRecentBatch = newBatch;
            }

            updateBatch(id, newBatch);
        }

        updateBatchData(mostRecentBatch, source);

        log.info("done!");

        DialogUtil.showInformationDialog(this, "Success", "Weekification completed!");
    }

    private void updateBatchData(String batch, String source) throws SQLException {
        String query = "update tblACBatchData set mostRecentBatch = ? where source = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        stmt.setString(1, batch);
        stmt.setString(2, source);

        stmt.executeUpdate();
    }

    private void updateBatch(int id, String newBatch) throws ParseException, SQLException {
        String query = "update tblACEligibility set batch = ? where id = ?";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        stmt.setString(1, newBatch);
        stmt.setInt(2, id);

        stmt.executeUpdate();
    }

    private Date nextSunday(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            cal.add(Calendar.DATE, 1);
        }

        return cal.getTime();
    }

    private void closeButtonActionPerformed() {
        System.exit(0);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        txtFile = new JTextField();
        btnSelectFile = new JButton();
        label2 = new JLabel();
        cboSource = new JComboBox();
        label3 = new JLabel();
        label4 = new JLabel();
        cboBatch = new JComboBox();
        label5 = new JLabel();
        label6 = new JLabel();
        cboField = new JComboBox();
        btnWeekify = new JButton();
        buttonBar = new JPanel();
        closeButton = new JButton();

        //======== this ========
        setTitle("ArdenCAT Weekifier Utility");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("1. Specify ArdenCAT Record Builder, MLM Engine, or Service configuration file:");

                //---- txtFile ----
                txtFile.setEditable(false);

                //---- btnSelectFile ----
                btnSelectFile.setText("Select File...");
                btnSelectFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnSelectFileActionPerformed();
                    }
                });

                //---- label2 ----
                label2.setText("2. Select a source and batch to weekify:");

                //---- cboSource ----
                cboSource.setEnabled(false);
                cboSource.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        cboSourceItemStateChanged(e);
                    }
                });

                //---- label3 ----
                label3.setText("Source:");

                //---- label4 ----
                label4.setText("Batch:");

                //---- cboBatch ----
                cboBatch.setEnabled(false);
                cboBatch.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        cboBatchItemStateChanged(e);
                    }
                });

                //---- label5 ----
                label5.setText("3. Select a date field to determine the new batch identification:");

                //---- label6 ----
                label6.setText("Field:");

                //---- cboField ----
                cboField.setEnabled(false);
                cboField.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        cboFieldItemStateChanged(e);
                    }
                });

                //---- btnWeekify ----
                btnWeekify.setText("Weekify!");
                btnWeekify.setEnabled(false);
                btnWeekify.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnWeekifyActionPerformed();
                    }
                });

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addGroup(contentPanelLayout.createSequentialGroup()
                                            .addGap(24, 24, 24)
                                            .addComponent(txtFile, GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnSelectFile))
                                        .addGroup(contentPanelLayout.createSequentialGroup()
                                            .addGroup(contentPanelLayout.createParallelGroup()
                                                .addComponent(label1)
                                                .addGroup(contentPanelLayout.createSequentialGroup()
                                                    .addGap(24, 24, 24)
                                                    .addGroup(contentPanelLayout.createParallelGroup()
                                                        .addComponent(label3)
                                                        .addComponent(label4))
                                                    .addGap(18, 18, 18)
                                                    .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(cboSource, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                                        .addComponent(cboBatch, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                                        .addComponent(cboField, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))))
                                            .addGap(0, 0, Short.MAX_VALUE)))
                                    .addGap(9, 9, 9))
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addGroup(contentPanelLayout.createSequentialGroup()
                                            .addGap(24, 24, 24)
                                            .addComponent(label6))
                                        .addComponent(label2)
                                        .addComponent(label5))
                                    .addGap(0, 0, Short.MAX_VALUE))))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnWeekify)
                            .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(btnSelectFile)
                                .addComponent(txtFile, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label2)
                            .addGap(15, 15, 15)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label3)
                                .addComponent(cboSource, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label4)
                                .addComponent(cboBatch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label5)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label6)
                                .addComponent(cboField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                            .addComponent(btnWeekify)
                            .addContainerGap())
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        closeButtonActionPerformed();
                    }
                });
                buttonBar.add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JTextField txtFile;
    private JButton btnSelectFile;
    private JLabel label2;
    private JComboBox cboSource;
    private JLabel label3;
    private JLabel label4;
    private JComboBox cboBatch;
    private JLabel label5;
    private JLabel label6;
    private JComboBox cboField;
    private JButton btnWeekify;
    private JPanel buttonBar;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
