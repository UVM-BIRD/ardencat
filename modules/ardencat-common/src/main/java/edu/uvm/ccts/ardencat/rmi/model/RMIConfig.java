/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rmi.model;

import edu.uvm.ccts.common.exceptions.ConfigurationException;

/**
 * Created by mbstorer on 10/27/15.
 */
public class RMIConfig {
    // TCP / UDP range 49152-65535 cannot be registered with IANA, and is reserved for private, customized,
    // or temporary purposes, and for allocation of ephemeral ports.
    // see https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers#Dynamic.2C_private_or_ephemeral_ports
    public static final int DEFAULT_REGISTRY_PORT = 51110;
    public static final int DEFAULT_COMM_PORT = 51111;

    private static final int MIN_PORT = 1;
    private static final int MAX_PORT = 65535;

    private int registryPort;
    private int commPort;

    public RMIConfig(int registryPort, int commPort) throws ConfigurationException {
        this.registryPort = registryPort;
        this.commPort = commPort;

        validate();
    }

    public int getRegistryPort() {
        return registryPort;
    }

    public int getCommPort() {
        return commPort;
    }


////////////////////////////////////////////////////////////////////////
// private methods
//

    private void validate() throws ConfigurationException {
        if (registryPort < MIN_PORT || registryPort > MAX_PORT) throw new ConfigurationException("registry port " +
                registryPort + " out of range (" + MIN_PORT + "-" + MAX_PORT + ")");

        if (commPort < MIN_PORT || commPort > MAX_PORT) throw new ConfigurationException("comm port " +
                commPort + " out of range (" + MIN_PORT + "-" + MAX_PORT + ")");

        if (registryPort == commPort) throw new ConfigurationException("registry port and comm port cannot have the same value");
    }
}
