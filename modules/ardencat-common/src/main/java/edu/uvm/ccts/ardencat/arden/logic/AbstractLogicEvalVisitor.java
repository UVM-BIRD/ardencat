/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenLib.
 *
 * ArdenLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenLib.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.arden.logic;

import edu.uvm.ccts.ardencat.arden.AbstractArden;
import edu.uvm.ccts.ardencat.arden.AbstractValue;
import edu.uvm.ccts.ardencat.arden.Scope;
import edu.uvm.ccts.arden.logic.antlr.LogicBaseVisitor;
import edu.uvm.ccts.arden.logic.antlr.LogicParser;
import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.DurationUtil;
import edu.uvm.ccts.arden.util.NumberUtil;
import edu.uvm.ccts.common.exceptions.InvalidAttributeException;
import edu.uvm.ccts.common.exceptions.antlr.GrammarException;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This visitor operates on the Arden Logic slot
 * @author Matt Storer
 */
public abstract class AbstractLogicEvalVisitor<T extends AbstractValue<T>, A extends AbstractArden<T>> extends LogicBaseVisitor<T> {
    private static final Log log = LogFactory.getLog(AbstractLogicEvalVisitor.class);

    private T concludeValue = null;
    private boolean concluded = false;
    private int loopDepth = 0;
    private boolean breakLoop = false;
    private boolean continueLoop = false;

    protected A arden;

    protected abstract T createValue(ADataType obj);
    protected abstract T NULL();
    protected abstract T VOID();
    protected abstract T TRUE();
    protected abstract T FALSE();

    public AbstractLogicEvalVisitor(Variables variables, A arden) {
        this.arden = arden;
        
        if (variables != null) {
            for (String id : variables.getVariableNames()) {
                assign(id, variables.getVariable(id));
            }
        }
    }

    public void setPrintStream(PrintStream printStream) {
        arden.setPrintStream(printStream);
    }

    public Variables getVariables() {
        Variables variables = new Variables();
        for (Map.Entry<String, ADataType> entry : arden.getVariablesAndObjectTypes().entrySet()) {
            variables.putVariable(entry.getKey(), entry.getValue());
        }
        return variables;
    }

    public ADataType getConcludeValue() {
        return concludeValue == null ?
                new ANull() :
                concludeValue.getBackingObject();
    }

    /**
     * Determines whether or not to continue parse-tree traversal.  Will return {@code true} unless a {@code conclude}
     * statement, or the end of the file has been reached.  See {@link #visitConclude} for details on {@code conclude}.
     * @see {@link #visitConclude}
     * @param node
     * @param currentResult
     * @return
     */
    @Override
    protected boolean shouldVisitNextChild(@NotNull RuleNode node, T currentResult) {
        return ! breakLoop &&
                ! continueLoop &&
                ! concluded &&
                super.shouldVisitNextChild(node, currentResult);
    }

    /**
     * The <strong>conclude</strong> statement ends execution in the logic slot. If the expression (&lt;expr&gt;) in the
     * conclude statement is a single true then the action slot is executed immediately. Otherwise the whole MLM
     * terminates immediately. No further execution in the logic slot occurs regardless of the expression. There may be
     * more than one conclude statement in the logic slot, but only one will be executed in a single run of the MLM.
     * <br>
     * The cautions for the if-then statement about null and list (in Section 10.2.1.2) also hold for the conclude
     * statement.
     * <br>
     * If no conclude statement is executed, then the logic slot terminates after it executes its last statement,
     * and the action slot is not executed. In effect, the default is <strong>conclude false</strong>.
     * @see 10.2.4
     * @param ctx
     * @return
     */
    @Override
    public T visitConclude(@NotNull LogicParser.ConcludeContext ctx) {
        concludeValue = visit(ctx.expr());
        concluded = true;
        return VOID();
    }


    private void assign(String id, ADataType adt) {
        arden.assign(id, adt);
    }

    private ADataType evaluateId(String id) {
        return arden.evaluateId(id);
    }


    /**
     * A block represents a part of the codebase that has its own scope.
     * @see {@link Scope}
     * @param ctx
     * @return
     */
    @Override
    public T visitBlock(@NotNull LogicParser.BlockContext ctx) {
        arden.getScope().shift();
        try {
            return visitChildren(ctx);

        } finally {
            arden.getScope().pop();
        }
    }

    /**
     * The <strong>if-then</strong> statement permits conditional execution based upon the value of an expression. It
     * tests whether the expression (&lt;expr&gt;) is equal to a single Boolean {@code true}. If it is, then a block of
     * statements (&lt;block&gt;) is executed. (A block of statements is simply a collection of valid statements possibly
     * including other if-then statements; thus the if-then statement is a nested structure.) If the expression is a
     * list, or if it is any single item other than {@code true}, then the block of statements is <strong>not</strong>
     * executed. The flow of control then continues with subsequent statements.
     * <br>
     * It is important to emphasize that non-{@code true} is different from {@code false}. That is, the
     * <strong>else</strong> portion of the if-then-else statement is executed whether the expression is {@code false},
     * or {@code null}, or anything other than {@code true}.
     * @see 10.2.2
     * @param ctx
     * @return
     */
    @Override
    public T visitIfStatement(@NotNull LogicParser.IfStatementContext ctx) {
        int exprSize = ctx.expr().size();
        int blockSize = ctx.block().size();
        boolean found = false;

        for (int i = 0; i < exprSize; i ++) {
            T v = visit(ctx.expr(i));
            if (v.asBoolean().hasValue(ABoolean.TRUE())) {
                found = true;
                visit(ctx.block(i));
                break;
            }
        }

        if ( ! found && blockSize == exprSize + 1 ) {       // has else clause
            visit(ctx.block(blockSize - 1));
        }

        return VOID();
    }

    /**
     * The <strong>switch-case</strong> statement permits conditional execution based on the value of an expression. It
     * tests whether an expression (&lt;expr1&gt;, &lt;expr2&gt;, &lt;expr3&gt; ...) is equal to the value of the provided variable
     * (&lt;var&gt;). If they are equal, the corresponding block of statements (&lt;block1&gt;, &lt;block2&gt;, &lt;block3&gt; ...) is executed.
     * A block of statements is simply a collection of valid statements, possibly including other switch- case
     * statements; thus the switch-case statement is a nested structure. If the expression does not match the value of
     * the provided variable, then the corresponding block of statements is not executed. The flow of control then
     * continues with subsequent statements.
     * @see 10.2.3
     * @param ctx
     * @return
     */
    @Override
    public T visitSwitchStatement(@NotNull LogicParser.SwitchStatementContext ctx) {
        ADataType obj = evaluateId(ctx.ID().getText());

        int exprSize = ctx.expr().size();
        int blockSize = ctx.block().size();
        boolean found = false;

        for (int i = 0; i < exprSize; i ++) {
            T v = visit(ctx.expr(i));
            if (obj.equals(v.getBackingObject())) {
                found = true;
                visit(ctx.block(i));
                break;
            }
        }

        if ( ! found && blockSize == exprSize + 1 ) {       // has default clause
            visit(ctx.block(blockSize - 1));
        }

        return VOID();
    }

    /**
     * The <strong>while loop</strong> tests whether an expression (&lt;expr&gt;) is equal to a single Boolean
     * {@code true} (similar to the conditional execution introduced in the if ... then syntax - see Section 10.2.1.2).
     * If it is, the block of statements (&lt;block&gt;) is executed repeatedly until &lt;expr&gt; is not {@code true}.
     * If &lt;expr&gt; is not {@code true}, the block is not executed.
     * @see 10.2.6
     * @param ctx
     * @return
     */
    @Override
    public T visitWhileLoop(@NotNull LogicParser.WhileLoopContext ctx) {
        T exprVal = visit(ctx.expr());

        try {
            enterLoop();
            while (exprVal.asBoolean().hasValue(ABoolean.TRUE())) {
                beginLoopIteration();
                visit(ctx.block());
                exprVal = visit(ctx.expr());
            }

        } finally {
            exitLoop();
        }

        return VOID();
    }

    /**
     * Another form of looping is provided by the <strong>for loop</strong>.  The &lt;expr&gt; will usually be a list
     * generator. If &lt;expr&gt; is empty or {@code null}, the block is not executed. Otherwise the block is executed
     * with the &lt;identifier&gt; taking on consecutive elements in &lt;expr&gt;. The &lt;identifier&gt; cannot be
     * assigned to inside the &lt;block&gt; (the compiler must produce a compilation error if this is attempted). After
     * the <strong>enddo</strong>, the &lt;identifier&gt; becomes undefined and its value should not be used. A compiler
     * may flag this as an error.
     * @see 10.2.7
     * @param ctx
     * @return
     */
    @Override
    public T visitForLoop(@NotNull LogicParser.ForLoopContext ctx) {
        String id = ctx.ID().getText();
        T v = visit(ctx.expr());
        AList list = v.asList();

        boolean retainValue = arden.getScope().isDefined(id);

        try {
            enterLoop();
            for (ADataType item : list) {
                beginLoopIteration();
                try {
                    arden.getScope().put(id, item);        // todo : do not permit re-assignment of id within for-loop scope
                    arden.getScope().freeze(id);

                    visit(ctx.block());

                } finally {
                    arden.getScope().thaw(id);
                }
            }

        } finally {
            exitLoop();
        }

        if ( ! retainValue ) {
            arden.getScope().remove(id);
        }

        return VOID();
    }

    /**
     * The block of statements (&lt;block&gt;) of a while loop may contain a <strong>breakloop</strong> statement. If
     * the execution reaches such a breakloop statement, the direct superior loop will be aborted immediately. If the
     * breakloop statement occurs within a nested loop, it will always apply to the innermost loop only. Breakloop
     * statements are only allowed inside of loops.
     * @see 10.2.6.1
     * @param ctx
     * @return
     */
    @Override
    public T visitBreakLoop(@NotNull LogicParser.BreakLoopContext ctx) {
        if (inLoop()) {
            breakLoop = true;
            return VOID();

        } else {
            throw new RuntimeException("breakloop only permitted within a loop");
        }
    }

    @Override
    public T visitContinueLoop(@NotNull LogicParser.ContinueLoopContext ctx) {
        if (inLoop()) {
            continueLoop = true;
            return VOID();

        } else {
            throw new RuntimeException("continue only permitted within a loop");
        }
    }

    private void enterLoop() {
        loopDepth ++;
    }

    private void exitLoop() {
        if (breakLoop)    breakLoop = false;
        if (continueLoop) continueLoop = false;
        loopDepth --;
    }

    private boolean inLoop() {
        return loopDepth > 0;
    }

    private void beginLoopIteration() {
        if (continueLoop) continueLoop = false;
    }

    @Override
    public T visitParens(@NotNull LogicParser.ParensContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public T visitId(@NotNull LogicParser.IdContext ctx) {
        String id = ctx.ID().getText();
        return arden.id(id);
    }

    @Override
    public T visitNullVal(@NotNull LogicParser.NullValContext ctx) {
        return arden.nullVal();
    }

    @Override
    public T visitStringVal(@NotNull LogicParser.StringValContext ctx) {
        String str = ctx.StringVal().getText();
        return arden.stringVal(str);
    }

    @Override
    public T visitNumberVal(@NotNull LogicParser.NumberValContext ctx) {
        String numStr = ctx.NumberVal().getText();
        return arden.numberVal(numStr);
    }

    @Override
    public T visitBooleanVal(@NotNull LogicParser.BooleanValContext ctx) {
        String boolStr = ctx.BooleanVal().getText();
        return arden.booleanVal(boolStr);
    }

    @Override
    public T visitBinaryList(@NotNull LogicParser.BinaryListContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.binaryList(left, right);
    }

    @Override
    public T visitUnaryList(@NotNull LogicParser.UnaryListContext ctx) {
        T v = visit(ctx.expr());
        return arden.unaryList(v);
    }

    @Override
    public T visitEmptyList(@NotNull LogicParser.EmptyListContext ctx) {
        return arden.emptyList();
    }

    @Override
    public T visitNow(@NotNull LogicParser.NowContext ctx) {
        return arden.now();
    }

    @Override
    public T visitCurrentTime(@NotNull LogicParser.CurrentTimeContext ctx) {
        return arden.currentTime();
    }

    @Override
    public T visitTimeVal(@NotNull LogicParser.TimeValContext ctx) {
        return arden.timeVal(ctx.getText());
    }

    @Override
    public T visitTimeOfDayVal(@NotNull LogicParser.TimeOfDayValContext ctx) {
        return arden.timeOfDayVal(ctx.getText());
    }

    /**
     * The duration data type signifies an interval of time that is not anchored to any particular point in absolute
     * time. There are no duration constants. Instead one builds durations using the duration operators (see Section
     * 9.10.7). For example, <strong>1 day</strong>, <strong>45 seconds</strong>, and <strong>3.2 months</strong> are
     * durations.
     * @see {@link Duration}, {@link DurationUnit}, {@link SecondsDuration}, {@link MonthsDuration}, 8.5, 9.10.7, 9.11
     * @param ctx
     * @return
     */
    @Override
    public T visitDuration(@NotNull LogicParser.DurationContext ctx) {
        return createValue(buildDuration(ctx.durationExpr()));
    }

    private Duration buildDuration(LogicParser.DurationExprContext durExprCtx) {
        // first, split duration ("3 days 2 hours") into component durations ("3 days", "2 hours").

        List<Duration> componentDurations = new ArrayList<Duration>();
        for (int i = 0; i < durExprCtx.NumberVal().size(); i ++) {
            ANumber n = NumberUtil.parseNumber(durExprCtx.NumberVal(i).getText());
            String s = durExprCtx.durationUnit(i).getText();

            try {
                componentDurations.add(DurationUtil.buildDuration(n, s));

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // next, build a Duration object that represents the sum of each component duration, generated above

        Duration duration = null;
        for (Duration d : componentDurations) {
            if (duration == null) {
                duration = d;

            } else {
                duration = duration.add(d);     // note: might change form from MonthsDuration to SecondsDuration
            }
        }

        return duration;
    }

    @Override
    public T visitDayOfWeek(@NotNull LogicParser.DayOfWeekContext ctx) {
        String dayOfWeekStr = ctx.DayOfWeek().getText();
        return arden.dayOfWeek(dayOfWeekStr);
    }

    /**
     * The <strong>new</strong> statement causes a new object to be created, and assigns it to the named variable.
     * <br>
     * In the simple case (without the <strong>with</strong> clause) all attributes of the object are initialized to
     * {@code null}. In the full statement, a set of 1 or more comma-separated expressions should follow the
     * <strong>with</strong> reserved word. Each expression is evaluated and assigned as a value of an attribute of
     * the object. They are assigned in the order the attributes were declared in the <strong>object</strong> statement.
     * If the number of expressions is less than the number of attributes, remaining attributes are initialized to
     * {@code null}. If the number of expressions is greater than the number of attributes, the extra expressions are
     * evaluated but the results are silently discarded.
     * @see {@link AObject}, 10.2.8
     * @param ctx
     * @return
     */
    @Override
    public T visitNewObject(@NotNull LogicParser.NewObjectContext ctx) {
        String typeName = ctx.ID().getText();

        AObjectType type = arden.getObjectType(typeName);
        if (type == null) throw new RuntimeException(typeName + " does not refer to a valid Arden object type");

        AObject obj = new AObject(type);

        // ordered-with refers to e.g., "new <object> with <attrVal1>, <attrVal2>"
        if (ctx.objOrderedWith() != null) {
            LogicParser.ObjOrderedWithContext octx = ctx.objOrderedWith();
            List<ADataType> list = new ArrayList<ADataType>();

            for (LogicParser.ExprContext ectx : octx.expr()) {
                T v = visit(ectx);
                list.add(v.getBackingObject());
            }

            obj.populate(list);
        }

        // named-with refers to e.g., "new <object> with <attrName1> := <attrVal1>, <attrName2> := <attrVal2>"
        if (ctx.objNamedWith() != null) {
            LogicParser.ObjNamedWithContext nctx = ctx.objNamedWith();
            int max = nctx.ID().size();

            for (int i = 0; i < max; i ++) {
                String attrName = nctx.ID(i).getText();
                T v = visit(nctx.expr(i));

                try {
                    obj.setAttribute(attrName, v.getBackingObject());

                } catch (InvalidAttributeException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return createValue(obj);
    }

    /**
     * The <strong>dot</strong> operator ("{@code .}") selects an attribute from an object based on the name following
     * the dot. It takes an expression and an identifier. The expression typically evaluates to an object or a list of
     * objects.
     * <br>
     * If the expression does not evaluate to an object, or if the object does not contain the named attribute, then
     * {@code null} is returned. If the expression evaluates to a list, normal Arden list handling is used and a list is
     * returned. Therefore, if the expression is a list of objects, then a list (of the same length) of the attribute
     * values named by the identifier is returned (a common usage).
     * @see {@link AObject}, 9.18.1
     * @param ctx
     * @return
     */
    @Override
    public T visitDot(@NotNull LogicParser.DotContext ctx) {
        T v = visit(ctx.expr());

        if (v.isObject()) {                                                         // if its an object, return the
            return createValue(visitDotHelper(v.asObject(), ctx));                  // requested attribute

        } else if (v.isList() && v.asList().getListType() == ListType.OBJECT) {     // if it's a list of objects,
            AList list = new AList();                                               // return a list containing the
            for (ADataType adt : v.asList()) {                                      // requested attribute, one for
                list.add(visitDotHelper((AObject) adt, ctx));                       // each object in the source list
            }
            return createValue(list);
        }

        return NULL();
    }

    private ADataType visitDotHelper(AObject ao, LogicParser.DotContext ctx) {
        String attrName = ctx.ID().getText();

        if (ao.hasAttribute(attrName)) {
            try {
                return ao.getAttribute(attrName);

            } catch (InvalidAttributeException e) {
                throw new RuntimeException(e);          // should never get here, for call to hasAttribute() above
            }
        }

        return new ANull();
    }

    @Override
    public T visitCall(@NotNull LogicParser.CallContext ctx) {
        throw new RuntimeException("call is not implemented");
    }

    @Override
    public T visitClone(@NotNull LogicParser.CloneContext ctx) {
        T v = visit(ctx.expr());
        return arden.clone(v);
    }

    @Override
    public T visitExtractAttrNames(@NotNull LogicParser.ExtractAttrNamesContext ctx) {
        T v = visit(ctx.expr());
        return arden.extractAttrNames(v);
    }

    @Override
    public T visitAttributeFrom(@NotNull LogicParser.AttributeFromContext ctx) {
        T vAttrName = visit(ctx.expr(0));
        T vObj = visit(ctx.expr(1));
        return arden.attributeFrom(vAttrName, vObj);
    }

    @Override
    public T visitSort(@NotNull LogicParser.SortContext ctx) {
        T v = visit(ctx.expr());
        boolean sortByTime = ctx.Time() != null;
        return arden.sort(v, sortByTime);
    }

    @Override
    public T visitMerge(@NotNull LogicParser.MergeContext ctx) {
        T v1 = visit(ctx.expr(0));
        T v2 = visit(ctx.expr(1));
        return arden.merge(v1, v2);
    }

    @Override
    public T visitWhereTimeIsPresent(@NotNull LogicParser.WhereTimeIsPresentContext ctx) {
        T v = visit(ctx.expr());
        return arden.whereTimeIsPresent(v);
    }

    @Override
    public T visitAddToList(@NotNull LogicParser.AddToListContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));

        if (ctx.At() != null) {
            T vAt = visit(ctx.expr(2));
            return arden.addToList(vObj, vList, vAt);

        } else {
            return arden.addToList(vObj, vList);
        }
    }

    @Override
    public T visitRemoveFromList(@NotNull LogicParser.RemoveFromListContext ctx) {
        T vPos = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.removeFromList(vPos, vList);
    }

    @Override
    public T visitWhere(@NotNull LogicParser.WhereContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vTest = visit(ctx.expr(1));
        return arden.where(vObj, vTest);
    }

    @Override
    public T visitAssignment(@NotNull LogicParser.AssignmentContext ctx) {
        String id = ctx.ID().getText();
        T v = visit(ctx.assignable());
        return arden.assignment(id, v);
    }

    @Override
    public T visitMultipleAssignment(@NotNull LogicParser.MultipleAssignmentContext ctx) {
        List<String> idList = new ArrayList<String>();
        for (TerminalNode tn : ctx.ID()) {
            idList.add(tn.getText());
        }

        T v = visit(ctx.assignable());

        return arden.multipleAssignment(idList, v);
    }

    /**
     * Permit the setting of object attributes, and the setting of list elements, even if they are deeply nested.
     * @see 10.2.1.1, 10.2.1.2
     * @param ctx
     * @return
     */
    @Override
    public T visitEnhancedAssignment(@NotNull LogicParser.EnhancedAssignmentContext ctx) {
        String id = ctx.ID().getText();

        ADataType newValue = visit(ctx.assignable()).getBackingObject();

        ADataType obj = evaluateId(id);

        for (int i = 0; i < ctx.objOrIndexRule().size(); i ++) {
            LogicParser.ObjOrIndexRuleContext octx = ctx.objOrIndexRule(i);
            boolean isLast = i == ctx.objOrIndexRule().size() - 1;

            if (octx.getChild(0).getText().equals(".")) {
                String attr = octx.ID().getText();

                if (obj instanceof AObject) {
                    if (((AObject) obj).hasAttribute(attr)) {
                        try {
                            if (isLast) {
                                ((AObject) obj).setAttribute(attr, newValue);

                            } else {
                                obj = ((AObject) obj).getAttribute(attr);
                            }

                        } catch (InvalidAttributeException e) {
                            throw new RuntimeException(e);      // should never get here for call to hasAttribute() above
                        }

                    } else {
                        throw new RuntimeException("object '" + id + "' does not have attribute '" + attr + "'");
                    }

                } else {
                    throw new RuntimeException("cannot access attribute '" + attr + "' from non-object variable");
                }

            } else {                                                    // index into a list
                T v = visit(octx.expr());

                if (v.isInteger()) {
                    int index = v.asInteger();

                    if (obj instanceof AList) {
                        AList list = (AList) obj;
                        if (index > 0 && index <= list.size()) {
                            if (isLast) {
                                list.set(index, newValue);

                            } else {
                                obj = list.get(index);
                            }

                        } else {
                            throw new RuntimeException("index out of bounds: cannot reference index " + index +
                                    " of list " + list);
                        }

                    } else {
                        throw new RuntimeException("cannot index into " + obj.getClass().getName());
                    }

                } else {
                    throw new RuntimeException("invalid index: " + v.getBackingObject());
                }
            }
        }

        return VOID();
    }

    @Override
    public T visitPrint(@NotNull LogicParser.PrintContext ctx) {
        T v = visit(ctx.expr());
        return arden.print(v);
    }

    @Override
    public T visitConcat(@NotNull LogicParser.ConcatContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.concat(left, right);
    }

    @Override
    public T visitBuildString(@NotNull LogicParser.BuildStringContext ctx) {
        T v = visit(ctx.expr());
        return arden.string(v);
    }

    @Override
    public T visitMatches(@NotNull LogicParser.MatchesContext ctx) {
        T vStr = visit(ctx.expr(0));
        T vPattern = visit(ctx.expr(1));
        return arden.matches(vStr, vPattern);
    }

    @Override
    public T visitLength(@NotNull LogicParser.LengthContext ctx) {
        T v = visit(ctx.expr());
        return arden.length(v);
    }

    @Override
    public T visitUppercase(@NotNull LogicParser.UppercaseContext ctx) {
        T v = visit(ctx.expr());
        return arden.uppercase(v);
    }

    @Override
    public T visitLowercase(@NotNull LogicParser.LowercaseContext ctx) {
        T v = visit(ctx.expr());
        return arden.lowercase(v);
    }

    /**
     * The <strong>trim</strong> operator removes leading and trailing white space from a string (see Section 7.1.10).
     * The optional <strong>left</strong> or <strong>right</strong> modifier can be applied to remove leading or
     * trailing white space respectively. Printable characters and embedded white space characters are not affected.
     * The trim of a non-string data type or empty list is {@code null}. Primary times are preserved.
     * @see 9.8.8
     * @param ctx
     * @return
     */
    @Override
    public T visitTrim(@NotNull LogicParser.TrimContext ctx) {
        T v = visit(ctx.expr());
        TrimType type;
        if      (ctx.Left() != null)    type = TrimType.LEFT;
        else if (ctx.Right() != null)   type = TrimType.RIGHT;
        else                            type = TrimType.LEFT_AND_RIGHT;
        return arden.trim(v, type);
    }

    /**
     * The <strong>find ... string</strong> operator locates a substring within a target string, and returns a number
     * that represents the starting position of the substring. <strong>Find ... string</strong> is similar to
     * <strong>matches pattern</strong>, but returns a number (rather than a boolean), and does not support wildcards.
     * <strong>Find ... string</strong> is case-sensitive, and returns a zero if the target string does not contain the
     * exact substring. If either the substring or target is not a string data type, {@code null} is returned. Primary
     * times are not preserved.
     * <br>
     * The optional modifier <strong>starting at...</strong> can be appended to the <strong>find ... string</strong>
     * operator to control where the search for the substring begins. Omitting the modifier causes the search to begin
     * at the first character of the string. The value following <strong>starting at...</strong> must be an integer,
     * otherwise {@code null} is returned. If the value following <strong>starting at...</strong> is an integer beyond
     * the length of the target string (i.e. less than 1 or greater than length target), zero is returned.
     * @see 9.8.9
     * @param ctx
     * @return
     */
    @Override
    public T visitFindInString(@NotNull LogicParser.FindInStringContext ctx) {
        T vNeedle = visit(ctx.expr(0));
        T vHaystack = visit(ctx.expr(1));

        if (vNeedle.isString() && vHaystack.isString()) {
            int start = 1;
            if (ctx.StartingAt() != null) {
                T vStartAt = visit(ctx.expr(2));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    start = vStartAt.asInteger();

                } else {
                    return NULL();
                }
            }

            return createValue(vHaystack.asString().findPositionOf(vNeedle.asString(), start));
        }

        return NULL();
    }

    /**
     * The <strong>substring ... characters [starting at ...] from ...</strong> operator returns a substring of
     * characters from a designated target string. This substring consists of the specified number of characters from
     * the source string beginning with the starting position (either the first character of the string or the specified
     * location within the string). For example <strong>substring 3 characters starting at 2 from "Example"</strong>
     * would return "xam" - a 3 character string beginning with the second character in the source string "Example".
     * <br>
     * The target string must be a string data type, the starting location within the string must be a positive
     * integer, and the number of characters to be returned must be an integer, or the operator returns {@code null}. If a
     * starting position is specified, its value must be an integer between 1 and the length of the string, otherwise
     * an empty string is returned. If the requested number of characters is greater than the length of the string,
     * the entire string is returned. If a starting point is specified, and the requested number of characters is
     * greater than the length of the string minus the starting point, the resulting string is the original string
     * to the right of and including the starting position. If the number of characters requested is positive the
     * characters are counted from left to right. If the number of characters requested is negative, the characters
     * are counted from right to left. The characters in a substring are always returned in the order that they
     * appear in the string. Default list handling is observed. Primary times are preserved.
     * @see 9.8.10
     * @param ctx
     * @return
     */
    @Override
    public T visitSubstring(@NotNull LogicParser.SubstringContext ctx) {
        T vNumChars = visit(ctx.expr(0));

        if (vNumChars.isNumber() && vNumChars.asNumber().isWholeNumber()) {
            T vObj;
            int numChars = vNumChars.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                T vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vObj = visit(ctx.expr(2));

            } else {
                vObj = visit(ctx.expr(1));
            }

            if (vObj.isString()) {
                return createValue(vObj.asString().substring(startAt, numChars));

            } else if (vObj.isList()) {
                AList list = new AList();
                for (ADataType obj : vObj.asList()) {
                    if (obj instanceof AString) {
                        list.add(((AString) obj).substring(startAt, numChars));

                    } else {
                        ANull n = new ANull();
                        n.setPrimaryTime(obj.getPrimaryTime());                     // preserve primary time
                        list.add(n);
                    }
                }
                return createValue(list);

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public T visitAsString(@NotNull LogicParser.AsStringContext ctx) {
        T v = visit(ctx.expr());
        return arden.asString(v);
    }

    @Override
    public T visitCount(@NotNull LogicParser.CountContext ctx) {
        T v = visit(ctx.expr());
        return arden.count(v);
    }

    @Override
    public T visitExist(@NotNull LogicParser.ExistContext ctx) {
        T v = visit(ctx.expr());
        return arden.exist(v);
    }

    @Override
    public T visitAverage(@NotNull LogicParser.AverageContext ctx) {
        T v = visit(ctx.expr());
        return arden.average(v);
    }

    @Override
    public T visitMedian(@NotNull LogicParser.MedianContext ctx) {
        T v = visit(ctx.expr());
        return arden.median(v);
    }

    @Override
    public T visitSum(@NotNull LogicParser.SumContext ctx) {
        T v = visit(ctx.expr());
        return arden.sum(v);
    }

    @Override
    public T visitStdDev(@NotNull LogicParser.StdDevContext ctx) {
        T v = visit(ctx.expr());
        return arden.stdDev(v);
    }

    @Override
    public T visitVariance(@NotNull LogicParser.VarianceContext ctx) {
        T v = visit(ctx.expr());
        return arden.variance(v);
    }

    @Override
    public T visitMinimum(@NotNull LogicParser.MinimumContext ctx) {
        T v = visit(ctx.expr());
        return arden.minimum(v);
    }

    @Override
    public T visitMaximum(@NotNull LogicParser.MaximumContext ctx) {
        T v = visit(ctx.expr());
        return arden.maximum(v);
    }

    @Override
    public T visitFirst(@NotNull LogicParser.FirstContext ctx) {
        T v = visit(ctx.expr());
        return arden.first(v);
    }

    @Override
    public T visitLast(@NotNull LogicParser.LastContext ctx) {
        T v = visit(ctx.expr());
        return arden.last(v);
    }

    @Override
    public T visitAny(@NotNull LogicParser.AnyContext ctx) {
        T v = visit(ctx.expr());
        return arden.any(v);
    }

    @Override
    public T visitAll(@NotNull LogicParser.AllContext ctx) {
        T v = visit(ctx.expr());
        return arden.all(v);
    }

    @Override
    public T visitNo(@NotNull LogicParser.NoContext ctx) {
        T v = visit(ctx.expr());
        return arden.no(v);
    }

    @Override
    public T visitElement(@NotNull LogicParser.ElementContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vIndex = visit(ctx.expr(1));
        return arden.element(vObj, vIndex);
    }

    @Override
    public T visitEarliest(@NotNull LogicParser.EarliestContext ctx) {
        T v = visit(ctx.expr());
        return arden.earliest(v);
    }

    @Override
    public T visitLatest(@NotNull LogicParser.LatestContext ctx) {
        T v = visit(ctx.expr());
        return arden.latest(v);
    }

    @Override
    public T visitExtractChars(@NotNull LogicParser.ExtractCharsContext ctx) {
        T v = visit(ctx.expr());
        return arden.extractChars(v);
    }

    @Override
    public T visitSeqto(@NotNull LogicParser.SeqtoContext ctx) {
        T vFrom = visit(ctx.expr(0));
        T vTo = visit(ctx.expr(1));
        return arden.seqto(vFrom, vTo);
    }

    @Override
    public T visitReverse(@NotNull LogicParser.ReverseContext ctx) {
        T v = visit(ctx.expr());
        return arden.reverse(v);
    }

    @Override
    public T visitIndex(@NotNull LogicParser.IndexContext ctx) {
        T vList = visit(ctx.expr());
        String indexTypeStr = ctx.indexType().getText();
        return arden.index(vList, indexTypeStr);
    }

    @Override
    public T visitIndexFrom(@NotNull LogicParser.IndexFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        String indexTypeStr = ctx.indexType().getText();
        return arden.indexFrom(vNum, vList, indexTypeStr);
    }

    @Override
    public T visitIndexOfFrom(@NotNull LogicParser.IndexOfFromContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.indexOfFrom(vObj, vList);
    }

    @Override
    public T visitAfter(@NotNull LogicParser.AfterContext ctx) {
        T vDur = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        return arden.after(vDur, vTime);
    }

    @Override
    public T visitBefore(@NotNull LogicParser.BeforeContext ctx) {
        T vDur = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        return arden.before(vDur, vTime);
    }

    @Override
    public T visitAgo(@NotNull LogicParser.AgoContext ctx) {
        T v = visit(ctx.expr());
        return arden.ago(v);
    }

    @Override
    public T visitTimeOfDayFunc(@NotNull LogicParser.TimeOfDayFuncContext ctx) {
        T v = visit(ctx.expr());
        return arden.timeOfDayFunc(v);
    }

    @Override
    public T visitTimeFunc(@NotNull LogicParser.TimeFuncContext ctx) {
        T v = visit(ctx.expr());
        return arden.timeFunc(v);
    }

    @Override
    public T visitAtTime(@NotNull LogicParser.AtTimeContext ctx) {
        T vTime = visit(ctx.expr(0));
        T vTimeOfDay = visit(ctx.expr(1));
        return arden.atTime(vTime, vTimeOfDay);
    }

    @Override
    public T visitAsTime(@NotNull LogicParser.AsTimeContext ctx) {
        T v = visit(ctx.expr());
        return arden.asTime(v);
    }

    @Override
    public T visitDayOfWeekFunc(@NotNull LogicParser.DayOfWeekFuncContext ctx) {
        T v = visit(ctx.expr());
        return arden.dayOfWeekFunc(v);
    }

    @Override
    public T visitExtract(@NotNull LogicParser.ExtractContext ctx) {
        T v = visit(ctx.expr());
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.extract(v, temporalUnitStr);
    }

    @Override
    public T visitReplace(@NotNull LogicParser.ReplaceContext ctx) {
        T vTime = visit(ctx.expr(0));
        T vNumber = visit(ctx.expr(1));
        String temporalUnitStr = ctx.temporalUnit().getText();
        return arden.replace(vTime, vNumber, temporalUnitStr);
    }

    @Override
    public T visitUnaryPlus(@NotNull LogicParser.UnaryPlusContext ctx) {
        T v = visit(ctx.expr());
        return arden.unaryPlus(v);
    }

    @Override
    public T visitUnaryMinus(@NotNull LogicParser.UnaryMinusContext ctx) {
        T v = visit(ctx.expr());
        return arden.unaryMinus(v);
    }

    @Override
    public T visitAdd(@NotNull LogicParser.AddContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.add(left, right);
    }

    @Override
    public T visitSubtract(@NotNull LogicParser.SubtractContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.subtract(left, right);
    }

    @Override
    public T visitMultiply(@NotNull LogicParser.MultiplyContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.multiply(left, right);
    }

    @Override
    public T visitDivide(@NotNull LogicParser.DivideContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.divide(left, right);
    }

    @Override
    public T visitRaiseToPower(@NotNull LogicParser.RaiseToPowerContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.raiseToPower(left, right);
    }

    @Override
    public T visitArccos(@NotNull LogicParser.ArccosContext ctx) {
        T v = visit(ctx.expr());
        return arden.arccos(v);
    }

    @Override
    public T visitArcsin(@NotNull LogicParser.ArcsinContext ctx) {
        T v = visit(ctx.expr());
        return arden.arcsin(v);
    }

    @Override
    public T visitArctan(@NotNull LogicParser.ArctanContext ctx) {
        T v = visit(ctx.expr());
        return arden.arctan(v);
    }

    @Override
    public T visitCosine(@NotNull LogicParser.CosineContext ctx) {
        T v = visit(ctx.expr());
        return arden.cosine(v);
    }

    @Override
    public T visitSine(@NotNull LogicParser.SineContext ctx) {
        T v = visit(ctx.expr());
        return arden.sine(v);
    }

    @Override
    public T visitTangent(@NotNull LogicParser.TangentContext ctx) {
        T v = visit(ctx.expr());
        return arden.tangent(v);
    }

    @Override
    public T visitExp(@NotNull LogicParser.ExpContext ctx) {
        T v = visit(ctx.expr());
        return arden.exp(v);
    }

    @Override
    public T visitLog(@NotNull LogicParser.LogContext ctx) {
        T v = visit(ctx.expr());
        return arden.log(v);
    }

    @Override
    public T visitLog10(@NotNull LogicParser.Log10Context ctx) {
        T v = visit(ctx.expr());
        return arden.log10(v);
    }

    @Override
    public T visitFloor(@NotNull LogicParser.FloorContext ctx) {
        T v = visit(ctx.expr());
        return arden.floor(v);
    }

    @Override
    public T visitCeiling(@NotNull LogicParser.CeilingContext ctx) {
        T v = visit(ctx.expr());
        return arden.ceiling(v);
    }

    @Override
    public T visitTruncate(@NotNull LogicParser.TruncateContext ctx) {
        T v = visit(ctx.expr());
        return arden.truncate(v);
    }

    @Override
    public T visitRound(@NotNull LogicParser.RoundContext ctx) {
        T v = visit(ctx.expr());
        return arden.round(v);
    }

    @Override
    public T visitAbs(@NotNull LogicParser.AbsContext ctx) {
        T v = visit(ctx.expr());
        return arden.abs(v);
    }

    @Override
    public T visitSqrt(@NotNull LogicParser.SqrtContext ctx) {
        T v = visit(ctx.expr());
        return arden.sqrt(v);
    }

    @Override
    public T visitAsNumber(@NotNull LogicParser.AsNumberContext ctx) {
        T v = visit(ctx.expr());
        return arden.asNumber(v);
    }

    @Override
    public T visitAnd(@NotNull LogicParser.AndContext ctx) {
        T vLeft = visit(ctx.expr(0));
        T vRight = visit(ctx.expr(1));
        return arden.and(vLeft, vRight);
    }

    @Override
    public T visitOr(@NotNull LogicParser.OrContext ctx) {
        T vLeft = visit(ctx.expr(0));
        T vRight = visit(ctx.expr(1));
        return arden.or(vLeft, vRight);
    }

    @Override
    public T visitNot(@NotNull LogicParser.NotContext ctx) {
        T v = visit(ctx.expr());
        return arden.not(v);
    }

    @Override
    public T visitIsEqual(@NotNull LogicParser.IsEqualContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isEqual(left, right);
    }

    @Override
    public T visitIsNotEqual(@NotNull LogicParser.IsNotEqualContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isNotEqual(left, right);
    }

    @Override
    public T visitIsLessThan(@NotNull LogicParser.IsLessThanContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isLessThan(left, right);
    }

    @Override
    public T visitIsLessThanOrEqual(@NotNull LogicParser.IsLessThanOrEqualContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isLessThanOrEqual(left, right);
    }

    @Override
    public T visitIsGreaterThan(@NotNull LogicParser.IsGreaterThanContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isGreaterThan(left, right);
    }

    @Override
    public T visitIsGreaterThanOrEqual(@NotNull LogicParser.IsGreaterThanOrEqualContext ctx) {
        T left = visit(ctx.expr(0));
        T right = visit(ctx.expr(1));
        return arden.isGreaterThanOrEqual(left, right);
    }

    @Override
    public T visitIsWithinTo(@NotNull LogicParser.IsWithinToContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vLow = visit(ctx.expr(1));
        T vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public T visitIsWithinPreceding(@NotNull LogicParser.IsWithinPrecedingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitIsWithinFollowing(@NotNull LogicParser.IsWithinFollowingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitIsWithinSurrounding(@NotNull LogicParser.IsWithinSurroundingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitIsWithinPast(@NotNull LogicParser.IsWithinPastContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinPast(vTest, vDur, flip);
    }

    @Override
    public T visitIsWithinSameDay(@NotNull LogicParser.IsWithinSameDayContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public T visitIsBefore(@NotNull LogicParser.IsBeforeContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isBefore(vTest, vTime, flip);
    }

    @Override
    public T visitIsAfter(@NotNull LogicParser.IsAfterContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isAfter(vTest, vTime, flip);
    }

    @Override
    public T visitIsIn(@NotNull LogicParser.IsInContext ctx) {
        T left = visit(ctx.expr(0));
        boolean flip = ctx.Not() != null;

        if (left.isNull()) {
            return flip ? FALSE() : TRUE();

        } else {
            T right = visit(ctx.expr(1));
            return arden.isIn(left, right, flip);
        }
    }

    @Override
    public T visitIsDataType(@NotNull LogicParser.IsDataTypeContext ctx) {
        T v = visit(ctx.expr());
        String dataTypeStr = ctx.dataType().getText();
        boolean flip = ctx.Not() != null;
        return arden.isDataType(v, dataTypeStr, flip);
    }

    @Override
    public T visitIsObjectType(@NotNull LogicParser.IsObjectTypeContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vType = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.isObjectType(vObj, vType, flip);
    }

    @Override
    public T visitOccurEqual(@NotNull LogicParser.OccurEqualContext ctx) {
        T vObj = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurEqual(vObj, vTime, flip);
    }

    @Override
    public T visitOccurWithinTo(@NotNull LogicParser.OccurWithinToContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vLow = visit(ctx.expr(1));
        T vHigh = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinTo(vTest, vLow, vHigh, flip);
    }

    @Override
    public T visitOccurWithinPreceding(@NotNull LogicParser.OccurWithinPrecedingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPreceding(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitOccurWithinFollowing(@NotNull LogicParser.OccurWithinFollowingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinFollowing(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitOccurWithinSurrounding(@NotNull LogicParser.OccurWithinSurroundingContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        T vTime = visit(ctx.expr(2));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSurrounding(vTest, vDur, vTime, flip);
    }

    @Override
    public T visitOccurWithinPast(@NotNull LogicParser.OccurWithinPastContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vDur = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinPast(vTest, vDur, flip);
    }

    @Override
    public T visitOccurWithinSameDay(@NotNull LogicParser.OccurWithinSameDayContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurWithinSameDay(vTest, vTime, flip);
    }

    @Override
    public T visitOccurBefore(@NotNull LogicParser.OccurBeforeContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurBefore(vTest, vTime, flip);
    }

    @Override
    public T visitOccurAfter(@NotNull LogicParser.OccurAfterContext ctx) {
        T vTest = visit(ctx.expr(0));
        T vTime = visit(ctx.expr(1));
        boolean flip = ctx.Not() != null;
        return arden.occurAfter(vTest, vTime, flip);
    }

    @Override
    public T visitMinimumFrom(@NotNull LogicParser.MinimumFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.minimumFrom(vNum, vList);
    }

    @Override
    public T visitMaximumFrom(@NotNull LogicParser.MaximumFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.maximumFrom(vNum, vList);
    }

    @Override
    public T visitFirstFrom(@NotNull LogicParser.FirstFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.firstFrom(vNum, vList);
    }

    @Override
    public T visitLastFrom(@NotNull LogicParser.LastFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.lastFrom(vNum, vList);
    }

    @Override
    public T visitNearest(@NotNull LogicParser.NearestContext ctx) {
        T vTime = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return ctx.Index() != null ?
                arden.indexNearest(vTime, vList) :
                arden.nearest(vTime, vList);
    }

    @Override
    public T visitAtMostOrLeast(@NotNull LogicParser.AtMostOrLeastContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));

        if      (ctx.Least() != null)   return arden.atLeast(vNum, vList);
        else if (ctx.Most() != null)    return arden.atMost(vNum, vList);
        else                            throw new GrammarException("expected 'least' or 'most'");
    }

    /**
     * The <strong>sublist ... elements [starting at ...] from</strong> operator returns a sublist of elements from a
     * designated target list and is similar to the <strong>substring</strong> operator (see 9.8.10). This sublist
     * consists of the specified number of elements from the source list beginning with the starting position (either
     * the first elements of the list or the specified location within the list).
     * <br>
     * The target list must be a list data type, the starting location within the list must be a positive integer,
     * and the number of elements to be returned must be an integer, or the operator returns {@code null}. If target is not a
     * list data type, a list with one element is assumed. If a starting position is specified, its value must be an
     * integer between 1 and the length of the list, otherwise an empty list is returned. If the requested number of
     * elements is greater than the length of the list, the entire list is returned. If a starting point is specified,
     * and the requested number of elements is greater than the size of the list minus the starting point, the
     * resulting list is the original list to the right of and including the starting position. If the number of
     * elements requested is positive the elements are counted from left to right. If the number of elements requested
     * is negative, the elements are counted from right to left. The elements in a sublist are always returned in the
     * order that they appear in the original list. Default list handling is observed. Primary times are preserved.
     * @see 9.14.6
     * @param ctx
     * @return
     */
    @Override
    public T visitSubList(@NotNull LogicParser.SubListContext ctx) {
        T vNumElements = visit(ctx.expr(0));

        if (vNumElements.isNumber() && vNumElements.asNumber().isWholeNumber()) {
            T vList;
            int numElements = vNumElements.asInteger();
            int startAt = 1;

            if (ctx.StartingAt() != null) {
                T vStartAt = visit(ctx.expr(1));
                if (vStartAt.isNumber() && vStartAt.asNumber().isWholeNumber()) {
                    startAt = vStartAt.asInteger();

                } else {
                    return NULL();
                }

                vList = visit(ctx.expr(2));

            } else {
                vList = visit(ctx.expr(1));
            }

            if (vList.isList()) {
                return createValue(vList.asList().sublist(startAt, numElements));

            } else {
                return NULL();
            }

        } else {
            return NULL();
        }
    }

    @Override
    public T visitIncrease(@NotNull LogicParser.IncreaseContext ctx) {
        T v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.increase(v, pct);
    }

    @Override
    public T visitDecrease(@NotNull LogicParser.DecreaseContext ctx) {
        T v = visit(ctx.expr());
        boolean pct = ctx.Percent() != null;
        return arden.decrease(v, pct);
    }

    @Override
    public T visitInterval(@NotNull LogicParser.IntervalContext ctx) {
        T v = visit(ctx.expr());
        return arden.interval(v);
    }

    @Override
    public T visitEarliestFrom(@NotNull LogicParser.EarliestFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.earliestFrom(vNum, vList);
    }

    @Override
    public T visitLatestFrom(@NotNull LogicParser.LatestFromContext ctx) {
        T vNum = visit(ctx.expr(0));
        T vList = visit(ctx.expr(1));
        return arden.latestFrom(vNum, vList);
    }
}
