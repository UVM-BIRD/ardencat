/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.model;

/**
 * Created by mstorer on 10/7/13.
 */

public enum EligibilityStatus {
    ELIGIBLE("E"),
    INELIGIBLE("I"),
    UNKNOWN("U");


    public static EligibilityStatus fromString(String s) {
        if      (s.equalsIgnoreCase(ELIGIBLE.getStringVal()))   return ELIGIBLE;
        else if (s.equalsIgnoreCase(INELIGIBLE.getStringVal())) return INELIGIBLE;
        else if (s.equalsIgnoreCase(UNKNOWN.getStringVal()))    return UNKNOWN;
        else                                                    throw new RuntimeException("invalid status '" + s + "'");
    }

    private String s;

    private EligibilityStatus(String s) {
        this.s = s;
    }

    public String getStringVal() {
        return s;
    }
}
