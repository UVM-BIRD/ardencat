/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenLib.
 *
 * ArdenLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenLib.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.arden;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.*;
import edu.uvm.ccts.common.exceptions.InvalidAttributeException;
import org.apache.commons.logging.Log;

import java.io.PrintStream;
import java.text.ParseException;
import java.util.*;

/**
 * Created by mstorer on 4/22/14.
 */
public abstract class AbstractArden<T extends AbstractValue<T>> {
    private static final String NEWLINE_PLACEHOLDER = "\u0000NL\u0000";

    private Log log;
    private Time now;
    private Scope scope = new Scope();
    private ObjectTypeRegistry objectTypeRegistry = new ObjectTypeRegistry();

    protected PrintStream printStream = System.out;

    protected abstract T createValue(ADataType obj);
    protected abstract T NULL();
    protected abstract T VOID();
    protected abstract T TRUE();
    protected abstract T FALSE();

    public AbstractArden(Log log, Time now) {
        this.log = log;
        this.now = now;
    }

    public AObjectType getObjectType(String typeName) {
        return objectTypeRegistry.get(typeName);
    }

    public Scope getScope() {
        return scope;
    }

    public void setPrintStream(PrintStream printStream) {
        this.printStream = printStream;
    }

    public boolean isDefined(String id) {
        return objectTypeRegistry.isDefined(id) || scope.isDefined(id);
    }

    public Map<String, ADataType> getVariablesAndObjectTypes() {
        Map<String, ADataType> map = new HashMap<String, ADataType>();

        map.putAll(scope.getVariables());
        map.putAll(objectTypeRegistry.getObjectTypes());

        return map;
    }

    public void assign(String id, ADataType adt) {
        if (id.length() < 1 || id.length() > 80)
            throw new RuntimeException("identifier must be between 1 and 80 characters in length"); // spec 7.1.2

        if (objectTypeRegistry.isLocallyDefined(id)) {
            throw new RuntimeException("may not redefine object type " + id);

        } else {
            if (adt instanceof AObjectType) {
                AObjectType type = (AObjectType) adt;
                type.setName(id);
                objectTypeRegistry.register(type);

            } else if (scope.isFrozen(id)) {
                // will get here if code attempts to assign to a loop variable within the corresponding loop block
                throw new RuntimeException("variable '" + id + "' cannot be assigned to within current scope");

            } else {
                scope.put(id, adt);
            }
        }
    }

    public ADataType evaluateId(String id) {
        if (id.length() < 1 || id.length() > 80)
            throw new RuntimeException("identifier must be between 1 and 80 characters in length"); // spec 7.1.2

        if (objectTypeRegistry.isDefined(id)) {
            return objectTypeRegistry.get(id);

        } else if (scope.isDefined(id)) {           // If a variable is referred to before its first assignment,
            return scope.get(id);                   // null is returned.  spec 10.2.1
        }

        return new ANull();
    }

    public AObjectType getObjectTypeByName(String typeName) {
        AObjectType type = objectTypeRegistry.get(typeName);
        if (type == null) throw new RuntimeException(typeName + " does not refer to a valid Arden object type");
        return type;
    }

    public void includeVariable(String id, ADataType obj) {
        scope.include(id, obj);
    }

    public void includeObjectType(AObjectType objType) {
        objectTypeRegistry.registerExternal(objType);
    }

    public Time getNow() {
        return now;
    }

    /**
     * Identifiers are alphanumeric tokens. The first character of an identifier must be a letter, and the rest
     * must be letters, digits, and underscores (_). Identifiers must be 1 to 80 characters in length. It is an
     * error for an identifier to be longer than 80 characters. Reserved words are not considered identifiers; for
     * example, {@code then} is a reserved word, not an identifier. Identifiers are used to represent variables, which
     * hold data.
     * @see 7.1.2
     * @param id
     * @return
     */
    public abstract T id(String id);

    /**
     * {@code null} is a special data type that signifies uncertainty. Such uncertainty may be the result of a lack of
     * information in the patient database or an explicit {@code null} value in the database. {@code null} results from an error in
     * execution, such as a type mismatch or division by zero. {@code null} may be specified explicitly within a slot using
     * the word <strong>null</strong> (that is, the {@code null} constant). Entities of data type {@code null} may
     * also have a primary time.
     * @see {@link ANull}, 8.1
     * @return
     */
    public T nullVal() {
        return NULL();
    }

    /**
     * Strings are streams of characters of variable length. String constants are defined in Section 7.1.6.
     * @see {@link edu.uvm.ccts.arden.model.AString}, 7.1.6, 8.6
     * @param str
     * @return
     */
    public T stringVal(String str) {
        str = str.substring(1, str.length() - 1);                   // strip opening and closing quotes
        str = str.replaceAll("\"\"", "\"")                          // replace double-quotes - spec 7.1.6.1
                .replaceAll("(\r?\n\\s*){2,}", NEWLINE_PLACEHOLDER) // replace multiple newlines with single newline
                                                                    // (spec 7.1.6.3, part 1)
                .replaceAll("(\r?\n\\s*)", " ")                     // replace single newline with whitespace - spec 7.1.6.2
                .replaceAll(NEWLINE_PLACEHOLDER, "\n");             // replace newline placeholder with newline
                                                                    // (spec 7.1.6.3, part 2)

        return createValue(new AString(str));
    }

    /**
     * There is a single number type, so there is no distinction between integer and floating point numbers.
     * Number constants (for example, 3.4E-12) are defined in Section 7.1.4. Internally, all arithmetic is done
     * in floating point. For example, 1/2 evaluates to 0.5.
     * @see {@link ANumber}, 7.1.4, 8.3
     * @param numStr
     * @return
     */
    public T numberVal(String numStr) {
        return createValue(NumberUtil.parseNumber(numStr));
    }

    /**
     * The Boolean data type includes the two truth values: {@code true} and {@code false}. The word
     * <strong>true</strong> signifies Boolean {@code true}, and the word <strong>false</strong> signifies Boolean
     * {@code false}.
     * <br>
     * The logical operators use tri-state logic by using {@code null} to signify the third state, uncertainty. For
     * example, {@code true or null} is {@code true}. Although {@code null} is uncertain, a disjunction that includes
     * {@code true} is always {@code true} regardless of the other arguments. However, {@code false or null} is
     * {@code null} because {@code false} in a disjunction adds no information. See Section 9.4 for full truth tables.
     * @see {@link edu.uvm.ccts.arden.model.ABoolean}, 8.2
     * @param boolStr
     * @return
     */
    public T booleanVal(String boolStr) {
        return createValue(new ABoolean(boolStr.equalsIgnoreCase("true")));
    }

    /**
     * A list is an ordered set of elements, each of which may be {@code null}, {@code Boolean}, {@code event},
     * {@code destination}, {@code message}, {@code term},{@code number}, {@code time}, {@code duration}, or
     * {@code string}.
     * <br>
     * <strong>There are no nested lists</strong>; that is, a list cannot be the element of another
     * list.
     * <br>
     * Lists may be heterogeneous; that is, the elements in a list may be of different types.
     * <br>
     * There is one list constant, the empty list, which is signified by using a pair of empty parentheses: {@code ()}.
     * White space is allowed within an empty list's parentheses. Other lists are created by using list operators like
     * the comma ({@code ,}) to build lists from single items (see Section 9.2). For the output format of lists
     * (including single element lists), see Section 9.8.
     * @see {@link edu.uvm.ccts.arden.model.AList}, 8.8, 9.2.1
     * @param left
     * @param right
     * @return
     */
    public T binaryList(T left, T right) {
        AList list = left.asList();
        list.addAll(right.asList());
        return createValue(list);
    }

    /**
     * Unary {@code ,} turns a single element into a list of length one. It does nothing if the argument is already a
     * list.
     * @see {@link AList}, 9.2.2
     * @param v
     * @return
     */
    public T unaryList(T v) {
        if (v.isList()) {
            return v;

        } else {
            AList list = new AList();
            list.add(v.getBackingObject());
            return createValue(list);
        }
    }

    /**
     * An empty list is represented as {@code ()}, and generates a list with zero elements.
     * @see {@link AList}, 8.8
     * @return
     */
    public T emptyList() {
        return createValue(new AList());
    }

    /**
     * The word <strong>now</strong> is a time constant that signifies the time when the MLM started execution.
     * <strong>now</strong> is constant through the execution of the MLM; that is, if <strong>now</strong> is used more
     * than once, it will have the same value within the same MLM. <strong>now</strong> inside a nested MLM will
     * therefore be different from the <strong>now</strong> of the calling MLM.
     * @see 8.4.3
     * @return
     */
    public T now() {
        return createValue(getNow());
    }

    /**
     * The word <strong>currenttime</strong> represents the system time at the instant the word is encountered during MLM
     * execution. <strong>currenttime</strong> differs from now in that <strong>currenttime</strong> constantly changes,
     * while now remains constant while an MLM runs. Thus, the time required to execute an MLM (or query) can be
     * determined by subtracting <strong>now</strong> from <strong>currenttime</strong>. The following inequality is
     * guaranteed within a single MLM: <strong>eventtime</strong> <= <strong>triggertime</strong> <=
     * <strong>now</strong> <= <strong>currenttime</strong>.
     * @see 8.4.6
     * @return
     */
    public T currentTime() {
        return createValue(new Time(new Date()));
    }

    /**
     * The time data type refers to points in absolute time; it is also referred to as timestamp in other systems.
     * Both date and time-of-day must be specified. Times back to the year 1800 must be supported and times before
     * 1800-01-01 are not valid. Time constants (for example, 1990-07-12T00:00:00) are defined in Section 7.1.5.
     * @see {@link Time}, 8.4
     * @param timeStr
     * @return
     */
    public T timeVal(String timeStr) {
        try {
            Time t = TimeUtil.parseTime(timeStr);

            if (t.getYear().intValue() < 1800) throw new RuntimeException("invalid time: " + t);

            return createValue(t);

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The time-of-day data type refers to points in time that are not directly linked to a specific date.
     * Time-of-day constants are analogously defined to time constants leaving the date portion blank. Time-of-day
     * constants (for example, 23:20:00) are defined in Section 7.1.6.
     * <br>
     * Operators that can use both time arguments and time-of-day arguments at the same time may follow the default
     * time-of-day handling as defined in Section 9.1.5 .The primary time handling is unaffected by these extension.
     * @see {@link TimeOfDay}, 8.11
     * @param timeOfDayStr
     * @return
     */
    public T timeOfDayVal(String timeOfDayStr) {
        try {
            TimeOfDay tod = TimeUtil.parseTimeOfDay(timeOfDayStr);
            return createValue(tod);

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The day-of-week data type is a special data type to represent specific days of the week to be used along
     * with the "day of week" operator. Values of this data type are either expressed by constants or by integer
     * values.
     * <br>
     * Day-of-week constants are defined by the following keywords: <strong>MONDAY</strong> ({@code 1}),
     * <strong>TUESDAY</strong> ({@code 2}), <strong>WEDNESDAY</strong> ({@code 3}), <strong>THURSDAY</strong>
     * ({@code 4}), <strong>FRIDAY</strong> ({@code 5}), <strong>SATURDAY</strong> ({@code 6}) <strong>SUNDAY</strong>
     * ({@code 7}).  These constants are defined in {@link DayOfWeek}.
     * @see {@link DayOfWeek}, 8.12
     * @param dayOfWeekStr
     * @return
     */
    public T dayOfWeek(String dayOfWeekStr) {
        DayOfWeek dow = DayOfWeek.valueOf(dayOfWeekStr.toUpperCase());
        return createValue(new ANumber(dow.intValue()));
    }

    /**
     * The <strong>clone</strong> operator returns a copy of its argument.
     * <br>
     * Practically, this only affects objects, because these are the only data types which retain identity across
     * multiple operations. (See Annex A6 for details of object identity). When an object is copied, a new object of
     * the same type is created, and all its fields are initialized by assigning values from corresponding fields in
     * the argument object. The fields, which may contain objects, are themselves cloned, resulting in a deep shallowCopy.
     * If any field contains a list, that list is cloned, and any objects stored in the list are also cloned.
     * <br>
     * The clone operator insures that no objects are shared between the argument and the result. The clone returns
     * another, distinct object that has the same structure and value as the original object.
     * @see {@link AObject}, 9.18.2
     * @param v
     * @return
     */
    public T clone(T v) {
        return createValue(v.getBackingObject().copy());
    }

    /**
     * The <strong>extract attribute names</strong> operator expects an object as its argument.
     * <br>
     * It returns a list containing the attribute names of the object argument. Only the immediate attribute names of
     * the argument are returned. If an attribute is itself an object, the attribute names of the embedded object are
     * not returned, i.e. no nested lists. If the argument is not an object, {@code null} is returned.
     * @see {@link AObject}, 9.18.3
     * @param v
     * @return
     */
    public T extractAttrNames(T v) {
        if (v.isObject()) {
            AList list = new AList();
            for (String attrName : v.asObject().getAttributeNames()) {
                list.add(new AString(attrName));
            }
            return createValue(list);
        }

        return NULL();
    }

    /**
     * The <strong>attribute ... from ...</strong> operator expects a string containing the name of an attribute and an
     * object as arguments. It returns the value of the named attribute. If the named attribute is itself an object,
     * the sub- object is returned. If no attributes with the supplied name exists within the named object,
     * {@code null} is returned.
     * <br>
     * This is analogous to referring to attributes using dot notation. However, the <strong>attribute ... from ...</strong>
     * operator allows the name of the attribute to be supplied at run-time rather than requiring knowing the attribute
     * name at design-time.
     * @see {@link AObject}, 9.18.4
     * @param vAttrName
     * @param vObj
     * @return
     */
    public T attributeFrom(T vAttrName, T vObj) {
        if (vObj.isObject() && vAttrName.isString()) {
            AObject obj = vObj.asObject();
            String attrName = vAttrName.asString().toString();

            if (obj.hasAttribute(attrName)) {
                try {
                    return createValue(obj.getAttribute(attrName));

                } catch (InvalidAttributeException e) {
                    throw new RuntimeException(e);          // should never get here, for call to hasAttribute() above
                }
            }
        }

        return NULL();
    }

    /**
     * The <strong>sort</strong> operator reorders a list based on element keys, which are either the element values
     * (keyword data) or the primary times (keyword time).
     * <br>
     * An optional modifier may be use with the sort operator. If used, the modifier must be placed immediately after
     * the <strong>sort</strong> keyword. The following keywords can be placed after the <strong>sort</strong> keyword:
     * <strong>data</strong> or <strong>time</strong>, which are mutually exclusive. If no modifier is used, the
     * <strong>sort</strong> operator defaults to a data sort. Direction of sorting is always ascending. For a
     * descending sort, <strong>reverse</strong> can be used (see {@link #reverse}).
     * <br>
     * The sort options are considered to be part of the sort operator for precedence purposes. This resolves the
     * potential conflict with the time [of] operator (9.17.1) (see {@link #timeFunc}). Thus the expression "sort
     * time x" should be parsed as "sort the list x by time" rather than as "extract the primary times from the list
     * x and sort the list of times."
     * <br>
     * When sorting by primary times, if any of the elements do not have primary times, the result is {@code null}.
     * (The sort argument can always be qualified by <strong>where time of it is present</strong>, if this is not
     * desired behavior.) Elements with the same key will be kept in the same order as they appear in the argument.
     * If any pair of element key cannot not be compared because of type clashes, sort returns {@code null} (that is,
     * when sorting by data any {@code null} value (or non-comparable value) results in {@code null}; when sorting by
     * time, any {@code null} primary time results in {@code null}).
     * @see {@link AList}, 9.2.4
     * @param v
     * @param sortByTime
     * @return
     */
    public T sort(T v, boolean sortByTime) {
        // todo : implement "using"

        if (v.isList()) {
            AList sorted = sortByTime ?
                    v.asList().sortByTime() :       // sortByTime and sortByData return sorted *copies* of the
                    v.asList().sortByData();        // original list

            return createValue(sorted);
        }

        return NULL();
    }

    /**
     * The <strong>merge</strong> operator appends two lists, appends a single item to a list, or creates a list from
     * two single items.
     * <br>
     * It then sorts the result in chronological order based on the primary times of the elements (as defined in
     * 9.2.4, see {@link #sort}). All elements of both lists must have primary times; otherwise {@code null} is
     * returned (the construct x where time of it is present can be used to select only elements of x that have primary
     * times). The primary times are maintained. Merge is typically used to put together the results of two separate
     * queries. The expression <strong>x merge y</strong> is equivalent to <strong>sort time (x,y)</strong>.
     * @see 9.2.3
     * @param v1
     * @param v2
     * @return
     */
    public T merge(T v1, T v2) {
        AList list = v1.asList().copy();        // copy list to start - don't want to append to existing list!
        if (v2.isList()) {
            list.addAll(v2.asList());

        } else {
            list.add(v2.getBackingObject());
        }

        return createValue(list.sortByTime());
    }

    public T whereTimeIsPresent(T v) {
        return createValue(v.asList().whereTimeIsPresent());
    }

    /**
     * The <strong>add ... to ... [at ...]</strong> operator expects an arbitrary data value as its first argument and a list as its
     * second argument. It adds this element to the given list. If no position is given, the element will be added to
     * the end of the list. If a position is provided, the element is inserted at this position and the index of all
     * elements from this to the end of the list will be increased by one. If the given position is greater than the
     * cardinality of the list, the element will be appended at the end of the list. In case a negative position or 0
     * is given, the element will be appended at the beginning of the list. If the second argument is not a list, the
     * argument is assumed a list with one element. When more than one position is given, the positions are first
     * identified and then the elements are inserted.
     * @see {@link AList}, 9.2.5
     * @param vObj
     * @param vList
     * @return
     */
    public T addToList(T vObj, T vList) {
        return addToList(vObj, vList, null);
    }

    public T addToList(T vObj, T vList, T vAt) {
        AList list = vList.asList().copy();

        if (vAt != null) {
            if (vAt.isNumber()) {
                // if inserting into a single index within the list -

                list.insert(vObj.getBackingObject(), vAt.asNumber().intValue());
                return createValue(list);

            } else if (vAt.isList() && vAt.asList().getListType() == ListType.NUMBER) {
                // if inserting into multiple indices within the list -

                List<Integer> posList = new ArrayList<Integer>();
                for (ADataType obj : vAt.asList()) {
                    posList.add(((ANumber) obj).intValue());
                }

                // sort positions into reverse order - we will insert in reverse order to prevent
                // indexes from getting screwed up after each iteration's insertion
                Collections.sort(posList);
                Collections.reverse(posList);

                for (int pos : posList) {
                    list.insert(vObj.getBackingObject(), pos);
                }

                return createValue(list);

            } else {
                return NULL();
            }

        } else {
            // otherwise, append to the end of the list

            list.add(vObj.getBackingObject());
            return createValue(list);
        }
    }

    /**
     * The <strong>remove ... from ... operator</strong> expects a number or list of numbers as its first argument and a list as its
     * second argument. The operator also accepts first and last as its first argument, they are interpreted as the
     * number representing the last (the first) index in the given list. The operator removes the elements with the
     * given indices from the list. The indices of all elements from the given index to the end of the list will be
     * decreased by one. If the second argument is not a list, the argument is assumed a list with one element. When
     * more than one position is given, the positions are first detected and then the elements are removed.
     * @see {@link AList}, 9.2.6
     * @param vPos
     * @param vList
     * @return
     */
    public T removeFromList(T vPos, T vList) {
        AList list = vList.asList().copy();

        if (vPos.isList() && vPos.asList().getListType() == ListType.NUMBER) {
            List<Integer> posList = new ArrayList<Integer>();
            for (ADataType obj : vPos.asList()) {
                posList.add(((ANumber) obj).intValue());
            }

            // want to remove items from the end of the list first, to prevent munging indices after each iteration
            Collections.sort(posList);
            Collections.reverse(posList);

            for (int pos : posList) {
                list.remove(pos);
            }

        } else if (vPos.isNumber()) {
            list.remove(vPos.asNumber().intValue());
        }

        return createValue(list);
    }

    /**
     * The <strong>where</strong> operator performs the equivalent of a relational select ... where ... on its left argument. In general,
     * the left argument is a list, often the result of a query to the database. The right argument is usually of type
     * Boolean (although this is not required), and must be the same length as the left argument. The result is a list
     * that contains only those elements of the left argument where the corresponding element in the right argument is
     * Boolean {@code true}. If the right argument is anything else, including {@code false}, {@code null}, or any other
     * type, then the element in the left argument is dropped. The where operator maintains the primary time(s) of the
     * operand(s) to the left of where. The primary time(s) of the operand(s) to the right of where are dropped.
     * <br>
     * Where handles mixed single items and lists in a manner analogous to the other binary operators. If the right
     * argument to where is a single item, then if it is {@code true}, the entire left argument is kept (whether or not
     * it is a list); if it is not {@code true}, then the empty list is returned. If only the left argument is a single
     * item, then the result is a list with as many of the single items as there are elements equal to true in the right
     * argument. If the two arguments are lists of different length, then a single {@code null} results (the rules in
     * Section 9.1.3.4 are used to replicate a single-element argument if necessary).
     * @see 9.3.1
     * @param vObj
     * @param vTest
     * @return
     */
    public T where(T vObj, T vTest) {
        // todo : there are difficult use-cases involving the 'where' operator - not all have been implemented - see spec for details

        if (vTest.isList()) {
            AList testList = vTest.asList();

            if (vObj.isList()) {
                AList objList = vObj.asList();

                if (testList.size() == objList.size()) {
                    AList list = new AList();
                    for (int i = 1; i <= testList.size(); i ++) {
                        ADataType test = testList.get(i);
                        if (test instanceof ABoolean && ((ABoolean) test).isTrue()) {
                            list.add(objList.get(i).copy());
                        }
                    }
                    return createValue(list);
                }

            } else {
                ADataType obj = vObj.getBackingObject();
                AList list = new AList();

                for (int i = 1; i <= testList.size(); i ++) {
                    ADataType test = testList.get(i);
                    if (test instanceof ABoolean && ((ABoolean) test).isTrue()) {
                        list.add(obj.copy());
                    }
                }
                return createValue(list);
            }

        } else if (vTest.isBoolean()) {
            return vTest.asBoolean().isTrue() ?
                    vObj :
                    createValue(new AList());

        }

        return NULL();
    }

    /**
     * The assignment statement places the value of an expression into a variable.
     * <br>
     * Any reference to the identifier that occurs after the assignment statement will return the value that was
     * assigned from the expression (even if it is in another structured slot; for example, the action slot). A
     * subsequent assignment to the same variable will overwrite the value. If a variable is referred to before
     * its first assignment, {@code null} is returned.
     * @see 7.1.2, 10.2.1
     * @param id
     * @param v
     * @return
     */
    public T assignment(String id, T v) {
        if (v.isArgumentList()) {
            List<ADataType> arguments = v.asArgumentList().getList();
            if (arguments.size() >= 1) {
                assign(id, arguments.get(0));

            } else {
                assign(id, new ANull());
            }

        } else {
            assign(id, v.getBackingObject());
        }

        return NULL();
    }

    /**
     * A query may return more than one result at a time. This is useful for batteries of tests in order to keep the
     * corresponding tests within one blood sample coordinated. The two versions are equivalent (the parentheses around
     * the where are optional).
     * <br>
     * There may be one or more <var> within the parentheses. <aggregation>, <constraint>, and <mapping> are defined as
     * above. The fact that multiple entities are being queried at once is represented in the institution-specific
     * part, <mapping>. The <aggregation> and <constraint> are performed separately on the individual variables; it is
     * institution-defined whether the <mapping> returns all the values with matching primary times.
     * @see 11.2.1.6, 11.2.1.7
     * @param idList
     * @param v
     * @return
     */
    public T multipleAssignment(List<String> idList, T v) {
        if (v.isArgumentList()) {                                       // see spec 11.2.5
            List<ADataType> arguments = v.asArgumentList().getList();
            if (idList.size() <= arguments.size()) {
                for (int i = 0; i < idList.size(); i ++) {
                    assign(idList.get(i), arguments.get(i));
                }

            } else {
                for (int i = 0; i < arguments.size(); i ++) {
                    assign(idList.get(i), arguments.get(i));
                }
                for (int i = arguments.size(); i < idList.size(); i ++) {
                    assign(idList.get(i), new ANull());
                }
            }

        } else if (v.isList()) {
            AList list = v.asList();
            if (list.size() == idList.size()) {
                for (int i = 0; i < list.size(); i ++) {
                    assign(idList.get(i), list.get(i + 1));     // AList indexes are 1-based
                }
            }

        } else if (v.isListOfLists()) {
            AListOfLists listOfLists = v.asListOfLists();
            if (listOfLists.size() == idList.size()) {
                for (int i = 0; i < listOfLists.size(); i ++) {
                    assign(idList.get(i), listOfLists.getList(i));
                }
            }

        } else if (idList.size() >= 1) {                    // todo : is this correct?  given "(a, b, c) = x;"
            assign(idList.get(0), v.getBackingObject());    // - should a = x, b = null, c = null, or
            for (int i = 1; i < idList.size(); i ++) {      //   should a = x, b = x, c = x ?
                assign(idList.get(i), new ANull());
            }

        } else {
            throw new RuntimeException("cannot perform multi-variable assignment to " +
                    v.getBackingObject().getClass().getName());
        }

        return NULL();
    }

    /**
     * Print out the string representation of an evaluated expression.
     * <br>
     * <strong>THIS METHOD IS NOT PART OF THE OFFICIAL ARDEN SPECIFICATION</strong>
     * @param v
     * @return
     */
    public T print(T v) {
        if (printStream != null) printStream.println(v.asString());

        return VOID();
    }

    /**
     * The <strong>||</strong> operator (string concatenation) converts its arguments to strings and then concatenates
     * those strings together.
     * <br>
     * The {@code null} data type is converted to the string "null" and then appended to the other argument. Thus ||
     * never returns {@code null}. Lists are converted to strings and then appended to the other argument; the list is
     * enclosed in parentheses and the elements are separated by , with no separating blanks. The string
     * representation of Booleans, numbers, times, and durations is location-specific to allow for the use of the
     * native language. The <strong>formatted with</strong> operator's {@code %s} operator is used to convert values to
     * strings (see Section 9.8.2). The <strong>string</strong> operator is a generalization of the || operator (see
     * Section 9.8.3), except that the string operator does not do anything special for lists. The primary times of
     * its arguments are lost.
     * @see 9.8.1
     * @param left
     * @param right
     * @return
     */
    public T concat(T left, T right) {
        return createValue(left.asString().concat(right.asString()));
    }

    /**
     * The <strong>string</strong> operator expects a string or list of strings as its argument. It returns a single
     * string made by concatenating all the elements, as the || operator (see Section 9.8.1).
     * <br>
     * If the argument is an empty list, the result is the empty string (""). The element operator (Section 9.12.18)
     * can be used to select certain items from the list. The primary times of its arguments are lost.
     * @see {@link #element}, 9.8.3
     * @param v
     * @return
     */
    public T string(T v) {
        if (v.isString()) {
            return createValue(v.asString());

        } else if (v.isList()) {                                // concatenate list elements into a single string
            ListType type = v.getListType();

            if (type == ListType.STRING) {                      // require list of strings, specifically
                StringBuilder sb = new StringBuilder();

                for (ADataType o : v.asList()) {
                    sb.append(o.toString());
                }

                return createValue(new AString(sb.toString()));

            } else if (type == ListType.EMPTY) {                // empty list becomes an empty string
                return createValue(new AString(""));
            }
        }

        return NULL();
    }

    /**
     * The effect of this operator is similar to the LIKE operator in SQL (ISO / IEC 9075). <strong>Matches
     * pattern</strong> is used to determine whether or not a particular string matches a pattern.
     * <br>
     * This operator expects two string arguments. The first argument is a string to be matched, and the second is the
     * pattern used for matching. <strong>Matches pattern</strong> returns a Boolean value: {@code true} if the pattern of the second
     * argument matches the first argument and {@code false} if it does not. The first argument also may be a list of
     * strings, in which case the result is a list of Boolean values, each corresponding to the match between one
     * string and the pattern of the second argument. If the arguments are not strings, {@code null} is returned.
     * Matching is case-insensitive. The primary times of the arguments are lost.
     * <br>
     * The pattern of the second argument may be any legal string character. In addition, two wild-card characters
     * may be used. The underscore (_) will match exactly any one character. The percent sign (%) will match 0 to
     * arbitrarily many characters. In order to match one of the literal wild-card character, precede it with an
     * escape (\) character.
     * @see 9.8.4
     * @param vStr
     * @param vPattern
     * @return
     */
    public T matches(T vStr, T vPattern) {
        if (vPattern.isString()) {
            if (vStr.isString()) {
                return createValue(vStr.asString().matches(vPattern.asString()));

            } else if (vStr.isList() && vStr.getListType() == ListType.STRING) {
                AList list = new AList();

                for (ADataType o : vStr.asList()) {
                    list.add(((AString) o).matches(vPattern.asString()));
                }

                return createValue(list);
            }
        }

        return NULL();
    }

    /**
     * The <strong>length</strong> operator returns the number of characters in a string.
     * <br>
     * Leading or trailing spaces are included in this calculation. Applying the length operator to an empty string
     * returns zero, while the length of a non-string data type or an empty list is {@code null}. The length operator
     * is different from the count operator (see Section 9.12.2), in that length is the number of characters in a single
     * string, while count is the number of items in a list. Primary times are not preserved.
     * @see 9.8.5
     * @param v
     * @return
     */
    public T length(T v) {
        if (v.isList()) {                       // default list handling, see 9.1.3.1
            AList objList = v.asList();
            if (objList.isEmpty()) {
                return NULL();

            } else {
                AList list = new AList();
                for (ADataType obj : objList) {
                    list.add(doLength(obj));
                }
                return createValue(list);
            }

        } else {
            return createValue(doLength(v.getBackingObject()));
        }
    }

    private ADataType doLength(ADataType obj) {
        return obj instanceof AString ?                 // do NOT preserve primary times!
                ((AString) obj).length() :
                new ANull();
    }

    /**
     * The <strong>uppercase</strong> operator converts all lowercase characters in a string to uppercase. Non-lowercase
     * characters, including numeric and punctuation characters, are not affected. The uppercase of a non-string data
     * type or an empty list is {@code null}. Primary times are preserved.
     * @see 9.8.6
     * @param v
     * @return
     */
    public T uppercase(T v) {
        if (v.isList()) {                       // default list handling, see 9.1.3.1
            AList objList = v.asList();
            if (objList.isEmpty()) {
                return NULL();

            } else {
                AList list = new AList();
                for (ADataType obj : objList) {
                    list.add(doUppercase(obj));
                }
                return createValue(list);
            }

        } else {
            return createValue(doUppercase(v.getBackingObject()));
        }
    }

    private ADataType doUppercase(ADataType obj) {
        if (obj instanceof AString) {
            return ((AString) obj).uppercase();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>lowercase</strong> operator converts all uppercase characters in a string to lowercase. Non-uppercase
     * characters, including numeric and punctuation characters, are not affected. The lowercase of a non-string data
     * type or empty list is {@code null}. Primary times are preserved.
     * @see 9.8.7
     * @param v
     * @return
     */
    public T lowercase(T v) {
        if (v.isList()) {                       // default list handling, see 9.1.3.1
            AList objList = v.asList();
            if (objList.isEmpty()) {
                return NULL();

            } else {
                AList list = new AList();
                for (ADataType obj : objList) {
                    list.add(doLowercase(obj));
                }
                return createValue(list);
            }

        } else {
            return createValue(doLowercase(v.getBackingObject()));
        }
    }

    private ADataType doLowercase(ADataType obj) {
        if (obj instanceof AString) {
            return ((AString) obj).lowercase();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }
    
    public T trim(T v, TrimType type) {
        if (v.isList()) {                       // default list handling, see 9.1.3.1
            AList objList = v.asList();
            if (objList.isEmpty()) {
                return NULL();

            } else {
                AList list = new AList();
                for (ADataType obj : objList) {
                    list.add(doTrim(obj, type));
                }
                return createValue(list);
            }

        } else {
            return createValue(doTrim(v.getBackingObject(), type));
        }
    }

    private ADataType doTrim(ADataType obj, TrimType type) {
        if (obj instanceof AString) {
            AString s = (AString) obj;
            switch (type) {
                case LEFT:  return s.trimLeft();
                case RIGHT: return s.trimRight();
                default:    return s.trim();
            }

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>as string</strong> operator attempts to convert any data type to a string. If conversion to a string
     * is possible, the string is returned, otherwise {@code null} is returned. The primary time of the argument is
     * preserved.
     * @see 9.8.13
     * @param v
     * @return
     */
    public T asString(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doAsString(obj));
            }
            return createValue(list);

        } else {
            return createValue(doAsString(v.getBackingObject()));
        }
    }

    private AString doAsString(ADataType obj) {
        AString rval = new AString(obj.toString());
        rval.setPrimaryTime(obj.getPrimaryTime());           // primary times are preserved
        return rval;
    }

    /**
     * The <strong>count</strong> operator returns the number of items (including {@code null} items) in a list. Count
     * never returns {@code null}. The result loses the primary time.
     * @see 9.12.2
     * @param v
     * @return
     */
    public T count(T v) {
        int c = v.isList() ? v.asList().size() : 1;                 // do NOT keep primary time!
        return createValue(new ANumber(c));
    }

    /**
     * The <strong>exist</strong> operator has one synonym: exists. It returns {@code true} if there is at least one
     * non-{@code null} item in a list of any type.
     * <br>
     * If the list argument is a single item, then it is treated as a list of length one. Exist never returns
     * {@code null}. If all the elements of the list have the same primary time, the result maintains that primary time
     * (otherwise the primary time is lost).
     * @see 9.12.3
     * @param v
     * @return
     */
    public T exist(T v) {
        boolean exists = false;
        if (v.isList()) {
            for (ADataType item : v.asList()) {
                if ( ! (item instanceof ANull) ) {
                    exists = true;
                    break;
                }
            }

        } else {
            exists = ! v.isNull();
        }

        ABoolean b = new ABoolean(exists);
        b.setPrimaryTime(v.getPrimaryTime());

        return createValue(b);
    }

    /**
     * The <strong>average</strong> operator has one synonym: <strong>avg</strong>. It calculates the average of a
     * number, time, or duration list. If all the elements of the list have the same primary time, the result maintains
     * that primary time (otherwise the primary time is lost).
     * @see 9.12.4
     * @param v
     * @return
     */
    public T average(T v) {
        if (v.isList()) {
            switch (v.getListType()) {
                case NUMBER:    return createValue(NumberUtil.mean(v.asNumberList()));
                case TIME:      return createValue(TimeUtil.mean(v.asTimeList()));
                case DURATION:  return createValue(DurationUtil.mean(v.asDurationList()));
            }

        } else if (v.isNumber() || v.isTime() || v.isDuration()) {
            return createValue(v.getBackingObject());
        }

        return NULL();
    }

    /**
     * The <strong>median</strong> operator calculates the median value of a number, time, or duration list. The list is first sorted.
     * If there is an odd number of items, it selects the middle value. If there is an even number of items, it
     * averages the middle two values. If there is a tie, then it selects the latest of those elements that have a
     * primary time. If a single element is selected or if the two selected elements of the list have the same
     * primary time, the result maintains that primary time (otherwise the primary time is lost).
     * @see 9.12.5
     * @param v
     * @return
     */
    public T median(T v) {
        if (v.isList()) {
            switch (v.getListType()) {
                case NUMBER:    return createValue(NumberUtil.median(v.asNumberList()));
                case TIME:      return createValue(TimeUtil.median(v.asTimeList()));
                case DURATION:  return createValue(DurationUtil.median(v.asDurationList()));
            }

        } else if (v.isNumber() || v.isTime() || v.isDuration()) {
            return createValue(v.getBackingObject());
        }

        return NULL();
    }

    /**
     * The <strong>sum</strong> operator calculates the sum of a number or duration list. If all the elements of the
     * list have the same primary time, the result maintains that primary time (otherwise the primary time is lost).
     * @see 9.12.6
     * @param v
     * @return
     */
    public T sum(T v) {
        if (v.isList()) {
            switch (v.getListType()) {
                case NUMBER:    return createValue(NumberUtil.sum(v.asNumberList()));
                case DURATION:  return createValue(DurationUtil.sum(v.asDurationList()));
            }

        } else if (v.isNumber() || v.isDuration()) {
            return createValue(v.getBackingObject());
        }

        return NULL();
    }

    /**
     * The <strong>stddev</strong> operator returns the sample standard deviation of a numeric list. If all the elements of the list
     * have the same primary time, the result maintains that primary time (otherwise the primary time is lost).
     * @see 9.12.7
     * @param v
     * @return
     */
    public T stdDev(T v) {
        if (v.isList() && v.getListType() == ListType.NUMBER) {
            return createValue(NumberUtil.stddev(v.asNumberList()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>variance</strong> operator returns the sample variance of a numeric list. If all the elements of the list have
     * the same primary time, the result maintains that primary time (otherwise the primary time is lost).
     * @see 9.12.8
     * @param v
     * @return
     */
    public T variance(T v) {
        if (v.isList() && v.getListType() == ListType.NUMBER) {
            return createValue(NumberUtil.variance(v.asNumberList()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>minimum</strong> operator has one synonym: min. It returns the smallest value in a homogeneous list
     * of an ordered type (that is, all numbers, all times, all durations, or all strings), using the <= operator
     * (see Section 9.5.4). If there is a tie, it selects the element with the latest primary time. The primary
     * time of the selected argument is maintained.
     * <br>
     * The <strong>minimum</strong> operator can also be extended by the using modifier as defined for the sort
     * operator (see 9.2.4) to allow more complex calculations of the minimum.
     * @see 9.12.9
     * @param v
     * @return
     */
    public T minimum(T v) {
        return createValue(v.asList().minimum());
    }

    /**
     * The <strong>maximum</strong> operator has one synonym: max. It returns the largest value in a homogeneous list of an
     * ordered type, using the <strong>&gt;=</strong> operator (see Section 9.5.6). If there is a tie, it selects the element with
     * the latest primary time. The primary time of the selected argument is maintained.
     * <br>
     * The maximum operator can also be extended by the using modifier as defined for the sort operator (see 9.2.4)
     * to allow more complex calculations of the maximum.
     * @see 9.12.10
     * @param v
     * @return
     */
    public T maximum(T v) {
        return createValue(v.asList().maximum());
    }

    /**
     * The first operator returns the value at the beginning of a list. If the list is empty, {@code null} is returned.
     * The expression first x is equivalent to x[1]. First on the result of a time-sorted query will return the
     * earliest value. The primary time of the selected argument is maintained. Note that first is different than
     * first specified in Arden Syntax version E 1460-92. That operator is now called earliest (see Section 9.12.17).
     * @see 9.12.12
     * @param v
     * @return
     */
    public T first(T v) {
        return createValue(v.asList().first());
    }

    /**
     * The last operator returns the value at the end of a list, regardless of type. If the list is empty, {@code null} is
     * returned. The expression last x is equivalent to x[count x]. Last on the result of a time-sorted query will
     * return the most recent value. The primary time of the selected argument is maintained. Note that last is
     * different than last specified in Arden Syntax version E 1460-92. That operator is now called latest (see
     * Section 9.12.16).
     * @see 9.12.11
     * @param v
     * @return
     */
    public T last(T v) {
        return createValue(v.asList().last());
    }

    /**
     * The any operator returns {@code true} if any of the items in a list is {@code true}. It returns {@code false} if
     * they are all {@code false}. Otherwise it returns {@code null}. The special case of a list with zero members,
     * results in {@code false}. If all the elements of the list have the same primary time, the result maintains that
     * primary time (otherwise the primary time is lost). The optional keyword "IsTrue" can be used to increase the
     * readability of statements using the any operator.
     * @see 9.12.13
     * @param v
     * @return
     */
    public T any(T v) {
        int falseCount = 0;

        AList list = v.asList();
        Time primaryTime = list.getPrimaryTime();

        for (ADataType o : list) {
            if (o instanceof ABoolean) {
                if (((ABoolean) o).isTrue()) {              // return true if any is true -
                    ABoolean b = ABoolean.TRUE();           // also, maintain primary time if all list elements
                    b.setPrimaryTime(primaryTime);          // have the same primary time
                    return createValue(b);

                } else if (((ABoolean) o).isFalse()) {
                    falseCount ++;
                }
            }
        }

        if (falseCount == list.size()) {
            ABoolean b = ABoolean.FALSE();
            b.setPrimaryTime(primaryTime);
            return createValue(b);                            // return false if all are false, or if list is empty

        } else {
            ANull n = new ANull();
            n.setPrimaryTime(primaryTime);
            return createValue(n);                            // otherwise, return null
        }
    }

    /**
     * The all operator returns {@code true} if all of the items in a list are {@code true}. It returns {@code false}
     * if any of the items is {@code false}. Otherwise it returns {@code null}. The special case of a list with zero
     * members, results in {@code true}. If all the elements of the list have the same primary time, the result
     * maintains that primary time (otherwise the primary time is lost). The optional keyword "AreTrue" can be used
     * to increase the readability of statements using the all operator.
     * @see 9.12.14
     * @param v
     * @return
     */
    public T all(T v) {
        int trueCount = 0;

        AList list = v.asList();
        Time primaryTime = list.getPrimaryTime();

        for (ADataType o : list) {
            if (o instanceof ABoolean) {
                if (((ABoolean) o).isFalse()) {
                    ABoolean b = ABoolean.FALSE();
                    b.setPrimaryTime(primaryTime);
                    return createValue(b);                        // return false if any is false

                } else if (((ABoolean) o).isTrue()) {
                    trueCount ++;
                }
            }
        }

        if (trueCount == list.size()) {
            ABoolean b = ABoolean.TRUE();
            b.setPrimaryTime(primaryTime);
            return createValue(b);                                // return true if all are true, or if list is empty

        } else {
            ANull n = new ANull();
            n.setPrimaryTime(primaryTime);
            return createValue(n);                                // otherwise, return null
        }
    }

    /**
     * The no operator returns {@code true} if all of the items in a list are {@code false}. It returns {@code false}
     * if any of the items is {@code true}. Otherwise it returns {@code null}. The special case of a list with zero
     * members, results in {@code true}. If all the elements of the list have the same primary time, the result
     * maintains that primary time (otherwise the primary time is lost). The optional keyword "IsTrue" can be used to
     * increase the readability of statements using the no operator.
     * @see 9.12.16
     * @param v
     * @return
     */
    public T no(T v) {
        int falseCount = 0;

        AList list = v.asList();
        Time primaryTime = list.getPrimaryTime();

        for (ADataType o : list) {
            if (o instanceof ABoolean) {
                if (((ABoolean) o).isTrue()) {
                    ABoolean b = ABoolean.FALSE();
                    b.setPrimaryTime(primaryTime);
                    return createValue(b);                        // return false if any is true

                } else if (((ABoolean) o).isFalse()) {
                    falseCount ++;
                }
            }
        }

        if (falseCount == list.size()) {
            ABoolean b = ABoolean.TRUE();
            b.setPrimaryTime(primaryTime);
            return createValue(b);                                // return true if all are false, or if list is empty

        } else {
            ANull n = new ANull();
            n.setPrimaryTime(primaryTime);
            return createValue(n);                                // otherwise, return null
        }
    }

    /**
     * The element ([...]) operator is used to select one or more elements from a list, based on ordinal position
     * starting at 1 for the first element. The arguments to "index" are a list expression (to the left of the [...])
     * and a list of integers (inside the [...]). The element operator maintains the primary times of the selected
     * arguments.
     * @see 9.12.18
     * @param vObj
     * @param vIndex
     * @return
     */
    public T element(T vObj, T vIndex) {
        if (vObj.isList()) {
            AList obj = vObj.asList();

            if (vIndex.isList()) {
                AList list = new AList();

                if (vIndex.asList().getListType() == ListType.NUMBER) {
                    for (ADataType adt : vIndex.asList()) {
                        ANumber index = (ANumber) adt;
                        if (index.isWholeNumber())  list.add(obj.get(index.intValue()));
                        else                        list.add(new ANull());
                    }
                }

                return createValue(list);

            } else if (vIndex.isInteger()) {
                return createValue(obj.get(vIndex.asInteger()));
            }
        }

        return NULL();
    }

    /**
     * The earliest operator returns the value with the earliest primary time in a list. If any of the elements do not
     * have primary times, the result is {@code null} (the argument can always be qualified by <strong>where time of it
     * is present</strong>, if this is not desired behavior). If more than one element has the earliest primary time,
     * the first (with the lowest index) of these elements will be returned. If the list is empty, {@code null} is
     * returned. The primary time of the argument is maintained.
     * @see 9.12.17
     * @param v
     * @return
     */
    public T earliest(T v) {
        return createValue(v.asList().earliest());
    }

    /**
     * The latest operator returns the value with the latest primary time in a list. If any of the elements do not have
     * primary times, the result is {@code null} (the argument can always be qualified by where time of it is present, if this
     * is not desired behavior). If the list is empty, {@code null} is returned. If more than one element has the latest
     * primary time, the first (with the lowest index) of these elements will be returned. The primary time of the
     * selected argument is maintained.
     * @see 9.12.16
     * @param v
     * @return
     */
    public T latest(T v) {
        return createValue(v.asList().latest());
    }

    /**
     * The extract characters operator expects a string as its argument. It returns a list of the single characters in
     * the string. If the argument has more than one element, the elements are first concatenated, as for the ||
     * operator (see Section 9.8.1). If the argument is an empty list, the result is the empty list (). The string
     * operator (Section 9.8.3) can be used to put the list back together; and the index operator (Section 9.12.18)
     * can be used to select certain items from the list. The primary times of its arguments are lost.
     * @see 9.12.19
     * @param v
     * @return
     */
    public T extractChars(T v) {
        if (v.isString()) {
            return createValue(v.asString().extractChars());

        } else if (v.isList()) {
            ListType type = v.asList().getListType();
            if (type == ListType.STRING) {
                StringBuilder sb = new StringBuilder();
                for (ADataType obj : v.asList()) {
                    sb.append(obj.toString());
                }
                AString str = new AString(sb.toString());
                return createValue(str.extractChars());

            } else if (type == ListType.EMPTY) {
                return createValue(new AList());
            }
        }

        return NULL();
    }

    /**
     * The seqto operator generates a list of integers in ascending order. Both arguments must be single integers;
     * otherwise {@code null} is returned. If the first argument is greater than the second argument, the result is the
     * empty list. The primary times are lost.
     * @see 9.12.20
     * @param vFrom
     * @param vTo
     * @return
     */
    public T seqto(T vFrom, T vTo) {
        if (vFrom.isInteger() && vTo.isInteger()) {
            int from = vFrom.asInteger();
            int to = vTo.asInteger();

            if (from > to) {
                return createValue(new AList());

            } else {
                AList list = new AList();
                for (int i = from; i <= to; i ++) {
                    list.add(new ANumber(i));
                }
                return createValue(list);
            }

        } else {
            return NULL();
        }
    }

    /**
     * The reverse operator generates a new list with the elements in the reverse order. The primary times of its
     * arguments are maintained.
     * @see 9.12.21
     * @param v
     * @return
     */
    public T reverse(T v) {
        AList list = v.asList().reverse();          // reverse returns a reversed *copy* of the original list
        return createValue(list);
    }

    /**
     * These operators behave similarly to their non-index extracting counterparts with the exception that they return
     * the value of the index of the element that matches the specified criteria rather than the value of the element.
     * These operators do not maintain primary times.
     * @see 9.12.22
     * <br>
     * The index <strong>latest</strong> operator returns the index of the element with the latest primary time in a
     * list. If any of the elements do not have primary times, the result is null (the argument can always be qualified
     * by where time of it is present, if this is not desired behavior). If the list is empty, null is returned. The
     * primary time of the selected argument is maintained.
     * @see 9.12.22.1
     * <br>
     * The index <strong>earliest</strong> operator returns the index of the element with the earliest primary time in
     * a list. If any of the elements do not have primary times, the result is null (the argument can always be
     * qualified by where time of it is present, if this is not desired behavior). If the list is empty, null is
     * returned. The primary time of the argument is maintained.
     * @see 9.12.22.2
     * <br>
     * The index <strong>minimum</strong> operator has one synonym: index min. It returns the index of the element
     * with the smallest value in a homogeneous list of an ordered type (that is, all numbers, all times, all durations,
     * or all strings), using the <= operator (see Section 9.5.4). If there is a tie, it selects the element with the
     * latest primary time.
     * @see 9.12.22.3
     * <br>
     * The index <strong>maximum</strong> operator has one synonym: index max. It returns the largest value in a
     * homogeneous list of an ordered type, using the >= operator (see Section 9.5.6). If there is a tie, it selects
     * the element with the latest primary time. The primary time of the selected argument is maintained.
     * @see 9.12.22.4
     * @param vList
     * @param indexTypeStr
     * @return
     */
    public T index(T vList, String indexTypeStr) {
        AList list = vList.asList();
        if (list.isSortable()) {
            IndexType it = IndexType.valueOf(indexTypeStr.toUpperCase());

            switch (it) {
                case EARLIEST:  return createValue(list.earliestIndex());
                case LATEST:    return createValue(list.latestIndex());
                case MINIMUM:
                case MIN:       return createValue(list.minimumIndex());
                case MAXIMUM:
                case MAX:       return createValue(list.maximumIndex());
            }

            log.warn("index : invalid index type '" + indexTypeStr + "'");
        }

        return NULL();
    }

    /**
     * These operators behave similarly to their non-index extracting counterparts with the exception that they return
     * the value of the index of the element that matches the specified criteria rather than the element itself. These
     * operators do not maintain primary times.
     * @see 9.14.13
     * <br>
     * The index minimum ... from operator has one synonym: index min ... from. It expects a number (call it N) as its
     * first argument and a homogeneous list of an ordered type as its second argument. It returns a list with the
     * indices of the N smallest items from the argument list, in the same order that they are in the second argument,
     * and with any duplicates preserved. The result is null if N is not a non-negative integer. If there are not
     * enough items in the argument list, then as many indices as possible are returned. If there is a tie, then it
     * selects the latest of those elements that have a primary time. The primary times of the argument are not
     * maintained.
     * @see 9.14.13.1
     * <br>
     * The index maximum ... from operator has one synonym: index max ... from. It expects a number (call it N) as its
     * first argument and a homogeneous list of an ordered type as its second argument. It returns a list with the
     * indices of the N largest items from the argument list, in the same order that they are in the second argument,
     * and with any duplicates preserved. The result is null if N is not a non-negative integer. If there are not
     * enough items in the argument list, then as many indices as possible are returned. If there is a tie, then it
     * selects the latest of those elements that have a primary time. The primary times of the argument are not
     * maintained.
     * @see 9.14.13.2
     * @param vNum
     * @param vList
     * @param indexTypeStr
     * @return
     */
    public T indexFrom(T vNum, T vList, String indexTypeStr) {
        AList list = vList.asList();
        if (vNum.isInteger() && vNum.asInteger() >= 0 && list.isSortable()) {
            IndexType it = IndexType.valueOf(indexTypeStr.toUpperCase());
            int num = vNum.asInteger();

            switch (it) {
                case EARLIEST:  return createValue(list.earliestIndex(num));        // UNDOCUMENTED
                case LATEST:    return createValue(list.latestIndex(num));          // UNDOCUMENTED
                case MINIMUM:
                case MIN:       return createValue(list.minimumIndex(num));
                case MAXIMUM:
                case MAX:       return createValue(list.maximumIndex(num));
            }

            log.warn("indexFrom : invalid index type '" + indexTypeStr + "'");
        }

        return NULL();
    }

    /**
     * The <strong>index of ... from</strong> operator expects an arbitrary data value as its first argument and a
     * list as its second argument. It returns a list containing the indices of the occurrences of the given data
     * value within the provided list. If there is more than one occurrence all occurrences are returned. The result
     * is {@code null} if no such value is found in the list or in case of invalid parameters. The primary times of
     * the arguments are not maintained.
     * @see 9.13.4
     * @param vObj
     * @param vList
     * @return
     */
    public T indexOfFrom(T vObj, T vList) {
        AList list = vList.asList().getAllIndexesFor(vObj.getBackingObject());
        return list.isEmpty() ? NULL() : createValue(list);
    }

    /**
     * The <strong>after</strong> operator is equivalent to addition between a {@link Duration} and a {@link Time}.
     * @see 9.10.1, 9.10.4
     * @param vDur
     * @param vTime
     * @return
     */
    @SuppressWarnings("unchecked")
    public T after(T vDur, T vTime) {
        Integer size = getListSize(vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doAfter(
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime)));

        } else {
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doAfter(durList.get(i), timeList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doAfter(ADataType dur, ADataType time) {
        if (dur instanceof Duration && time instanceof Time) {
            return ((Time) time).add((Duration) dur);
        }

        return new ANull();
    }

    /**
     * The <strong>before</strong> operator is equivalent to the subtraction of a {@link Duration} from a {@link Time}.
     * @see 9.10.2
     * @param vDur
     * @param vTime
     * @return
     */
    @SuppressWarnings("unchecked")
    public T before(T vDur, T vTime) {
        Integer size = getListSize(vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doBefore(
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime)));

        } else {
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doBefore(durList.get(i), timeList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doBefore(ADataType dur, ADataType time) {
        if (dur instanceof Duration && time instanceof Time) {
            return ((Time) time).subtract((Duration) dur);
        }

        return new ANull();
    }

    /**
     * The <strong>ago</strong> operator subtracts a {@link Duration} from {@code now} ({@link #now}), resulting
     * in a {@link Time}.
     * @see 9.10.3
     * @param v
     * @return
     */
    public T ago(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doAgo(obj));
            }
            return createValue(list);

        } else {
            return createValue(doAgo(v.getBackingObject()));
        }
    }

    private ADataType doAgo(ADataType obj) {
        return obj instanceof Duration ?
                getNow().subtract((Duration) obj) :
                new ANull();
    }

    /**
     * The <strong>time of day</strong> operator extracts the {@link TimeOfDay} from a {@link Time}. Primary times are
     * lost.
     * @see 8.11, 9.10.5
     * @param v
     * @return
     */
    public T timeOfDayFunc(T v) {                               // do NOT preserve primary times!  see 9.10.5
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doTimeOfDayFunc(obj));
            }
            return createValue(list);

        } else {
            return createValue(doTimeOfDayFunc(v.getBackingObject()));
        }
    }

    private ADataType doTimeOfDayFunc(ADataType obj) {       // do NOT preserve primary times!  see 9.10.5
        return obj instanceof Time ?
                new TimeOfDay((Time) obj) :
                new ANull();
    }

    /**
     * The <strong>time</strong> operator returns the primary time (that is, time of occurrence) of the result of a
     * value derived from a query (see Section 8.9).
     * <br>
     * {@code null} is returned if it is used on data that has no primary time. The result of time preserves the
     * primary time of its argument; so <strong>time time x</strong> is equivalent to <strong>time x</strong>.
     * @see {@link Time}, 9.17.1
     * @param v
     * @return
     */
    public T timeFunc(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doTimeFunc(obj));
            }
            return createValue(list);

        } else {
            return createValue(doTimeFunc(v.getBackingObject()));
        }
    }

    private ADataType doTimeFunc(ADataType obj) {
        Time t = obj.getPrimaryTime();

        if (t != null) {
            t.setPrimaryTime(t.copy());             // preserve primary time of the argument
            return t;
        }

        return new ANull();
    }

    /**
     * The <strong>attime</strong> operator constructs a {@link Time} value from two {@link Time} and {@link TimeOfDay}
     * arguments.
     * <br>
     * The result consists of the date of the time arguments and the time of the time-of-day argument. {@code null} is
     * returned if it is used with other arguments than time and time-of-day. The primary times are lost.
     * @param vTime
     * @param vTimeOfDay
     * @return
     */
    public T atTime(T vTime, T vTimeOfDay) {
        if (vTime.isTime() && vTimeOfDay.isTimeOfDay()) {
            Time t = vTime.asTime().copy();
            TimeOfDay tod = vTimeOfDay.asTimeOfDay();

            t.setPrimaryTime(null);                             // primary times are lost

            t.setHour(tod.getHour());
            t.setMinute(tod.getMinute());
            t.setSecond(tod.getSecond());

            return createValue(t);
        }

        return NULL();
    }

    /**
     * The <strong>as time</strong> operator attempts to convert a given {@link AString} to a {@link Time}.
     * <br>
     * If conversion to a time is possible, the time is returned, otherwise {@code null} is returned. The primary time
     * of the argument is preserved. The common use for this is to convert a string containing a valid date/time format
     * as described in ISO 8601:1988(E), e.g., "1999-12-12" or "1999-12-12T13:41", into a time.
     * @see {@link Time}, 9.17.4
     * @param v
     * @return
     */
    public T asTime(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doAsTime(obj));
            }
            return createValue(list);

        } else {
            return createValue(doAsTime(v.getBackingObject()));
        }
    }

    private ADataType doAsTime(ADataType obj) {
        if (obj instanceof Time) {
            return obj.copy();                      // copy constructor preserves primary time

        } else if (obj instanceof AString) {
            try {
                Time t = TimeUtil.parseTime(obj.toString());
                t.setPrimaryTime(obj.getPrimaryTime());             // preserve primary time
                return t;

            } catch (ParseException pe) {
                // handle silently
            }
        }

        ANull rval = new ANull();
        rval.setPrimaryTime(obj.getPrimaryTime());                  // preserve primary time
        return rval;
    }

    /**
     * The <strong>day of week</strong> operator returns a positive integer from 1 to 7 that represents the day of the
     * week of a specified time (Section 8.12). The number 1 corresponds to Monday, 2 corresponds to Tuesday, etc. The
     * number 7 represents Sunday.
     * @see {@link DayOfWeek}, 9.10.6
     * @param v
     * @return
     */
    public T dayOfWeekFunc(T v) {
        if (v.isTime()) {
            DayOfWeek dow = v.asTime().getDayOfWeek();
            return createValue(new ANumber(dow.intValue()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>extract</strong> operator extracts a {@link TemporalUnit} from a {@link Time} or {@link TimeOfDay}.
     * @see 9.10.7 - 9.10.12
     * @param v
     * @param temporalUnitStr
     * @return
     */
    public T extract(T v, String temporalUnitStr) {
        if (v.isTime()) {
            TemporalUnit tu = TemporalUnit.valueOf(temporalUnitStr.toUpperCase());

            switch (tu) {
                case YEAR:      return createValue(v.asTime().getYear());
                case MONTH:     return createValue(v.asTime().getMonth());
                case DAY:       return createValue(v.asTime().getDay());
                case HOUR:      return createValue(v.asTime().getHour());
                case MINUTE:    return createValue(v.asTime().getMinute());
                case SECOND:    return createValue(v.asTime().getSecond());
            }

            log.warn("extract : invalid temporal unit for Time '" + temporalUnitStr + "'");

        } else if (v.isTimeOfDay()) {
            TemporalUnit tu = TemporalUnit.valueOf(temporalUnitStr.toUpperCase());

            switch (tu) {
                case HOUR:      return createValue(v.asTimeOfDay().getHour());
                case MINUTE:    return createValue(v.asTimeOfDay().getMinute());
                case SECOND:    return createValue(v.asTimeOfDay().getSecond());
            }

            log.warn("extract : invalid temporal unit for TimeOfDay '" + temporalUnitStr + "'");
        }

        return NULL();
    }

    /**
     * The <strong>replace {@link TemporalUnit} of ... with</strong> operator allows the replacement of the specified
     * temporal-unit part of a {@link Time}. The result of the operation preserves the primary time of the first
     * argument. The numeric second argument must evaluate to a positive integer valid for the specified temporal-unit,
     * otherwise {@code null} is returned. Any fractional part of the second argument will be removed before evaluation
     * (except in the case where <strong>second</strong> is specified as the temporal-unit, in which case the fractional
     * part is preserved).
     * <ul>
     *     <li>Year: 1800 <= year</li>
     *     <li>Month: 1 <= month <= 12</li>
     *     <li>Day: 1 <= day <= # of days in the existing month of the first operator</li>
     *     <li>Hour: 0 <= hour <= 23</li>
     *     <li>Minute: 0 <= minute <= 59</li>
     *     <li>Second: 0 <= second <= 59</li>
     * </ul>
     * @see 9.10.13 - 9.10.18
     * @param vTime
     * @param vNumber
     * @param temporalUnitStr
     * @return
     */
    @SuppressWarnings("unchecked")
    public T replace(T vTime, T vNumber, String temporalUnitStr) {
        TemporalUnit tu = TemporalUnit.valueOf(temporalUnitStr.toUpperCase());

        Integer size = getListSize(vTime, vNumber);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doReplace(
                    getFirstListItemOrSelf(vTime),
                    getFirstListItemOrSelf(vNumber),
                    tu));

        } else {
            AList timeList = replicateObjectToList(vTime, size);
            AList numList = replicateObjectToList(vNumber, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doReplace(timeList.get(i), numList.get(i), tu));
            }
            return createValue(list);
        }
    }

    private ADataType doReplace(ADataType time, ADataType num, TemporalUnit tu) {
        if (time instanceof Time && num instanceof ANumber) {
            Time t = new Time((Time) time);
            ANumber n = (ANumber) num;
            int i = n.intValue();

            if      (tu == TemporalUnit.YEAR    && i >= 1800)                           t.setYear(n);
            else if (tu == TemporalUnit.MONTH   && i >= 1 && i <= 12)                   t.setMonth(n);
            else if (tu == TemporalUnit.DAY     && i >= 1 && i <= t.getDaysInMonth())   t.setDay(n);
            else if (tu == TemporalUnit.HOUR    && i >= 0 && i <= 23)                   t.setHour(n);
            else if (tu == TemporalUnit.MINUTE  && i >= 0 && i <= 59)                   t.setMinute(n);
            else if (tu == TemporalUnit.SECOND  && i >= 0 && i <= 59)                   t.setSecond(n);
            else {
                ANull an = new ANull();
                an.setPrimaryTime(t.getPrimaryTime());
                return an;
            }

            return t;
        }

        return new ANull();
    }

    /**
     * Unary + has no effect on its argument if it is of a valid type.  May operate on {@link ANumber} and
     * {@link Duration} data types.  Generates {@code null} if type mismatch.
     * @see 9.9.2
     * @param v
     * @return
     */
    public T unaryPlus(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doUnaryPlus(obj));
            }
            return createValue(list);

        } else {
            return createValue(doUnaryPlus(v.getBackingObject()));
        }
    }

    private ADataType doUnaryPlus(ADataType obj) {
        return obj instanceof ANumber || obj instanceof Duration ?
                obj :
                new ANull();
    }

    /**
     * Unary - is used for arithmetic negation; this is how one makes negative number constants. Underflow or
     * overflow results in null. One cannot put two arithmetic operators together, so the following expression is
     * illegal: 3 + -4. Instead one must use one of these: 3 + (-4), 3 - 4, or -4 + 3.  May operate on {@link ANumber}
     * and {@link Duration} data types.  Generates {@code null} if type mismatch.
     * @see 9.9.4
     * @param v
     * @return
     */
    public T unaryMinus(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doUnaryMinus(obj));
            }
            return createValue(list);

        } else {
            return createValue(doUnaryMinus(v.getBackingObject()));
        }
    }

    private ADataType doUnaryMinus(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).multiply(-1);

        } else if (obj instanceof Duration) {
            return ((Duration) obj).multiply(-1);
        }

        return new ANull();
    }

    /**
     * Binary </strong>+</strong> (addition) adds the left and right arguments. It can perform simple addition, add
     * two durations, or increment a time by a duration. Underflow or overflow results in {@code null}.
     * @see 9.9.1
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T add(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doAdd(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doAdd(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doAdd(ADataType left, ADataType right) {
        if (left instanceof ANumber && right instanceof ANumber) {
            return ((ANumber) left).add((ANumber) right);

        } else if (left instanceof Duration && right instanceof Duration) {
            return ((Duration) left).add((Duration) right);

        } else if (left instanceof Time && right instanceof Duration) {
            return ((Time) left).add((Duration) right);

        } else if (left instanceof Duration && right instanceof Time) {
            return ((Time) right).add((Duration) left);
        }

        return new ANull();
    }

    /**
     * Binary </strong>-</strong> (subtraction) subtracts the right argument from the left. It can perform numeric
     * subtraction, subtract two durations, decrement a time by a duration, or find the duration between two times.
     * Underflow or overflow results in {@code null}. In writing expressions, care must be taken that the subtraction
     * operator is not confused with the "-" in time constant (Section 7.1.5). Any ambiguity is resolved in favor of
     * time constants.
     * @see 9.9.3
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T subtract(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doSubtract(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doSubtract(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doSubtract(ADataType left, ADataType right) {
        if (left instanceof ANumber && right instanceof ANumber) {
            return ((ANumber) left).subtract((ANumber) right);

        } else if (left instanceof Duration && right instanceof Duration) {
            return ((Duration) left).subtract((Duration) right);

        } else if (left instanceof Time && right instanceof Duration) {     // generate time - spec 8.5.2.2
            return ((Time) left).subtract((Duration) right);

        } else if (left instanceof Time && right instanceof Time) {         // generate duration:seconds - spec 8.5.2.1
            return ((Time) left).subtract((Time) right);
        }

        return new ANull();
    }

    /**
     * The </strong>*</strong> operator (multiplication) multiplies the left and right arguments. Underflow or overflow
     * results in {@code null}. It can perform numeric multiplication or multiply a duration by a number.
     * @see 9.9.5
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T multiply(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doMultiply(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doMultiply(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doMultiply(ADataType left, ADataType right) {
        if (left instanceof ANumber && right instanceof ANumber) {
            return ((ANumber) left).multiply((ANumber) right);

        } else if (left instanceof Duration && right instanceof ANumber) {
            return ((Duration) left).multiply((ANumber) right);

        } else if (left instanceof ANumber && right instanceof Duration) {
            return ((Duration) right).multiply((ANumber) left);
        }

        return new ANull();
    }

    /**
     * The </strong>/</strong> operator (division) divides the left argument by the right one. It can perform numeric
     * division, divide a duration by a number, or find the ratio between two durations. {@code null} results from
     * division by zero, underflow, or overflow. Duration unit conversion can be done with the / operator (e.g., ... /
     * 1 year turns any duration into years).
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T divide(T vLeft, T vRight) {
        // todo : implement "/ 1 <temporal-unit>" duration unit conversion

        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doDivide(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doDivide(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doDivide(ADataType left, ADataType right) {
        if (left instanceof ANumber && right instanceof ANumber) {
            return ((ANumber) right).doubleValue() == 0 ?           // return null if attempting to divide by zero
                    new ANull() :
                    ((ANumber) left).divide((ANumber) right);

        } else if (left instanceof Duration && right instanceof ANumber) {
            return ((ANumber) right).doubleValue() == 0 ?           // return null if attempting to divide by zero
                    new ANull() :
                    ((Duration) left).divide((ANumber) right);

        } else if (left instanceof Duration && right instanceof Duration) {
            return ((Duration) right).getSeconds() == 0 ?           // return null if attempting to divide by zero
                    new ANull() :
                    ((Duration) left).divide((Duration) right);
        }

        return new ANull();
    }

    /**
     * The <strong>**</strong> operator (exponentiation) raises the left argument to the power of the right argument.
     * @see 9.9.7
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T raiseToPower(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doRaiseToPower(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doRaiseToPower(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    private ADataType doRaiseToPower(ADataType left, ADataType right) {
        if (left instanceof ANumber && right instanceof ANumber) {
            return ((ANumber) left).raiseToPower((ANumber) right);
        }

        return new ANull();
    }

    /**
     * The <strong>arccos</strong> operator calculates the arc-cosine (expressed in radians) of its argument.
     * @see 9.16.1
     * @param v
     * @return
     */
    public T arccos(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doArccos(obj));
            }
            return createValue(list);
            
        } else {
            return createValue(doArccos(v.getBackingObject()));
        }
    }

    private ADataType doArccos(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).arccos();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }


    /**
     * The <strong>arcsin</strong> operator calculates the arc-sine (expressed in radians) of its argument.
     * @see 9.16.2
     * @param v
     * @return
     */
    public T arcsin(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doArcsin(obj));
            }
            return createValue(list);

        } else {
            return createValue(doArcsin(v.getBackingObject()));
        }
    }

    private ADataType doArcsin(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).arcsin();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>arctan</strong> operator calculates the arc-tangent (expressed in radians) of its argument.
     * @see 9.16.3
     * @param v
     * @return
     */
    public T arctan(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doArctan(obj));
            }
            return createValue(list);

        } else {
            return createValue(doArctan(v.getBackingObject()));
        }
    }

    private ADataType doArctan(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).arctan();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>cosine</strong> operator has one synonym: cos. It calculates the cosine of its argument (expressed
     * in radians).
     * @see 9.16.4
     * @param v
     * @return
     */
    public T cosine(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doCosine(obj));
            }
            return createValue(list);

        } else {
            return createValue(doCosine(v.getBackingObject()));
        }
    }

    private ADataType doCosine(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).cosine();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>sine</strong> operator has one synonym: sin. It calculates the sine of its argument (expressed in
     * radians).
     * @see 9.16.5
     * @param v
     * @return
     */
    public T sine(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doSine(obj));
            }
            return createValue(list);

        } else {
            return createValue(doSine(v.getBackingObject()));
        }
    }

    private ADataType doSine(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).sine();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>tangent</strong> operator has one synonym: tan. It calculates the tangent of its argument (expressed
     * in radians).
     * @see 9.16.6
     * @param v
     * @return
     */
    public T tangent(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doTangent(obj));
            }
            return createValue(list);

        } else {
            return createValue(doTangent(v.getBackingObject()));
        }
    }

    private ADataType doTangent(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).tangent();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>exp</strong> operator raises mathematical e to the power of its argument
     * @see 9.16.7
     * @param v
     * @return
     */
    public T exp(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doExp(obj));
            }
            return createValue(list);

        } else {
            return createValue(doExp(v.getBackingObject()));
        }
    }

    private ADataType doExp(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).exp();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>log</strong> operator returns the natural logarithm of its argument.
     * @see 9.16.8
     * @param v
     * @return
     */
    public T log(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doLog(obj));
            }
            return createValue(list);

        } else {
            return createValue(doLog(v.getBackingObject()));
        }
    }

    private ADataType doLog(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).log();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>log10</strong> operator returns the base 10 logarithm of its argument.
     * @see 9.16.9
     * @param v
     * @return
     */
    public T log10(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doLog10(obj));
            }
            return createValue(list);

        } else {
            return createValue(doLog10(v.getBackingObject()));
        }
    }

    private ADataType doLog10(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).log10();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>floor</strong> operator is synonymous with <strong>int</strong>. It returns the largest integer less
     * than or equal to its argument (truncates towards negative infinity).
     * @see 9.16.10, 9.16.11
     * @param v
     * @return
     */
    public T floor(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doFloor(obj));
            }
            return createValue(list);

        } else {
            return createValue(doFloor(v.getBackingObject()));
        }
    }

    private ADataType doFloor(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).floor();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>ceiling</strong> operator returns the smallest integer greater than or equal to its argument
     * (truncates towards positive infinity).
     * @see 9.16.12
     * @param v
     * @return
     */
    public T ceiling(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doCeiling(obj));
            }
            return createValue(list);

        } else {
            return createValue(doCeiling(v.getBackingObject()));
        }
    }

    private ADataType doCeiling(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).ceiling();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>truncate</strong> operator removes any fractional part of a number (truncates towards zero).
     * @see 9.16.13
     * @param v
     * @return
     */
    public T truncate(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doTruncate(obj));
            }
            return createValue(list);

        } else {
            return createValue(doTruncate(v.getBackingObject()));
        }
    }

    private ADataType doTruncate(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).truncate();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>round</strong> operator rounds a number to an integer.
     * <br>
     * For positive numbers: If the fractional portion of the operand is greater than or equal to 0.5, the operator
     * rounds to the next highest integer. Fractional portions less than 0.5 round to the next lowest integer.
     * <br>
     * For negative numbers: If the absolute value of the fractional portion of the operand is greater than or equal
     * 0.5, the operator rounds to the next lower negative integer. Fractional portions with absolute values less
     * than 0.5 round to the next highest integer.
     * @see 9.16.14
     * @param v
     * @return
     */
    public T round(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doRound(obj));
            }
            return createValue(list);

        } else {
            return createValue(doRound(v.getBackingObject()));
        }
    }

    private ADataType doRound(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).round();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>abs</strong> operator returns absolute value of its argument.
     * @see 9.16.15
     * @param v
     * @return
     */
    public T abs(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doAbs(obj));
            }
            return createValue(list);

        } else {
            return createValue(doAbs(v.getBackingObject()));
        }
    }

    private ADataType doAbs(ADataType obj) {
        if (obj instanceof ANumber) {
            return ((ANumber) obj).abs();

        } else {
            ANull rval = new ANull();
            rval.setPrimaryTime(obj.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>sqrt</strong> operator returns the square root of its argument. Because imaginary numbers are not
     * supported, the square root of a negative number results in {@code null}.
     * @see 9.16.16
     * @param v
     * @return
     */
    public T sqrt(T v) {
        if (v.isList()) {
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doSqrt(obj));
            }
            return createValue(list);

        } else {
            return createValue(doSqrt(v.getBackingObject()));
        }
    }

    private ADataType doSqrt(ADataType obj) {
        if (obj instanceof ANumber) {
            ANumber n = (ANumber) obj;
            if (n.isPositive()) return n.sqrt();
        }
        
        ANull rval = new ANull();
        rval.setPrimaryTime(obj.getPrimaryTime());              // preserve primary time
        return rval;
    }
    
    /**
     * The </strong>as number</strong> operator attempts to convert a string or Boolean to a number.
     * <br>
     * If conversion to a number is possible, the number is returned, otherwise {@code null} is returned. The primary
     * time of the argument is preserved. The usual use for this will be to convert a string which contains a valid
     * number representation i.e. "123" into the represented number. If the string does not contain a valid number then
     * the result will be {@code null}. Boolean values are translated at follows: Boolean {@code true} is represented
     * at 1 and Boolean {@code false} is represented at 0.
     * @see 9.16.17
     * @param v
     * @return
     */
    public T asNumber(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doAsNumber(obj));
            }
            return createValue(list);

        } else {
            return createValue(doAsNumber(v.getBackingObject()));
        }
    }

    private ADataType doAsNumber(ADataType obj) {
        if (obj instanceof ANumber) {
            return obj.copy();                          // copy constructor preserves primary time

        } else if (obj instanceof ABoolean) {
            ABoolean b = (ABoolean) obj;
            if (b.isTrue() || b.isFalse()) {
                ANumber rval = new ANumber(b.asInteger());
                rval.setPrimaryTime(b.getPrimaryTime());                    // preserve primary time
                return rval;
            }

        } else if (obj instanceof AString) {
            try {
                ANumber rval = NumberUtil.parseNumber(obj.toString());
                rval.setPrimaryTime(obj.getPrimaryTime());                  // preserve primary time
                return rval;

            } catch (NumberFormatException e) {
                // handle silently
            }
        }

        ANull rval = new ANull();
        rval.setPrimaryTime(obj.getPrimaryTime());                          // preserve primary time
        return rval;
    }

    /**
     * The <strong>and</strong> operator performs the logical conjunction of its two arguments.
     * <br>
     * If either argument is {@code false} (even if the other is not Boolean), the result is {@code false}. If both
     * arguments are {@code true}, the result is {@code true}. Otherwise the result is {@code null}.
     * @see 9.4.2
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T and(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.and(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.and(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong>or</strong> operator performs the logical disjunction of its two arguments.
     * <br>
     * If either argument is {@code true} (even if the other is not Boolean), the result is {@code true}. If both
     * arguments are {@code false}, the result is {@code false}. Otherwise the result is {@code null}.
     * @see 9.4.1
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T or(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.or(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.or(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong>not</strong> operator performs the logical negation of its argument.
     * <br>
     * Thus {@code true} becomes {@code false}, {@code false} becomes {@code true}, and anything else becomes
     * {@code null}.
     * @see 9.4.3
     * @param v
     * @return
     */
    public T not(T v) {
        if (v.isList()) {                           // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doNot(ObjectUtil.asBoolean(obj)));
            }
            return createValue(list);

        } else {
            return createValue(doNot(v.asBoolean()));
        }
    }

    private ADataType doNot(ABoolean b) {
        if (b.isUnknown()) {
            ANull rval = new ANull();
            rval.setPrimaryTime(b.getPrimaryTime());
            return rval;

        } else {
            ABoolean rval = b.asBoolean() ? new ABoolean(false) : new ABoolean(true);
            rval.setPrimaryTime(b.getPrimaryTime());
            return rval;
        }
    }

    /**
     * The <strong>=</strong> operator has two synonyms: <strong>eq</strong> and <strong>is equal</strong>.
     * <br>
     * It checks for equality, returning {@code true} or {@code false}. If the arguments are of different types,
     * {@code false} is returned. If an argument is {@code null}, then {@code null} is always returned. Primary times
     * are not used in determining equality; the primary time of the result is determined by the rules in
     * Section 9.1.4.
     * @see 9.5.1
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isEqual(T vLeft, T vRight) {
        // todo : consider 9.1.4 re: primary time of the result

        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isEqualTo(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isEqualTo(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong><></strong> operator has two synonyms: <strong>ne</strong> and <strong>is not equal</strong>.
     * <br>
     * It checks for inequality, returning {@code true} or {@code false}. If the arguments are of different types,
     * {@code true} is returned. If an argument is {@code null}, then {@code null} is returned.
     * @see 9.5.2
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isNotEqual(T vLeft, T vRight) {
        // todo : consider 9.1.4 re: primary time of the result

        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isNotEqualTo(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isNotEqualTo(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong><</strong> operator has three synonyms: <strong>lt</strong>, <strong>is less than</strong>, and
     * <strong>is not greater than or equal</strong>.
     * <br>
     * It is used on ordered types; if the types do not match, {@code null} is returned.
     * @see 9.5.3
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isLessThan(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isLessThan(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isLessThan(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong><=</strong> operator has three synonyms: <strong>le</strong>, <strong>is less than or equal</strong>,
     * and <strong>is not greater than</strong>.
     * <br>
     * It is used on ordered types; if the types do not match, {@code null} is returned.
     * @see 9.5.4
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isLessThanOrEqual(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isLessThanOrEqualTo(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isLessThanOrEqualTo(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong>></strong> operator has three synonyms: <strong>gt</strong>, <strong>is greater than</strong>, and
     * <strong>is not less than or equal</strong>.
     * <br>
     * It is used on ordered types; if the types do not match, {@code null} is returned.
     * @see 9.5.5
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isGreaterThan(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isGreaterThan(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isGreaterThan(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong>>=</strong> operator has three synonyms: <strong>ge</strong>, <strong>is greater than or
     * equal</strong>, and <strong>is not less than</strong>.
     * <br>
     * It is used on ordered types; if the types do not match, {@code null} is returned.
     * @see 9.5.6
     * @param vLeft
     * @param vRight
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isGreaterThanOrEqual(T vLeft, T vRight) {
        Integer size = getListSize(vLeft, vRight);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(ObjectUtil.isGreaterThanOrEqualTo(
                    getFirstListItemOrSelf(vLeft),
                    getFirstListItemOrSelf(vRight)));

        } else {
            AList leftList = replicateObjectToList(vLeft, size);
            AList rightList = replicateObjectToList(vRight, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(ObjectUtil.isGreaterThanOrEqualTo(leftList.get(i), rightList.get(i)));
            }
            return createValue(list);
        }
    }

    /**
     * The <strong>is within ... to</strong> operator checks whether the first argument is within the range specified
     * by the second and third arguments; the range is inclusive.
     * <br>
     * It is used on ordered types; if the types do not match, {@code null} is returned. When used with time-of-day
     * arguments, the order of the right and middle argument may be relevant, as the specified time frame may span over
     * midnight.
     * @see 9.6.6
     * @param vTest
     * @param vLow
     * @param vHigh
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinTo(T vTest, T vLow, T vHigh, boolean flip) {
        Integer size = getListSize(vTest, vLow, vHigh);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinTo(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vLow),
                    getFirstListItemOrSelf(vHigh),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList lowList = replicateObjectToList(vLow, size);
            AList highList = replicateObjectToList(vHigh, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinTo(testList.get(i), lowList.get(i), highList.get(i), flip));
            }
            return createValue(list);
        }
    }

    @SuppressWarnings("unchecked")
    private ADataType doIsWithinTo(ADataType test, ADataType low, ADataType high, boolean flip) {
        if (test.isComparable() && test.isLike(low) && test.isLike(high)) {
            Comparable cTest = (Comparable) test;
            Comparable cLow = (Comparable) low;
            Comparable cHigh = (Comparable) high;

            boolean b = cTest.compareTo(cLow) >= 0 && cTest.compareTo(cHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is within ... preceding</strong> operator checks whether the left argument is within the inclusive
     * time period defined by the second two arguments (from the third argument minus the second to the third).
     * @see 9.6.7
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinPreceding(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinPreceding(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinPreceding(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsWithinPreceding(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test instanceof Time && dur instanceof Duration && time instanceof Time) {
            Time tTest = (Time) test;
            Time tHigh = (Time) time;
            Time tLow = tHigh.subtract((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is within ... following</strong> operator checks whether the left argument is within the inclusive
     * time period defined by the second two arguments (from the third argument to the third plus the second).
     * @see 9.6.8
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinFollowing(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinFollowing(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinFollowing(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsWithinFollowing(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test instanceof Time && dur instanceof Duration && time instanceof Time) {
            Time tTest = (Time) test;
            Time tLow = (Time) time;
            Time tHigh = tLow.add((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is within ... surrounding</strong> operator checks whether the left argument is within the inclusive
     * time period defined by the second two arguments (from the third argument minus the second to the third plus the
     * second).
     * @see 9.6.9
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinSurrounding(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinSurrounding(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinSurrounding(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsWithinSurrounding(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test instanceof Time && dur instanceof Duration && time instanceof Time) {
            Time tTest = (Time) test;
            Time tLow = ((Time) time).subtract((Duration) dur);
            Time tHigh = ((Time) time).add((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is within past</strong> checks whether the left argument is within the time period defined by the
     * right argument (<strong>now</strong> minus the right argument to <strong>now</strong>).
     * @see {@link #now}, 9.6.10
     * @param vTest
     * @param vDur
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinPast(T vTest, T vDur, boolean flip) {
        Integer size = getListSize(vTest, vDur);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinPast(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinPast(testList.get(i), durList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsWithinPast(ADataType test, ADataType dur, boolean flip) {
        if (test instanceof Time && dur instanceof Duration) {
            Time tTest = (Time) test;
            Time tHigh = getNow();
            Time tLow = tHigh.subtract((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is within same day as</strong> operator checks whether the left argument is on the same day as the
     * second argument.
     * @see 9.6.11
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isWithinSameDay(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsWithinSameDay(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsWithinSameDay(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsWithinSameDay(ADataType test, ADataType time, boolean flip) {
        if (test instanceof Time && time instanceof Time) {
            Time t1 = (Time) test;
            Time t2 = (Time) time;

            boolean b = t1.getYear().hasValue(t2.getYear()) &&
                    t1.getMonth().hasValue(t2.getMonth()) &&
                    t1.getDay().hasValue(t2.getDay());

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is before</strong> operator checks whether the left argument is before the second argument; it is
     * not inclusive.
     * @see 9.6.12
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isBefore(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsBefore(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsBefore(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsBefore(ADataType test, ADataType time, boolean flip) {
        if (test instanceof Time && time instanceof Time) {
            Time tTest = (Time) test;
            Time tTime = (Time) time;

            boolean b = tTest.compareTo(tTime) < 0;     // not inclusive!

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is after</strong> operator checks whether the left argument is after the second argument; it is
     * not inclusive.
     * @see 9.6.13
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T isAfter(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doIsAfter(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doIsAfter(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doIsAfter(ADataType test, ADataType time, boolean flip) {
        if (test instanceof Time && time instanceof Time) {
            Time tTest = (Time) test;
            Time tTime = (Time) time;

            boolean b = tTest.compareTo(tTime) > 0;     // not inclusive!

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>is in</strong> operator does not follow the default list handling. It checks for membership of the
     * left argument in the right argument, which is usually a list.
     * <br>
     * If the left argument is a list, then a list results; if the left argument is a single item, then a single item
     * results. If the right argument is a single item, then it is treated as a list of length one. If the first
     * operand is {@code null}, {@code true} is always returned. If the second operand is {@code null} then {@code null}
     * is returned, except the first one is also {@code null}. Primary times are retained only if they match (that is,
     * the = operator is used for determining membership, except that {@code null} will match).
     * @see 9.6.14, 9.6.24
     * @param vLeft
     * @param vRight
     * @return
     */
    public T isIn(T vLeft, T vRight, boolean flip) {
        if      (vLeft.isNull())     return flip ? FALSE() : TRUE();
        else if (vRight.isNull())    return NULL();

        AList list = new AList();
        for (ADataType test : vLeft.asList()) {
            boolean found = false;
            boolean retainPrimaryTime = false;

            for (ADataType obj : vRight.asList()) {
                if (obj.hasValue(test)) {
                    found = true;

                    // todo : what if there are two items in the list with the same value but different primary times?
                    // - and the first encountered item doesn't have the same primary time, but the second one does?

                    retainPrimaryTime = obj.hasPrimaryTime() && test.hasPrimaryTime() &&
                                        obj.getPrimaryTime().equals(test.getPrimaryTime());
                    break;
                }
            }

            ABoolean b = new ABoolean(flip ? ! found : found);
            if (retainPrimaryTime) b.setPrimaryTime(test.getPrimaryTime());
            list.add(b);
        }

        return vLeft.isList() ?                 // If the left argument is a list,
                createValue(list) :             // then a list results; if the left
                createValue(list.get(1));       // argument is a single item, then a single item results
    }

    /**
     * The <strong>is data-type</strong> operator returns {@code true} if the data type of the specified object
     * matches the specified data type.
     * @see {@link DataType}, 9.6.15 - 9.6.23, 9.6.25
     * @param v
     * @param dataTypeStr
     * @param flip
     * @return
     */
    public T isDataType(T v, String dataTypeStr, boolean flip) {
        DataType dt = DataType.valueOf(dataTypeStr.toUpperCase());

        if (dt == DataType.LIST) {                          // default list handling, see 9.1.3.2
            boolean b = flip ? ! v.isList() : v.isList();
            return createValue(new ABoolean(b));

        } else if (v.isList()) {                            // default list handling, see 9.1.3.1
            AList list = new AList();
            for (ADataType obj : v.asList()) {
                list.add(doIsDataType(obj, dt, flip));
            }
            return createValue(list);

        } else {
            return createValue(doIsDataType(v.getBackingObject(), dt, flip));
        }
    }

    private ABoolean doIsDataType(ADataType obj, DataType dt, boolean flip) {
        boolean b;
        switch (dt) {
            case PRESENT:       b = ! (obj instanceof ANull);   break;  // present is opposite of null
            case NULL:          b = obj instanceof ANull;       break;
            case BOOLEAN:       b = obj instanceof ABoolean;    break;
            case NUMBER:        b = obj instanceof ANumber;     break;
            case STRING:        b = obj instanceof AString;     break;
            case TIME:          b = obj instanceof Time;        break;
            case TIME_OF_DAY:   b = obj instanceof TimeOfDay;   break;
            case DURATION:      b = obj instanceof Duration;    break;
//            case LIST:          b = obj instanceof AList;       break;        // list handling is handled in isDataType above
            case OBJECT:        b = obj instanceof AObject;     break;
            default:            throw new RuntimeException("unexpected data type '" + dt + "'");
        }

        if (flip) b = ! b;

        ABoolean rval = new ABoolean(b);
        rval.setPrimaryTime(obj.getPrimaryTime());
        return rval;
    }

    /**
     * The <strong>is object-type</strong> operator returns {@code true} if the argument is an object of the
     * named type (as previously defined with an Object declaration, as described in Section 11.2.17.
     * Otherwise it returns {@code false}.
     * @see {@link AObjectType}, 9.6.26
     * @param vObj
     * @param vType
     * @param flip
     * @return
     */
    public T isObjectType(T vObj, T vType, boolean flip) {
        if (vType.isObjectType()) {
            boolean b = vObj.isObject() ?
                    vObj.asObject().isType(vType.asObjectType()).asBoolean() :
                    false;

            if (flip) b = ! b;

            return createValue(new ABoolean(b));

        } else {
            return NULL();
        }
    }

    /**
     * The following comparison operators are analogous to the is comparison operators in Section 9.6. They use the
     * word <strong>occur</strong> instead of <strong>is</strong>. The word <strong>occur</strong> can be replaced
     * with <strong>occurs</strong> or <strong>occurred</strong>. An optional <strong>not</strong> may follow the
     * <strong>occur</strong>, negating the result (using the definition of <strong>not</strong>, see Section 9.4.3 -
     * see {@link #not}).
     * <br>
     * The effect is that rather than using the left argument directly, the primary time of the left argument is used
     * instead (that is, the time of the left argument is used; see Section 9.17).
     * @see 9.7.2
     * @param vObj
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurEqual(T vObj, T vTime, boolean flip) {
        Integer size = getListSize(vObj, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurEqual(
                    getFirstListItemOrSelf(vObj),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList objList = replicateObjectToList(vObj, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurEqual(objList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurEqual(ADataType obj, ADataType time, boolean flip) {
        if (obj.hasPrimaryTime() && time instanceof Time) {
            boolean b = obj.getPrimaryTime().hasValue(time);

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinTo}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.3
     * @param vTest
     * @param vLow
     * @param vHigh
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinTo(T vTest, T vLow, T vHigh, boolean flip) {
        Integer size = getListSize(vTest, vLow, vHigh);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinTo(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vLow),
                    getFirstListItemOrSelf(vHigh),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList lowList = replicateObjectToList(vLow, size);
            AList highList = replicateObjectToList(vHigh, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinTo(testList.get(i), lowList.get(i), highList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinTo(ADataType test, ADataType low, ADataType high, boolean flip) {
        if (test.hasPrimaryTime() && low instanceof Time && high instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tLow = (Time) low;
            Time tHigh = (Time) high;

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinPreceding}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.4
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinPreceding(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinPreceding(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinPreceding(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinPreceding(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && dur instanceof Duration && time instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tHigh = (Time) time;
            Time tLow = tHigh.subtract((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinFollowing}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.5
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinFollowing(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinFollowing(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinFollowing(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinFollowing(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && dur instanceof Duration && time instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tLow = (Time) time;
            Time tHigh = tLow.add((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinSurrounding}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.6
     * @param vTest
     * @param vDur
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinSurrounding(T vTest, T vDur, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vDur, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinSurrounding(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinSurrounding(testList.get(i), durList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinSurrounding(ADataType test, ADataType dur, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && dur instanceof Duration && time instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tLow = ((Time) time).subtract((Duration) dur);
            Time tHigh = ((Time) time).add((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinPast}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.7
     * @param vTest
     * @param vDur
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinPast(T vTest, T vDur, boolean flip) {
        Integer size = getListSize(vTest, vDur);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinPast(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vDur),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList durList = replicateObjectToList(vDur, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinPast(testList.get(i), durList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinPast(ADataType test, ADataType dur, boolean flip) {
        if (test.hasPrimaryTime() && dur instanceof Duration) {
            Time tTest = test.getPrimaryTime();
            Time tHigh = getNow();
            Time tLow = tHigh.subtract((Duration) dur);

            boolean b = tTest.compareTo(tLow) >= 0 && tTest.compareTo(tHigh) <= 0;

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isWithinSameDay}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.8
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurWithinSameDay(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurWithinSameDay(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurWithinSameDay(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurWithinSameDay(ADataType test, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && time instanceof Time) {
            Time t1 = test.getPrimaryTime();
            Time t2 = (Time) time;

            boolean b = t1.getYear().hasValue(t2.getYear()) &&
                    t1.getMonth().hasValue(t2.getMonth()) &&
                    t1.getDay().hasValue(t2.getDay());

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isBefore}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.9
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurBefore(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurBefore(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurBefore(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurBefore(ADataType test, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && time instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tTime = (Time) time;

            boolean b = tTest.compareTo(tTime) < 0;     // not inclusive!

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * Functions similarly to {@link #isAfter}, except that it operates on the primary time of the
     * specified object.
     * @see 9.7.10
     * @param vTest
     * @param vTime
     * @param flip
     * @return
     */
    @SuppressWarnings("unchecked")
    public T occurAfter(T vTest, T vTime, boolean flip) {
        Integer size = getListSize(vTest, vTime);

        if (size == null) {
            return NULL();

        } else if (size == 1) {
            return createValue(doOccurAfter(
                    getFirstListItemOrSelf(vTest),
                    getFirstListItemOrSelf(vTime),
                    flip));

        } else {
            AList testList = replicateObjectToList(vTest, size);
            AList timeList = replicateObjectToList(vTime, size);

            AList list = new AList();
            for (int i = 1; i <= size; i ++) {
                list.add(doOccurAfter(testList.get(i), timeList.get(i), flip));
            }
            return createValue(list);
        }
    }

    private ADataType doOccurAfter(ADataType test, ADataType time, boolean flip) {
        if (test.hasPrimaryTime() && time instanceof Time) {
            Time tTest = test.getPrimaryTime();
            Time tTime = (Time) time;

            boolean b = tTest.compareTo(tTime) > 0;     // not inclusive!

            if (flip) b = ! b;

            return new ABoolean(b);
        }

        return new ANull();
    }

    /**
     * The <strong>minimum ... from</strong> operator has one synonym: <strong>min ... from</strong>. It expects a
     * number (call it N) as its first argument and a homogeneous list of an ordered type as its second argument. It
     * returns a list with the N smallest items from the argument list, in the same order that they are in the second
     * argument, and with any duplicates preserved.
     * <br>
     * The result is {@code null} if N is not a non-negative integer. If there are not enough items in
     * the argument list, then as many as possible are returned. If there is a tie, then it selects the latest of
     * those elements that have a primary time. The primary times of the argument are maintained.
     * @see 9.14.2
     * @param vNum
     * @param vList
     * @return
     */
    public T minimumFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().minimum(vNum.asInteger()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>maximum ... from</strong> operator has one synonym: <strong>max ... from</strong>. It expects a
     * number (call it N) as its first argument and a homogeneous list of an ordered type as its second argument. It
     * returns a list with the N largest items from the argument list, in the same order that they are in the second
     * argument, and with any duplicates preserved.
     * <br>
     * The result is {@code null} if N is not a non-negative integer. If there are not enough items in the argument
     * list, then as many as possible are returned. If there is a tie, then it selects the latest of those elements
     * that have a primary time. The primary times of the argument are maintained.
     * @see 9.14.3
     * @param vNum
     * @param vList
     * @return
     */
    public T maximumFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().maximum(vNum.asInteger()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>first ... from</strong> operator expects a number (call it N) as its first argument and a list as
     * its second argument. It returns a list with the first N items from the argument list.
     * <br>
     * The result is {@code null} if N is not a non-negative integer. If the list is the result of a time-sorted query,
     * then the returned items are the earliest in time. If there are not enough items in the argument list, then as
     * many as possible are returned.
     * <br>
     * This means that first 1 from x differs from first x if x is empty; the former returns () and the latter
     * returns {@code null}. The primary times of the argument are maintained.
     * @see 9.14.4
     * @param vNum
     * @param vList
     * @return
     */
    public T firstFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().first(vNum.asInteger()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>last ... from</strong> operator expects a number (call it N) as its first argument and a list as its
     * second argument. It returns a list with the last N items from the argument list.
     * <br>
     * The result is {@code null} if N is not a non-negative integer. If the list is the result of a time-sorted query,
     * then the returned items are the latest in time. If there are not enough items in the argument list, then as many
     * as possible are returned. This means that <strong>last 1 from x</strong> differs from <strong>last x</strong>
     * if x is empty; the former returns {@code ()} and the latter returns {@code null}. The primary times of the
     * argument are maintained.
     * @see 9.14.5
     * @param vNum
     * @param vList
     * @return
     */
    public T lastFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().last(vNum.asInteger()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>nearest ... from</strong> operator expects a time as its first argument and a list as its second
     * argument. It selects the item from the list whose time of occurrence is nearest the specified time. If any of
     * the elements do not have primary times, the result is {@code null} (the argument can always be qualified by
     * <strong>where time of it is present</strong>, if this is not desired behavior). In the case of a tie, the
     * element with the smallest index is used. The primary times of the argument are maintained.
     * @see 9.13.2
     * @param vTime
     * @param vList
     * @return
     */
    public T nearest(T vTime, T vList) {
        if (vList.isList()) {
            Time t = null;

            if (vTime.isTimeOfDay()) {
                t = now.copy();
                t.setTimeOfDay(vTime.asTimeOfDay());

            } else if (vTime.isTime()) {
                t = vTime.asTime();
            }

            if (t != null) {
                return createValue(vList.asList().nearest(t));
            }
        }

        return NULL();
    }

    /**
     * The <strong>index nearest ... from</strong> operator functions exactly as the <strong>nearest ... from</strong>
     * operator (Section 9.13.2), except that it returns the index of the element rather than the element itself.
     * <strong>Index nearest ... from</strong> does not maintain primary time.
     * @see 9.13.3
     * @param vTime
     * @param vList
     * @return
     */
    public T indexNearest(T vTime, T vList) {
        if (vList.isList()) {
            Time t = null;

            if (vTime.isTimeOfDay()) {
                t = now.copy();
                t.setTimeOfDay(vTime.asTimeOfDay());

            } else if (vTime.isTime()) {
                t = vTime.asTime();
            }

            if (t != null) {
                return createValue(vList.asList().nearestIndex(t));
            }
        }

        return NULL();
    }

    /**
     * The <strong>at least ... from</strong> operator expects a number (call it {@code N}) as its first argument and a
     * homogeneous list of {@link ABoolean} as its second argument. The <strong>at least ... from</strong> operator
     * returns {@code true} if at least {@code N} items of the list are {@code true}. If the first argument is not a
     * number or the second parameter contains a non-{@link ABoolean}, {@code null} is returned. If {@code N} is
     * greater than the cardinality of the list, {@code false} is returned. The primary times of the arguments are not
     * maintained. The optional keywords “IsTrue” and “AreTrue” can be used to increase the readability of statements
     * using the <strong>at least ... from</strong> operator.
     * @see 9.13.5
     * @param vNum
     * @param vList
     * @return
     */
    public T atLeast(T vNum, T vList) {
        return doAtMostOrLeast(vNum, vList, true);
    }

    /**
     * The <strong>at most ... from</strong> operator expects a number (call it {@code N}) as its first argument and a
     * homogeneous list of {@link ABoolean} as its second argument. The <strong>at most ... from</strong> operator
     * returns {@code true} if at most {@code N} items of the list are {@code true}. If the first argument is not a
     * number or the second parameter contains a non-{@link ABoolean}, {@code null} is returned. If {@code N} is
     * greater than the cardinality of the list, {@code false} is returned. The primary times of the arguments are not
     * maintained. The optional keywords “IsTrue” and “AreTrue” can be used to increase the readability of statements
     * using the <strong>at most ... from</strong> operator.
     * @see 9.3.6
     * @param vNum
     * @param vList
     * @return
     */
    public T atMost(T vNum, T vList) {
        return doAtMostOrLeast(vNum, vList, false);
    }

    private T doAtMostOrLeast(T vNum, T vList, boolean isLeast) {
        if (vNum.isNumber() && vList.isList() && vList.asList().getListType() == ListType.BOOLEAN) {
            int n = vNum.asNumber().intValue();
            AList list = vList.asList();

            if (n > list.size()) {
                return FALSE();

            } else {
                int trueCount = 0;
                for (ADataType obj : list) {
                    if (((ABoolean) obj).isTrue()) trueCount ++;
                }

                boolean rval = isLeast ? trueCount >= n : trueCount <= n;

                return createValue(new ABoolean(rval));
            }
        }

        return NULL();
    }

    /**
     * The <strong>increase</strong> operator returns a list of the differences between successive items in a
     * homogeneous numeric, time, or duration list. There is one fewer item in the result than in the argument; if the
     * argument is an empty list, then {@code null} is returned. The primary time of the second item in each successive
     * pair is kept.
     * <br>
     * The <strong>% increase</strong> operator has one synonym: <strong>percent increase</strong>. It returns a list
     * of the percent increase between items in successive pairs in a homogeneous number or duration list (the
     * denominator is the first item in each pair; if it is zero, then {@code null} is returned). The primary time of
     * the second item in each successive pair is kept.
     * @see 9.14.7, 9.14.9
     * @param v
     * @param pct
     * @return
     */
    public T increase(T v, boolean pct) {
        ListType type = v.asList().getListType();

        if (pct && (type == ListType.NUMBER || type == ListType.DURATION)) {
            return createValue(v.asList().increasePct());

        } else if (type == ListType.NUMBER || type == ListType.TIME || type == ListType.DURATION) {
            return createValue(v.asList().increase());
        }

        return NULL();
    }

    /**
     * The <strong>decrease</strong> operator returns a list of the negative differences between successive items in a
     * homogeneous numeric, time, or duration list. There is one fewer item in the result than in the argument; if the
     * argument is an empty list, then {@code null} is returned. Decrease is the additive inverse of increase (see
     * {@link #increase}). The primary time of the second item in each successive pair is kept.
     * <br>
     * The <strong>% decrease</strong> operator has one synonym: <strong>percent decrease</strong>. It returns a list
     * of the percent decrease between items in successive pairs in a homogeneous number or duration list (the
     * denominator is the first item in each pair, if it is zero, then {@code null} is returned). The primary time of
     * the second item in each successive pair is kept.
     * @see 9.14.8, 9.14.10
     * @param v
     * @param pct
     * @return
     */
    public T decrease(T v, boolean pct) {
        ListType type = v.asList().getListType();

        if (pct && (type == ListType.NUMBER || type == ListType.DURATION)) {
            return createValue(v.asList().decreasePct());

        } else if (type == ListType.NUMBER || type == ListType.TIME || type == ListType.DURATION) {
            return createValue(v.asList().decrease());
        }

        return NULL();
    }

    /**
     * The interval operator returns the difference between the primary times of succeeding items in a list. It is
     * analogous to increase. The primary times of the argument are lost.
     * @see 9.15.2
     * @param v
     * @return
     */
    public T interval(T v) {
        if (v.isList()) {
            return createValue(v.asList().interval());
        }
        return NULL();
    }

    /**
     * The <strong>earliest ... from</strong> operator expects a number (call it N) as its first argument and a list as
     * its second argument. It returns a list with the earliest N items from the argument list, in the order they appear
     * in the argument list. The result is {@code null} if N is not a non-negative integer. If any of the elements do
     * not have primary times, the result is {@code null} (the argument can always be qualified by where time of it is
     * present, if this is not desired behavior). If there are not enough items in the argument list, then as many as
     * possible are returned. This means that <strong>earliest 1 from x</strong> differs from <strong>earliest
     * x</strong> if x is empty; the former returns {@code ()} and the latter returns {@code null}. The primary times
     * of the argument are maintained.
     * @see 9.14.11
     * @param vNum
     * @param vList
     * @return
     */
    public T earliestFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().earliest(vNum.asInteger()));

        } else {
            return NULL();
        }
    }

    /**
     * The <strong>latest ... from</strong> operator expects a number (call it N) as its first argument and a list as
     * its second argument. It returns a list with the latest N items from the argument list, in the order they appear
     * in the argument list. The result is {@code null} if N is not a non-negative integer. If any of the elements do
     * not have primary times, the result is {@code null} (the argument can always be qualified by where time of it is
     * present, if this is not desired behavior). If there are not enough items in the argument list, then as many as
     * possible are returned. This means that <strong>latest 1 from x</strong> differs from <strong>latest x</strong>
     * if x is empty; the former returns {@code ()} and the latter returns {@code null}. The primary times of the
     * argument are maintained.
     * @see 9.14.12
     * @param vNum
     * @param vList
     * @return
     */
    public T latestFrom(T vNum, T vList) {
        if (vNum.isInteger() && vNum.asInteger() >= 0) {
            return createValue(vList.asList().latest(vNum.asInteger()));

        } else {
            return NULL();
        }
    }


////////////////////////////////////////////////////////////////////////////////////
// default list handling helper functions
//

    /**
     * Used by list handling routines to ensure that each value contains a single non-list object, or a list of size
     * {@code X}.  Only single-objects and equal-sized lists are permitted - any instance where there are lists
     * of multiple sizes (not including lists of size 1).  All parameter values must be either single objects, single
     * element lists, or lists of size {@code X}.  No other list sizes are permitted.
     * <br/>
     * Example: params of size 1, 1, 1 -> 1.  1, 1, 2 -> 2.  1, 2, 2 -> 2.  1, 2, 3 -> null.
     * @param objs
     * @return
     */
    private Integer getListSize(T ... objs) {
        Integer size = null;

        for (T obj : objs) {
            int i = obj.isList() ? obj.asList().size() : 1;

            if      (size == null)                      size = i;
            else if (size == 1 && i > 1)                size = i;
            else if (size != 1 && i != 1 && size != i)  return null;
        }

        return size;
    }

    /**
     * Used to create a list with {@code size} elements, with each element being {@code obj}.  This facilitates
     * and simplifies calling logic.
     * @param obj
     * @param size
     * @return
     */
    private AList replicateObjectToList(T obj, int size) {
        if (obj.isList()) {
            AList objList = obj.asList();

            if (objList.size() == size) {
                return obj.asList();

            } else if (size > 1 && objList.size() == 1) {
                AList list = new AList();
                ADataType o = objList.get(1);
                for (int i = 0; i < size; i ++) {       // IMPORTANT NOTE:
                    list.add(o);                        // not copying, so each object in this list is identical to all
                }                                       // others in this list.  for the intended use case, this should be fine.
                return list;

            } else {
                throw new RuntimeException("cannot resize list of size " + objList.size() + " to " + size);
            }

        } else {
            AList list = new AList();
            ADataType o = obj.getBackingObject();
            for (int i = 0; i < size; i ++) {       // IMPORTANT NOTE:
                list.add(o);                        // not copying, so each object in this list is identical to all
            }                                       // others in this list.  for the intended use case, this should be fine.
            return list;
        }
    }

    /**
     * If {@code obj} is an {@link AList}, then this function returns its first element.  Otherwise, {@code obj} is
     * returned.  Used to simplify calling logic.
     * @param obj
     * @return
     */
    private ADataType getFirstListItemOrSelf(T obj) {
        return obj.isList() ? obj.asList().get(1) : obj.getBackingObject();
    }


//////////////////////////////////////////////////////////////////////////////////////////////

    private static final class ObjectTypeRegistry {
        private Map<String, AObjectType> map = new HashMap<String, AObjectType>();
        private Map<String, AObjectType> included = new HashMap<String, AObjectType>();

        private void register(AObjectType objectType) {
            map.put(objectType.getName().toLowerCase(), objectType);
        }

        private void registerExternal(AObjectType objectType) {
            included.put(objectType.getName().toLowerCase(), objectType);
        }

        private AObjectType get(String typeName) {
            String key = typeName.toLowerCase();
            return map.containsKey(key) ? map.get(key) : included.get(key);
        }

        private boolean isDefined(String typeName) {
            String key = typeName.toLowerCase();
            return map.containsKey(key) || included.containsKey(key);
        }

        private boolean isLocallyDefined(String typeName) {
            String key = typeName.toLowerCase();
            return map.containsKey(key);
        }

        private Map<String, AObjectType> getObjectTypes() {
            Map<String, AObjectType> m = new HashMap<String, AObjectType>();

            for (AObjectType objectType : map.values()) {
                m.put(objectType.getName().toLowerCase(), objectType);
            }

            for (AObjectType objectType : included.values()) {
                String key = objectType.getName().toLowerCase();
                if ( ! m.containsKey(key) ) {
                    m.put(key, objectType);
                }
            }

            return m;
        }
    }
}
