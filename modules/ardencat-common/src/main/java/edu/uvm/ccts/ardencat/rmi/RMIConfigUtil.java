/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.rmi;

import edu.uvm.ccts.ardencat.rmi.model.RMIConfig;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by mbstorer on 10/27/15.
 */
public class RMIConfigUtil {
    private static final Log log = LogFactory.getLog(RMIConfigUtil.class);

    private static final String RMI_BOOTSTRAP_FILE = ".rmi-bootstrap";

    private static final String REGISTRY_PORT_PROPERTY = "rmi.registryPort";
    private static final String COMM_PORT_PROPERTY = "rmi.commPort";

    public static RMIConfig getRMIConfig() throws ConfigurationException {
        Properties props = new Properties();

        File f = new File(RMI_BOOTSTRAP_FILE);

        if (f.isFile()) {
            try {
                props.load(new FileInputStream(f));

            } catch (IOException e) {
                log.error("caught " + e.getClass().getName() + " loading RMI bootstrap properties - " + e.getMessage(), e);
                log.info("loading default RMI bootstrap properties -");
            }

        } else {
            props.setProperty(REGISTRY_PORT_PROPERTY, String.valueOf(RMIConfig.DEFAULT_REGISTRY_PORT));
            props.setProperty(COMM_PORT_PROPERTY, String.valueOf(RMIConfig.DEFAULT_COMM_PORT));

            try {
                props.store(new FileOutputStream(f), "---ArdenCAT RMI Bootstrap Configuration---");

            } catch (IOException e) {
                log.error("caught " + e.getClass().getName() + " saving RMI bootstrap properties - " + e.getMessage(), e);
            }
        }

        int registryPort = toValidPort(props.getProperty(REGISTRY_PORT_PROPERTY), RMIConfig.DEFAULT_REGISTRY_PORT);
        int commPort = toValidPort(props.getProperty(COMM_PORT_PROPERTY), RMIConfig.DEFAULT_COMM_PORT);

        return new RMIConfig(registryPort, commPort);
    }

    private static int toValidPort(String s, int dflt) throws ConfigurationException {
        if (s == null || "".equals(s.trim())) return dflt;

        try {
            return Integer.parseInt(s);

        } catch (NumberFormatException nfe) {
            throw new ConfigurationException("invalid port '" + s + "'", nfe);
        }
    }
}
