/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenLib.
 *
 * ArdenLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenLib.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.arden;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.arden.util.ObjectUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * A wrapper-class that represents an abstract value as referenced by DataEvalVisitor.
 * @author Matt Storer
 */
public abstract class AbstractValue<T extends AbstractValue> {
    private static final Log log = LogFactory.getLog(AbstractValue.class);

    protected final ADataType obj;

    protected abstract T VOID();
    protected abstract T NULL();
    protected abstract T TRUE();
    protected abstract T FALSE();

    /**
     * Create a new Value represented by the Arden-object <code>obj</code>
     * @param obj an Arden-object
     */
    public AbstractValue(ADataType obj) {
        this.obj = obj;
    }

    /**
     * @return the backing Arden-object
     */
    public ADataType getBackingObject() {
        return obj;
    }

    @Override
    public String toString() {
        return obj.toString();
    }

    /**
     * @return the primary-time of the backing Arden-object
     */
    public Time getPrimaryTime() {
        return obj.getPrimaryTime();
    }

    /**
     * @return whether or not the backing Arden-object has a primary time
     */
    public boolean hasPrimaryTime() {
        return obj.hasPrimaryTime();
    }

    /**
     * Set the primary-time for this Value's backing Arden-object
     *
     * @param primaryTime an Arden Time object
     */
    public void setPrimaryTime(Time primaryTime) {
        if (obj instanceof PrimaryTimeDataType) {
            ((PrimaryTimeDataType) obj).setPrimaryTime(primaryTime);

        } else {
            log.warn("setPrimaryTime : cannot set primary time on " + obj.getClass().getName() + ".");
        }
    }

    /**
     * Determines whether or not this Value's backing Arden-object is ordered (i.e., sortable, comparable).
     *
     * @return <code>true</code> if the backing Arden-object is ordered; <code>false</code> otherwise.
     */
    public boolean isOrdered() {
        return ObjectUtil.isOrdered(obj);
    }

    /**
     * Determines whether or not one Value's backing Arden-object is of the same type as another's.
     *
     * @param v a Value object
     * @return <code>true</code> if this Value's backing Arden-object's class is an instance of the same class
     * as the parameter <code>v</code>'s backing Arden-object's class; <code>false</code> otherwise.
     */
    public boolean isLike(AbstractValue v) {
        return obj.isLike(v.getBackingObject());
    }

    /**
     * Determine whether or not this Value's backing Arden-object is an Arden-Boolean class.
     *
     * @return <code>true</code> if this Value's backing object is an Arden-Boolean class.
     */
    public boolean isBoolean() {
        return ObjectUtil.isBoolean(obj);
    }

    /**
     * @return the backing Arden-object if the backing object is an Arden-Boolean class.  If the backing
     * object is not an Arden-Boolean class, returns <code>ABoolean.UNKNOWN</code> instead.
     */
    public ABoolean asBoolean() {
        return ObjectUtil.asBoolean(obj);
    }

    public boolean isNumber() {
        return ObjectUtil.isNumber(obj);
    }

    public ANumber asNumber() {
        return ObjectUtil.asNumber(obj);
    }

    public boolean isInteger() {
        return ObjectUtil.isInteger(obj);
    }

    public int asInteger() {
        return ObjectUtil.asInteger(obj);
    }

    public boolean isString() {
        return ObjectUtil.isString(obj);
    }

    public AString asString() {
        return ObjectUtil.asString(obj);
    }

    public boolean isArgumentList() {
        return ObjectUtil.isArgumentList(obj);
    }

    public ArgumentList asArgumentList() {
        return ObjectUtil.asArgumentList(obj);
    }

    public boolean isList() {
        return ObjectUtil.isList(obj);
    }

    public boolean isListOfLists() {
        return ObjectUtil.isListOfLists(obj);
    }

    public AList asList() {
        return ObjectUtil.asList(obj);
    }

    public AListOfLists asListOfLists() {
        return ObjectUtil.asListOfLists(obj);
    }

    public ListType getListType() {
        return ObjectUtil.getListType(obj);
    }

    public List<ABoolean> asBooleanList() {
        return ObjectUtil.asBooleanList(obj);
    }

    public List<AString> asStringList() {
        return ObjectUtil.asStringList(obj);
    }

    public List<ANumber> asNumberList() {
        return ObjectUtil.asNumberList(obj);
    }

    public List<Time> asTimeList() {
        return ObjectUtil.asTimeList(obj);
    }

    public List<Duration> asDurationList() {
        return ObjectUtil.asDurationList(obj);
    }

    public boolean isNull() {
        return ObjectUtil.isNull(obj);
    }

    public boolean isTime() {
        return ObjectUtil.isTime(obj);
    }

    public Time asTime() {
        return ObjectUtil.asTime(obj);
    }

    public boolean isTimeOfDay() {
        return ObjectUtil.isTimeOfDay(obj);
    }

    public TimeOfDay asTimeOfDay() {
        return ObjectUtil.asTimeOfDay(obj);
    }

    public boolean isDuration() {
        return ObjectUtil.isDuration(obj);
    }

    public Duration asDuration() {
        return ObjectUtil.asDuration(obj);
    }

    public boolean isObject() {
        return ObjectUtil.isObject(obj);
    }

    public AObject asObject() {
        return ObjectUtil.asObject(obj);
    }

    public boolean isObjectType() {
        return ObjectUtil.isObjectType(obj);
    }

    public AObjectType asObjectType() {
        return ObjectUtil.asObjectType(obj);
    }
}
