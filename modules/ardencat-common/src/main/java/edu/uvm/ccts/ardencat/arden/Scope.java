/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenLib.
 *
 * ArdenLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenLib.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.arden;

import edu.uvm.ccts.arden.model.ADataType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

/**
 * Maintains separate inheritable memory environments, and methods to shift and pop environments to and
 * from the memory environment stack.
 * @author Matt Storer
 */
public class Scope {
    private static final Log log = LogFactory.getLog(Scope.class);

    private List<Map<String, ADataType>> memory = new ArrayList<Map<String, ADataType>>();
    private Set<String> frozenIds = new HashSet<String>();
    private Map<String, ADataType> included = new HashMap<String, ADataType>();

    /**
     * Creates a new Scope object with a single memory environment on the stack.  This initial memory
     * environment can be considered the top-level or "global" scope.
     */
    public Scope() {
        memory.add(new HashMap<String, ADataType>());
    }

    /**
     * Creates a new memory environment that is placed on the top of the stack.
     */
    public void shift() {
        memory.add(0, new HashMap<String, ADataType>());
    }

    /**
     * Removes the memory environment from the top of the stack, if doing so will not destroy the top-level
     * or "global" scope.
     */
    public void pop() {
        if (memory.size() > 1) {
            memory.remove(0);
        }
    }

    /**
     * Retrieve an object from memory by its assigned variable name.
     *
     * If a variable doesn't exist with the specified id in the current scope, each parent scope is
     * subsequently checked for the requested object up to and including the top-level or "global" scope.
     * If the object cannot be found in any memory environment, a RuntimeException is thrown.
     *
     * Variable names are case-insensitive (7.1.2.1).
     *
     * @param id the variable name
     * @return the Arden-object identified by <code>id</code>
     * @throws RuntimeException if the requested variable cannot be found in the current or any higher-level
     * memory environment
     */
    public ADataType get(String id) {
        for (Map<String, ADataType> m : memory) {
            if (m.containsKey(id.toLowerCase())) {
                return m.get(id.toLowerCase());
            }
        }

        if (included.containsKey(id.toLowerCase())) {       // included variables always take a back-seat to
            return included.get(id.toLowerCase());          // locally-defined variables
        }

        throw new RuntimeException("variable '" + id + "' is not defined.");
    }

    public Map<String, ADataType> getVariables() {
        Map<String, ADataType> map = new HashMap<String, ADataType>();

        for (Map<String, ADataType> m : memory) {
            for (String id : m.keySet()) {
                if ( ! map.containsKey(id) ) {
                    map.put(id, m.get(id));
                }
            }
        }

        for (String id : included.keySet()) {
            if ( ! map.containsKey(id) ) {
                map.put(id, included.get(id));
            }
        }

        return map;
    }

    /**
     * Store an Arden-object into memory.
     *
     * If an object already exists anywhere in memory (whether it is in the current scope or any of its
     * parent scopes) with the specified {@code id}, that record is updated.  Otherwise, the specified
     * object {@code obj} is written stored in the current scope's memory-environment as a "local"
     * variable.
     *
     * Variable names are case-insensitive (7.1.2.1).
     *
     * @param id the variable name
     * @param obj the Arden-object to-be identified by <code>id</code>.
     */
    public void put(String id, ADataType obj) {
        boolean found = false;
        for (Map<String, ADataType> m : memory) {
            if (m.containsKey(id.toLowerCase())) {
                found = true;
                m.put(id.toLowerCase(), obj);
                break;
            }
        }

        if ( ! found ) {
            memory.get(0).put(id.toLowerCase(), obj);
        }
    }

    /**
     * Introduce a variable from an external MLM to this scope
     * @param id
     * @param obj
     */
    public void include(String id, ADataType obj) {
        included.put(id.toLowerCase(), obj);
    }

    /**
     * Makes a defined variable immutable
     * @param id
     */
    public void freeze(String id) {
        if (isDefined(id)) {
            frozenIds.add(id.toLowerCase());
        }
    }

    /**
     * Makes a defined, immutable variable mutable once again
     * @param id
     */
    public void thaw(String id) {
        if (isDefined(id) && isFrozen(id)) {
            frozenIds.remove(id.toLowerCase());
        }
    }

    /**
     * Deterine whether or not a variable is mutable
     * @param id
     * @return {@code true} if the specified variable is immutable; {@code false} otherwise
     */
    public boolean isFrozen(String id) {
        return frozenIds.contains(id.toLowerCase());
    }

    /**
     * Determine whether or not a variable is defined within any available scope's memory-environment.
     *
     * Variable names are case-insensitive (7.1.2.1).
     *
     * @param id the variable name to test for existence
     * @return {@code true} if {@code id} refers to a variable in the current or any higher-level scope;
     * {@code false} otherwise.
     */
    public boolean isDefined(String id) {
        for (Map<String, ADataType> m : memory) {
            if (m.containsKey(id.toLowerCase())) {
                return true;
            }
        }

        return included.containsKey(id.toLowerCase());
    }

    /**
     * Remove a variable from the current scope.
     *
     * The current and each parent memory-environment is checked for the specified object identified by
     * {@code id}.  If an object is found, the object is removed from scope.
     *
     * Variable names are case-insensitive (7.1.2.1).
     *
     * @param id the name of the variable to remove
     */
    public void remove(String id) {
        for (Map<String, ADataType> m : memory) {
            if (m.containsKey(id.toLowerCase())) {
                m.remove(id.toLowerCase());
                break;
            }
        }
    }
}
