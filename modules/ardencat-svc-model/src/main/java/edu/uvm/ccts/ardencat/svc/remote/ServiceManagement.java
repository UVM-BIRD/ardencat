/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.remote;

import edu.uvm.ccts.ardencat.mlmengine.exceptions.ScheduleException;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.LogRO;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.LogVO;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.ScheduleVO;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.remote.model.ServiceConfigRO;
import edu.uvm.ccts.ardencat.svc.remote.model.ServiceConfigVO;
import edu.uvm.ccts.common.exceptions.ConfigurationException;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by mstorer on 7/1/15.
 */
public interface ServiceManagement<C extends ServiceConfig> extends Remote {
    boolean isAlive() throws RemoteException;

    void closeConnections() throws RemoteException;

    ServiceConfigVO<C> getConfig() throws RemoteException;
    void setConfig(ServiceConfigRO<C> configRo) throws RemoteException, ScheduleException, ConfigurationException;

    LogVO getLog(LogRO logRo) throws RemoteException;

    ScheduleVO getSchedule() throws RemoteException, ScheduleException;
}
