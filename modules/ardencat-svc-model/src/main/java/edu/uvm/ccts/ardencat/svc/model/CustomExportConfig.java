/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.exceptions.ConfigurationException;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mbstorer on 7/27/15.
 */
public class CustomExportConfig extends ExportConfig implements Serializable {
    private String className;

    public CustomExportConfig(String className, boolean identify, List<EligibilityStatus> statusList) {
        super(identify, statusList);
        this.className = className;
    }

    public void validateClass() throws ConfigurationException {
        try {
            Class cls = Class.forName(className, false, this.getClass().getClassLoader());

            if ( ! CustomExporter.class.isAssignableFrom(cls) ) {
                throw new ConfigurationException("custom exporter class '" + cls.getName() + "' does not extend CustomExporter");
            }

        } catch (ClassNotFoundException e) {
            throw new ConfigurationException("class not found : " + className);
        }
    }

    public String getClassName() {
        return className;
    }
}
