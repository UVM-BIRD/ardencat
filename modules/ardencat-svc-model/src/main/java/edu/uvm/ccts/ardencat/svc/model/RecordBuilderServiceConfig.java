/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.ardencat.rb.model.EligibilityConfig;
import edu.uvm.ccts.ardencat.rb.model.AbstractRecordBuilderConfig;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 7/6/15.
 */
public class RecordBuilderServiceConfig extends AbstractRecordBuilderConfig<EligibilityConfig> implements Serializable {
    private String id;
    private List<ExportConfig> exportConfigs;

    public RecordBuilderServiceConfig(File file, Frame frame) throws ConfigurationException {
        super(file, frame);
    }

    public RecordBuilderServiceConfig(String filename, Frame frame, DataSource ardencatDataSource, Element root)
            throws ConfigurationException {

        super(filename, frame, ardencatDataSource, root);
    }


///////////////////////////////////////////////////////////////////////////////////
// public methods
//

    public String getId() {
        return id;
    }

    public List<ExportConfig> getExportConfigs() {
        return exportConfigs;
    }


///////////////////////////////////////////////////////////////////////////////////
// private methods
//

    @Override
    protected void buildOtherConfigs(Element root) throws ConfigurationException {
        id = root.getAttribute("id");

        buildExportConfig(root);
    }

    @Override
    protected EligibilityConfig createEligibilityConfig(String source) {
        return new EligibilityConfig(source);
    }

    @Override
    protected void buildOtherEligibilityConfigs(EligibilityConfig eligibilityConfig, Element xmlEligibility) throws IOException {
        // do nothing
    }

    private void buildExportConfig(Element root) throws ConfigurationException {
        List<ExportConfig> exportConfigs = new ArrayList<ExportConfig>();

        Elements xmlExports = root.getElement(new XPath("exports")).getElements();
        while (xmlExports.hasMoreElements()) {
            ExportConfig exportConfig;

            Element xmlExport = xmlExports.next();
            if (xmlExport.getName().equals("fileExport")) {
                String filename = xmlExport.getAttribute("filename");
                String method = xmlExport.getAttribute("method");
                boolean identify = xmlExport.getAttribute("identify").equalsIgnoreCase("true");
                List<EligibilityStatus> statusList = buildStatusList(xmlExport);

                exportConfig = new FileExportConfig(filename, method, identify, statusList);

            } else if (xmlExport.getName().equals("customExport")) {
                String className = xmlExport.getAttribute("class");
                boolean identify = xmlExport.getAttribute("identify").equalsIgnoreCase("true");
                List<EligibilityStatus> statusList = buildStatusList(xmlExport);

                exportConfig = new CustomExportConfig(className, identify, statusList);

            } else {
                throw new ConfigurationException("unhandled export element type '" + xmlExport.getName() + "'");
            }

            exportConfigs.add(exportConfig);
        }

        this.exportConfigs = exportConfigs;
    }

    private List<EligibilityStatus> buildStatusList(Element xmlElement) {
        List<EligibilityStatus> statusList = new ArrayList<EligibilityStatus>();
        for (String s : xmlElement.getAttribute("status").split("\\s*,\\s*")) {
            statusList.add(EligibilityStatus.valueOf(s));
        }
        return statusList;
    }
}
