/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.model;

import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenCATConfiguration;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMEngineConfig;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMConfigData;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import electric.xml.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mstorer on 7/2/15.
 */
public class ServiceConfig implements ArdenCATConfiguration, Serializable {
    private DataSource ardencatDataSource = null;
    private MLMEngineConfig meConfig;
    private List<RecordBuilderServiceConfig> rbConfigs;

    protected transient Frame frame = null;

    public ServiceConfig(File file, Frame frame) throws ConfigurationException {
        try {
            this.frame = frame;

            String filename = file.getCanonicalPath();
            populateFromXml(filename, file);

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }

        validateRBRefs();
    }

    public ServiceConfig(MLMEngineConfig meConfig, List<RecordBuilderServiceConfig> rbConfigs) throws ConfigurationException {
        this.meConfig = meConfig;
        this.rbConfigs = rbConfigs;

        validateRBRefs();
    }


///////////////////////////////////////////////////////////////////////////////////
// public methods
//

    @Override
    public String getFilename() {
        return meConfig != null ?
                meConfig.getFilename() :
                null;
    }

    @Override
    public DataSource getArdencatDataSource() {
        return ardencatDataSource;
    }

    @Override
    public List<ArdenDataSource> getMLMDataSources() {
        return meConfig != null ?
                meConfig.getMLMDataSources() :
                new ArrayList<ArdenDataSource>();
    }

    @Override
    public List<MLMConfigData> getMLMConfigs() {
        return meConfig != null ?
                meConfig.getMLMConfigs() :
                new ArrayList<MLMConfigData>();
    }

    public MLMEngineConfig getMEConfig() {
        return meConfig;
    }

    public List<RecordBuilderServiceConfig> getRBConfigs() {
        return rbConfigs;
    }

    public RecordBuilderServiceConfig getRBConfig(String recordBuilderName) {
        if (recordBuilderName != null) {
            for (RecordBuilderServiceConfig rbConfig : rbConfigs) {
                if (rbConfig.getId().equalsIgnoreCase(recordBuilderName)) {
                    return rbConfig;
                }
            }
        }
        return null;
    }

///////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void populateFromXml(String filename, File file) throws ConfigurationException {
        try {
            Document doc = new Document(file);
            Element root = doc.getElement(new XPath("ardencat"));

            buildArdencatDataSource(root);
            buildMLMEngineConfig(filename, root);
            buildRecordBuilderConfigs(filename, root);

        } catch (ParseException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    private void buildArdencatDataSource(Element root) throws ConfigurationException {
        Element xmlSystemDataSource = root.getElement(new XPath("ardencatDataSource"));
        ardencatDataSource = DataSource.buildFromXml("ardencat", xmlSystemDataSource, frame);
    }

    private void buildMLMEngineConfig(String filename, Element root) throws ConfigurationException {
        Element xmlMLMEngineConfig = root.getElement(new XPath("mlmEngine"));
        meConfig = new MLMEngineConfig(filename, frame, ardencatDataSource, xmlMLMEngineConfig);
    }

    private void buildRecordBuilderConfigs(String filename, Element root) throws ConfigurationException {
        Elements xmlRecordBuilderConfigs = root.getElements(new XPath("recordBuilders/recordBuilder"));

        Set<String> foundIds = new HashSet<String>();
        rbConfigs = new ArrayList<RecordBuilderServiceConfig>();
        while (xmlRecordBuilderConfigs.hasMoreElements()) {
            RecordBuilderServiceConfig rbConfig = new RecordBuilderServiceConfig(filename, frame, ardencatDataSource, xmlRecordBuilderConfigs.next());

            if (foundIds.contains(rbConfig.getId())) {
                throw new ConfigurationException("encountered recordBuilder element with duplicate id '" + rbConfig.getId() + "'");
            }

            rbConfigs.add(rbConfig);

            foundIds.add(rbConfig.getId());
        }
    }

    private void validateRBRefs() throws ConfigurationException {
        List<String> validList = new ArrayList<String>();
        for (RecordBuilderServiceConfig cfg : rbConfigs) {
            validList.add(cfg.getId().toLowerCase());
        }

        for (MLMConfigData cfg : meConfig.getMLMConfigs()) {
            String id = cfg.getRecordBuilderName();
            if (id != null && ! validList.contains(id.toLowerCase())) {
                throw new ConfigurationException("MLM references invalid Record Builder id '" + id + "'");
            }
        }
    }
}
