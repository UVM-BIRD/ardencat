/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.ui.model;

import edu.uvm.ccts.ardencat.mlmengine.model.log.LogItem;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 5/1/14.
 */
public class LogTableModel extends AbstractTableModel {
    private List<LogItem> list;

    public LogTableModel(List<LogItem> list) {
        this.list = list;
    }

    public LogItem getLogItemAtRow(int row) {
        return list.get(row);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:  return String.class;
            case 1:  return Date.class;
            case 2:  return String.class;
            case 3:  return String.class;
            default: return super.getColumnClass(columnIndex);
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:  return "MLM ID";
            case 1:  return "Execution Time";
            case 2:  return "Status";
            case 3:  return "Message";
            default: return super.getColumnName(column);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LogItem item = list.get(rowIndex);
        switch (columnIndex) {
            case 0:  return item.getMlmId();
            case 1:  return item.getExecutionTime();
            case 2:  return item.getStatus();
            case 3:  return firstLineOf(item.getMessage());
            default: return null;
        }
    }

    private String firstLineOf(String str) {
        if (str != null) {
            int pos = str.indexOf('\n');
            if (pos >= 0) {
                return str.substring(0, pos);
            }
        }

        return "";
    }
}
