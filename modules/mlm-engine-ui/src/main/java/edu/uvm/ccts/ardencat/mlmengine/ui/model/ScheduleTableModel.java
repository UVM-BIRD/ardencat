/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.ui.model;

import edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.ScheduleItem;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by mstorer on 5/2/14.
 */
public class ScheduleTableModel extends AbstractTableModel {
    private List<ScheduleItem> list;

    public ScheduleTableModel(List<ScheduleItem> list) {
        this.list = list;
    }

    public ScheduleItem getScheduleItemAtRow(int row) {
        return list.get(row);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:  return "Group";
            case 1:  return "Name";
            default: return super.getColumnName(column);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Job job = list.get(rowIndex).getJob();

        switch (columnIndex) {
            case 0:  return job.getGroup();
            case 1:  return job.getName();
            default: return null;
        }
    }
}
