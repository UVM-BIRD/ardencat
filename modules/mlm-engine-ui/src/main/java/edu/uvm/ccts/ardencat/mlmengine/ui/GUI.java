/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Mon Apr 28 11:22:06 EDT 2014
 */

package edu.uvm.ccts.ardencat.mlmengine.ui;

import edu.uvm.ccts.ardencat.mlmengine.model.MLMEngineConfig;
import edu.uvm.ccts.ardencat.mlmengine.exceptions.ScheduleException;
import edu.uvm.ccts.ardencat.mlmengine.model.log.LogItem;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.ScheduleItem;
import edu.uvm.ccts.ardencat.mlmengine.remote.StandardManagement;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.ConfigRO;
import edu.uvm.ccts.ardencat.mlmengine.remote.model.LogRO;
import edu.uvm.ccts.ardencat.mlmengine.ui.model.*;
import edu.uvm.ccts.ardencat.ui.model.DateInputVerifier;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.db.DomainCredentialRegistry;
import edu.uvm.ccts.common.exceptions.CanceledException;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.TableUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Matthew Storer
 */
public class GUI extends JFrame {
    private static final Log log = LogFactory.getLog(GUI.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static final String APPLY_TEXT = "Apply";
    private static final String RELOAD_TEXT = "Reload";

    private static MaskFormatter dateFormatter;
    static {
        try {
            dateFormatter = new MaskFormatter("####-##-##");
            dateFormatter.setPlaceholderCharacter('_');
            dateFormatter.setAllowsInvalid(false);
            dateFormatter.setCommitsOnValidEdit(true);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " creating date formatter - " + e.getMessage(), e);
        }
    }


////////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private StandardManagement mgmt;

    public GUI(StandardManagement mgmt) throws Exception {
        initComponents();

        this.mgmt = mgmt;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        txtEndDate.setText(sdf.format(cal.getTime()));

        cal.add(Calendar.DATE, -7);
        txtStartDate.setText(sdf.format(cal.getTime()));

        tblLog.setDefaultRenderer(Date.class, new DateTimeRenderer());

        MLMEngineConfig cfg = mgmt.getConfig().getConfig();
        if (cfg != null) {
            txtFilename.setText(cfg.getFilename());
            refresh();
        }

        TableUtil.makeAutoResizable(tblLog);
        TableUtil.addContextMenu(tblLog);

        TableUtil.makeAutoResizable(tblSchedule);
        TableUtil.addContextMenu(tblSchedule);
    }


//////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private Date getStartDate() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(txtStartDate.getText()));
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date getEndDate() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(txtEndDate.getText()));
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    private void refresh() throws RemoteException, ScheduleException, ParseException {
        refreshButtons();
        refreshSchedule();
        refreshLog();
    }

    private void refreshButtons() throws RemoteException {
        MLMEngineConfig cfg = mgmt.getConfig().getConfig();

        btnClear.setEnabled(cfg != null);
        btnApply.setEnabled(!"".equals(txtFilename.getText()));
        btnApply.setText(cfg != null && cfg.getFilename().equalsIgnoreCase(txtFilename.getText()) ?
                RELOAD_TEXT :
                APPLY_TEXT);
    }

    private void refreshSchedule() throws RemoteException, ScheduleException {
        tblSchedule.setModel(new ScheduleTableModel(mgmt.getSchedule().getScheduleItemList()));
        TableUtil.adjustColumnWidths(tblSchedule, 10, 400);
    }

    private void refreshLog() throws ParseException, RemoteException {
        LogRO logRo = new LogRO();
        logRo.setStartDate(getStartDate());
        logRo.setEndDate(getEndDate());

        tblLog.setModel(new LogTableModel(mgmt.getLog(logRo).getLogItemList()));

        TableUtil.adjustColumnWidths(tblLog, 10, 500);
    }

    private void updateConfig() {
        try {
            String filename = txtFilename.getText();

            if ( ! "".equals(filename) ) {
                MLMEngineConfig config = null;
                try {
                    config = new MLMEngineConfig(new File(filename), this);

                } catch (CanceledException ce) {
                    throw ce;

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " setting configuration - " + e.getMessage(), e);
                    DialogUtil.showErrorDialog(this, "Invalid configuration file '" + filename + "' - " + e.getMessage());

                } finally {
                    DomainCredentialRegistry.getInstance().clear();
                }

                if (config != null) {
                    ConfigRO ro = new ConfigRO();
                    ro.setConfig(config);

                    mgmt.setConfig(ro);

                    refresh();
                }
            }

        } catch (CanceledException e) {
            log.info("operation canceled : " + e.getMessage());

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " setting configuration - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());
        }
    }

    private void btnSelectFileActionPerformed() {
        File file = FileUtil.selectFileToOpen(this,
                new FileNameExtensionFilter("XML Files", "xml"),
                System.getProperty("user.dir"));

        if (file != null) {
            try {
                String filename = file.getCanonicalPath();
                txtFilename.setText(filename);

                refreshButtons();

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " selecting config file - " + e.getMessage(), e);
            }
        }
    }

    private void btnApplyActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            updateConfig();

        } finally {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void btnClearActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            ConfigRO ro = new ConfigRO();
            ro.setConfig(null);

            mgmt.setConfig(ro);

            refresh();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " clearing active configuration - " + e.getMessage(), e);

        } finally {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void btnRefreshLogActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            refreshLog();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " refreshing log - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());

        } finally {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void tblLogMouseClicked(MouseEvent event) {
        Point p = event.getPoint();
        LogItem item = ((LogTable) tblLog).getLogItemAtPoint(p);
        new LogDetails(this, item).setVisible(true);
    }

    private void btnRefreshScheduleActionPerformed() {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            refreshSchedule();

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " refreshing schedule - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(this, e.getMessage());

        } finally {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void tblScheduleMouseClicked(MouseEvent event) {
        Point p = event.getPoint();
        ScheduleItem item = ((ScheduleTable) tblSchedule).getLogItemAtPoint(p);
        new ScheduleDetails(this, item).setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        contentPanel = new JPanel();
        tabbedPane1 = new JTabbedPane();
        tabGeneral = new JPanel();
        label1 = new JLabel();
        txtFilename = new JTextField();
        btnSelectFile = new JButton();
        btnApply = new JButton();
        btnClear = new JButton();
        tabSchedule = new JPanel();
        scrollPane1 = new JScrollPane();
        tblSchedule = new ScheduleTable();
        btnRefreshSchedule = new JButton();
        tabLog = new JPanel();
        scrollPane2 = new JScrollPane();
        tblLog = new LogTable();
        panel1 = new JPanel();
        label2 = new JLabel();
        label3 = new JLabel();
        btnRefreshLog = new JButton();
        txtStartDate = new JFormattedTextField(dateFormatter);
        txtStartDate.setInputVerifier(new DateInputVerifier());
        txtEndDate = new JFormattedTextField(dateFormatter);
        txtEndDate.setInputVerifier(new DateInputVerifier());

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("ArdenCAT MLM Engine Control Panel");
        Container contentPane = getContentPane();

        //======== contentPanel ========
        {

            //======== tabbedPane1 ========
            {

                //======== tabGeneral ========
                {

                    //---- label1 ----
                    label1.setText("Configuration file:");

                    //---- txtFilename ----
                    txtFilename.setEditable(false);

                    //---- btnSelectFile ----
                    btnSelectFile.setText("Select File...");
                    btnSelectFile.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnSelectFileActionPerformed();
                        }
                    });

                    //---- btnApply ----
                    btnApply.setText("Apply");
                    btnApply.setEnabled(false);
                    btnApply.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnApplyActionPerformed();
                        }
                    });

                    //---- btnClear ----
                    btnClear.setText("Clear");
                    btnClear.setEnabled(false);
                    btnClear.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnClearActionPerformed();
                        }
                    });

                    GroupLayout tabGeneralLayout = new GroupLayout(tabGeneral);
                    tabGeneral.setLayout(tabGeneralLayout);
                    tabGeneralLayout.setHorizontalGroup(
                        tabGeneralLayout.createParallelGroup()
                            .addGroup(tabGeneralLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(tabGeneralLayout.createParallelGroup()
                                    .addGroup(tabGeneralLayout.createSequentialGroup()
                                        .addComponent(label1)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtFilename, GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnSelectFile))
                                    .addGroup(GroupLayout.Alignment.TRAILING, tabGeneralLayout.createSequentialGroup()
                                        .addGap(0, 458, Short.MAX_VALUE)
                                        .addComponent(btnClear)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnApply)))
                                .addContainerGap())
                    );
                    tabGeneralLayout.setVerticalGroup(
                        tabGeneralLayout.createParallelGroup()
                            .addGroup(tabGeneralLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(tabGeneralLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(label1)
                                    .addComponent(btnSelectFile)
                                    .addComponent(txtFilename, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 199, Short.MAX_VALUE)
                                .addGroup(tabGeneralLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnApply)
                                    .addComponent(btnClear))
                                .addContainerGap())
                    );
                }
                tabbedPane1.addTab("General", tabGeneral);

                //======== tabSchedule ========
                {

                    //======== scrollPane1 ========
                    {

                        //---- tblSchedule ----
                        tblSchedule.setAutoCreateRowSorter(true);
                        tblSchedule.setSelectionForeground(Color.black);
                        tblSchedule.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                tblScheduleMouseClicked(e);
                            }
                        });
                        scrollPane1.setViewportView(tblSchedule);
                    }

                    //---- btnRefreshSchedule ----
                    btnRefreshSchedule.setText("Refresh");
                    btnRefreshSchedule.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            btnRefreshScheduleActionPerformed();
                        }
                    });

                    GroupLayout tabScheduleLayout = new GroupLayout(tabSchedule);
                    tabSchedule.setLayout(tabScheduleLayout);
                    tabScheduleLayout.setHorizontalGroup(
                        tabScheduleLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, tabScheduleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(tabScheduleLayout.createParallelGroup()
                                    .addGroup(tabScheduleLayout.createSequentialGroup()
                                        .addGap(0, 526, Short.MAX_VALUE)
                                        .addComponent(btnRefreshSchedule))
                                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 617, Short.MAX_VALUE))
                                .addContainerGap())
                    );
                    tabScheduleLayout.setVerticalGroup(
                        tabScheduleLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, tabScheduleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnRefreshSchedule)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                                .addGap(20, 20, 20))
                    );
                }
                tabbedPane1.addTab("Schedule", tabSchedule);

                //======== tabLog ========
                {

                    //======== scrollPane2 ========
                    {

                        //---- tblLog ----
                        tblLog.setAutoCreateRowSorter(true);
                        tblLog.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                        tblLog.setSelectionForeground(Color.black);
                        tblLog.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                tblLogMouseClicked(e);
                            }
                        });
                        scrollPane2.setViewportView(tblLog);
                    }

                    //======== panel1 ========
                    {
                        panel1.setBorder(new TitledBorder("Log Search Parameters"));

                        //---- label2 ----
                        label2.setText("Start date:");

                        //---- label3 ----
                        label3.setText("End date:");

                        //---- btnRefreshLog ----
                        btnRefreshLog.setText("Refresh");
                        btnRefreshLog.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                btnRefreshLogActionPerformed();
                            }
                        });

                        GroupLayout panel1Layout = new GroupLayout(panel1);
                        panel1.setLayout(panel1Layout);
                        panel1Layout.setHorizontalGroup(
                            panel1Layout.createParallelGroup()
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(label2)
                                    .addGap(6, 6, 6)
                                    .addComponent(txtStartDate, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(label3)
                                    .addGap(2, 2, 2)
                                    .addComponent(txtEndDate, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                                    .addComponent(btnRefreshLog)
                                    .addContainerGap())
                        );
                        panel1Layout.setVerticalGroup(
                            panel1Layout.createParallelGroup()
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(label2)
                                        .addComponent(btnRefreshLog)
                                        .addComponent(label3)
                                        .addComponent(txtEndDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtStartDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addGap(0, 6, Short.MAX_VALUE))
                        );
                    }

                    GroupLayout tabLogLayout = new GroupLayout(tabLog);
                    tabLog.setLayout(tabLogLayout);
                    tabLogLayout.setHorizontalGroup(
                        tabLogLayout.createParallelGroup()
                            .addGroup(tabLogLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(tabLogLayout.createParallelGroup()
                                    .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 617, Short.MAX_VALUE)
                                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                    );
                    tabLogLayout.setVerticalGroup(
                        tabLogLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, tabLogLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                .addContainerGap())
                    );
                }
                tabbedPane1.addTab("Logs", tabLog);
            }

            GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
            contentPanel.setLayout(contentPanelLayout);
            contentPanelLayout.setHorizontalGroup(
                contentPanelLayout.createParallelGroup()
                    .addComponent(tabbedPane1, GroupLayout.Alignment.TRAILING)
            );
            contentPanelLayout.setVerticalGroup(
                contentPanelLayout.createParallelGroup()
                    .addComponent(tabbedPane1)
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createParallelGroup()
                    .addGroup(contentPaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(contentPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap()))
                .addGap(0, 718, Short.MAX_VALUE)
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createParallelGroup()
                    .addGroup(contentPaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(contentPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap()))
                .addGap(0, 381, Short.MAX_VALUE)
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel contentPanel;
    private JTabbedPane tabbedPane1;
    private JPanel tabGeneral;
    private JLabel label1;
    private JTextField txtFilename;
    private JButton btnSelectFile;
    private JButton btnApply;
    private JButton btnClear;
    private JPanel tabSchedule;
    private JScrollPane scrollPane1;
    private JTable tblSchedule;
    private JButton btnRefreshSchedule;
    private JPanel tabLog;
    private JScrollPane scrollPane2;
    private JTable tblLog;
    private JPanel panel1;
    private JLabel label2;
    private JLabel label3;
    private JButton btnRefreshLog;
    private JFormattedTextField txtStartDate;
    private JFormattedTextField txtEndDate;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
