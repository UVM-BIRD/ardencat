/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Fri May 02 11:22:46 EDT 2014
 */

package edu.uvm.ccts.ardencat.mlmengine.ui;

import edu.uvm.ccts.ardencat.mlmengine.model.log.LogItem;
import edu.uvm.ccts.ardencat.mlmengine.model.log.Status;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Matthew Storer
 */
public class LogDetails extends JDialog {
    private static final Color RUNNING_COLOR = new Color(225, 225, 0);
    private static final Color SUCCESS_COLOR = new Color(0, 225, 0);
    private static final Color FAIL_COLOR = new Color(225, 0, 0);

    public LogDetails(Frame owner, LogItem logItem) {
        super(owner);
        initComponents();

        lblMLMID.setText(logItem.getMlmId());
        lblExecutionTime.setText(logItem.getExecutionTime().toString());

        Status status = logItem.getStatus();
        lblStatus.setText(status.toString());
        switch (status) {
            case RUNNING: lblStatus.setForeground(RUNNING_COLOR); break;
            case SUCCESS: lblStatus.setForeground(SUCCESS_COLOR); break;
            case FAIL:    lblStatus.setForeground(FAIL_COLOR); break;
        }

        txtMessage.setText(logItem.getMessage());
        txtMessage.setCaretPosition(0);
    }

    public LogDetails(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void okButtonActionPerformed() {
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        lblMLMID = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        label5 = new JLabel();
        lblStatus = new JLabel();
        lblExecutionTime = new JLabel();
        scrollPane1 = new JScrollPane();
        txtMessage = new JTextArea();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setAlwaysOnTop(true);
        setTitle("Log Details");
        setModal(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("MLM ID:");

                //---- lblMLMID ----
                lblMLMID.setText("some_mlm:Institution:testing:1.00");

                //---- label3 ----
                label3.setText("Execution time:");

                //---- label4 ----
                label4.setText("Status:");

                //---- label5 ----
                label5.setText("Message:");

                //---- lblStatus ----
                lblStatus.setText("SUCCESS");

                //---- lblExecutionTime ----
                lblExecutionTime.setText("2014-05-02 11:11:21");

                //======== scrollPane1 ========
                {

                    //---- txtMessage ----
                    txtMessage.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 11));
                    txtMessage.setEditable(false);
                    txtMessage.setForeground(Color.darkGray);
                    scrollPane1.setViewportView(txtMessage);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(label3)
                                        .addComponent(label4))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(lblMLMID, GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                                        .addComponent(lblExecutionTime, GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                                        .addComponent(lblStatus, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)))
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(label1)
                                        .addComponent(label5))
                                    .addGap(0, 776, Short.MAX_VALUE))
                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
                            .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label1)
                                .addComponent(lblMLMID))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(lblExecutionTime)
                                .addComponent(label3))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(lblStatus)
                                .addComponent(label4))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(label5)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                            .addContainerGap())
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed();
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        setSize(900, 400);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel lblMLMID;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JLabel lblStatus;
    private JLabel lblExecutionTime;
    private JScrollPane scrollPane1;
    private JTextArea txtMessage;
    private JPanel buttonBar;
    private JButton okButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
