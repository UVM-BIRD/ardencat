/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.ui.model;

import edu.uvm.ccts.ardencat.mlmengine.model.schedule.OneTimeTrigger;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.RecurringTrigger;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.Trigger;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 5/2/14.
 */
public class TriggersTableModel extends AbstractTableModel {
    private List<Trigger> list;

    public TriggersTableModel(List<Trigger> list) {
        this.list = list;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:  return "Type";
            case 1:  return "Start date";
            case 2:  return "End date";
            case 3:  return "Repeat interval";
            case 4:  return "Repeat count";
            case 5:  return "Times triggered";
            default: return super.getColumnName(column);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:  return String.class;
            case 1:  return Date.class;
            case 2:  return Date.class;
            case 3:  return Integer.class;
            case 4:  return Integer.class;
            case 5:  return Integer.class;
            default: return super.getColumnClass(columnIndex);
        }
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Trigger t = list.get(rowIndex);
        if (t instanceof OneTimeTrigger) {
            if (columnIndex == 0) {
                return "Once only";

            } else if (columnIndex == 1) {
                OneTimeTrigger ott = (OneTimeTrigger) t;
                return ott.getExecutionTime();
            }

        } else if (t instanceof RecurringTrigger) {
            RecurringTrigger rt = (RecurringTrigger) t;
            switch (columnIndex) {
                case 0:  return "Recurring";
                case 1:  return rt.getStartTime();
                case 2:  return rt.getEndTime();
                case 3:  return rt.getRepeatInterval();
                case 4:  return rt.getRepeatCount();
                case 5:  return rt.getTimesTriggered();
                default: return null;
            }
        }

        return null;
    }
}
