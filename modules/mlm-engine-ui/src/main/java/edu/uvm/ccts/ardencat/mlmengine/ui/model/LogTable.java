/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.ui.model;

import edu.uvm.ccts.ardencat.mlmengine.model.log.LogItem;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by mstorer on 5/2/14.
 */
public class LogTable extends JTable {
    private static final Color EVEN_ROW_COLOR = new Color(229, 243, 255);
    private static final Color ODD_ROW_COLOR = new Color(255, 255, 255);
    private static final Color RUNNING_COLOR = new Color(255, 255, 150);
//    private static final Color SUCCESS_COLOR = new Color(150, 255, 150);
    private static final Color FAIL_COLOR = new Color(255, 150, 150);

    public LogItem getLogItemAtPoint(Point p) {
        int row = convertRowIndexToModel(rowAtPoint(p));
        return ((LogTableModel) getModel()).getLogItemAtRow(row);
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        Point p = event.getPoint();
        return String.valueOf(getModel().getValueAt(
                convertRowIndexToModel(rowAtPoint(p)),
                convertColumnIndexToModel(columnAtPoint(p))));
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);

        LogItem item = ((LogTableModel) getModel()).getLogItemAtRow(convertRowIndexToModel(row));

        switch (item.getStatus()) {
            case RUNNING:   comp.setBackground(RUNNING_COLOR);  break;
            case SUCCESS:
                if (row % 2 == 0) comp.setBackground(EVEN_ROW_COLOR);
                else              comp.setBackground(ODD_ROW_COLOR);
                break;
            case FAIL:      comp.setBackground(FAIL_COLOR);     break;
            default:        comp.setBackground(null);
        }

        return comp;
    }
}
