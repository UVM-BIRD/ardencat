/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Fri May 02 14:16:24 EDT 2014
 */

package edu.uvm.ccts.ardencat.mlmengine.ui;

import edu.uvm.ccts.ardencat.mlmengine.model.schedule.Job;
import edu.uvm.ccts.ardencat.mlmengine.model.schedule.ScheduleItem;
import edu.uvm.ccts.ardencat.mlmengine.ui.model.*;
import edu.uvm.ccts.ardencat.ui.model.DateTimeRenderer;
import edu.uvm.ccts.common.util.TableUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * @author Matthew Storer
 */
public class ScheduleDetails extends JDialog {
    public ScheduleDetails(Frame owner, ScheduleItem item) {
        super(owner);
        initComponents();

        Job job = item.getJob();

        lblJobName.setText(job.getName());
        lblJobGroup.setText(job.getGroup());

        tblJobData.setModel(new JobDataTableModel(job.getJobDataList()));
        tblJobData.setDefaultRenderer(Date.class, new DateTimeRenderer());
        TableUtil.adjustColumnWidths(tblJobData);
        TableUtil.makeAutoResizable(tblJobData);

        tblTriggers.setModel(new TriggersTableModel(job.getTriggerList()));
        tblTriggers.setDefaultRenderer(Date.class, new DateTimeRenderer());
        TableUtil.adjustColumnWidths(tblTriggers);
        TableUtil.makeAutoResizable(tblTriggers);
    }

    public ScheduleDetails(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void okButtonActionPerformed() {
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        lblJobName = new JLabel();
        label3 = new JLabel();
        lblJobGroup = new JLabel();
        label5 = new JLabel();
        scrollPane1 = new JScrollPane();
        tblJobData = new JobDataTable();
        label6 = new JLabel();
        scrollPane2 = new JScrollPane();
        tblTriggers = new TriggersTable();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setAlwaysOnTop(true);
        setModal(true);
        setTitle("Schedule Details");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- label1 ----
                label1.setText("Job name:");

                //---- lblJobName ----
                lblJobName.setText("job_name");

                //---- label3 ----
                label3.setText("Job group:");

                //---- lblJobGroup ----
                lblJobGroup.setText("job_group");

                //---- label5 ----
                label5.setText("Job data:");

                //======== scrollPane1 ========
                {

                    //---- tblJobData ----
                    tblJobData.setSelectionForeground(Color.black);
                    scrollPane1.setViewportView(tblJobData);
                }

                //---- label6 ----
                label6.setText("Triggers:");

                //======== scrollPane2 ========
                {

                    //---- tblTriggers ----
                    tblTriggers.setSelectionForeground(Color.black);
                    scrollPane2.setViewportView(tblTriggers);
                }

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(label1)
                                        .addComponent(label3))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(lblJobName, GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)
                                        .addComponent(lblJobGroup, GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)))
                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addGroup(contentPanelLayout.createParallelGroup()
                                        .addComponent(label5)
                                        .addComponent(label6))
                                    .addGap(0, 778, Short.MAX_VALUE))
                                .addComponent(scrollPane2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
                            .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label1)
                                .addComponent(lblJobName))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(label3)
                                .addComponent(lblJobGroup))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(label5)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(label6)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                            .addGap(20, 20, 20))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed();
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        setSize(900, 400);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel lblJobName;
    private JLabel label3;
    private JLabel lblJobGroup;
    private JLabel label5;
    private JScrollPane scrollPane1;
    private JTable tblJobData;
    private JLabel label6;
    private JScrollPane scrollPane2;
    private JTable tblTriggers;
    private JPanel buttonBar;
    private JButton okButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
