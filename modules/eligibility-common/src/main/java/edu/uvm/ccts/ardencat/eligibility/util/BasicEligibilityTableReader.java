/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.eligibility.util;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.eligibility.model.EligibilityData;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;
import edu.uvm.ccts.common.util.Base64Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by mstorer on 2/12/15.
 */
public class BasicEligibilityTableReader {
    private static final Log log = LogFactory.getLog(BasicEligibilityTableReader.class);
    private static final int MAX_VARS_PER_QUERY = 999;

    public static List<BasicEligibilityRecord> buildList(DataSource dataSource, String source,
                                                    List<String> keyFields,
                                                    Object firstBatch, Object lastBatch)
            throws SQLException, ValidationException, DataException {

        return buildList(dataSource, source, keyFields, null, firstBatch, lastBatch);
    }

    public static List<BasicEligibilityRecord> buildList(DataSource dataSource, String source,
                                                         List<String> keyFields, List<String> sortByFields,
                                                         Object firstBatch, Object lastBatch)
            throws SQLException, ValidationException, DataException {

        String query = "select id, source, status, overrideStatus, batch, data " +
                "from tblACEligibility " +
                "where source = ? and exportId is null and batch >= ? and batch <= ? " +
                "order by batch desc";

        PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

        int index = 1;

        stmt.setString(index++, source);
        stmt.setObject(index++, firstBatch);

        if (lastBatch != null) stmt.setObject(index++, lastBatch);

        ResultSet rs = stmt.executeQuery();

        Set<String> foundKeys = new HashSet<String>();
        List<BasicEligibilityRecord> eligibleList = new ArrayList<BasicEligibilityRecord>();
        List<BasicEligibilityRecord> unknownList = new ArrayList<BasicEligibilityRecord>();
        List<BasicEligibilityRecord> ineligibleList = new ArrayList<BasicEligibilityRecord>();

        while (rs.next()) {
            BasicEligibilityRecord rec = buildRecord(rs);

            String key = buildKey(keyFields, rec.getData());
            if ( ! foundKeys.contains(key) ) {
                switch (rec.getOriginalStatus()) {
                    case ELIGIBLE:      eligibleList.add(rec);      break;
                    case UNKNOWN:       unknownList.add(rec);       break;
                    case INELIGIBLE:    ineligibleList.add(rec);    break;
                }

                foundKeys.add(key);
            }
        }

        if (sortByFields != null && sortByFields.size() > 0) {
            Comparator<BasicEligibilityRecord> c = new EligibilityRecordComparator(sortByFields);

            Collections.sort(eligibleList, c);
            Collections.sort(unknownList, c);
            Collections.sort(ineligibleList, c);
        }

        List<BasicEligibilityRecord> records = new ArrayList<BasicEligibilityRecord>();
        records.addAll(eligibleList);
        records.addAll(unknownList);
        records.addAll(ineligibleList);

        return records;
    }

    public static List<BasicEligibilityRecord> buildList(DataSource dataSource, List<Integer> idList) throws SQLException, DataException {
        Map<Integer, BasicEligibilityRecord> map = new HashMap<Integer, BasicEligibilityRecord>();

        for (List<Integer> ids : chunkRecordIds(idList)) {
            String query = "select id, source, status, overrideStatus, batch, data " +
                    "from tblACEligibility " +
                    "where id in " + generateIdPlaceholders(ids.size()) + " and exportId is null " +
                    "order by batch desc";

            PreparedStatement stmt = dataSource.getConnection().prepareStatement(query);

            int i = 1;
            for (Integer id : idList) stmt.setInt(i++, id);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BasicEligibilityRecord record = buildRecord(rs);
                map.put(record.getId(), record);
            }
        }

        List<BasicEligibilityRecord> records = new ArrayList<BasicEligibilityRecord>();

        for (Integer id : idList) {
            BasicEligibilityRecord record = map.get(id);

            if (record != null) records.add(map.get(id));
            else                log.warn("couldn't find eligibility record with id=" + id + " - skipping");
        }

        return records;
    }

    @SuppressWarnings("unchecked")
    private static BasicEligibilityRecord buildRecord(ResultSet rs) throws SQLException, DataException {
        BasicEligibilityRecord rec = new BasicEligibilityRecord();

        rec.setId(rs.getInt(1));
        rec.setSource(rs.getString(2));
        rec.setOriginalStatus(EligibilityStatus.fromString(rs.getString(3)));

        String s = rs.getString(4);
        if (s != null) {
            rec.setOverrideStatus(EligibilityStatus.fromString(s));
        }

        rec.setBatch(rs.getString(5));

        String dataStr = rs.getString(6);

        try {
            Map<String, Object> m = (Map<String, Object>) Base64Util.deserialize(dataStr);
            rec.setData(new EligibilityData(m));

        } catch (Exception e) {
            throw new DataException(e);
        }

        return rec;
    }

    private static String generateIdPlaceholders(int count) {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (int i = 0; i < count - 1; i ++) sb.append("?,");
        sb.append("?)");
        return sb.toString();
    }

    private static List<List<Integer>> chunkRecordIds(Collection<Integer> recordIds) {
        int chunks = recordIds.size() / MAX_VARS_PER_QUERY;
        if (recordIds.size() % MAX_VARS_PER_QUERY > 0) chunks += 1;

        List<List<Integer>> chunkedRecordIds = new ArrayList<List<Integer>>();
        for (int i = 0; i < chunks; i ++) {
            chunkedRecordIds.add(new ArrayList<Integer>());
        }

        int i = 0;
        Iterator<Integer> iter = recordIds.iterator();
        while (iter.hasNext()) {
            chunkedRecordIds.get(i++ % chunks).add(iter.next());
        }

        return chunkedRecordIds;
    }

    private static String buildKey(List<String> keys, EligibilityData data) {
        List<String> list = new ArrayList<String>();

        for (String key : keys) {
            Object obj = data.getByKey(key);
            String s = obj == null ? "null" : obj.toString();
            list.add(s);
        }

        return StringUtils.join(list, "|").toLowerCase();
    }

    private static final class EligibilityRecordComparator implements Comparator<BasicEligibilityRecord> {
        private List<String> sortByFields;

        public EligibilityRecordComparator(List<String> sortByFields) {
            this.sortByFields = sortByFields;
        }

        @Override
        @SuppressWarnings("unchecked")
        public int compare(BasicEligibilityRecord er1, BasicEligibilityRecord er2) {
            for (String field : sortByFields) {
                Object o1 = er1.getData().getByKey(field);
                Object o2 = er2.getData().getByKey(field);

                int rval;

                if (o1 == null && o2 == null) {
                    rval = 0;

                } else if (o1 == null) {
                    rval = -1;

                } else if (o2 == null) {
                    rval = 1;

                } else if (o1 instanceof Comparable && o1.getClass().isAssignableFrom(o2.getClass())) {
                    rval = ((Comparable) o1).compareTo(o2);

                } else {
                    rval = o1.toString().compareTo(o2.toString());
                }

                if (rval != 0) return rval;
            }

            return 0;
        }
    }
}
