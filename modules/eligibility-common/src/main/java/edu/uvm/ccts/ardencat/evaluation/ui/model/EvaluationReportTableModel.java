/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.evaluation.ui.model;

import edu.uvm.ccts.ardencat.evaluation.model.EvaluationResults;
import edu.uvm.ccts.ardencat.evaluation.model.EvaluationReportData;

import javax.swing.table.AbstractTableModel;

/**
 * Created by mstorer on 2/21/14.
 */
public class EvaluationReportTableModel extends AbstractTableModel {
    private EvaluationReportData data = null;

    public EvaluationReportTableModel() {
    }

    public EvaluationReportTableModel(EvaluationResults results) {
        data = new EvaluationReportData(results);
    }

    @Override
    public String getColumnName(int column) {
        return data != null ? data.getColumnName(column) : "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        return data != null ? data.getRowCount() : 0;
    }

    @Override
    public int getColumnCount() {
        return data != null ? data.getColumnCount() : 0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data != null ? data.getValue(rowIndex, columnIndex) : null;
    }
}
