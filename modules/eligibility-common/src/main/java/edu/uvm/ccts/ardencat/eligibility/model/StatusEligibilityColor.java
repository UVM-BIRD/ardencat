/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.eligibility.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.awt.*;

/**
 * Created by mstorer on 10/7/14.
 */
public class StatusEligibilityColor implements EligibilityColor {
    private String label;
    private Color color;
    private EligibilityStatus status;

    public StatusEligibilityColor(EligibilityStatus status, Color color) {
        this.label = StringUtils.capitalize(status.toString().toLowerCase());
        this.status = status;
        this.color = color;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public EligibilityStatus getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        StatusEligibilityColor sec = (StatusEligibilityColor) o;
        return new EqualsBuilder()
                .append(label, sec.label)
                .append(color, sec.color)
                .append(status, sec.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(853, 1447)
                .append(label)
                .append(color)
                .append(status)
                .toHashCode();
    }
}
