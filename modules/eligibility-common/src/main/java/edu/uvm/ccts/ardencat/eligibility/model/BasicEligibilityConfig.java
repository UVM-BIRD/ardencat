/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.eligibility.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by mstorer on 2/11/15.
 */
public class BasicEligibilityConfig implements Serializable {
    protected static final EligibilityColor ELIGIBLE_DFLT = new StatusEligibilityColor(
            EligibilityStatus.ELIGIBLE,
            new Color(150, 255, 150)
    );

    protected static final EligibilityColor UNKNOWN_DFLT = new StatusEligibilityColor(
            EligibilityStatus.UNKNOWN,
            new Color(255, 255, 150)
    );

    protected static final EligibilityColor INELIGIBLE_DFLT = new StatusEligibilityColor(
            EligibilityStatus.INELIGIBLE,
            new Color(210, 210, 210)
    );

    public EligibilityColor getEligibleColor() {
        return ELIGIBLE_DFLT;
    }

    public EligibilityColor getUnknownColor() { return UNKNOWN_DFLT; }

    public EligibilityColor getIneligibleColor() { return INELIGIBLE_DFLT; }
}
