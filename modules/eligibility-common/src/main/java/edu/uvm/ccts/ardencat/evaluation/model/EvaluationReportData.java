/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.evaluation.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mstorer on 2/21/14.
 */
public class EvaluationReportData {
    private static final List<String> HEADERS = Arrays.asList("True Positive", "False Positive", "False Negative");
    private List<List<Object>> data;

    public EvaluationReportData(EvaluationResults results) {
        List<Object> tpList = results.getTruePositives();
        List<Object> fpList = results.getFalsePositives();
        List<Object> fnList = results.getFalseNegatives();

        int rowCount = tpList.size();
        if (fpList.size() > rowCount) rowCount = fpList.size();
        if (fnList.size() > rowCount) rowCount = fnList.size();

        data = new ArrayList<List<Object>>();
        for (int i = 0; i < rowCount; i ++) {
            List<Object> row = new ArrayList<Object>();

            if (i < tpList.size())  row.add(tpList.get(i));
            else                    row.add(null);

            if (i < fpList.size())  row.add(fpList.get(i));
            else                    row.add(null);

            if (i < fnList.size())  row.add(fnList.get(i));
            else                    row.add(null);

            data.add(row);
        }
    }

    public int getColumnCount() {
        return HEADERS.size();
    }

    public int getRowCount() {
        return data.size();
    }

    public List<String> getColumnNames() {
        return HEADERS;
    }

    public String getColumnName(int col) {
        return HEADERS.get(col);
    }

    public List<Object> getRow(int row) {
        return data.get(row);
    }

    public Object getValue(int row, int col) {
        return data.get(row).get(col);
    }
}
