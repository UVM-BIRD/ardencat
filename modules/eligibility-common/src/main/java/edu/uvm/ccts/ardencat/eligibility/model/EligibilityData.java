/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.eligibility.model;

import edu.uvm.ccts.ardencat.model.FieldConstants;

import java.io.Serializable;
import java.util.*;

/**
 * Created by mstorer on 1/29/14.
 */
public class EligibilityData implements FieldConstants, Serializable {
    public static final String ID = FieldConstants.ID_FIELD_NAME;

    private Map<String, Object> map = new LinkedHashMap<String, Object>();
    private List<String> keys = new ArrayList<String>();

    public EligibilityData(Map<String, Object> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey().toLowerCase();
                keys.add(key);
                this.map.put(key, entry.getValue());
            }
        }
    }

    public Collection<String> getKeys() {
        return keys;
    }

    public int size() {
        return map.size();
    }

    public Object getId() {
        return map.get(ID);
    }

    public String getKeyAtIndex(int index) {
        return keys.get(index);
    }

    public Object getByIndex(int index) {
        return map.get(keys.get(index));
    }

    public void putByIndex(int index, Object obj) {
        map.put(keys.get(index), obj);
    }

    public Object getByKey(String key) {
        return map.get(key.toLowerCase());
    }
}
