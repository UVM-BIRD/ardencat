/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.eligibility.model;

import edu.uvm.ccts.ardencat.model.EligibilityStatus;

import java.io.Serializable;

/**
 * Created by mstorer on 2/11/15.
 */

public class BasicEligibilityRecord implements Serializable {
    public static final String ID_FIELD = "id";
    public static final String SOURCE_FIELD = "source";
    public static final String ORIG_STATUS_FIELD = "originalStatus";
    public static final String OVER_STATUS_FIELD = "overrideStatus";
    public static final String BATCH_FIELD = "batch";
    public static final String DATA_FIELD = "data";

    private int id;
    private String source;
    private EligibilityStatus originalStatus;
    private EligibilityStatus overrideStatus = null;
    private String batch;
    protected EligibilityData data;

    public BasicEligibilityRecord() {
    }

    public BasicEligibilityRecord(BasicEligibilityRecord rec) {
        this.id = rec.id;
        this.source = rec.source;
        this.originalStatus = rec.originalStatus;
        this.overrideStatus = rec.overrideStatus;
        this.batch = rec.batch;
        this.data = rec.data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public EligibilityStatus getOriginalStatus() {
        return originalStatus;
    }

    public void setOriginalStatus(EligibilityStatus originalStatus) {
        this.originalStatus = originalStatus;
    }

    public EligibilityStatus getOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(EligibilityStatus overrideStatus) {
        this.overrideStatus = overrideStatus;
    }

    public EligibilityStatus getStatus() {
        return overrideStatus != null ? overrideStatus : originalStatus;
    }

    public boolean hasOverriddenStatus() {
        return overrideStatus != null;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public EligibilityData getData() {
        return data;
    }

    public void setData(EligibilityData data) {
        this.data = data;
    }
}
