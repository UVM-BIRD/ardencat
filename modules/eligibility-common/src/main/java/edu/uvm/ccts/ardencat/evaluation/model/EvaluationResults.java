/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.evaluation.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mstorer on 2/21/14.
 */
public class EvaluationResults {
    private String fieldName;
    private List<Object> tpList = new ArrayList<Object>();
    private List<Object> fnList = new ArrayList<Object>();
    private List<Object> fpList = new ArrayList<Object>();
    private int expectedRecordCount;
    private int foundRecordCount;
    private int tp;
    private int fp;
    private int fn;
    private float p;
    private float r;

    public EvaluationResults(String fieldName, Collection<Object> expectedValues, Collection<Object> actualValues) {
        this.fieldName = fieldName;
        expectedRecordCount = expectedValues.size();
        foundRecordCount = actualValues.size();

        for (Object expectedValue : expectedValues) {
            if (actualValues.contains(expectedValue)) tpList.add(expectedValue);
            else                                      fnList.add(expectedValue);
        }

        for (Object actualValue : actualValues) {
            if ( ! expectedValues.contains(actualValue) ) fpList.add(actualValue);
        }

        // true positive - in both validated list and actual results
        // false negative - in validated list but not in actual results
        // false positive - in actual results but not in validated list

        tp = tpList.size();
        fp = fpList.size();
        fn = fnList.size();
        p = (float) tp / (tp + fp);
        r = (float) tp / (tp + fn);
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getExpectedRecordCount() {
        return expectedRecordCount;
    }

    public int getFoundRecordCount() {
        return foundRecordCount;
    }

    public int getTruePositiveCount() {
        return tp;
    }

    public int getFalsePositiveCount() {
        return fp;
    }

    public int getFalseNegativeCount() {
        return fn;
    }

    public float getPrecision() {
        return p;
    }

    public float getRecall() {
        return r;
    }

    public List<Object> getTruePositives() {
        return tpList;
    }

    public List<Object> getFalseNegatives() {
        return fnList;
    }

    public List<Object> getFalsePositives() {
        return fpList;
    }
}
