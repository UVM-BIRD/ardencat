/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Thu Jul 02 10:33:28 EDT 2015
 */

package edu.uvm.ccts.ardencat.svc.ui;

import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.remote.ServiceManagement;
import edu.uvm.ccts.ardencat.svc.remote.model.ServiceConfigRO;
import edu.uvm.ccts.common.exceptions.ConfigurationException;

import java.awt.*;
import java.io.File;

/**
 * @author Matthew Storer
 */
public class GUI extends AbstractGUI<ServiceConfig> {
    public GUI(ServiceManagement<ServiceConfig> mgmt) throws Exception {
        super(mgmt);
    }

    @Override
    protected ServiceConfig buildConfig(String filename, Frame frame) throws ConfigurationException {
        return new ServiceConfig(new File(filename), frame);
    }

    @Override
    protected ServiceConfigRO<ServiceConfig> buildRO(ServiceConfig cfg) {
        ServiceConfigRO<ServiceConfig> ro = new ServiceConfigRO<ServiceConfig>();
        ro.setConfig(cfg);
        return ro;
    }
}
