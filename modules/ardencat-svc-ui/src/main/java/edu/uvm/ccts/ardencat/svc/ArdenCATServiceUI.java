/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.rmi.RMIConfigUtil;
import edu.uvm.ccts.ardencat.rmi.model.RMIConfig;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.remote.ServiceManagement;
import edu.uvm.ccts.ardencat.svc.remote.ArdenCATServiceRemoteEndpoint;
import edu.uvm.ccts.ardencat.svc.ui.GUI;
import edu.uvm.ccts.common.rmi.RMILoopbackSocketFactory;
import edu.uvm.ccts.common.util.DialogUtil;
import edu.uvm.ccts.common.util.LicenseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMISocketFactory;

/**
 * Created by mstorer on 7/2/15.
 */
public class ArdenCATServiceUI implements ArdenCATServiceRemoteEndpoint {
    private static final Log log = LogFactory.getLog(ArdenCATServiceUI.class);

    private static ServiceManagement<ServiceConfig> mgmt;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " attempting to set system look and feel - " + e.getMessage(), e);
        }

        LicenseUtil.requireLicenseAcceptance(ArdenCATServiceUI.class, "ArdenCAT Licensing", "/gpl-3.0-standalone.html");

        try {
            mgmt = setupRMI();
            GUI gui = new GUI(mgmt);
            setupHeartbeatMonitor(gui);
            gui.setVisible(true);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " connecting to ArdenCAT Service - " + e.getMessage(), e);
            DialogUtil.showErrorDialog(null, e.getMessage());
            System.exit(-1);
        }
    }

    @SuppressWarnings("unchecked")
    private static ServiceManagement<ServiceConfig> setupRMI() throws Exception {
        RMISocketFactory.setSocketFactory(new RMILoopbackSocketFactory());

        RMIConfig rmiConfig = RMIConfigUtil.getRMIConfig();
        Registry registry = LocateRegistry.getRegistry(rmiConfig.getRegistryPort());

        return (ServiceManagement<ServiceConfig>) registry.lookup(REGISTRY_NAME);
    }

    private static void setupHeartbeatMonitor(final Component component) {
        final Thread heartbeatMonitor = new Thread() {
            @Override
            public void run() {
                boolean running = false;

                try {
                    if (mgmt.isAlive()) {       // initial heartbeat check - don't want to keep resetting 'running' var
                        running = true;         // over and over again in the loop below.
                        Thread.sleep(1000);
                    }

                    while (mgmt.isAlive()) {
                        Thread.sleep(1000);
                    }

                } catch (InterruptedException ie) {
                    // handle silently

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);

                    String msg = running ?
                            "ArdenCAT Service has terminated unexpectedly.  Exiting..." :
                            "ArdenCAT Service is not running.  Exiting...";

                    DialogUtil.showErrorDialog(component, msg);

                    System.exit(-1);
                }
            }
        };

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    mgmt.closeConnections();

                } catch (Exception e) {
                    // handle silently
                }

                heartbeatMonitor.interrupt();
            }
        });

        heartbeatMonitor.start();
    }
}
