/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.model;

import edu.uvm.ccts.ardencat.mlmengine.exceptions.PrimaryTimeNotFoundException;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by mstorer on 11/11/13.
 */
public class ArdenDataSource extends DataSource implements Serializable {
    public static ArdenDataSource buildFromXml(Element xmlDataSource) throws ConfigurationException {
        return buildFromXml(xmlDataSource, null);
    }

    public static ArdenDataSource buildFromXml(Element xmlDataSource, Frame frame) throws ConfigurationException {
        Properties info = new Properties();
        Elements xmlProperties = xmlDataSource.getElements(new XPath("properties/property"));
        while (xmlProperties.hasMoreElements()) {
            Element xmlProperty = xmlProperties.next();
            info.setProperty(xmlProperty.getAttribute("name"), xmlProperty.getAttribute("value"));
        }

        Map<String, PrimaryTimeField> tablePtFieldMap = new HashMap<String, PrimaryTimeField>();
        Elements xmlPtFields = xmlDataSource.getElements(new XPath("primaryTimeFields/forTable"));
        while (xmlPtFields.hasMoreElements()) {
            Element xmlForTable = xmlPtFields.next();

            String name = xmlForTable.getAttribute("name");
            String field = xmlForTable.getAttribute("field");

            PrimaryTimeField ptf = xmlForTable.hasAttribute("fromTable") ?
                    new PrimaryTimeField(name, field, xmlForTable.getAttribute("fromTable")) :
                    new PrimaryTimeField(name, field);

            tablePtFieldMap.put(name.toLowerCase(), ptf);
        }

        return new ArdenDataSource(xmlDataSource.getAttribute("name"),
                xmlDataSource.getAttribute("driver"),
                xmlDataSource.getAttribute("url"),
                xmlDataSource.getAttribute("note"),
                info, tablePtFieldMap, frame);
    }


///////////////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    private Map<String, PrimaryTimeField> tablePtFieldMap;

    public ArdenDataSource(String name, String driver, String url, String note, Properties info,
                           Map<String, PrimaryTimeField> tablePtFieldMap) throws ConfigurationException {

        this(name, driver, url, note, info, tablePtFieldMap, null);
    }

    public ArdenDataSource(String name, String driver, String url, String note, Properties info,
                           Map<String, PrimaryTimeField> tablePtFieldMap, Frame frame) throws ConfigurationException {

        super(name, driver, url, note, info, frame);
        this.tablePtFieldMap = tablePtFieldMap;
    }

    public Map<String, PrimaryTimeField> getTablePtFieldMap() {
        return tablePtFieldMap;
    }

    public PrimaryTimeField getPrimaryTimeFieldForTable(String tableName) throws PrimaryTimeNotFoundException {
        if (tablePtFieldMap.containsKey(tableName.toLowerCase())) {
            return tablePtFieldMap.get(tableName.toLowerCase());

        } else {
            throw new PrimaryTimeNotFoundException("no primary time field for table '" + tableName + "'");
        }
    }
}
