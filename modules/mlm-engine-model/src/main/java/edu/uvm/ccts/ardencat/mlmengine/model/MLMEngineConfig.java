/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.model;

import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.mail.MailConfig;
import electric.xml.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 1/16/14.
 */
public class MLMEngineConfig implements ArdenCATConfiguration, Serializable {
    private String filename = null;
    private MailConfig mailConfig = null;
    private List<ArdenDataSource> mlmDataSources = new ArrayList<ArdenDataSource>();
    private DataSource ardencatDataSource = null;
    private List<MLMConfigData> mlmConfigs = new ArrayList<MLMConfigData>();

    private transient Frame frame;

    public MLMEngineConfig(File file, Frame frame) throws ConfigurationException {
        try {
            this.filename = file.getCanonicalPath();
            this.frame = frame;
            populateFromXml(file);

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    public MLMEngineConfig(String filename, Frame frame, DataSource ardencatDataSource, Element root) throws ConfigurationException {
        this.filename = filename;
        this.frame = frame;
        this.ardencatDataSource = ardencatDataSource;
        populateFromXml(root);
    }


///////////////////////////////////////////////////////////////////////////////////
// public methods
//

    @Override
    public String getFilename() {
        return filename;
    }

    public MailConfig getMailConfig() {
        return mailConfig;
    }

    @Override
    public DataSource getArdencatDataSource() {
        return ardencatDataSource;
    }

    @Override
    public List<ArdenDataSource> getMLMDataSources() {
        return mlmDataSources;
    }

    @Override
    public List<MLMConfigData> getMLMConfigs() {
        return mlmConfigs;
    }


////////////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void populateFromXml(File file) throws ConfigurationException {
        try {
            Document doc = new Document(file);
            Element root = doc.getElement(new XPath("mlmEngine"));

            buildArdencatDataSource(root);
            populateFromXml(root);

        } catch (ParseException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    private void populateFromXml(Element root) throws ConfigurationException {
        try {
            buildMailConfig(root);
            buildMLMDataSources(root);
            buildMLMFileList(root);

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    private void buildArdencatDataSource(Element root) throws ConfigurationException {
        Element xmlDataSource = root.getElement(new XPath("ardencatDataSource"));
        ardencatDataSource = DataSource.buildFromXml("ardencat", xmlDataSource, frame);
    }

    private void buildMailConfig(Element root) throws IOException, ConfigurationException {
        Element xmlMailConfig = root.getElement(new XPath("mailConfig"));
        if (xmlMailConfig != null) {
            mailConfig = MailConfig.buildFromXml(xmlMailConfig, frame);
        }
    }

    private void buildMLMDataSources(Element root) throws ConfigurationException {
        Elements xmlDataSources = root.getElements(new XPath("mlmDataSources/dataSource"));

        List<String> foundIds = new ArrayList<String>();
        while (xmlDataSources.hasMoreElements()) {
            Element xmlDataSource = xmlDataSources.next();
            ArdenDataSource dataSource = ArdenDataSource.buildFromXml(xmlDataSource, frame);

            String key = dataSource.getName().toLowerCase();
            if (foundIds.contains(key)) {
                throw new ConfigurationException("MLM data source IDs must be unique - found multiple definitions for '" +
                        dataSource.getName() + "'");
            }
            mlmDataSources.add(dataSource);
            foundIds.add(key);
        }
    }

    private void buildMLMFileList(Element root) throws IOException, ConfigurationException {
        Elements xmlMlms = root.getElements(new XPath("mlms/mlm"));
        while (xmlMlms.hasMoreElements()) {
            Element xmlMlm = xmlMlms.next();
            String filename = xmlMlm.getAttribute("file");

            String rbName = xmlMlm.hasAttribute("recordBuilder") ?
                    xmlMlm.getAttribute("recordBuilder") :
                    null;

            if (rbName != null && "".equals(rbName.trim())) {
                rbName = null;
            }

            File f = new File(filename);
            if ( ! f.isFile() ) throw new ConfigurationException("MLM '" + filename + "' does not exist");

            mlmConfigs.add(new MLMConfigData(filename, rbName));
        }
    }
}
