/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.mlmengine.model.schedule;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mstorer on 4/25/14.
 */
public class OneTimeTrigger implements Trigger, Serializable {
    private Date executionTime;

    public OneTimeTrigger(Date executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public String toString() {
        return "OneTimeTrigger{executionTime=" + executionTime + "}";
    }

    public Date getExecutionTime() {
        return executionTime;
    }
}
