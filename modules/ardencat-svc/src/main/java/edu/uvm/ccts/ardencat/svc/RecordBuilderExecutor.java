/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.mlmengine.DataSourceRegistry;
import edu.uvm.ccts.ardencat.rb.exceptions.AppenderException;
import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.renderers.Renderer;
import edu.uvm.ccts.ardencat.rb.util.EligibilityUtil;
import edu.uvm.ccts.ardencat.rb.util.ExportUtil;
import edu.uvm.ccts.ardencat.svc.model.*;
import edu.uvm.ccts.ardencat.svc.util.RecordBuilderUtil;
import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 7/6/15.
 */
public class RecordBuilderExecutor {
    private static final Log log = LogFactory.getLog(RecordBuilderExecutor.class);

    public static final String EXPORT_METHOD = "AUTO";

    public void executeAsynchronously(final String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    execute(id);

                } catch (Exception e) {
                    log.error("caught " + e.getClass().getName() + " executing Record Builder '" + id + "' - " + e.getMessage(), e);

                } finally {

                    // todo : data sources not getting closed properly?  maintenance thread is killing these connections -

                    DataSourceRegistry.getInstance().closeAll();
                }
            }
        }).start();
    }

    public void execute(String id) throws DataException, SQLException, ValidationException, LocatorException, AppenderException, IOException {
        log.info("executing Record Builder '" + id + "'");

        try {
            RecordBuilderServiceConfig config = RecordBuilderConfigRegistry.getInstance().getConfig(id);

            List<String> batchList = EligibilityUtil.buildBatchList(config);
            if (batchList.isEmpty()) {
                log.info("no batches to process for source=" + config.getEligibilityConfig().getSource());
                return;
            }

            String firstBatch = batchList.get(0);
            String lastBatch = batchList.get(batchList.size() - 1);

            List<Record> records = RecordBuilderUtil.buildRecords(config, firstBatch, lastBatch);

            int exportId = ExportUtil.writeExportRecords(
                    config.getArdencatDataSource(),
                    config.getEligibilityConfig().getSource(),
                    firstBatch, lastBatch,
                    EXPORT_METHOD,
                    null);

            export(config, records, exportId);

            DataSource ardencatDataSource = config.getArdencatDataSource();
            String source = config.getEligibilityConfig().getSource();

            ExportUtil.markBatchRangeExported(ardencatDataSource, source, firstBatch, lastBatch, exportId);

        } finally {
            log.info("FINISHED executing Record Builder configuration '" + id + "'");
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void export(RecordBuilderServiceConfig config, List<Record> records, int exportId) {
        for (ExportConfig exportConfig : config.getExportConfigs()) {
            List<Record> filteredRecords = new ArrayList<Record>();
            for (Record rec : records) {
                if (exportConfig.getStatusList().contains(rec.getStatus())) {
                    filteredRecords.add(rec);
                }
            }

            if (exportConfig instanceof FileExportConfig) {
                doFileExport((FileExportConfig) exportConfig, filteredRecords);

            } else if (exportConfig instanceof CustomExportConfig) {
                doCustomExport((CustomExportConfig) exportConfig, filteredRecords, exportId);

            } else {
                throw new RuntimeException("unhandled export class '" + exportConfig.getClass().getName() + "'");
            }
        }
    }

    private void doFileExport(FileExportConfig exportConfig, List<Record> records) {
        Renderer renderer = RendererRegistry.getInstance().getRenderer(exportConfig.getMethod());

        try {
            renderer.render(records, exportConfig.isIdentify(), new FileOutputStream(exportConfig.getFilename()));

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " exporting records to " + exportConfig.getFilename() + " - " +
                    e.getMessage(), e);

            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private void doCustomExport(CustomExportConfig exportConfig, List<Record> records, int exportId) {
        try {
            Class c = Class.forName(exportConfig.getClassName());
            CustomExporter t = (CustomExporter) c.newInstance();
            t.export(records, exportConfig.isIdentify(), exportId);

        } catch (Exception e) {
            log.error("caught " + e.getClass().getName() + " exporting records through " + exportConfig.getClassName() + " - " +
                    e.getMessage(), e);

            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
