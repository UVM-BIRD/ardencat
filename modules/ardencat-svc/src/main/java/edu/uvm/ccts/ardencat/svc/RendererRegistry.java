/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.rb.renderers.Renderer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mstorer on 7/8/15.
 */
public class RendererRegistry {
    private static RendererRegistry registry = null;

    public static RendererRegistry getInstance() {
        if (registry == null) registry = new RendererRegistry();
        return registry;
    }

    private Map<String, Renderer> map = new HashMap<String, Renderer>();

    private RendererRegistry() {
        // private to make singleton
    }

    public void registerRenderer(String method, Renderer renderer) {
        map.put(method.toLowerCase(), renderer);
    }

    public Renderer getRenderer(String method) {
        return map.get(method.toLowerCase());
    }

    public void clear() {
        map.clear();
    }
}
