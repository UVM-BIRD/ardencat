/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.util;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.config.Configurator;
import edu.uvm.ccts.ardencat.svc.ServiceConfigRegistry;
import edu.uvm.ccts.ardencat.svc.mlm.ServiceMLMBuilder;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.model.CustomExportConfig;
import edu.uvm.ccts.ardencat.svc.model.ExportConfig;
import edu.uvm.ccts.ardencat.svc.model.RecordBuilderServiceConfig;
import edu.uvm.ccts.common.exceptions.ConfigurationException;

/**
 * Created by mbstorer on 7/29/15.
 */
public abstract class ServiceConfigurator<C extends ServiceConfig> extends Configurator<C> {

    @Override
    protected MLMBuilder getMLMBuilder() {
        return new ServiceMLMBuilder();
    }

    @Override
    protected void registerConfig(C cfg) {
        ServiceConfigRegistry.getInstance().registerConfig(cfg);
    }

    @Override
    protected void clearConfigRegistry() {
        ServiceConfigRegistry.getInstance().clear();
    }

    @Override
    protected void doBeforeConfigLoadTasks(C cfg) throws Exception {
        super.doBeforeConfigLoadTasks(cfg);
        validateCustomExportClasses(cfg);
    }


////////////////////////////////////////////////////////////////////////////////
// private methods
//

    private void validateCustomExportClasses(C cfg) throws ConfigurationException {
        for (RecordBuilderServiceConfig rbCfg : cfg.getRBConfigs()) {
            for (ExportConfig eCfg : rbCfg.getExportConfigs()) {
                if (eCfg instanceof CustomExportConfig) {
                    ((CustomExportConfig) eCfg).validateClass();
                }
            }
        }
    }
}
