/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.mlmengine.exceptions.ImmediateShutdownException;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.util.StandardServiceConfigurator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 7/1/15.
 */
public class ArdenCATService extends AbstractArdenCATService<ServiceConfig> {
    private static final Log log = LogFactory.getLog(ArdenCATService.class);

    private static final Object sync = new Object();

    private static boolean running = true;
    private static ArdenCATService svc;

    public static void main(String[] args) {
        System.out.println("ArdenCAT Service");
        System.out.println("----------------");
        System.out.println("Copyright 2015 The University of Vermont and State Agricultural College, Vermont Oxford Network, and The University of Vermont Medical Center.  All rights reserved.\n");

        svc = new ArdenCATService();

        try {
            svc.start();
            log.info("ArdenCAT Service started successfully.");

        } catch (ImmediateShutdownException e) {
            log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);

            svc.shutdown();
            running = false;
        }

        if (running) {
            // fix to ensure service stays running - main thread cannot terminate before stop() is called
            // see http://www.advancedinstaller.com/forums/viewtopic.php?f=2&t=1996#p6803
            synchronized(sync) {
                while (running) {
                    try {
                        sync.wait();

                    } catch (InterruptedException e) {
                        log.error("caught " + e.getClass().getName() + " - " + e.getMessage(), e);
                    }
                }
            }
        }

        log.info("ArdenCAT Service has stopped.");
    }

    public static void stop() {         // used by Windows Service -
                                        // see http://www.advancedinstaller.com/user-guide/tutorial-java-service.html#preparing
        svc.shutdown();

        running = false;
        synchronized(sync) {
            sync.notify();
        }
    }


/////////////////////////////////////////////////////////////////////////////////////
// instance methods
//

    protected ArdenCATService() {
        super();
    }

    @Override
    protected StandardServiceConfigurator getConfigurator() {
        return new StandardServiceConfigurator();
    }

    @Override
    protected StandardServiceManagementImpl getManagementImpl() {
        return new StandardServiceManagementImpl();
    }
}
