/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.mlmengine.MLMEngineConfigRegistry;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.ardencat.svc.model.RecordBuilderServiceConfig;

/**
 * Created by mbstorer on 7/28/15.
 */
public class ServiceConfigRegistry {
    private static ServiceConfigRegistry registry = null;

    public static ServiceConfigRegistry getInstance() {
        if (registry == null) registry = new ServiceConfigRegistry();
        return registry;
    }

    private MLMEngineConfigRegistry meRegistry = MLMEngineConfigRegistry.getInstance();
    private RecordBuilderConfigRegistry rbRegistry = RecordBuilderConfigRegistry.getInstance();
    private ServiceConfig config = null;

    private ServiceConfigRegistry() {}

    public ServiceConfig getConfig() {
        return config;
    }

    public void registerConfig(ServiceConfig cfg) {
        config = cfg;
        meRegistry.registerConfig(cfg.getMEConfig());
        for (RecordBuilderServiceConfig rbCfg : cfg.getRBConfigs()) {
            rbRegistry.registerConfig(rbCfg);
        }
    }

    public void clear() {
        config = null;
        meRegistry.clear();
        rbRegistry.clear();
    }
}
