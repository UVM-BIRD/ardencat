/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.util;

import edu.uvm.ccts.ardencat.svc.ArdenCATService;
import edu.uvm.ccts.ardencat.svc.model.ServiceConfig;
import edu.uvm.ccts.common.util.FileUtil;
import edu.uvm.ccts.common.util.JarUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 7/6/15.
 */
public class StandardServiceConfigurator extends ServiceConfigurator<ServiceConfig> {
    private static final Log log = LogFactory.getLog(StandardServiceConfigurator.class);

    private static String CONFIG_FILE;
    static {
        try {
            CONFIG_FILE = FileUtil.buildPath(JarUtil.getJarDir(ArdenCATService.class), ".ardencat-config");
        } catch (Exception e) {
            CONFIG_FILE = ".ardencat-config";
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ServiceConfig getActiveConfig() {
        if (FileUtil.exists(CONFIG_FILE)) {
            try {
                return (ServiceConfig) FileUtil.deserializeFromFile(CONFIG_FILE);

            } catch (Exception e) {
                log.error("caught " + e.getClass().getName() + " reading file '" + CONFIG_FILE + "' - " + e.getMessage(), e);
            }
        }
        
        return null;
    }

    @Override
    protected void setActiveConfig(ServiceConfig cfg) throws Exception {
        FileUtil.serializeToFile(CONFIG_FILE, cfg);
    }

    @Override
    protected void deleteActiveConfig() {
        FileUtil.delete(CONFIG_FILE);
    }
}
