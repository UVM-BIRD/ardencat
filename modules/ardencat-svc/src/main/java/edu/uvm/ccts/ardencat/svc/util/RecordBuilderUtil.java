/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.util;

import edu.uvm.ccts.ardencat.eligibility.model.BasicEligibilityRecord;
import edu.uvm.ccts.ardencat.eligibility.util.BasicEligibilityTableReader;
import edu.uvm.ccts.ardencat.model.EligibilityStatus;
import edu.uvm.ccts.ardencat.rb.appenders.RecordAppender;
import edu.uvm.ccts.ardencat.rb.exceptions.AppenderException;
import edu.uvm.ccts.ardencat.rb.exceptions.LocatorException;
import edu.uvm.ccts.ardencat.rb.model.EligibilityConfig;
import edu.uvm.ccts.ardencat.rb.model.Record;
import edu.uvm.ccts.ardencat.rb.util.EligibilityUtil;
import edu.uvm.ccts.ardencat.svc.model.ExportConfig;
import edu.uvm.ccts.ardencat.svc.model.RecordBuilderServiceConfig;
import edu.uvm.ccts.common.exceptions.DataException;
import edu.uvm.ccts.common.exceptions.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mbstorer on 10/12/15.
 */
public class RecordBuilderUtil {
    private static final Log log = LogFactory.getLog(RecordBuilderUtil.class);

    public static List<Record> buildRecords(RecordBuilderServiceConfig config) throws DataException, SQLException, ValidationException,
            LocatorException, AppenderException {

        List<String> batchList = EligibilityUtil.buildBatchList(config);
        if (batchList.isEmpty()) {
            log.info("no batches to process for source=" + config.getEligibilityConfig().getSource());
            return new ArrayList<Record>();
        }

        String firstBatch = batchList.get(0);
        String lastBatch = batchList.get(batchList.size() - 1);

        return buildRecords(config, firstBatch, lastBatch);
    }

    public static List<Record> buildRecords(RecordBuilderServiceConfig config, String firstBatch, String lastBatch)
            throws DataException, SQLException, ValidationException, LocatorException, AppenderException {

        EligibilityConfig eligConfig = config.getEligibilityConfig();

        List<BasicEligibilityRecord> eligibilityRecords = BasicEligibilityTableReader.buildList(
                config.getArdencatDataSource(), eligConfig.getSource(),
                eligConfig.getKeyFields(),
                firstBatch, lastBatch);

        List<Record> records = buildRecordsForExport(config, eligibilityRecords);

        log.info("generated " + records.size() + " records for {source=" + eligConfig.getSource() + ", firstBatch=" +
                firstBatch + ", lastBatch=" + lastBatch + "}");

        return records;
    }

    private static List<Record> buildRecordsForExport(RecordBuilderServiceConfig config, List<BasicEligibilityRecord> eligibilityRecords)
            throws LocatorException, AppenderException {

        // ensure we only process records for those statuses we intend to export
        Set<EligibilityStatus> statusesToProcess = new HashSet<EligibilityStatus>();
        for (ExportConfig exportConfig : config.getExportConfigs()) {
            statusesToProcess.addAll(exportConfig.getStatusList());
        }

        List<BasicEligibilityRecord> filteredEligibilityRecords = new ArrayList<BasicEligibilityRecord>();
        for (BasicEligibilityRecord eligibilityRecord : eligibilityRecords) {
            if (statusesToProcess.contains(eligibilityRecord.getStatus())) {
                filteredEligibilityRecords.add(eligibilityRecord);
            }
        }

        // build records
        List<Record> records = config.getBaseLocator().execute(filteredEligibilityRecords);

        if (config.getAppenders().size() > 0) {
            for (Record rec : records) {
                for (RecordAppender appender : config.getAppenders()) {
                    appender.execute(rec);
                }
            }
        }

        return records;
    }
}
