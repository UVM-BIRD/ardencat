/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.mlm;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMEvalVisitor;
import edu.uvm.ccts.ardencat.mlmengine.model.ArdenDataSource;

/**
 * Created by mbstorer on 10/12/15.
 */
public class ServiceMLMEvalVisitor extends MLMEvalVisitor {
    private String recordBuilderName;

    public ServiceMLMEvalVisitor(String recordBuilderName) {
        super();
        this.recordBuilderName = recordBuilderName;
    }

    public ServiceMLM getMLM() {
        return new ServiceMLM(categories, recordBuilderName);
    }
}
