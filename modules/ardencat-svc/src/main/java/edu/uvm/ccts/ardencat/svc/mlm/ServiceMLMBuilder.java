/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc.mlm;

import edu.uvm.ccts.ardencat.mlmengine.arden.mlm.MLMBuilder;
import edu.uvm.ccts.ardencat.mlmengine.model.MLMConfigData;

import java.io.File;
import java.io.IOException;

/**
 * Created by mbstorer on 10/12/15.
 */
public class ServiceMLMBuilder extends MLMBuilder<ServiceMLM> {
    @Override
    public ServiceMLM build(MLMConfigData mlmConfig) throws IOException {
        return build(mlmConfig.getFilename(), mlmConfig.getRecordBuilderName());
    }

    public ServiceMLM build(String filename, String recordBuilderName) throws IOException {
        ServiceMLMEvalVisitor visitor = new ServiceMLMEvalVisitor(recordBuilderName);
        visitor.visit(buildParseTree(new File(filename).getCanonicalPath()));
        return visitor.getMLM();
    }
}
