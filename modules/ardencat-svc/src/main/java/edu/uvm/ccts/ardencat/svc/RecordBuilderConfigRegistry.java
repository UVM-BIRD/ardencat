/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of ArdenCAT.
 *
 * ArdenCAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ArdenCAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ArdenCAT.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.ardencat.svc;

import edu.uvm.ccts.ardencat.svc.model.RecordBuilderServiceConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstorer on 7/6/15.
 */
public class RecordBuilderConfigRegistry {
    private static RecordBuilderConfigRegistry registry = null;

    public static RecordBuilderConfigRegistry getInstance() {
        if (registry == null) registry = new RecordBuilderConfigRegistry();
        return registry;
    }

    private Map<String, RecordBuilderServiceConfig> map = new HashMap<String, RecordBuilderServiceConfig>();

    private RecordBuilderConfigRegistry() {}

    public RecordBuilderServiceConfig getConfig(String id) {
        return map.get(id.toLowerCase());
    }

    public void registerConfig(RecordBuilderServiceConfig cfg) {
        map.put(cfg.getId().toLowerCase(), cfg);
    }

    public List<String> getConfigIds() {
        return new ArrayList<String>(map.keySet());
    }

    public void clear() {
        map.clear();
    }
}
